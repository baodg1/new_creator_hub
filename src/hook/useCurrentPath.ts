import { routesConfig } from "@/route/config";
import { matchRoutes, useLocation } from "react-router-dom";

export const useCurrentPath = () => 
{
	const location = useLocation();
	const res = matchRoutes(routesConfig, location);

	if (res && res[0] && res[0].route) 
	{
		return res[0].route.path;
	}
};
