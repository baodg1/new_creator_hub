import { useSelector, useDispatch } from "react-redux";
import { RootState } from "@/redux/reducer";
import { actSetListTrack, actSetSelectedFrame } from "@/redux/action/app";
import { MAX_LENGTH_UNDO_REDO } from "@/config/constant";
import {
	actSaveToUndo,
	actSetRedoStack,
	actSetUndoStack
} from "@/redux/action/undoRedo";
import { useUpdateProject } from "./useUpdateProject";

const useUndoRedo = () => 
{
	const selectedFrame = useSelector(
		(state: RootState) => state?.App.selectedFrame
	);
	const listTrack = useSelector((state: RootState) => state?.App.listTrack);
	const undoStack = useSelector(
		(state: RootState) => state?.UndoRedo.undoStack
	);
	const redoStack = useSelector(
		(state: RootState) => state?.UndoRedo.redoStack
	);
	const dispatch = useDispatch();
	const { updateProject } = useUpdateProject();

	const updateTrackState = (item: string) => 
	{
		const parsedItem = JSON?.parse(item);

		dispatch(actSetListTrack(parsedItem?.listTrack));
		dispatch(actSetSelectedFrame(parsedItem?.selectedFrame));
	};

	const onUndo = () => 
	{
		if (undoStack.length === 0) 
		{
			return;
		}

		const tmpRedoStack = [ ...redoStack ];
		const tmpUndoStack = [ ...undoStack ];

		const lastUndoItem = tmpUndoStack[tmpUndoStack?.length - 1];

		tmpUndoStack?.splice(tmpUndoStack?.length - 1, 1);

		// push latest state to redo
		if (tmpRedoStack.length === 0) 
		{
			tmpRedoStack.push(JSON.stringify({ listTrack, selectedFrame }));
		}

		tmpRedoStack.push(lastUndoItem);

		// remove most last used item in stack if stack overflow
		if (tmpRedoStack.length > MAX_LENGTH_UNDO_REDO) 
		{
			tmpRedoStack.splice(0, 1);
		}

		dispatch(actSetUndoStack(tmpUndoStack));
		dispatch(actSetRedoStack(tmpRedoStack));
		updateTrackState(lastUndoItem);
	};

	const onRedo = () => 
	{
		if (redoStack.length <= 1) 
		{
			return;
		}

		const tmpRedoStack = [ ...redoStack ];
		const tmpUndoStack = [ ...undoStack ];

		const lastRedoItem = tmpRedoStack[tmpRedoStack?.length - 1];

		tmpRedoStack?.splice(tmpRedoStack?.length - 1, 1);
		tmpUndoStack.push(lastRedoItem);

		// remove most last used item in stack if stack overflow
		if (tmpUndoStack.length > MAX_LENGTH_UNDO_REDO) 
		{
			tmpUndoStack.splice(0, 1);
		}

		dispatch(actSetUndoStack(tmpUndoStack));
		dispatch(actSetRedoStack(tmpRedoStack));
		updateTrackState(tmpRedoStack[tmpRedoStack.length - 1]);
	};

	const saveTrackState = () => 
	{
		updateProject();
		dispatch(actSaveToUndo(JSON.stringify({ listTrack, selectedFrame })));
	};

	return { onUndo, onRedo, saveTrackState, redoStack, undoStack };
};

export { useUndoRedo };
