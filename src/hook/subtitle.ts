import { IFrame, ITextFrame, TextAlignOption } from "@/redux/interface";
import { RootState } from "@/redux/reducer";
import { useDispatch, useSelector } from "react-redux";
import { uuidv4 } from "@firebase/util";
import { actCreateSubtitleTrack, actUpdateSubtitleFrames } from "@/redux/action/app";
import { MediaType } from "@/services/interface/project";
import { createFrameFromSubtitle } from "@/utils/subtitle";
import { DEFAULT_SUBTITLE, DEFAULT_SUBTITLE_DURATION } from "@/redux/constant/subtitle";
import { ISubtitle } from "@/redux/interface/subtitle";

export const useSubtitle = () => 
{
	const dispatch = useDispatch();
	const subtitleTrack = useSelector((state: RootState) => state.App.subtitleTrack);
	const subtitleFrames = subtitleTrack.frames;

	const createSubtitleTrack = (subtitles: ISubtitle[]) =>
	{
		dispatch(actCreateSubtitleTrack(subtitles));
	};

	const insertNewSubtitle = (index: number) =>
	{
		
		let startTime = 0;
		let offset = 0;

		if (index === -1) // insert at first subtitle
		{
			startTime = 0;
		}
		else
		{
			startTime = subtitleFrames[index].timeline.toSecond;
			if (subtitleFrames.length > index + 1) // if there is still frames behind inserted frame.
			{
				offset = Math.max(0, 
					startTime + DEFAULT_SUBTITLE_DURATION - subtitleFrames[index+1].timeline.fromSecond);
			}
		}

		const newSubtitle: ISubtitle = {
			startTime   : startTime,
			duration    : DEFAULT_SUBTITLE_DURATION,
			textContent : `${DEFAULT_SUBTITLE.textContent} ${index + 1}`
		};
		const newSubtitleFrame = createFrameFromSubtitle(newSubtitle, index + 1);
		const prevFrames = subtitleFrames.slice(0, index + 1);
		const nextFrames = subtitleFrames.slice(index + 1).map((frame) =>
		{
			frame.frameIndex += 1;
			frame.timeline.fromSecond += offset;
			frame.timeline.toSecond += offset;
			
			return frame;
		});

		const newSubtitleFrames = prevFrames.concat(newSubtitleFrame).concat(nextFrames);

		dispatch(actUpdateSubtitleFrames(newSubtitleFrames));
	};

	const pushNewSubtitle = () =>
	{
		insertNewSubtitle(subtitleTrack.frames.length - 1);
	};

	const updateSubtitles = (subtitles: ISubtitle[]) =>
	{
		const newSubtitleFrames = subtitles.map((subtitle, index) => createFrameFromSubtitle(subtitle, index));

		dispatch(actUpdateSubtitleFrames(newSubtitleFrames));
	};

	const mergeSubtitles = (prevIndex: number) =>
	{
		const prevFrame = subtitleFrames[prevIndex];
		const nextFrame = subtitleFrames[prevIndex + 1];

		prevFrame.timeline.toSecond = nextFrame.timeline.toSecond;
		prevFrame.timeline.duration = nextFrame.timeline.toSecond - prevFrame.timeline.fromSecond;
		prevFrame.timeline.trimEndSecond = prevFrame.timeline.duration;
		prevFrame.textConfig.textContent = `${prevFrame.textConfig.textContent} ${nextFrame.textConfig.textContent}`;
		
		const prevFrames = subtitleFrames.slice(0, prevIndex);
		const nextFrames = subtitleFrames.slice(prevIndex + 2).map((frame) =>
		{
			frame.frameIndex -= 1;
			
			return frame;
		});

		const newSubtitleFrames = prevFrames.concat(prevFrame).concat(nextFrames);

		dispatch(actUpdateSubtitleFrames(newSubtitleFrames));
		
	};

	const deleteSubtitle = (index: number) =>
	{

		const prevFrames = subtitleFrames.slice(0, index);
		const nextFrames = subtitleFrames.slice(index + 1).map((frame) =>
		{
			frame.frameIndex -= 1;
			
			return frame;
		});

		const newSubtitleFrames = prevFrames.concat(nextFrames);

		dispatch(actUpdateSubtitleFrames(newSubtitleFrames));
	};

	const updateTextContent = (index: number, textContent: string) =>
	{
		subtitleFrames[index].textConfig.textContent = textContent;
		dispatch(actUpdateSubtitleFrames(subtitleFrames));
	};

	const updateSubtitleTime = (index: number, startTime: number, endTime: number) =>
	{
		const timeline = subtitleFrames[index].timeline;

		timeline.fromSecond = startTime;
		timeline.toSecond = endTime;
		timeline.duration = endTime - startTime;
		timeline.trimEndSecond = timeline.duration;

		subtitleFrames[index].timeline = timeline;
		dispatch(actUpdateSubtitleFrames(subtitleFrames));
	};

	const defaultFrame: ITextFrame = {
		id         : uuidv4(),
		trackId    : "",
		trackIndex : 0,
		frameIndex : 0,
		type       : MediaType.Subtitle,
		timeline   : {
			fromSecond      : 0,
			toSecond        : 3,
			duration        : 3,
			trimStartSecond : 0,
			trimEndSecond   : 3,
			speed           : 1
		},
		textConfig : {
			textContent     : "Subtitle",
			textColor       : "#DEDEDE",
			fontWeight      : 500,
			align           : TextAlignOption.Center,
			fontSize        : 48,
			lineHeight      : 1,
			letterSpacing   : 1,
			fontFamily      : "Roboto",
			backgroundColor : "#535A61",
			justify         : 1,
			isUppercase     : false,
			fontStyle       : "",
			borderRadius    : 0
		},
		preview : {
			width          : 0,
			height         : 0,
			maxWidth       : 0,
			maxHeight      : 0,
			centerX        : 0,
			centerY        : 0,
			rotateAngle    : 0,
			isFlipXApplied : false,
			isFlipYApplied : false
		}
	};

	const createSubtitleFrame = (
		startTime: number,
		duration: number,
		textContent: string,
		trackID?: string
	): IFrame => 
	{
		return {
			...defaultFrame,
			trackId  : trackID!,
			id       : uuidv4(),
			timeline : {
				speed           : 1,
				fromSecond      : startTime,
				duration        : duration,
				trimStartSecond : 0,
				trimEndSecond   : duration,
				toSecond        : startTime + duration
			}
		};
	};

	const createSubtitleFrames = (subtitles: ISubtitle[], trackID?: string) => 
	{
		return subtitles.map((x) => (createSubtitleFrame(x.startTime, x.duration, x.textContent, trackID)));
	};

	return { 
		createSubtitleTrack, 
		deleteSubtitle,
		insertNewSubtitle, 
		pushNewSubtitle, 
		createSubtitleFrame, 
		createSubtitleFrames,
		mergeSubtitles,
		updateSubtitles,
		updateTextContent,
		updateSubtitleTime
	};
};
