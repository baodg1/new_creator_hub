import { RootState } from "@/redux/reducer";
import { useSelector, useDispatch } from "react-redux";
import { IProjectResponse } from "@/services/interface/project";
import { convertProjectSettingToApi, getProjectSettingFromApi, getTrackListFromApi } from "@/utils/project";
import { actSetProjectSetting } from "@/redux/action/preview";
import { actSetListTrack, actUpdateSubtitleFrames } from "@/redux/action/app";
import { ProjectAPIService } from "@/services/project";
import { DEFAULT_PROJECT_SETTING } from "@/constant/project";
import { message } from "antd";
import { Lang } from "@/utils/config";

export function useUpdateProject()
{
	const listTrack = useSelector((state: RootState) => state.App.listTrack);
	const subtitleTrack = useSelector((state: RootState) => state.App.subtitleTrack);
	// const currentProject = useSelector((state: RootState) => state.Project.currentProject);
	const projectSetting = useSelector((state: RootState) => state.Preview.setting);
	const dispatch = useDispatch();

	const updateProject = async () => 
	{
		if (projectSetting.projectId)
		{
			const projectData = convertProjectSettingToApi(projectSetting, listTrack, subtitleTrack);

			try
			{
				ProjectAPIService.update(projectData);
			}
			catch (e)
			{
				message.warn(Lang.trans("update_project_failed"));
			}
			
		}
		
	};

	const loadProjectData = (projectId: number, projectData: IProjectResponse) => 
	{
		try 
		{
			const projectSettingFromApi = getProjectSettingFromApi(projectData);
			const { listSubtitleFrame, listTrack: listNormalTrack } = 
			getTrackListFromApi(projectData.media_asset_list);

			dispatch(actSetProjectSetting(projectSettingFromApi));
			dispatch(actSetListTrack(listNormalTrack));
			dispatch(actUpdateSubtitleFrames(listSubtitleFrame));
		}
		catch (e)
		{
			const defaultProjectSetting = DEFAULT_PROJECT_SETTING;

			defaultProjectSetting.projectId = projectId;

			dispatch(actSetProjectSetting(defaultProjectSetting));
			message.error(Lang.trans("retrieve_project_failed"));
		}
	};
	
	return {
		updateProject,
		setListTractFromApi : loadProjectData
	};
}
