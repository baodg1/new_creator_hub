import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: var(--text-font);
    font-size: 62.5%;
  }

  * {
    box-sizing: border-box;
  }

  /* thay đổi màu mặc định khi kéo chọn vùng chữ của trình duyệt */
  ::selection {
    background: var(--color-primary) !important;
  }

  ::-moz-selection { /* Code for Firefox */
    background: var(--color-primary) !important;
  }

  :root {
    --text-font: 'Inter', sans-serif;
    --color-text: #262935;
    --color-text-secondary: #333333;
    --color-text-gray: #E1E1E1;
    --color-text-light: #ffffff;
    --color-primary: #1890ff;
    --color-primary-light: #867ae9;
    --color-blue: #2A85FF;
    --color-gray: #BCBCBC;
    --color-light-gray: #32343E;
    --color-border: #757575;
    --color-border-primary: rgba(94, 242, 224, 0.3);
    --color-background: #F5F5F5;
    --color-background-white: #FEFDFF;
    --color-background-gray: #262935;
    --color-background-dark: #1A1E2B;
    --color-background-frame: #1D262F;
    --color-background-primary: rgba(94, 242, 224, 0.2);
    --color-black: rgba(0,0,0,0.87);
    --box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.05);
    --border-radius: 0.8rem;
    --border-radius-small: 0.4rem;
    --border-radius-1: 1rem;
    --padding: 3rem 2.5rem;
    --padding-small: 1.5rem 1.5rem;
    --padding-page: 2rem 3rem;
    --margin-bottom: 1.5rem;
    --margin-bottom-small: 1rem;
    --margin-bottom-large: 2.5rem;
    --margin-right: 1.5rem;
    --margin-left: 1.5rem;
    --margin-left-large: 2rem;
    --margin-top: 1.5rem;
    --color-success: #7FC8A9;
    --color-error: #FF6767;
    --color-red: #FC3400;
    --background-primary: rgba(133,122,233,0.5);
    --background-error: rgba(255, 103, 103, 0.1);
    --background-input: rgba(255, 255, 255, 0.1);
    --background-white: rgba(255,255,255,0.7);
    --background-blue: rgba(42, 133, 255, 0.1);
  }

  .primary-btn, .ant-btn-primary[disabled] {
    background-color: var(--color-primary);
    color: var(--color-text);
    font-size: 1.3rem;
    border-radius: 0.8rem;
    border: none;

    &:hover, &:active, &:focus {
      background-color: var(--color-primary);
      color: var(--color-text);
      opacity: 0.85;
    }
  }

  .ghost-btn {
    background-color: transparent;
    color: var(--color-text);
    font-size: 1.3rem;
    border-radius: 0.8rem;
    border: 1px solid var(--color-primary) !important;

    &:hover, &:active, &:focus {
      background-color: transparent;
      color: var(--color-primary);
      opacity: 0.85;
    }
  }

  .ant-form-item {
    margin-bottom: 1.5rem !important;
  }

  .ant-radio-inner {
    background-color: transparent;
  }

  .ant-radio-group {
    font-size: 1.4rem;
  }

  .ant-form-item-label > label {
    color: var(---color-primary) !important;
    font-size: 1.4rem;
    font-weight: 500;
  }

  .ant-slider-handle {
    border: none;
    width: 11px;
    height: 11px;

    &, &:hover {
      background-color: var(--color-primary);
    }
  }

  .ant-slider-vertical .ant-slider-handle {
    margin-left: -4px;
  }

  .ant-slider-rail {
    height: 3px;
  }

  .ant-slider-track {
    background-color: var(--color-primary);
    height: 3px;
  }

  .ant-slider-mark-text {
    color: var(--color-text-gray);
    font-size: 1rem;
  }

  .ant-switch-checked:focus {
    box-shadow: none;
  }

  .ant-form-item-label {
    color: var(--color-text-gray);
  }

  .ant-popover-arrow-content {
    --antd-arrow-background-color: var(--color-background-dark);
  }
  .ant-popover-message-title {
    color: var(--color-background-white);
  }

  .ant-popover-inner {
    background: var(--color-background-dark);

    .ant-popover-inner-content {
      padding: 1rem 0.5rem;
    }
  }


  .custom-input {
    background-color: var(--background-input);
    border-radius: 0.4rem;
    font-weight: 500;
    border: 1px solid transparent;
    box-shadow: none;
    font-size: 1.3rem;
    padding: 0.5rem 1rem;
    color: var(--color-text-gray);
    border: 1px solid var(--color-border);

    input {
      background: transparent;
      font-weight: 500;
      font-size: 1.4rem;
    }

    &:hover, &:focus {
      border: 1px solid transparent;
      box-shadow: none;
    }
  }

  .custom-input-number {
    background-color: var(--color-background-gray);
    border-radius: 0.4rem;
    color: var(--color-text-gray);
    font-size: 1.3rem;
    border: 1px solid transparent;
    box-shadow: none;

    &:hover, &:focus {
      border: 1px solid transparent;
    }

    .ant-input-number-group-addon {
      background-color: var(--color-background-gray);
      background-color: transparent;
      border: 1px solid transparent;
      color: var(--color-text-gray);
      font-size: 2rem;
    }

    .ant-input-number-handler-wrap {
      visibility: hidden;
    }
  }

  .custom-select {
    width: 100%;
    font-size: 1.3rem;
    border: 1px solid var(--color-border);
    border-radius: 3px;

    & * {
      color: var(--color-text-gray) !important;
    }
    
    .ant-select-selector {
      border: 1px solid transparent !important;
      outline: none !important;
      border-radius: 0.4rem !important;
      box-shadow: none !important;
      background-color: transparent!important;
    }

    &.ant-select-disabled {
      background: var(--color-background-gray) !important;
      border-radius: 0.4rem !important;

      .ant-select-selector {
        background: transparent !important;
      }
    }
  }

  .custom-time-picker {
    background-color: var(--color-background-gray);
    border-radius: 0.4rem;
    color: var(--color-text-gray);
    border: 1px solid transparent;
    box-shadow: none;
    padding: 7px 11px;

    .ant-picker-input * {
      color: var(--color-text-gray);
      font-size: 1.2rem;
    }

    .ant-picker-separator, .ant-picker-suffix {
      color: var(--color-text-gray);
    }
  }

  .color-picker-popver {
    .ant-popover-inner-content {
      padding: 0;
    }

    .ant-popover-arrow-content {
      --antd-arrow-background-color: var(--color-background-gray);
    }

    .ant-popover-inner {
      background: transparent;
    }

    .sketch-picker  {
      background: var(--color-background-gray) !important;
      border-radius: 0.8rem !important;  
      padding: 1.5rem 1.5rem !important;
      width: 22rem !important;

      input {
        background: var(--color-background-gray) !important;
        color: var(--color-text-gray) !important;
        border: 0.5px solid var(--color-border) !important;
        font-size: 1rem !important; 
        border-radius: 0.3rem !important;
        box-shadow: none !important;

        &:focus-visible {
          outline: none !important;
        }
      }

      label {
        color: var(--color-text-gray) !important;
      }
    }
  }

  .custom-modal {
    color: white;

    .ant-modal-content {
      background-color: var(--color-background-dark);
    }

    .ant-modal-header {
      background-color: var(--color-background-dark);
      border-bottom: 1px solid var(--color-background-gray);
    }

    .ant-modal-title {
      color: white;
    }

    .ant-modal-close-icon {
      color: white;
    }
  }

  /* style for scrollbar */
  ::-webkit-scrollbar {
    width: 0.5rem;
  }

  ::-webkit-scrollbar-thumb {
    background: rgba(255, 255, 255, 0.17);
    border-radius: 1px;
  }

  ::-webkit-scrollbar-track {
    background: rgba(241, 241, 241, 0.1);
    border-radius: 1px;
  }

  ::-webkit-scrollbar:horizontal {
    height: 0.5rem;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    border-radius: 1px;
  }


  ::-webkit-scrollbar-corner {
    background-color: rgba(241, 241, 241, 0.1) !important;
  }
  .accountWrap {
    background: #C4C4C4;
    min-height: 100vh;
    display: inline-table;
    width: 100%;
  }
  .scroll-custom {
    &::-webkit-scrollbar {
      width: 6px !important;
    }
    &::-webkit-scrollbar-track {
      background: #E7E7E7;
    }
    &::-webkit-scrollbar-thumb {
      background: #B1B1B1;
      border-radius: 5px;
    }
  }

  .apero-notification-snackbar{
    .ant-notification-notice-message{
      margin-bottom: 0px;
      padding-right: 0px;
    }
  }
`;

export { GlobalStyle };
