import { Modal } from "antd";
import { ModalVideoDetailProps } from "../../interface/VideoLibrary";

export const ModalVideoDetail = (props:ModalVideoDetailProps) => 
{
	const { open, onCancel } = props;
	
	return <Modal onCancel={onCancel} open={open} >Content</Modal>;
};
