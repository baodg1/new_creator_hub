import { Heading } from '@/component';
import { Checkbox, Input, Row, Col } from 'antd';
import { CheckboxValueType } from 'antd/es/checkbox/Group';
import React, { useState, useEffect } from 'react';
import { TYPE_VIDEO_LIBRARY } from '../constant/VideoLibrary';
import { IVideoLibrary } from '../interface/VideoLibrary';
import { SliderVideo } from './SliderVideo';
import { VideoLibraryWrapper } from './style';
import { VideoItem } from './VideoItem';
import IconArrow from '@/asset/arrow-right.svg';

const dataFake =[
	{
		category  : 'Travel',
		listVideo : [
			{
				thumbnail : 'https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671294538203-426567341-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim - Trim/frame001.png',
				videoUrl  : `https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671251471009-357821737-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim.mp4`,
				duration  : 120
			},
			{
				thumbnail : 'https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671294538203-426567341-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim - Trim/frame001.png',
				videoUrl  : `https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671251471009-357821737-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim.mp4`,
				duration  : 120
			}, {
				thumbnail : 'https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671294538203-426567341-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim - Trim/frame001.png',
				videoUrl  : `https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671251471009-357821737-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim.mp4`,
				duration  : 120
			}, {
				thumbnail : 'https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671294538203-426567341-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim - Trim/frame001.png',
				videoUrl  : `https://sms-file.s3.ap-southeast-1.amazonaws.com/files/videos/117/1671251471009-357821737-y2meta.com-Sia - Snowman _ EspaÃ±ol & English-(480p)_Trim.mp4`,
				duration  : 120
			}
		]
	}
];

export const VideoLibrary : React.FC = () => 
{
	const [ seeCategory, setSeeCategory ] = useState<string|null>();
	const [ videoCategory, setVideoCategory ] = useState<IVideoLibrary>();
	const onChange = (checkedValues: CheckboxValueType[]) => 
	{
	};

	const handleSeeAll = (value:string|null) => 
	{
		setSeeCategory(value);
	};

	const handleGetVideoByCategory = () => 
	{
		const res = dataFake.filter((item) => item.category === seeCategory);

		setVideoCategory(res[0]);
	};

	useEffect(() => { handleGetVideoByCategory(); }, [ seeCategory ]);

	const renderCategory = () => 
	{
		return (<>
			{videoCategory?.category && <>
				<div style={{ display: "flex" }}>
					<img onClick={() => { handleSeeAll(null); }} style={{ marginRight: "10px", cursor: 'pointer' }} src={IconArrow} alt=''/>
					<Heading title={videoCategory?.category} isLarge />
				</div>
				
			</>}
			<br/>
			<Row gutter={[ 16, 16 ]}>
				{videoCategory?.listVideo?.map((item, index) => 
				{
					return (
						<Col key={index} span={12}>
							<VideoItem detailVideo={item} />
						</Col>
						
					);
				})}
			</Row>
			
		</>);
	};

	const renderAll = () => 
	{
		return (
		
			<>
				<Heading title='Video Library' isLarge />
				<div className='search'>
					<Input placeholder='Search for videos'/>
				</div>
				<div className='select-type'>
					<Checkbox.Group
						options={TYPE_VIDEO_LIBRARY}
						defaultValue={[ TYPE_VIDEO_LIBRARY[0].value, TYPE_VIDEO_LIBRARY[1].value ]}
						onChange={onChange}
					/>
				</div>
				{
					dataFake.map((item, index) => 
					{
						return (
							<div key={index} className='list-video-category'>
								<div className='title'>
									<div className='category-name'>{item.category}</div>
									<div className='see-all' onClick={() => handleSeeAll(item.category)}>See all</div>
								</div>
								<SliderVideo listVideo={item.listVideo}/>
							</div>
						);
					})
				}
			</>
		);
	};
	
	return (
		<VideoLibraryWrapper>
			
			<div className='content'>
				{
					seeCategory ?<>{renderCategory()}</> :<>{renderAll()}</>

				}
				
			</div>
		</VideoLibraryWrapper>
	);
};
