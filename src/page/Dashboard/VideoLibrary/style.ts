import styled from "styled-components";

export const VideoLibraryWrapper = styled.div`
    .content {
        .search {
            padding: 16px 0px 18px 0px;
            .input {
                width: 100%;
            }
        }
        .select-type {
            .ant-checkbox-group-item {
                color: #ffffff;
            }
        }
        .list-video-category {
            
            .title{
                display: flex;
                align-items: center;
                justify-content: space-between;
                margin-bottom: 8px;
                margin-top: 18px;
                .category-name {
                    font-weight: 400;
                    font-size: 12px;
                    line-height: 16px;
                    color: #AFAFAF;
                }
                .see-all {
                    font-weight: 400;
                    font-size: 10px;
                    line-height: 12px;
                    color: #545454;
                    cursor: pointer;
                }
            }
        }
    }
`;
