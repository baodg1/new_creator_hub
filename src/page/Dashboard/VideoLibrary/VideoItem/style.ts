import styled from "styled-components";

export const VideoItemWrapper = styled("div")`
    width: 128px;
    height: 228px;
    background-color: black;
    position: relative;
    video{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .thumbnail {
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 4px;
    }
    .duration, .add-video {
        background: #32343E;
        border-radius: 4px;
        color: #ffffff;
        position: absolute;
        cursor: pointer;
    }
    .duration {
        left: 4px;
        bottom: 4px;
        padding: 2.5px 8px;
        font-weight: 600;
        font-size: 12px;
        line-height: 15px;
    }
    .add-video {
        right: 4px;
        bottom: 4px;
        padding: 6px;
        width: 20px;
        height: 20px;
        display: flex;
        align-items: center;
        background: #5EF2E0;
    }
`;
