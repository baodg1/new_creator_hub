import React, { useRef, useState } from 'react';
import { VideoItemWrapper } from './style';
import IconPlus from '@/asset/ic_plus.svg';
import { ModalVideoDetail } from '../ModalVideoDetail';
import { DetailVideoProps } from '../../interface/VideoLibrary';

export const VideoItem : React.FC<DetailVideoProps> = (props:DetailVideoProps) => 
{
	const videoRef = useRef<HTMLVideoElement|null>(null);
	const [ showModalDetail, setShowModalDetail ] = useState(false);

	const { detailVideo } = props;

	const handlePlayVideo = () => 
	{
		videoRef.current?.play();
	};

	const handlePauseVideo = () => 
	{
		videoRef.current?.pause();
		videoRef.current?.load();
	};

	const handleShowModal = () => 
	{
		setShowModalDetail(true);
	};
	const handleCloseModal = () => 
	{
		setShowModalDetail(false);
	};
	
	return (
		<>
			<VideoItemWrapper className='video-item' onMouseOver={() => handlePlayVideo()} onMouseOut={() => handlePauseVideo()} onClick={() => handleShowModal()}>
				
				<video ref={videoRef} poster={detailVideo.thumbnail} muted >
					<source src={detailVideo.videoUrl} type='video/mp4'/>
				</video>
				<div className='duration'>00:00</div>
				<div className='add-video'>
					<img src={IconPlus} alt=''/>
				</div>
			</VideoItemWrapper>
			<ModalVideoDetail open={showModalDetail} onCancel={handleCloseModal}/>
		</>
	);
};
