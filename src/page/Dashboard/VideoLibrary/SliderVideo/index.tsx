import { Carousel } from "antd";
import React from "react";
import { StyledVideoSlider } from "./styled";
import type { CustomArrowProps } from '@ant-design/react-slick';
import IconNext from "@/asset/ic_arrow_right.svg";
import { SilderVideoProps, IVideoItem } from "../../interface/VideoLibrary";
import { VideoItem } from "../VideoItem";

function SampleNextArrow(props:CustomArrowProps) 
{
	const { className, style, onClick } = props;
	
	return (
		<div
			className={className}
			style={{ ...style, display: "block" }}
			onClick={onClick}
		>
			<img src={IconNext} alt=''/>
		</div>
	);
}
  
function SamplePrevArrow(props:CustomArrowProps) 
{
	
	const { className, style, onClick } = props;
	
	return (
		<div
			className={className}
			style={{ ...style, display: "block" }}
			onClick={onClick}
		><img src={IconNext} alt=''/></div>
	);
}

export const SliderVideo:React.FC<SilderVideoProps> = (props:SilderVideoProps) => 
{
	const settings = {
		dots           : false,
		infinite       : false,
		speed          : 500,
		slidesToShow   : 1,
		slidesToScroll : 1,
		swipeToSlide   : true,
		draggable      : true,
		arrows         : true,
		nextArrow      : <SampleNextArrow />,
		prevArrow      : <SamplePrevArrow />,
		variableWidth  : true
	};

	const { listVideo } = props;
	
	return (
		<StyledVideoSlider>
			<Carousel {...settings} >
				{
					listVideo.map((item:IVideoItem, index:number) => 
					{
						return <VideoItem key={index} detailVideo={item}/>;
					})
				}
			</Carousel>
		</StyledVideoSlider>
		
	);
};
