import styled from "styled-components";

export const StyledVideoSlider = styled.div`
    .ant-carousel{
        /* padding: 0px 20px; */
        .slick-slider{
            .slick-track{
            }
        }
        .slick-list{ 
            margin: 0 -8px;
            .slick-slide {
                pointer-events: initial;
                .video-item {
                    margin: 8px;
                }
        }
        }
        
        .slick-prev{
            left: 0px;
            z-index: 1000;
            img{
                transform: rotate(180deg);
            }
        }
        .slick-next{
            right: 0px;
            z-index: 10000;
        }
    }
`;
