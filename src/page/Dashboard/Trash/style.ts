import styled from "styled-components";
const TrashWrapper = styled("div")`
  background-color: white;
  height: 100%;
  padding: 40px 60px 30px 60px;
`;

export { TrashWrapper };
