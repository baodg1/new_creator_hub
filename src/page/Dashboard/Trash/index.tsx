import TrashComponent from "@/component/ProjectManagement/Trash";
import { TrashWrapper } from "./style";

const Trash = () => 
{
	return (
		<TrashWrapper>
			<TrashComponent />
		</TrashWrapper>
	);
};

export default Trash;
