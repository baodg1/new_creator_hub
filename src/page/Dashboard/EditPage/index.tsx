import React, { useEffect, useMemo, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { connect } from "react-redux";
import { useMenuState } from "@szhsin/react-menu";
import { ACTION, KEY_MAP } from "@/config/undoRedo";
import { useUndoRedo } from "@/hook";
import {
  // SettingPanel,
  // Preview,
  // Timeline,
  // Toolbar,
  ModalExportVideo,
  ContextMenu,
} from "@/component";
import { actSaveGetListFile } from "@/redux/action/file";
import {
  actSetModalAlertReloadVisible,
  actSetListTrack,
} from "@/redux/action/app";
import { actSetProjectSetting } from "@/redux/action/preview";
import { RootState } from "@/redux/reducer";
import { useParams } from "react-router-dom";

import { EditPageWrapper } from "./style";
import { EditPageProps } from "../interface/EditPage";
import { useUpdateProject } from "@/hook/useUpdateProject";
import { ProjectAPIService } from "@/services/project";
import { IProjectResponse } from "@/services/interface/project";

const EditPage = (props: EditPageProps) => {
  const {
    isFullScreen,
    rotateAngle,
    selectedFrame,
    onPhone,
    undoStack,
    redoStack,
  } = props;
  const [menuProps, toggleMenu] = useMenuState();
  const [anchorPoint, setAnchorPoint] = useState({ x: 0, y: 0 });
  const [refresh, setRefresh] = useState(false);
  const { onRedo, onUndo } = useUndoRedo();
  const params = useParams();
  const { id } = params;
  const { setListTractFromApi } = useUpdateProject();

  const alertWhenReload = (event: BeforeUnloadEvent) => {
    event.preventDefault();
    event.returnValue = "";

    return "";
  };

  const onReloadLoadpage = () => {
    props?.actSaveGetListFile([]);
    props?.actSetListTrack([]);
  };

  useEffect(() => {
    window.addEventListener("beforeunload", alertWhenReload);
    window.addEventListener("unload", onReloadLoadpage);

    return () => {
      window.removeEventListener("beforeunload", alertWhenReload);
      window.removeEventListener("unload", onReloadLoadpage);
    };
  }, []);

  // const dispatch = useDispatch();
  const handleGetDetailProject = async () => {
    if (id) {
      ProjectAPIService.detail(Number(id)).then((res) => {
        const projectData = res.data as IProjectResponse;

        setListTractFromApi(Number(id), projectData);
      });
      // const res = await projectApi.detailProject(Number(id));

      // if (res && res.data)
      // {
      // 	dispatch(actSaveCurrentProject(res.data));
      // 	setListTractFromApi(res.data);
      // }
    }
  };

  useEffect(() => {
    setRefresh(!refresh);
  }, [undoStack?.length, redoStack?.length]);

  useHotkeys(KEY_MAP[ACTION.UNDO], onUndo, [undoStack, redoStack]);
  useHotkeys(KEY_MAP[ACTION.REDO], onRedo, [undoStack, redoStack]);
  useHotkeys(KEY_MAP[ACTION.REDO_WITH_SHIFF], onRedo, [undoStack, redoStack]);

  useEffect(() => {
    window.addEventListener("beforeunload", alertWhenReload);
    window.addEventListener("unload", onReloadLoadpage);

    return () => {
      window.removeEventListener("beforeunload", alertWhenReload);
      window.removeEventListener("unload", onReloadLoadpage);
    };
  }, []);

  useEffect(() => {
    handleGetDetailProject();
  }, [id]);

  const onOpenContextMenu = (event: React.MouseEvent) => {
    event?.preventDefault();
    setAnchorPoint({ x: event?.clientX, y: event?.clientY });
    toggleMenu(true);
  };

  const shouldDisplayAngle = useMemo(() => {
    return !onPhone.enable;
  }, [selectedFrame, onPhone]);

  return (
    <EditPageWrapper onContextMenu={onOpenContextMenu}>
      <div className="config-wrapper">
        {!isFullScreen ? (
          <div className="panel-wrapper">
            <div className="panel-content">
              {/*SettingPanel  */}
            </div>
          </div>
        ) : null}
      </div>
      <div className="main-edit-wrapper">
        <div
          className="preview-wrapper"
          style={{
            padding: isFullScreen
              ? 0
              : shouldDisplayAngle
              ? "3rem 2rem 1rem 2rem"
              : "1rem 2rem 1rem 1rem",
          }}>
          {/* {shouldDisplayAngle ? (
						<div className='angle'>{Math.round(rotateAngle)}°</div>
					) : null} */}
          <>
            <div>Preview</div>
          </>
          {/* <Preview /> */}
        </div>
        <div className="toolbar-wrapper">
          {/* <Toolbar /> */}
          <div>Toolbar</div>
        </div>
        {!isFullScreen ? (
          <div className="time-line-container">
            <div className="timeline-wrapper">
              {/* <Timeline /> */}
              <div>Timeline</div>
            </div>
          </div>
        ) : null}
      </div>

      {/* <ModalExportVideo /> */}

      <ContextMenu
        menuProps={menuProps}
        anchorPoint={anchorPoint}
        toggleMenu={toggleMenu}
      />
    </EditPageWrapper>
  );
};

export default connect(
  (state: RootState) => ({
    isFullScreen: state?.App?.isFullScreen,
    rotateAngle: state?.Preview?.rotateAngle,
    selectedFrame: state?.App?.selectedFrame,
    onPhone: state?.Preview?.onPhone,
    undoStack: state?.UndoRedo?.undoStack,
    redoStack: state?.UndoRedo?.redoStack,
  }),
  {
    actSetModalAlertReloadVisible,
    actSetProjectSetting,
    actSaveGetListFile,
    actSetListTrack,
  }
)(EditPage);
