import styled from "styled-components";

const EditPageWrapper = styled.div`
  height: 100%;
  display: flex;

  .config-wrapper {
    display: flex;
    justify-content: space-between;
    z-index: 10;

    .panel-wrapper {
      flex: 1;
      width: 35rem;
      overflow-y: hidden;
      position: relative;
      .panel-content{
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        overflow-y: scroll;
        padding-bottom: 15px;
      }
    }
  }
  .main-edit-wrapper {
    flex: 1;
    position: relative;
    display: flex;
    flex-direction: column;
  }
  .preview-wrapper {
    height: 40vh;
    background-color: var(--color-background-gray);
    flex-grow: 1;
    padding: 3rem 2rem 1rem 2rem;
    color: var(--color-text-light);
    display: flex;
    justify-content: center;
    position: relative;

    .angle {
      position: absolute;
      background-color: var(--color-primary);
      border-radius: 5px;
      padding: 0.3rem 0.5rem;
      font-size: 1rem;
      top: 0.5rem;
      left: 50%;
      transform: translateX(-50%);
    }
  }
  .toolbar-wrapper {
    margin-top: auto;
    box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.15),
      inset 0px 1px 0px rgba(255, 255, 255, 0.15);
  }

  .time-line-container {
    min-height: 30rem;
    height: 30vh;
    width: 100%;
    overflow: hidden;
    display: flex;
    position: relative;
    .timeline-wrapper {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      color: var(--color-text-light);
      padding: 1rem 1.2rem;
    }
  }
`;

export { EditPageWrapper };
