import styled from "styled-components";

const ExportSuccessWrapper = styled.div`
  height: 100%;
  display: flex;

  .main {
    flex-grow: 1;
    display: flex;
    flex-direction: column;

    .preview {
      flex-grow: 1;
    }

    .toolbar {
      padding: 1rem 1rem;
      background-color: var(--color-background-gray);
    }
  }

  .sidebar {
    height: 100%;
    width: 30rem;
    display: flex;
    flex-direction: column;
    background-color: var(--color-background-gray);
    padding: 2rem 2rem;

    .filename {
      color: white;
      font-size: 1.6rem;
      font-weight: 500;
      margin-bottom: 0.5rem;
    }

    .capacity {
      font-size: 1.1rem;
      color: var(--color-text-gray);
      margin-bottom: 2rem;
    }

    button {
      margin-bottom: 1.5rem;
    }
  }
`;

export { ExportSuccessWrapper };
