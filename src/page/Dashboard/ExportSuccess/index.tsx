import { Button } from "antd";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { PlusOutlined } from "@ant-design/icons";
// import { Preview, Toolbar } from "@/component";
import { actSetIsExporting, actSetPreviewPlay } from "@/redux/action/preview";
import { actSetTimeRulerValue } from "@/redux/action/timeRuler";
import { RootState } from "@/redux/reducer";
import { ExportSuccessWrapper } from "./style";
import { useEffect } from "react";

interface ExportSuccessProps {
  isFullScreen: boolean;
  actSetPreviewPlay: (payload: boolean) => any;
  actSetIsExporting: (payload: boolean) => any;
  actSetTimeRulerValue: (payload: number) => any;
}

const ExportSuccess = (props: ExportSuccessProps) => 
{
	const { isFullScreen } = props;
	const navigate = useNavigate();

	useEffect(() => 
	{
		props?.actSetTimeRulerValue(0);
	}, []);

	const onGoBackToEdit = () => 
	{
		props?.actSetIsExporting(false);
		props?.actSetPreviewPlay(false);
		navigate({ pathname: "/edit" });
	};

	return (
		<ExportSuccessWrapper>
			<div className='main'>
				<div className='preview'>
					{/* <Preview /> */}
				</div>

				<div className='toolbar'>
					{/* <Toolbar /> */}
				</div>
			</div>

			{!isFullScreen ? (
				<div className='sidebar'>
					<div className='filename'>video-name.mp4</div>
					<div className='capacity'>1.8MB / MP4</div>

					<Button type='primary' className='primary-btn'>
						Download
					</Button>

					<Button ghost className='ghost-btn' onClick={onGoBackToEdit}>
						Re - Edit Video
					</Button>

					<Button
						type='primary'
						className='primary-btn'
						icon={<PlusOutlined />}
					>
						Create Post
					</Button>
				</div>
			) : null}
		</ExportSuccessWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		isFullScreen : state?.App?.isFullScreen
	}),
	{ actSetIsExporting, actSetTimeRulerValue, actSetPreviewPlay }
)(ExportSuccess);
