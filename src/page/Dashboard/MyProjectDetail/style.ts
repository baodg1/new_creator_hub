import styled from "styled-components";
const MyProjectWrapper = styled("div")`
  background-color: white;
  height: 100%;
`;
const ContentWrapper = styled("div")`
  padding: 40px 60px 30px 60px;
  min-height: 100%;
`;

export { MyProjectWrapper, ContentWrapper };
