import DetailProjectComponent from "@/component/MyProject/DetailProject";
import { MyProjectWrapper, ContentWrapper } from "./style";

const MyProjectDetail = () => 
{
	return (
		<MyProjectWrapper>
			<ContentWrapper>
				<DetailProjectComponent />
			</ContentWrapper>
		</MyProjectWrapper>
	);
};

export default MyProjectDetail;
