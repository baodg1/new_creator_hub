import styled from "styled-components";
const ProfileUserWrapper = styled("div")`
  background-color: white;
  height: 100%;
`;
const ContentWrapper = styled("div")`
  padding: 40px 60px 30px 60px;
  min-height: 100%;
`;

export { ProfileUserWrapper, ContentWrapper };
