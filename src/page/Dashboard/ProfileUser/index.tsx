import ProfileUserComponent from "@/component/ProfileUser";
import { ContentWrapper, ProfileUserWrapper } from "./style";

const ProfileUser = () => 
{
	return (
		<ProfileUserWrapper>
			<ContentWrapper>
				<ProfileUserComponent />
			</ContentWrapper>
		</ProfileUserWrapper>
	);
};

export default ProfileUser;
