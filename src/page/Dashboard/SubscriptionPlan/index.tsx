import SubscriptionPlanComponent from "@/component/SubscriptionPlan";
import { SubscriptionPlanWrapper } from "./style";

const SubscriptionPlan = () => 
{
	return (
		<SubscriptionPlanWrapper>
			<SubscriptionPlanComponent />
		</SubscriptionPlanWrapper>
	);
};

export default SubscriptionPlan;
