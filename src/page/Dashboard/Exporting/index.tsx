import { Button } from "antd";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RootState } from "@/redux/reducer";
import { actSetIsExporting } from "@/redux/action/preview";
import { ExportingVideoWrapper } from "./style";
import { useEffect } from "react";

interface ExportingVideoProps {
  actSetIsExporting: (payload: boolean) => any;
}

const ExportingVideo = (props: ExportingVideoProps) => 
{
	const navigate = useNavigate();

	useEffect(() => 
	{
		setTimeout(() => 
		{
			navigate({ pathname: "/export-success" });
		}, 3000);
	}, []);

	const onCancelExport = () => 
	{
		props?.actSetIsExporting(false);
		navigate({ pathname: "/edit" });
	};

	return (
		<ExportingVideoWrapper>
			<div className='percent'>78%</div>

			<div className='title'>Exporting ...</div>
			<div className='description'>
				Please be patient as this process may take several minute
			</div>

			<Button type='primary' className='primary-btn' onClick={onCancelExport}>
				Cancel
			</Button>
		</ExportingVideoWrapper>
	);
};

export default connect((state: RootState) => ({}), { actSetIsExporting })(
	ExportingVideo
);
