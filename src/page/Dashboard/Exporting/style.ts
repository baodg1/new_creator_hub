import styled from "styled-components";

const ExportingVideoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;

  .percent {
    color: white;
    font-size: 3rem;
    font-weight: 600;
    margin-bottom: 4rem;
  }

  .title {
    color: white;
    font-size: 2rem;
    font-weight: 500;
    margin-bottom: 1rem;
  }

  .description {
    color: var(--color-text-gray);
    font-size: 1.4rem;
    margin-bottom: 5rem;
  }
`;

export { ExportingVideoWrapper };
