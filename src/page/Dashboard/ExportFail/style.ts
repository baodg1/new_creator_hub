import styled from "styled-components";

const ExportFailWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;

  img {
    margin-bottom: 4rem;
    width: 20rem;
  }

  .title {
    color: white;
    font-size: 2rem;
    font-weight: 500;
    margin-bottom: 1rem;
  }

  .description {
    color: var(--color-text-gray);
    font-size: 1.4rem;
    margin-bottom: 5rem;
  }
`;

export { ExportFailWrapper };
