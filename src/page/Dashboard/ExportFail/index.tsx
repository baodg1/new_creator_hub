import { Button } from "antd";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import exportFailImg from "@/asset/export-fail.png";
import { RootState } from "@/redux/reducer";
import { actSetIsExporting } from "@/redux/action/preview";
import { ExportFailWrapper } from "./style";

interface ExportingFailProps {
  actSetIsExporting: (payload: boolean) => any;
}

const ExportingFail = (props: ExportingFailProps) => 
{
	const navigate = useNavigate();

	const onExport = () => 
	{
		props?.actSetIsExporting(true);
		navigate({ pathname: "/exporting" });
	};

	return (
		<ExportFailWrapper>
			<img src={exportFailImg} alt='' />

			<div className='title'>Render Failed</div>
			<div className='description'>
				We cannot export your video. Please click “ Export Again” to re-export
			</div>

			<Button type='primary' className='primary-btn' onClick={onExport}>
				Export again
			</Button>
		</ExportFailWrapper>
	);
};

export default connect((state: RootState) => ({}), { actSetIsExporting })(
	ExportingFail
);
