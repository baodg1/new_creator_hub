import { MyProjectWrapper, TabsWrapper, ContentWrapper } from "./style";
import TabPaneAllProject from "@/component/MyProject/TabAllProject";
import TabPaneDraft from "@/component/MyProject/Draft";
import TabPaneExports from "@/component/MyProject/Exports";
const MyProject = () => 
{
	const items = [
		{ label: "All Projects", key: "item-1", children: <TabPaneAllProject /> }, // remember to pass the key prop
		{ label: "Draft", key: "item-2", children: <TabPaneDraft /> },
		{ label: "Exports", key: "item-3", children: <TabPaneExports /> }
	];

	return (
		<MyProjectWrapper>
			<ContentWrapper>
				<TabsWrapper defaultActiveKey='1' items={items} />
			</ContentWrapper>
		</MyProjectWrapper>
	);
};

export default MyProject;
