import styled from "styled-components";
import { Tabs } from "antd";
const MyProjectWrapper = styled.div`
  background-color: white;
  height: 100%;
`;
const TabsWrapper = styled(Tabs)`
  /* padding-bottom: 24px; */
  .ant-tabs-tab {
    font-size: 16px;
  }
  .ant-tabs-tab + .ant-tabs-tab {
    margin-left: 20px;
  }
  .ant-tabs-tab-btn {
    color: #131317 !important;
    font-weight: 600;
    padding: 0 8px;
  }
  .ant-tabs-ink-bar {
    background-color: #fe2c55;
  }
`;

const ContentWrapper = styled("div")`
  padding: 40px 60px 30px 60px;
  min-height: 100%;
`;

export { MyProjectWrapper, TabsWrapper, ContentWrapper };
