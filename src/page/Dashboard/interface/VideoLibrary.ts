export interface ModalVideoDetailProps {
    open:boolean,
    onCancel:(e: React.MouseEvent<HTMLElement>) => void
}

export interface IVideoItem {
    videoUrl: string,
    duration: number,
    thumbnail: string
}

export interface SilderVideoProps {
    listVideo: IVideoItem[]
}

export interface DetailVideoProps {
    detailVideo:IVideoItem
}

export interface IVideoLibrary {
    category:string,
    listVideo: IVideoItem[]
}
