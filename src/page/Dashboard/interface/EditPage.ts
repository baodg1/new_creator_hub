import { IFrame, IPreviewOnPhone, ITrack } from "@/redux/interface";
import { IProjectSetting } from "@/redux/interface/setting";

export interface EditPageProps {
	isFullScreen: boolean;
	actSetModalAlertReloadVisible: (payload: boolean) => any;
	actSaveGetListFile: (payload: any) => any;
	actSetListTrack: (payload: ITrack[]) => any;
	actSetProjectSetting: (payload: IProjectSetting) => any;
	rotateAngle: number;
	selectedFrame: IFrame | null;
	onPhone: IPreviewOnPhone;
	undoStack: any[];
	redoStack: any[];
}
