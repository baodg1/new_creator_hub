import { ReactComponent as CloseIcon } from "@/asset/x-close.svg";
import { useNavigate } from "react-router-dom";
import { XCloseIconWrapper } from "./style";

export const XCloseIcon = () => 
{
	const navigate = useNavigate();

	const onClickCloseAuthForm = () => 
	{
		navigate("/");
	};
	
	return (
		<XCloseIconWrapper onClick={() => onClickCloseAuthForm()}>
			<CloseIcon />
		</XCloseIconWrapper>
	);
};
