import styled from "styled-components";

export const XCloseIconWrapper = styled('div')`
    position: absolute;
    top: 18px;
    right: 18px;
    width: 20px;
    height: 20px;
    cursor: pointer;
    &:hover{
        background-color: #f5f5f5;
        border-radius: 4px;
    }
`;
