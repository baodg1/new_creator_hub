import styled from "styled-components";
import bgImage from "@/asset/auth/login_bg.svg";

export const LoginWrapper = styled.div`
  background-image: url(${bgImage});
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  overflow-x: hidden;
`;

export const LoginContent = styled("div")`
  background-color: #ffffff;
  border-radius: 0.4rem;
  padding: 5% 2.5%;
  font-size: 1.6rem;
  height: 90%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  position: relative;
  @media screen and (max-width: 576px) {
    width: 90%;
  }

  @media screen and (min-width: 576px) {
    width: 80%;
  }

  @media screen and (min-width: 768px) {
    width: 60%;
  }

  @media screen and (min-width: 992px) {
    width: 600px;
  }

  .logo {
    /* margin-bottom: 4rem; */
    text-align: center;
  }
  .login-fb-gg {
    .btn {
      border: 1px solid #e7e7e7;
      border-radius: 0.8rem;
      margin: 1rem 0rem;
      width: 100%;
      color: #131317;
      line-height: 2.4rem;
      display: flex;
      align-items: center;
      justify-content: center;
      img {
        margin-right: 1.6rem;
        width: 2.4rem;
        height: 2.4rem;
      }
    }
  }
  .title {
    font-weight: 600;
    font-size: 2.8rem;
    line-height: 3.4rem;
    color: #313335;
    text-align: center;
  }
  .content {
    font-weight: 400;
    line-height: 2.4rem;
    color: #313335;
    text-align: center;
  }
`;
