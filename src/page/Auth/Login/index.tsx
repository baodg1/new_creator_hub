import { RootState } from "@/redux/reducer";
import { connect } from "react-redux";
import { LoginContent, LoginWrapper } from "./styled";
import Logo from "@/asset/auth/logo.svg";
import LoginForm from "@/component/Auth/LoginForm";
import LoginWithSocial from "@/component/Auth/LoginWithSocial";
import { XCloseIcon } from "../Common/XCloseIcon";

export const Login = () => 
{
	return (
		<LoginWrapper>
			<LoginContent>
				<XCloseIcon />
				<div className='logo'>
					<img src={Logo} alt='' />
				</div>
				<div className='login-fb-gg'>
					<LoginWithSocial />
				</div>
				<div className='title'>Log in</div>
				<LoginForm />
			</LoginContent>
		</LoginWrapper>
	);
};

const mapStateToProps = (state: RootState) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
