import styled from "styled-components";
import loginBg from "@/asset/auth/login_bg.svg";
import logo from "@/asset/auth/logo.svg";
import forrgotSuccess from "@/asset/auth/forgot_success.svg";
import { Button } from "antd";

const ForgotPasswordWrapper = styled("div")`
  font-size: 1.6rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  overflow-x: hidden;
  background-color: var(--color-background);
  min-height: 100vh;

  .forgot-container {
    background-image: url(${loginBg});
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .font-bold {
    font-weight: 600;
  }
  .text-center {
    text-align: center;
  }
`;

const ForgotFormWrapper = styled("div")`
  background-color: #fff;
  width: 33%;
  min-width: 400px;
  max-width: 600px;
  height: 90%;
  min-height: 600px;
  max-height: 800px;
  border-radius: 4px;
  padding: 100px 40px;

  font-family: "Inter";
  font-style: normal;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  @media screen and (max-width: 576px) {
    width: 90%;
  }

  @media screen and (min-width: 576px) {
    width: 80%;
  }

  @media screen and (min-width: 768px) {
    width: 60%;
  }

  @media screen and (min-width: 992px) {
    width: 600px;
  }
  .title-forgot {
    font-weight: 600;
    font-size: 28px;
    line-height: 34px;
    color: #313335;
    text-align: center;
  }

  .sub-title-forgot {
    font-size: 16px;
    line-height: 24px;
    text-align: center;
  }
  form.ant-form {
    width: 100%;
  }
  .mt-btn {
    margin-top: 25px !important;
    margin-bottom: 0px !important;
  }
  .ant-form-item-required:before {
    display: none !important;
  }

  .label-color {
    color: #313335 !important;
  }
`;
const AuthLogoWrapper = styled("div")`
  background-image: url(${logo});
  width: 220px;
  height: 60px;
  background-size: cover;
  position: absolute;
  top: 30px;
  left: auto;
`;

const ButtonSubmitWrapper = styled(Button)`
  width: 100%;
  border-radius: 8px;
`;

const FormForgotDoneWrapper = styled("div")`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;

  .forgot-success {
    background-image: url(${forrgotSuccess});
    background-repeat: no-repeat;
    background-size: cover;
    width: 120px;
    height: 120px;
  }
  .message-title {
    font-weight: 600;
  }
  .btn-back-login {
    margin: 30px 0;
  }
  .orther-message {
    color: #787878;
  }
  .message-title {
    font-size: 28px;
  }
  .message-content {
    font-size: 14px;
  }
`;

export {
	ForgotPasswordWrapper,
	ForgotFormWrapper,
	ButtonSubmitWrapper,
	AuthLogoWrapper,
	FormForgotDoneWrapper
};
