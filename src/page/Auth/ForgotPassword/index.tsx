import ForgotPasswordForm from "@/component/Auth/ForgotPassword";
import { XCloseIcon } from "../Common/XCloseIcon";
import {
	AuthLogoWrapper,
	ForgotFormWrapper,
	ForgotPasswordWrapper
} from "./style";

const ForgotPassword = () => 
{

	return (
		<ForgotPasswordWrapper>
			<div className='forgot-container'>
				<ForgotFormWrapper>
					<XCloseIcon />
					<AuthLogoWrapper className='logo' />
					<ForgotPasswordForm />
				</ForgotFormWrapper>
			</div>
		</ForgotPasswordWrapper>
	);
};

export default ForgotPassword;
