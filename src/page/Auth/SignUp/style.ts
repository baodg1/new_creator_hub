import styled from "styled-components";
import bgImage from "@/asset/auth/login_bg.svg";

export const SignUpWrapper = styled("div")`
  height: 100vh;
  display: flex;
  background-image: url(${bgImage});
  align-items: center;
  justify-content: center;
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 1rem;
  line-height: 24px;
  color: #131317;
`;
