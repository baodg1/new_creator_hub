import { SignUpWrapper } from "./style";
import SignUpForm from "@/component/Auth/SignUpForm";

const SignUp = () => 
{
	return (
		<SignUpWrapper>
			<SignUpForm />
		</SignUpWrapper>
	);
};

export default SignUp;
