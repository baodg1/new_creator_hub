import { useNavigate } from "react-router-dom";
import { Header, Footer, ResetEditData } from "@/component";
import notFoundImg from "@/asset/not-found.png";
import { NotFoundPageWrapper } from "./style";

const NotFoundPage = () => 
{
	const navigate = useNavigate();

	const onClick = () => 
	{
		navigate({ pathname: "/" });
	};

	return (
		<ResetEditData>
			<NotFoundPageWrapper>
				<Header />

				<div className='content'>
					<img src={notFoundImg} alt='' />
					<h2>Something went wrong</h2>
					<div className='description'>
						We are working on the problem, and appreciate your patience You may
						also refesh the page or try again later
					</div>

					<div className='button' onClick={onClick}>
						Back to main page
					</div>
				</div>

				<Footer />
			</NotFoundPageWrapper>
		</ResetEditData>
	);
};

export default NotFoundPage;
