import styled from "styled-components";

const NotFoundPageWrapper = styled.div`
  font-size: 1.6rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  overflow-x: hidden;
  background-color: var(--color-background);
  min-height: 100vh;

  .content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-grow: 1;

    img {
      margin-bottom: 3rem;
    }

    h2 {
      font-weight: 500;
      font-size: 2.5rem;
      color: var(--color-text);
    }

    .description {
      font-size: 1.6rem;
      color: var(--color-text-secondary);
      width: 50rem;
      text-align: center;
    }

    .button {
      color: var(--color-light-gray);
      font-size: 1.4rem;
      font-weight: 500;
      padding: 1rem 2rem;
      background-color: #e9e9e9;
      border-radius: 0.8rem;
      margin-top: 3rem;
      cursor: pointer;
    }
  }
`;

export { NotFoundPageWrapper };
