import { Header, Footer, ResetEditData } from "@/component";
import Content from "./Content";
import { HomePageWrapper } from "./style";

const HomePage = () => 
{
	return (
		<ResetEditData>
			<HomePageWrapper>
				<Header />
				<Content />
				<Footer />
			</HomePageWrapper>
		</ResetEditData>
	);
};

export default HomePage;
