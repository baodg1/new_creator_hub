import styled from "styled-components";

const ContentWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  align-items: center;

  .content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-bottom: 20vh;

    h1 {
      font-weight: 700;
      font-size: 3rem;
      color: var(--color-text);
    }

    h2 {
      font-weight: 300;
      font-size: 2.2rem;
    }

    .list-item {
      display: flex;
      width: 100%;
      margin-top: 8vh;

      .item-wrapper {
        flex-basis: 25%;
        padding: 0 1.5rem;
      }
    }
  }
`;

const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background-color: white;
  padding: 3rem 0;
  border-radius: 2.5rem;

  img {
    height: 6rem;
    width: 6rem;
    margin-bottom: 2rem;
  }

  div {
    color: var(--color-text-secondary);
  }
`;

export { ContentWrapper, ItemWrapper };
