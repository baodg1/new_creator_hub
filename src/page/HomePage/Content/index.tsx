import { Row, Col } from "antd";
import icon1 from "@/asset/convert-icon.png";
import icon2 from "@/asset/compress-icon.png";
import icon3 from "@/asset/edit-icon.png";
import icon4 from "@/asset/download-icon.png";
import { ContentWrapper } from "./style";
import Item from "./Item";

const listItem = [
	{ icon: icon1, label: "Convert Video", path: "/edit" },
	{ icon: icon2, label: "Compress Video", path: "/edit" },
	{ icon: icon3, label: "Edit Video", path: "/edit" },
	{ icon: icon4, label: "Download Video", path: "/edit" }
];

const Content = () => 
{
	return (
		<ContentWrapper>
			<Row justify='center' style={{ width: "100%" }}>
				<Col span={20} xs={22} sm={22} md={21} lg={21} xl={18} xxl={14}>
					<div className='content'>
						<h1>Online Video Editor - FreeLabConvert</h1>
						<h2>
							Every tool you need to edit videos online. 100% FREE and easy to
							use!
						</h2>

						<div className='list-item'>
							{listItem?.map((item: any, index: number) => (
								<div className='item-wrapper' key={index}>
									<Item {...item} />
								</div>
							))}
						</div>
					</div>
				</Col>
			</Row>
		</ContentWrapper>
	);
};

export default Content;
