import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { actSaveUserLogIn } from "@/redux/action/auth";
import { message } from "antd";
import { actSaveCreatedProject } from "@/redux/action/project";
import { actSetProjectSetting } from "@/redux/action/preview";
import projectApi from "@/services/projectAPI";
import { IProjectSetting } from "@/redux/interface/setting";
import { DURATION_OPTION } from "@/config/constant";
import { RootState } from "@/redux/reducer";
import { ItemWrapper } from "./style";
import { BackgroundType } from "@/services/interface/project";

interface ItemProps {
  icon: any;
  label: string;
  path: string;
  actSaveUserLogIn: (payload: any) => any;
  actSaveCreatedProject: (payload: any) => any;
  actSetProjectSetting: (payload: IProjectSetting) => any;
  previewSetting: IProjectSetting;
}

const Item = (props: ItemProps) => 
{
	const { icon, label, path, previewSetting } = props;
	const navigate = useNavigate();

	const onGoToDashboard = async () => 
	{
		props?.actSaveUserLogIn({
			token : "seed_token"
		});

		const res = await projectApi.createProject({}, {});

		props?.actSaveCreatedProject(res?.projectId);
		message.info(`Create new project with id: ${res?.projectId}`);

		// set default setting
		props?.actSetProjectSetting({
			...previewSetting,
			aspectRatioXValue : 16,
			aspectRatioYValue : 9,
			backgroundType    : BackgroundType.BackgroundColor,
			durationType      : DURATION_OPTION.AUTOMATIC,
			backgroundColor   : "#000000"
		});

		navigate({
			pathname : `${path}/res?.projectId`
		});
	};

	return (
		<ItemWrapper onClick={onGoToDashboard}>
			<img alt='' src={icon} className='icon' />
			<div>{label}</div>
		</ItemWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		previewSetting : state?.Preview?.setting
	}),
	{
		actSaveUserLogIn,
		actSaveCreatedProject,
		actSetProjectSetting
	}
)(Item);
