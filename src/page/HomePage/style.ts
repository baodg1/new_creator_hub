import styled from "styled-components";

const HomePageWrapper = styled.div`
  font-size: 1.6rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  overflow-x: hidden;
  background-color: var(--color-background);
  min-height: 100vh;
`;

export { HomePageWrapper };
