import styled from 'styled-components';

const ConnectAccountPage = styled.div`
  padding: 15px;
  h1 {
    font-style: normal;
    font-weight: 600;
    font-size: 22px;
    line-height: 27px;
    display: flex;
    align-items: center;
    color: #313335;
    background: #FFFFFF;
    border-radius: 12px;
    padding: 24px 30px;
    margin-bottom: 50px;
    justify-content: space-between;
    span {
      &:last-child {
        font-style: normal;
        font-weight: 400;
        font-size: 20px;
        line-height: 25px;
        color: #313335;
        background: #F7F8F9;
        border-radius: 4px;
        padding: 8px 16px;
      }
    }
  }
  .ant-card {
    background: #FFFFFF;
    border-radius: 12px;
    display: flex;
    justify-content: center;
    height: 100%;
    text-align: center;
    img {
      margin-top: -54px;
    }
    h3 {
      font-style: normal;
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      text-align: center;
      color: #313335;
      margin-bottom: 20px;
    }
    p {
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #387DDE;
      margin-bottom: 10px;
      cursor: pointer;
      &:last-child {
        margin-bottom: 0;
      }
    }
  }
`;

export default ConnectAccountPage;
