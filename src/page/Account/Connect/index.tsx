import React, { useEffect, useState } from "react";
import { Row, Col, Card, Spin } from "antd";
import ConnectAccountPage from "./style";
import { useNavigate, useSearchParams } from "react-router-dom";
import { APIConnectAccount } from "@/services/connectAccount";
import { APILoginTiktok } from "@/services/createPost";
import { loginFB, loginGoogle, loginTwitter } from "./social";
import {
	TYPE_FB,
	TYPE_SOCIAL_TEXT,
	TYPE_SOCIAL_INDEX,
	TYPE_FB_INDEX
} from "./../constant/connect";
import { Lang } from "@/utils/config";
import { success, error } from "@/component/Message";
// icon social
import fbIcon from "@/asset/connect-social/fb.svg";
import instagramIcon from "@/asset/connect-social/instagram.svg";
import twitterIcon from "@/asset/connect-social/twitter.svg";
import youtubeIcon from "@/asset/connect-social/youtube.svg";
import tiktokIcon from "@/asset/connect-social/tiktok.svg";
import { PLATFORM_SOCIAL_SERVICE } from "@/config/constant";

const ConnectAccount: React.FC = () => 
{
	const [ isLoading, setIsLoading ] = useState(false);
	const navigate = useNavigate();
	const [ searchParams ] = useSearchParams();

	const handleConnectSocial = (
		accountName: string,
		platformId: number,
		accountId: string,
		token: string,
		socialAccountType: string,
		tokenSecret?: string
	) => 
	{
		const data = {
			socials : [
				{
					"account_name"        : accountName,
					"platform_id"         : platformId,
					"account_id"          : accountId,
					"token"               : token,
					"link"                : "",
					"social_account_type" : socialAccountType,
					"token_secret"        : tokenSecret
				}
			]
		};

		APIConnectAccount(data)
			.then((res) => 
			{
				if (!res?.success) return error(Lang.trans("connect_failed"));
				success(Lang.trans("connect_success"));
				setIsLoading(false);
				navigate(`/account/manage-account`);
			})
			.catch((err) => 
			{
				if (err?.response?.data?.message) error(err.response.data.message);
				setIsLoading(false);
			});
	};

	useEffect(() => 
	{
		const code = searchParams.get("code");

		if (!code) return;
		setIsLoading(true);
		APILoginTiktok({ code }).then((res) => 
		{
			if (res?.success && res?.data) 
			{
				const { data } = res;

				handleConnectSocial(
					data?.account_name,
					TYPE_SOCIAL_INDEX.TIKTOK,
					data?.account_id,
					data?.token,
					TYPE_SOCIAL_TEXT.TIKTOK
				);
			}
		});
	}, []);

	const connectFB = (type: string, typeIndex: number) => 
	{
		loginFB(typeIndex).then((res) => 
		{
			if (res) 
			{
				const params = {
					token    : res?.accessToken || "",
					type,
					platform : PLATFORM_SOCIAL_SERVICE.FACEBOOK
				};
				const convertParams = new URLSearchParams(params).toString();

				navigate(`/account/connect-account-detail?${convertParams}`);
			}
		});
	};

	const connectPageFB = () => 
	{
		connectFB(TYPE_FB.PAGE, TYPE_FB_INDEX.PAGE);
	};

	const connectGroupFB = () => 
	{
		connectFB(TYPE_FB.GROUP, TYPE_FB_INDEX.GROUP);
	};

	// connect channel youtube
	const ConnectChannelYoutube = () => 
	{
		loginGoogle().then((res) => 
		{
			if (!res) return;
			handleConnectSocial(
				res?.user?.displayName || "",
				TYPE_SOCIAL_INDEX.YOUTUBE,
				res?.user?.uid || "",
				res?.token || "",
				TYPE_SOCIAL_TEXT.YOUTUBE
			);
		});
	};

	// connect twitter
	const ConnectProfileTwitter = () => 
	{
		loginTwitter().then((res) => 
		{
			if (!res) return;
			handleConnectSocial(
				res?.user?.displayName || "",
				TYPE_SOCIAL_INDEX.TWITTER,
				res?.user?.uid || "",
				res?.token || "",
				TYPE_SOCIAL_TEXT.TWITTER,
				res?.secret || ""
			);
		});
	};

	// connect instagram
	const connectInstagram = () => 
	{
		loginFB(TYPE_FB_INDEX.INS).then((res) => 
		{
			if (!res) return;
			handleConnectSocial(
				res?.user?.displayName || "",
				TYPE_SOCIAL_INDEX.INSTAGRAM,
				res?.user?.uid || "",
				res?.accessToken || "",
				TYPE_SOCIAL_TEXT.INSTAGRAM
			);
		});
	};

	const ConnectProfileTiktok = () => 
	{
		const state = Math.random().toString(36)
			.substring(2);
		const params = {
			"client_key"    : `${process.env.REACT_APP_CLIENT_KEY_TIKTOK}`,
			"scope"         : "user.info.basic",
			"response_type" : "code",
			"redirect_uri"  : `${process.env.REACT_APP_WEB_URL}/account/connect-account`,
			state
		};
		const convertParams = new URLSearchParams(params).toString();

		window.location.href = `${process.env.REACT_APP_URL_AUTH_TIKTOK}?${convertParams}`;
	};

	return (
		<Spin tip='Loading...' spinning={isLoading}>
			<ConnectAccountPage className='accountWrap'>
				<h1>
					<span>{Lang.trans("connect_account")}</span>
					<span>{Lang.trans("connect_account")}: 5/50</span>
				</h1>
				<Row gutter={[ 16, 48 ]}>
					<Col sm={24} md={12} lg={8} xl={6}>
						<Card bordered={false}>
							<img src={fbIcon} alt='Facebook' />
							<h3>Facebook</h3>
							<p onClick={connectPageFB}>{Lang.trans("connect_page")}</p>
							<p onClick={connectGroupFB}>{Lang.trans("connect_group")}</p>
						</Card>
					</Col>
					<Col sm={24} md={12} lg={8} xl={6}>
						<Card bordered={false}>
							<img src={instagramIcon} alt='Instagram' />
							<h3>Instagram</h3>
							<p onClick={connectInstagram}>{Lang.trans("connect_business")}</p>
						</Card>
					</Col>
					<Col sm={24} md={12} lg={8} xl={6}>
						<Card bordered={false}>
							<img src={youtubeIcon} alt='Youtube' />
							<h3>Youtube</h3>
							<p onClick={ConnectChannelYoutube}>
								{Lang.trans("connect_channel")}
							</p>
						</Card>
					</Col>
					<Col sm={24} md={12} lg={8} xl={6}>
						<Card bordered={false}>
							<img src={tiktokIcon} alt='Tik Tok' />
							<h3>Tik Tok</h3>
							<p onClick={ConnectProfileTiktok}>
								{Lang.trans("connect_profile")}
							</p>
						</Card>
					</Col>
					<Col sm={24} md={12} lg={8} xl={6}>
						<Card bordered={false}>
							<img src={twitterIcon} alt='Twitter' />
							<h3>Twitter</h3>
							<p onClick={ConnectProfileTwitter}>
								{Lang.trans("connect_profile")}
							</p>
						</Card>
					</Col>
				</Row>
			</ConnectAccountPage>
		</Spin>
	);
};

export default ConnectAccount;
