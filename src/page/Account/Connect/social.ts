import { initializeApp } from "firebase/app";
import {
	getAuth,
	signInWithPopup,
	FacebookAuthProvider,
	GoogleAuthProvider,
	TwitterAuthProvider
} from "firebase/auth";
import { firebaseConfig } from "@/config/firebaseConfig";
import { TYPE_FB_INDEX, PERMISSIONS_PAGE, PERMISSIONS_GROUP, PERMISSIONS_INS } from "./../constant/connect";

initializeApp(firebaseConfig);
const auth = getAuth();

const providerFacebook = new FacebookAuthProvider();

const handlePermissions = (list: string[]) => 
{
	list.map((item) => 
	{
		providerFacebook.addScope(item);

		return item;
	});
};

export const loginFB = (type: number) => 
{
	switch (type) 
	{
	case TYPE_FB_INDEX.PAGE: 
		handlePermissions(PERMISSIONS_PAGE);
		break;
	case TYPE_FB_INDEX.GROUP: 
		handlePermissions(PERMISSIONS_GROUP);
		break;
	case TYPE_FB_INDEX.INS:
		handlePermissions(PERMISSIONS_INS);
		break; 
	}
	
	return signInWithPopup(auth, providerFacebook)
		.then((result) => 
		{
			// The signed-in user info.
			const user = result.user;

			// This gives you a Facebook Access Token. You can use it to access the Facebook API.
			const credential = FacebookAuthProvider.credentialFromResult(result);
			const accessToken = credential?.accessToken;

			return { accessToken, user };
		})
		.catch((error) => 
		{
			// Handle Errors here.
			// const errorCode = error.code;
			// const errorMessage = error.message;
			// The email of the user's account used.
			// const email = error.customData.email;
			// The AuthCredential type that was used.
			// const credential = FacebookAuthProvider.credentialFromError(error);

			// ...
		});
};

const providerGoogle = new GoogleAuthProvider();

providerGoogle.addScope("https://www.googleapis.com/auth/youtube");
export const loginGoogle = () => 
{
	return signInWithPopup(auth, providerGoogle)
		.then((result) => 
		{
			// This gives you a Google Access Token. You can use it to access the Google API.
			const credential = GoogleAuthProvider.credentialFromResult(result);
			const token = credential?.accessToken;
			// The signed-in user info.
			const user = result.user;
			// ...

			return { token, user };
		})
		.catch((error) => 
		{
			// Handle Errors here.
			// const errorCode = error.code;
			// const errorMessage = error.message;
			// The email of the user's account used.
			// const email = error.customData.email;
			// The AuthCredential type that was used.
			// const credential = GoogleAuthProvider.credentialFromError(error);
			// ...
		});
};

const providerTwitter = new TwitterAuthProvider();

export const loginTwitter = () => 
{
	return signInWithPopup(auth, providerTwitter)
		.then((result) => 
		{
			// This gives you a the Twitter OAuth 1.0 Access Token and Secret.
			// You can use these server side with your app's credentials to access the Twitter API.
			const credential = TwitterAuthProvider.credentialFromResult(result);
			const token = credential?.accessToken;
			const secret = credential?.secret;

			// The signed-in user info.
			const user = result.user;
			
			return { token, user, secret };
			// ...
		})
		.catch((error) => 
		{
			// Handle Errors here.
			// const errorCode = error.code;
			// const errorMessage = error.message;
			// The email of the user's account used.
			// const email = error.customData.email;
			// The AuthCredential type that was used.
			// const credential = TwitterAuthProvider.credentialFromError(error);
			// ...
		});
};
