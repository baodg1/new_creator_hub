export interface chooseType {
  page_token: string;
  access_token: number;
  link: string;
  name: string;
  id: string;
  connect: number;
  can_post: boolean;
}

export interface listSelectType {
  name: string;
  not_connected: chooseType[];
  connected: chooseType[];
  link: string;
}
