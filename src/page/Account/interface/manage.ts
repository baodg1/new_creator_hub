export interface ColumnTableType {
  key: string;
  platform: object;
  account_name: any;
  admin_name: string[];
  create_date: string;
  update_date: string;
  status: string;
}

export interface PaginateType {
  limit: number;
  offset: number;
  total: number;
  currentPage: number;
}
