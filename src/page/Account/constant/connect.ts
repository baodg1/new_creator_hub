export const TYPE_FB = {
	PAGE  : "page",
	GROUP : "group"
};

export const TYPE_FB_INDEX = {
	PAGE  : 1,
	GROUP : 2,
	INS   : 3
};

export const TYPE_SOCIAL_INDEX = {
	FACEBOOK  : 1,
	INSTAGRAM : 2,
	YOUTUBE   : 3,
	TIKTOK    : 4,
	TWITTER   : 5
};

export const TYPE_SOCIAL_TEXT = {
	FACEBOOK  : 1,
	INSTAGRAM : "BUSINESS",
	YOUTUBE   : "CHANNEL",
	TIKTOK    : "PROFILE",
	TWITTER   : "PROFILE"
};

export const PERMISSIONS_PAGE = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile"
];

export const PERMISSIONS_GROUP = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"groups_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile",
	"pages_manage_posts"
];

export const PERMISSIONS_INS = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile",
	"instagram_basic",
	"instagram_manage_comments",
	"instagram_manage_insights",
	"instagram_content_publish"
];
