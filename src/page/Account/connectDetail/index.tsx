import { Input, Button, Row, Col, Checkbox } from "antd";
import ConnectAccountDetailPage from "./style";
import { SearchOutlined } from "@ant-design/icons";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import {
	APIPageList,
	APIGroupList,
	APIConnectAccount
} from "@/services/connectAccount";
import { covertParams, toLowerCaseNonAccentVietnamese } from "@/utils/help";
import { listSelectType, chooseType } from "./../interface/connectDetail";
import { TYPE_FB } from "./../constant/connect";
import { success, error } from "@/component/Message";
import { Lang } from "@/utils/config";

const ConnectAccountDetail = () => 
{
	const [ listSelect, setListSelect ] = useState<listSelectType>();
	const [ showListSelect, setShowListSelect ] = useState<chooseType[]>();
	const [ choose, setChoose ] = useState<chooseType[] | []>([]);
	const [ isLoading, setIsLoading ] = useState(false);
	const history = useLocation();
	const params = covertParams(history?.search);
	const navigate = useNavigate();

	useEffect(() => 
	{
		if (params?.type === TYPE_FB.PAGE) 
		{
			APIPageList(params).then((res) => 
			{
				if (!res?.success || !res?.data) return Promise.resolve("Fails");
				setListSelect(res.data);
				setShowListSelect(res.data?.not_connected);
			});
		}
		if (params?.type === TYPE_FB.GROUP) 
		{
			APIGroupList(params).then((res) => 
			{
				if (!res?.success || !res?.data) return Promise.resolve("Fails");
				setListSelect(res.data);
				setShowListSelect(res.data?.not_connected);
			});
		}
	}, []);

	const handleConnectAccount = () => 
	{
		setIsLoading(true);
		const data: any = {
			socials : []
		};

		choose.map((item, key) => 
		{
			data.socials.push({
				"account_name"        : item?.name,
				"platform_id"         : 1,
				"account_id"          : item?.id,
				token                 : item?.access_token,
				link                  : item?.link,
				"social_account_type" : params?.type.toUpperCase(),
				"link_admin"          : listSelect?.link,
				"account_admin_name"  : listSelect?.name
			});
			
			return item;
		});
		APIConnectAccount(data).then((res) => 
		{
			if (!res?.success) return error(Lang.trans("connect_failed"));
			success(Lang.trans("connect_success"));
			setIsLoading(false);
			navigate(`/account/manage-account`);
		});
	};

	const onChange = (value: boolean, objectSelect: any) => 
	{
		if (!choose.length) return setChoose([ objectSelect ]);

		let checkDump = false;

		choose.map((item) => 
		{
			if (item.id === objectSelect.id) checkDump = true;
			
			return item;
		});
		const newChoose = [ ...choose, objectSelect ];

		!checkDump && value && setChoose(newChoose);
		checkDump &&
      !value &&
      setChoose(choose.filter((item) => item.id !== objectSelect.id));
	};

	const onChangeSearch = (value: string) => 
	{
		const listNotConnect = listSelect?.not_connected;

		if (!listNotConnect || !listNotConnect.length) return;
		const newList = listNotConnect?.filter((item) => 
		{
			return (
				toLowerCaseNonAccentVietnamese(item?.name)?.indexOf(
					value?.toLowerCase()
				) !== -1
			);
		});

		setShowListSelect(newList);
	};

	const renderNotFound = (type: string) => (
		<div className='not-found'>
			{type} {Lang.trans("not_found")}
		</div>
	);

	const renderListSelect = (type: string) => 
	{
		if (!showListSelect?.length) return renderNotFound(type);

		return showListSelect?.map((item: any, key: number) => 
		{
			return (
				<Col md={6} key={key}>
					<Checkbox onChange={(e) => onChange(e.target.checked, item)}>
						{item?.name}
					</Checkbox>
				</Col>
			);
		});
	};

	const renderConnected = (type: string) => 
	{
		if (!listSelect?.connected?.length) return renderNotFound(type);
		
		return listSelect?.connected?.map((item: any, key: number) => 
		{
			return (
				<Col md={6} key={key}>
					<span className='dot-item' />
					<span>{item?.name}</span>
				</Col>
			);
		});
	};

	const type =
    params?.type === TYPE_FB.PAGE ? Lang.trans("pages") : Lang.trans("groups");
	const countConnect = listSelect?.connected?.length;
	const countNotConnect = listSelect?.not_connected?.length;
	const countAccount =
    typeof countConnect === "number" && typeof countNotConnect === "number" ? countConnect + countNotConnect : null;

	return (
		<ConnectAccountDetailPage className='accountWrap'>
			<div className='info-social box'>
				<h1>
					<span> {Lang.trans("connect_account")}</span>
					<span>
						{Lang.trans("connect_account")}: {countConnect}/{countAccount}
					</span>
				</h1>
				<div>
					<div>
						<span className='label'>{Lang.trans("name")}</span>
						<span>{listSelect?.name}</span>
					</div>
					<div>
						<span className='label'>{Lang.trans("link")}</span>
						<span>
							https://www.facebook.com/sharer/sharer.php?u=stephanie6216
						</span>
					</div>
				</div>
			</div>
			<div className='box type-connect'>
				<h3>
					{Lang.trans("select")} {type}
				</h3>
				<div className='search-submit'>
					<div className='search'>
						<SearchOutlined className='icon-search' />
						<Input
							placeholder={`${Lang.trans("search")}`}
							onChange={(e) => onChangeSearch(e.target.value)}
						/>
						<Button type='primary' danger>
							{Lang.trans("search")}
						</Button>
					</div>
					<Button
						type='primary'
						danger
						className='submit'
						onClick={handleConnectAccount}
						loading={isLoading}
						disabled={!choose.length}
					>
						{Lang.trans("connect_account")}
					</Button>
				</div>
				<div className='list-select-connect'>
					<Row gutter={[ 14, 14 ]}>{renderListSelect(type)}</Row>
				</div>
			</div>
			<div className='box type-connect connected-item'>
				<h3>
					{Lang.trans("connect")} {type}
				</h3>
				<div className='list-select-connect'>
					<Row gutter={[ 14, 14 ]}>{renderConnected(type)}</Row>
				</div>
			</div>
		</ConnectAccountDetailPage>
	);
};

export default ConnectAccountDetail;
