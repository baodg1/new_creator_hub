import styled from "styled-components";

const ConnectAccountDetailPage = styled.div`
  padding: 15px;
  span {
    font-size: 16px;
  }
  .info-social {
    h1 {
      line-height: 27px;
      display: flex;
      align-items: center;
      color: #313335;
      justify-content: space-between;
      border-bottom: 1px solid #dddd;
      margin-bottom: 20px;
      padding-bottom: 15px;
      span {
        font-style: normal;
        font-weight: 600;
        font-size: 22px;
        &:last-child {
          font-style: normal;
          font-weight: 400;
          font-size: 20px;
          line-height: 25px;
          color: #313335;
          background: #f7f8f9;
          border-radius: 4px;
          padding: 8px 16px;
        }
      }
    }
    .label {
      width: 75px;
      display: inline-block;
    }
  }
  .box {
    background: #ffffff;
    border-radius: 8px;
    padding: 24px 30px;
  }
  .type-connect {
    margin-top: 20px;
    h3 {
      color: #313335;
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
    }
    .search-submit {
      display: flex;
      justify-content: space-between;
      margin-top: 20px;
      .search {
        max-width: 400px;
        display: flex;
        align-items: center;
        background: #f7f8f9;
        border-radius: 12px;
        padding: 5px;
        input {
          border: none;
          box-shadow: none;
          background: #f7f8f9;
          &:focus {
            outline: none;
          }
        }
        .icon-search {
          padding-left: 10px;
        }
        button {
          background: #ff4d67;
          border-radius: 8px;
          padding-right: 15px;
          padding-left: 15px;
          span {
            font-size: 14px;
          }
        }
      }
      .submit {
        background: #ff4d67;
        border-radius: 4px;
        padding-right: 15px;
        padding-left: 15px;
        span {
          font-size: 14px;
        }
      }
    }
    .list-select-connect {
      margin-top: 20px;
    }
  }
  .connected-item {
    .dot-item {
      background: #ff4d67;
      width: 16px;
      height: 16px;
      border-radius: 50%;
      display: inline-block;
      margin-right: 10px;
    }
    .ant-col {
      display: flex;
      align-items: center;
    }
  }
  .not-found {
    font-size: 13px;
    color: gray;
    text-align: center;
    width: 100%;
  }
`;

export default ConnectAccountDetailPage;
