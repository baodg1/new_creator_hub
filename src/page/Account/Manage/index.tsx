/* eslint-disable camelcase */
import React, { useEffect, useState } from "react";
import { Table, Pagination, Button, Select } from "antd";
import moment from "moment";
import ManageAccountPage from "./style";
import { APIGetSocialList } from "@/services/manageAccount";
import { DoubleLeftOutlined, DoubleRightOutlined } from "@ant-design/icons";
import { ColumnTableType, PaginateType } from "./../interface/manage";
import columns from "./columns";
import {
	FORMAT_DATETIME,
	TYPE_SOCIAL,
	STATUS_SOCIAL,
	STATUS_SOCIAL_UPPER
} from "./../constant/manage";
// icon social
import fbIcon from "@/asset/list-social/fb.svg";
import instagramIcon from "@/asset/list-social/instagram.svg";
import twitterIcon from "@/asset/list-social/twitter.svg";
import youtubeIcon from "@/asset/list-social/youtube.svg";
import tiktokIcon from "@/asset/list-social/tiktok.svg";
import { Lang } from "@/utils/config";

const getIconTypeSocial = (type: string) => 
{
	switch (type) 
	{
	case TYPE_SOCIAL.FACEBOOK:
		return fbIcon;
	case TYPE_SOCIAL.INSTAGRAM:
		return instagramIcon;
	case TYPE_SOCIAL.YOUTUBE:
		return youtubeIcon;
	case TYPE_SOCIAL.TIKTOK:
		return tiktokIcon;
	case TYPE_SOCIAL.TWITTER:
		return twitterIcon;
	default:
		break;
	}
};

const ManageAccount: React.FC = () => 
{
	const [ socialList, setSocialList ] = useState<ColumnTableType[] | []>([]);
	const [ paginate, setPaginate ] = useState<PaginateType>({
		limit       : 10,
		offset      : 0,
		total       : 1,
		currentPage : 1
	});
	const [ isLoading, setIsLoading ] = useState(false);

	const getSocialList = (page = 0, passLimit = 0) => 
	{
		const { limit, offset, currentPage } = paginate;

		setIsLoading(true);
		const params = {
			limit : passLimit || limit,
			offset,
			page  : page || currentPage
		};

		APIGetSocialList(params).then((res: any) => 
		{
			setIsLoading(false);
			if (!res?.success || !res?.data) return Promise.resolve("Fails");
			const newList: ColumnTableType[] = [];

			res?.data?.map((item: any) => 
			{
				newList.push({
					key      : item?.id,
					platform : {
						name : item?.social_account?.platform?.name,
						url  : getIconTypeSocial(item?.social_account?.platform?.name)
					},
					account_name : [
						item?.social_account?.account_name,
						item?.social_account.link,
						getSocialList
					],
					admin_name : [
						item?.social_account?.account_admin_name,
						item?.social_account?.link_admin
					],
					create_date :
            item?.created_at &&
            moment(item?.created_at).format(FORMAT_DATETIME),
					update_date : moment(item?.updated_at).format(FORMAT_DATETIME),
					status :
            item?.status === STATUS_SOCIAL_UPPER.ACTIVE ? STATUS_SOCIAL.ACTIVE : STATUS_SOCIAL.INACTIVE
				});

				return item;
			});
			setPaginate({
				currentPage : page || paginate.currentPage,
				limit       : res?.limit,
				offset      : res?.offset,
				total       : res?.total
			});
			setSocialList(newList);
		});
	};

	useEffect(() => 
	{
		getSocialList();
	}, []);

	const handlePagination = (value: number) => 
	{
		getSocialList(value);
		setPaginate({
			...paginate,
			currentPage : value
		});
	};

	const handleChangePerPage = (value: string | number) => 
	{
		getSocialList(0, Number(value));
		setPaginate({ ...paginate, limit: Number(value) });
	};

	return (
		<div className='accountWrap'>
			<ManageAccountPage>
				<h1 className='title-page'>{Lang.trans("list_manage")}</h1>
				<Table
					columns={columns}
					dataSource={socialList}
					loading={isLoading}
					pagination={false}
				/>
				<div className='paginate'>
					<div className='l-paginate'>
						<Button
							className='first-page'
							onClick={() => handlePagination(1)}
							disabled={paginate?.currentPage === 1}
						>
							<DoubleLeftOutlined style={{ fontSize: "12px" }} />
						</Button>
						<Pagination
							pageSize={paginate.limit}
							current={paginate.currentPage}
							total={paginate.total}
							onChange={handlePagination}
							style={{ bottom: "0px" }}
						/>
						<Button
							className='last-page'
							onClick={() =>
								handlePagination(Math.ceil(paginate?.total / paginate.limit))
							}
							disabled={paginate?.currentPage === paginate?.total}
						>
							<DoubleRightOutlined style={{ fontSize: "12px" }} />
						</Button>
					</div>
					<div className='paginate-per-page'>
						<span>
							{Lang.trans("show")} 1 {Lang.trans("to")} {paginate.limit}{" "}
							{Lang.trans("of")} {paginate.total} {Lang.trans("items")}
						</span>
						<Select
							value={paginate.limit}
							style={{ width: 70 }}
							options={[
								{
									value : "10",
									label : "10"
								},
								{
									value : "50",
									label : "50"
								}
							]}
							onChange={handleChangePerPage}
						/>
						<span>{Lang.trans("items_per_page")}</span>
					</div>
				</div>
			</ManageAccountPage>
		</div>
	);
};

export default ManageAccount;
