import { Button, Space, Popconfirm } from "antd";
import type { ColumnsType } from "antd/es/table";
import { DeleteOutlined, StopOutlined } from "@ant-design/icons";
import { ColumnTableType } from "./../interface/manage";
import { APIChangeStatus, APIDeleteConnect } from "@/services/manageAccount";
import { STATUS_SOCIAL_UPPER, STATUS_SOCIAL } from "./../constant/manage";
import { success, error } from "@/component/Message";
import { Lang } from "@/utils/config";

const confirmChangeStatus = (index: any, callback: any) => 
{
	APIChangeStatus({
		id : index?.key,
		status :
      index?.status === STATUS_SOCIAL.ACTIVE ? STATUS_SOCIAL_UPPER.INACTIVE : STATUS_SOCIAL_UPPER.ACTIVE
	}).then((res) => 
	{
		if (res?.success) 
		{
			success(Lang.trans("change_status_success"));
			callback();
		}
		else 
		{
			error(Lang.trans("change_status_failed"));
		}
	});
};

const deleteStatus = (index: any, callback: any) => 
{
	APIDeleteConnect(index?.key).then((res) => 
	{
		if (res?.success) 
		{
			success(Lang.trans("delete_success"));
			callback();
		}
		else 
		{
			error(Lang.trans("delete_failed"));
		}
	});
};

const columns: ColumnsType<ColumnTableType> = [
	{
		title     : "Platform",
		dataIndex : "platform",
		key       : "platform",
		render    : (record) => (
			<div className='platform'>
				<img src={record?.url} alt={record?.alt?.toLowerCase()} />
				<div>{record?.name}</div>
			</div>
		)
	},
	{
		title     : "Account Name",
		dataIndex : "account_name",
		key       : "account_name",
		className : "account_name",
		render    : (record, index) => 
		{
			const textStatus = index.status === "Active" ? "Inactive" : "Active";
      
			return (
				<div>
					<p>{record[0]}</p>
					<p className='text14'>{record[1]}</p>

					<Space
						className='active-row-social'
						direction='vertical'
						style={{ width: "156px" }}
					>
						<Popconfirm
							placement='topLeft'
							title={`Are you sure you want to become ${textStatus}`}
							onConfirm={() => confirmChangeStatus(index, record[2])}
							okText='Yes'
							cancelText='No'
							className='confirm-active-row-social'
						>
							<Button block>
								<StopOutlined /> {textStatus}
							</Button>
						</Popconfirm>
						<Popconfirm
							placement='topLeft'
							title={Lang.trans("confirm_delete")}
							onConfirm={() => deleteStatus(index, record[2])}
							okText='Yes'
							cancelText='No'
							className='confirm-active-row-social'
						>
							<Button block>
								<DeleteOutlined /> {Lang.trans("delete")}
							</Button>
						</Popconfirm>
					</Space>
				</div>
			);
		}
	},
	{
		title     : "Admin Name",
		dataIndex : "admin_name",
		key       : "admin_name",
		className : "admin_name",
		render    : (record) => (
			<div>
				<p>{record[0]}</p>
				<p className='text14'>{record[1]}</p>
			</div>
		)
	},
	{
		title     : "Create Date",
		dataIndex : "create_date",
		key       : "create_date"
	},
	{
		title     : "Update Date",
		dataIndex : "update_date",
		key       : "update_date"
	},
	{
		title     : "Status",
		dataIndex : "status",
		key       : "status",
		render    : (text) => (
			<div
				className={
					`status text14 ${ text === "Active" ? ` active` : ` inactive`}`
				}
			>
				{text}
			</div>
		)
	}
];

export default columns;
