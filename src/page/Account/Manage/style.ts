import styled from "styled-components";

const ManageAccountPage = styled.div`
  padding: 15px;
  margin: 15px;
  background: #ffffff;
  border-radius: 8px;
  .title-page {
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    color: #313335;
    margin: 15px 0;
    padding-left: 15px;
    padding-right: 15px;
  }
  table {
    border-bottom: 1px solid #e7e7e7;
    thead {
      th {
        text-transform: uppercase;
        font-size: 16px;
        background: none;
      }
    }
    tbody {
      tr {
        &:hover {
          .active-row-social {
            display: block;
          }
        }
        td {
          font-size: 16px;
          border-bottom: none;
          .active-row-social {
            position: absolute;
            top: 30px;
            display: none;
            box-shadow: 0px 10px 15px rgba(31, 41, 55, 0.1),
              0px 0px 6px rgba(31, 41, 55, 0.05);
            border-radius: 8px;
            z-index: 11;
          }
        }
      }
    }
    .account_name {
      max-width: 300px;
    }
    .admin_name {
      max-width: 400px;
    }
    .platform {
      display: flex;
      align-items: center;
      img {
        margin-right: 10px;
      }
    }
    .status {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding: 4px 20px;
      gap: 10px;
      border-radius: 24px;
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      text-align: center;
    }
    .active {
      background: rgba(116, 192, 89, 0.1);
      color: #2fb56d;
    }
    .inactive {
      background: rgb(220 36 36 / 82%);
      color: white;
    }
    .text14 {
      font-size: 14px;
    }
  }
  .paginate {
    margin-top: 15px;
    display: flex;
    justify-content: space-between;
    .l-paginate {
      display: flex;
      align-items: center;
    }
    .anticon {
      color: #fe2c55;
    }
    li {
      border: none;
      background: #f7f8f9;
      border-radius: 4px;
      &.ant-pagination-item-active {
        background: #fe2c55;
        a {
          color: #ffffff;
        }
      }
      &:hover {
        background: #fe2c55;
        a {
          color: #ffffff;
        }
      }
    }
    .ant-pagination-next,
    .ant-pagination-prev {
      button {
        border: none;
        color: #fe2c55;
        &:hover {
          background: #fe2c55;
          span {
            color: #ffffff;
          }
        }
      }
    }
    .first-page,
    .last-page {
      border: none;
      &:hover {
        background: #fe2c55;
        span {
          color: #ffffff;
        }
      }
    }
    .paginate-per-page {
      span {
        font-size: 14px;
        &:first-child {
          margin-right: 10px;
        }
        &:last-child {
          margin-left: 10px;
        }
      }
      .ant-select-selector {
        padding: 0;
        border-radius: 10px;
        .ant-select-selection-item {
          font-weight: 600;
          color: #181a27;
          font-size: 14px;
        }
      }
    }
  }
`;

export default ManageAccountPage;
