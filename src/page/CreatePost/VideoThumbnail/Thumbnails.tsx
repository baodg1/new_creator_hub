import { Spin } from "antd";
import useVideoThumbnailForm from "./../hooks/useVideoThumbnailForm";
import ThumbnailBox from "./style";

type ThumbnailsProps = {
  setSelectedThumbnail: (thumbnail: string) => void;
  selectedThumbnail?: string;
  thumbnails?: string[];
  isError?: string;
};

const Thumbnails = (props: ThumbnailsProps) => 
{
	const {
		thumbnails = [],
		selectedThumbnail,
		setSelectedThumbnail,
		isError
	} = props;

	const { numberOfThumbnails } = useVideoThumbnailForm({
		maxThumbnails : 20,
		type          : "url"
	});

	if (isError) 
	{
		return (
			<pre style={{ maxWidth: 800, margin: "auto", overflow: "auto" }}>
				{JSON.stringify(isError, undefined, 2)}
			</pre>
		);
	}

	return (
		<ThumbnailBox>
			<h3 className='title'>Video Thumbnail</h3>
			<div className='thumbnails-container scroll-custom'>
				<div
					className='wrap-thumbnail-video'
					style={{ width: `${(100 * (numberOfThumbnails + 1)) + 55 }px` }}
				>
					<Spin tip='Loading...' spinning={!thumbnails[0] ? true : false}>
						{thumbnails?.map((image, index) => 
						{
							if (image === "") return null;

							return (
								<img
									src={image}
									alt='thumbnails'
									className={`width-100 ${
										image === selectedThumbnail ? "active" : ""
									}`}
									style={{ maxWidth: 100 }}
									onClick={() => setSelectedThumbnail(image)}
									key={index}
								/>
							);
						})}
					</Spin>
				</div>
			</div>
		</ThumbnailBox>
	);
};

export default Thumbnails;
