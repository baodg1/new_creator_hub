import styled from "styled-components";

const ThumbnailBox = styled.div`
  margin-top: 10px;
  .title {
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
  }
  .thumbnails-container {
    overflow-y: auto;
    .wrap-thumbnail-video {
      img {
        margin-right: 5px;
      }
    }
  }
`;

export default ThumbnailBox;
