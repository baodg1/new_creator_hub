import styled from "styled-components";

const SelectAccountBox = styled.div`
  ul {
    list-style-type: none;
    padding: 0;
    margin-top: 15px;
    height: 265px;
    overflow-y: auto;
    li {
      margin-bottom: 20px;
      label {
        margin-right: 10px;
      }
      .ant-avatar {
        margin-right: 5px;
      }
    }
  }
  .list-select {
    border-right: 1px solid #e7e7e7;
    padding-right: 20px;
    ul {
      margin-right: -23.5px !important;
    }
  }
  .chosen-accounts {
    ul {
    }
  }
`;

export default SelectAccountBox;
