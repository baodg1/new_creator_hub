import { Modal, Row, Col, Input, Checkbox, Avatar } from "antd";
import SelectAccountBox from "./style";

interface SelectAccoutProps {
  showSelectAccount: boolean;
  setShowSelectAccount: Function;
  accountList: any[];
  setAccountList: Function;
}

const SelectAccountModal = ({
	showSelectAccount,
	setShowSelectAccount,
	accountList,
	setAccountList
}: SelectAccoutProps) => 
{
	const onChangeCheckbox = (value: boolean, id: number) => 
	{
		const newAccounts = accountList.map((item) => 
		{
			if (item?.id === id) item.selected = value;

			return item;
		});

		setAccountList(newAccounts);
	};

	const renderListAccountChosen = () => 
	{
		const dataRender: any = [];

		accountList.map((item, key) => 
		{
			if (item?.selected) 
			{
				dataRender.push(
					<li key={key}>
						<Checkbox
							onChange={(e) => onChangeCheckbox(e?.target?.checked, item?.id)}
							checked={item?.selected}
						/>
						<Avatar src={item?.social_account?.avatar} />
						<span>{item?.social_account?.account_name}</span>
					</li>
				);
			}

			return item;
		});
		if (!dataRender.length) return <div>No accounts selected yet</div>;

		return dataRender;
	};

	const renderListAccount = () => 
	{
		const dataRender: any = [];

		accountList.map((item, key) => 
		{
			if (!item?.selected) 
			{
				dataRender.push(
					<li key={key}>
						<Checkbox
							onChange={(e) => onChangeCheckbox(e?.target?.checked, item?.id)}
							checked={item?.selected}
						/>
						<Avatar src={item?.social_account?.avatar} />
						<span>{item?.social_account?.account_name}</span>
					</li>
				);
			}

			return item;
		});
		if (!dataRender.length) return <div>No accounts found</div>;

		return dataRender;
	};

	return (
		<Modal
			title='Social Media Accounts'
			open={showSelectAccount}
			onOk={() => setShowSelectAccount(false)}
			onCancel={() => setShowSelectAccount(false)}
			centered
			width={1000}
			footer={null}
		>
			<SelectAccountBox>
				<Row gutter={[ 20, 16 ]}>
					<Col md={12}>
						<div className='list-select'>
							<Input placeholder='Basic usage' />
							<ul className='scroll-custom'>{renderListAccount()}</ul>
						</div>
					</Col>
					<Col md={12}>
						<div className='chosen-accounts'>
							<h3>Chosen {renderListAccountChosen().length} accounts</h3>
							<ul className='scroll-custom'>{renderListAccountChosen()}</ul>
						</div>
					</Col>
				</Row>
			</SelectAccountBox>
		</Modal>
	);
};

export default SelectAccountModal;
