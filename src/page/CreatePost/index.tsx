import { useState, useEffect } from "react";
import { Row, Col, Button, Switch, Avatar, Form, DatePicker } from "antd";
import { CreatePostPage, TabsWrapper } from "./style";
import UploadFile from "./UploadFile";
import SelectAccountModal from "./SelectAccountModal";
import { getIcon, bodyCreatePost } from "@/utils/socialUtils";
import addIcon from "@/asset/create-post/icon-add.svg";
import PreviewPost from "./PreviewPost";
import { APIGetSocialList } from "@/services/manageAccount";
import { ColumnTableType, TabType } from "./interface/manage";
import { 
	FileUploadI, 
	PostContent,
	PostScheduleI
} from "./interface/post";
import { TYPE_SOCIAL_INDEX } from "./constant/connect";
import ContentPost from "./Content";
import { CButton } from "@/component/Common";
import postSocialApi from "@/services/postSocialApi";
import { success } from "@/component/Message";
import iconEditDate from "@/asset/create-post/icon-edit-date.svg";
import moment from "moment";

const CreatePost = () => 
{
	const [ content, setContent ] = useState<PostContent>({});
	const [ showSelectAccount, setShowSelectAccount ] = useState<boolean>(false);
	const [ listImage, setListImage ] = useState<FileUploadI[]>([]);
	const [ videoUrl, setVideoUrl ] = useState("");
	const [ socialList, setSocialList ] = useState<ColumnTableType[] | []>([]);
	const [ isLoading, setIsLoading ] = useState(false);
	const [ currentTab, setCurrentTab ] = useState("0");
	const [ postNow, setPostNow ] = useState<boolean>(false);
	const [ postSchedule, setpostSchedule ] = useState<boolean>(false);
	const [ postDraft, setPostDraft ] = useState<boolean>(false);
	const [ errorPublish, setErrorPublish ] = useState<String>("");
	const [ schedule, setSchedule ] = useState<PostScheduleI []>([]);
	const items = [
		{
			label : "Draft",
			key   : "0",
			icon  : null,
			children : (
				<ContentPost
					content={content?.draf}
					setContent={(data: any) => 
					{
						setContent((prev) => ({
							...prev,
							...{ draft: data }
						}));
					}}
				/>
			)
		}
	];
	const [ listTab, setListTab ] = useState<TabType[]>(items);

	// Handle get list social account
	const getSocialList = () => 
	{
		setIsLoading(true);
		// Call API get social account list
		APIGetSocialList({ limit: 50, offset: 0, page: 0 }).then((res: any) => 
		{
			setIsLoading(false);
			if (!res?.success || !res?.data) return Promise.resolve("Fails");
			setSocialList(res?.data);
		});
	};

	useEffect(() => 
	{
		getSocialList();
	}, []);

	// Render List Avatar has chosen
	const renderAvatarAccountChosen = () => 
	{
		const dataRender: any = [];

		socialList.map((item, key) => 
		{
			if (item?.selected) 
			{
				const avatar = item?.social_account.avatar;

				dataRender.push(<Avatar key={key} src={avatar} />);
			}

			return item;
		});

		return dataRender;
	};

	const avatarAccountChosen = renderAvatarAccountChosen();

	const handleChangeTab = (key: string) => 
	{
		setCurrentTab(key);
	};
	
	const renderNewTab = () => 
	{
		const newListTab: TabType[] = [ listTab[0] ];

		// Handle check social account seletecd
		socialList.map((item, key) => 
		{
			if (item?.selected) 
			{
				// Add tab content with each social
				const currentListTab = newListTab.find(
					(e) => e.key == item!.social_account.platform.id?.toString()
				);

				if (!currentListTab) 
				{
					const newContent = (data: string) => 
					{
						switch (item!.social_account.platform.id) 
						{
						case TYPE_SOCIAL_INDEX.FACEBOOK:
							return { facebook: data };
						case TYPE_SOCIAL_INDEX.TIKTOK:
							return { tiktok: data };
						case TYPE_SOCIAL_INDEX.INSTAGRAM:
							return { instagram: data };
						case TYPE_SOCIAL_INDEX.YOUTUBE:
							return { youtube: data };
						case TYPE_SOCIAL_INDEX.TWITTER:
							return { twitter: data };
						default:
							return { draf: data };
						}
					};

					newListTab.push({
						label    : item.social_account.platform.name,
						key      : item!.social_account.platform.id?.toString(),
						icon     : getIcon(item!.social_account.platform.id),
						platform : item.social_account.platform,
						// preview  : getPreview(item!.social_account.platform.id),
						children : (
							<ContentPost
								setContent={(data: any) => 
								{
									const dataContent = newContent(data);

									setContent((prev) => ({
										...prev,
										...dataContent
									}));
								}}
							/>
						)
					});
				}
			}

			return item;
		});
		setListTab(newListTab);
	};

	// handle get list social account depend on platform id
	const accountSelectedPlatform = (id: number): ColumnTableType[] => 
	{
		return socialList.filter(
			(e) => e.selected && e.social_account.platform.id == id
		);
	};

	// get content platform
	const getContent = (id: number) => 
	{
		switch (id) 
		{
		case TYPE_SOCIAL_INDEX.FACEBOOK:
			return content!.facebook;
		case TYPE_SOCIAL_INDEX.INSTAGRAM:
			return content!.instagram;
		case TYPE_SOCIAL_INDEX.TIKTOK:
			return content!.tiktok;
		case TYPE_SOCIAL_INDEX.TWITTER:
			return content!.twitter;
		case TYPE_SOCIAL_INDEX.YOUTUBE:
			return content!.youtube;
		default:
			return content!.draf;
		}
	};

	const resetPage = () => 
	{
		setListTab(items);
		setContent({});
		setListImage([]);
		setPostDraft(false);
		setpostSchedule(false);
		setPostNow(false);
		getSocialList();
		setSchedule([]);
	};

	const getTime = () => 
	{
		if (postNow) 
		{
			return [
				{
					"publish_at" : new Date()
				}
			];
		}
		else if (postDraft) 
		{
			return [];
		}
		
		return schedule;
	};
	// Handle create post
	const handleUpload = () => 
	{
		setErrorPublish("");
		if (!postNow && !postSchedule && !postDraft) 
		{
			setErrorPublish(
				"In this section, you need to select at least 1 option to continue the process."
			);
			
			return;
		}
		const platForms = listTab.filter((e) => e.platform).map((e) => e.platform);
		const postPlatFormList = platForms.map((e) => 
		{
			const postContent = getContent(e!.id);
			
			return {
				"platform_id"         : e!.id,
				"brief_content"       : "",
				"content"             : postContent,
				"title"               : "",
				"social_account_list" : accountSelectedPlatform(e!.id).map(
					(socialAccount) => socialAccount?.id
				)
			};
		});

		setIsLoading(true);
		const body = bodyCreatePost(listImage, postPlatFormList, getTime());

		postSocialApi.create(body).then((res) => 
		{
			if (res.success) 
			{
				success("Create post success");
				setIsLoading(false);
				resetPage();
			}
		});
	};
	
	return (
		<CreatePostPage className='accountWrap'>
			<Form
				name='basic'
				labelCol={{ span: 8 }}
				wrapperCol={{ span: 16 }}
				initialValues={{ remember: true }}
				onFinish={handleUpload}
				autoComplete='off'
			>
				<Row>
					<Col span={24}>
						<div className='tab-type-post'>
							<span className='active'>Create Post</span>
							<span className='border-div' />
							<span>Drafts</span>
						</div>
					</Col>
				</Row>
				<Row gutter={[ 16, 16 ]}>
					<Col xl={12}>
						<div className='choose-account'>
							<Row>
								<Col span={24} className='head'>
									<span>Social Media</span>
									<Button onClick={() => setShowSelectAccount(true)}>
										Choose account
									</Button>
								</Col>
							</Row>
							{avatarAccountChosen.length ? (
								<Row>
									<Col span={24}>
										<div className='list-account-choose'>
											<Avatar.Group
												maxCount={2}
												maxStyle={{
													color           : "#f56a00",
													backgroundColor : "#fde3cf"
												}}
												size={44}
											>
												{avatarAccountChosen}
											</Avatar.Group>
											<span>{avatarAccountChosen.length} Accounts chosen</span>
										</div>
									</Col>
								</Row>
							) : null}
						</div>
						<div className='post-content'>
							<Row className='head'>
								<Col span={24}>
									<div className='title'>Post content</div>
								</Col>
							</Row>
							<TabsWrapper
								className='tab-wrapper'
								defaultActiveKey={currentTab}
								items={listTab.map((e, index) => 
								{
									const label = e.icon ? (
										e.key === currentTab ? (
											<span className='tab-info'>
												<img
													src={e.icon}
													alt={e.label}
													width={24}
													height={24}
												/>
												{e.label}
											</span>
										) : (
											<span className='tab-info'>
												<img
													src={e.icon}
													alt={e.label}
													width={24}
													height={24}
												/>
											</span>
										)
									) : (
										<span className='tab-info'>{e.label}</span>
									);

									return {
										label    : label,
										key      : e.key,
										children : e.children
									};
								})}
								onChange={handleChangeTab}
							/>
						</div>
						<div className='upload-video-image'>
							<Row>
								<Col span={24}>
									<div className='title-section'>Upload image/video</div>
								</Col>
							</Row>
							<Row>
								<Col span={24}>
									<UploadFile
										setVideoUrl={setVideoUrl}
										videoUrl={videoUrl}
										listImage={listImage}
										setListImage={(data: FileUploadI[]) => 
										{
											setListImage(data);
										}}
									/>
								</Col>
							</Row>
						</div>
						<div className='when-to-post'>
							<Row>
								<Col span={24}>
									<div className='title-section'>When to post</div>
									{errorPublish != "" ? (
										<div className='error'>{errorPublish}</div>
									) : (
										<span />
									)}
								</Col>
							</Row>
							<Row>
								<Col span={24}>
									<ul>
										<li>
											<div className='title'>
												<h3>Post now</h3>
												<Switch
													checked={postNow}
													onChange={(value) => 
													{
														setPostDraft(false);
														setpostSchedule(false);
														setPostNow(value);
													}}
												/>
											</div>
											<p>
												After you click create post, your posting will be
												published immediately.
											</p>
										</li>
										<li>
											<div className='title'>
												<h3>Set a schedule</h3>
												<Switch
													checked={postSchedule}
													onChange={(value) => 
													{
														setPostDraft(false);
														setpostSchedule(value);
														setPostNow(false);
														setSchedule([ {
															"publish_at" : new Date()
														} ]);
													}}
												/>
											</div>
											<p>
												After you click create post, your posting will be
												published following the schedule.
											</p>
											{/* <CButton
												className='btn btn-add-time'
												label='Add posting time'
												type={2}
												icon={<img src={iconAddTime} />}
												onClick={() => {}}
											/> */}
											{ postSchedule ? 
												<DatePicker showTime 
													onOk={(date) => 
													{
														setSchedule([ {
															"publish_at" : date.toDate()
														} ]);
													}}
													defaultValue={moment()}
													className='date-time mb-10'
													suffixIcon={<img src={iconEditDate}/>}
													format='DD-MM-YYYY hh:mm A'
													placeholder='Add posting time'
													allowClear={false}
												/> : <span/>
											}

										</li>
										<li>
											<div className='title'>
												<h3>Save as draft</h3>
												<Switch
													checked={postDraft}
													onChange={(value) => 
													{
														setPostDraft(value);
														setpostSchedule(false);
														setPostNow(false);
													}}
												/>
											</div>
											<p>
												After you click create post, your posting will be saved
												in drafts.
											</p>
										</li>
									</ul>
								</Col>
							</Row>
						</div>
					</Col>
					<PreviewPost 
						listTab={listTab.filter((e) => e.icon)} 
						previewSocial={{ listImage: listImage, content: content }}
					/>
				</Row>
				<Row gutter={[ 16, 16 ]}>
					<Col xl={24} span={24}>
						<div className='bottom-create'>
							<CButton
								className='btn btn-cancel'
								label='Cancel'
								type={2}
								onClick={() => {}}
							/>
							<CButton
								className='btn btn-create'
								label='Create Post'
								type={2}
								loading={isLoading}
								icon={<img src={addIcon} />}
								onClick={handleUpload}
							/>
						</div>
					</Col>
				</Row>
			</Form>
			<SelectAccountModal
				showSelectAccount={showSelectAccount}
				setShowSelectAccount={setShowSelectAccount}
				accountList={socialList}
				setAccountList={(data: any) => 
				{
					setSocialList(data);
					renderNewTab();
				}}
			/>
		</CreatePostPage>
	);
};

export default CreatePost;
