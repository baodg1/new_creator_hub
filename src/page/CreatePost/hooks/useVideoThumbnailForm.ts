import React, { useEffect, useState } from "react";

import {
	generateVideoThumbnails,
	importFileandPreview
} from "./../handleThumbnail";

type useVideoThumbnailFormProps = {
  maxThumbnails: number;
  type: "url" | "file";
};

const useVideoThumbnailForm = (props: useVideoThumbnailFormProps) => 
{
	const { maxThumbnails, type } = props;

	const [ isLoading, setIsLoading ] = useState(false);
	const [ isError, setIsError ] = useState("");

	const [ inputUrl, setInputUrl ] = useState<string>("");
	const [ inputFile, setInputFile ] = useState<File | null>(null);

	const [ numberOfThumbnails, setNumberOfThumbnails ] = useState(10);
	const [ thumbnails, setThumbnails ] = useState<string[]>();
	const [ loadAssync, setLoadAssync ] = useState(true);
	const [ selectedThumbnail, setSelectedThumbnail ] = useState<string>();

	const updateThumbnails = (url: string, index: number) => 
	{
		setThumbnails((prev) => 
		{
			const newThumbnails = [ ...prev! ];

			newThumbnails[index] = url;
			
			return newThumbnails;
		});
	};

	const handleGenerateThumbnails = async () => 
	{
		const input = type === "url" ? inputUrl : inputFile;

		const callback = loadAssync ? updateThumbnails : undefined;

		setThumbnails(loadAssync ? Array(numberOfThumbnails).fill("") : [ "" ]);

		setIsError("");
		setIsLoading(true);

		generateVideoThumbnails(input as any, numberOfThumbnails, type, callback)
			.then((res: string[]) => 
			{
				setThumbnails(res);
			})
			.catch((err: any) => 
			{
				setIsError(err);
			})
			.finally(() => 
			{
				setIsLoading(false);
			});
	};

	useEffect(() => 
	{
		if (!inputUrl) return;
		handleGenerateThumbnails();
	}, [ inputUrl ]);

	const clearForm = () => 
	{
		// setNumberOfThumbnails(0);
		setSelectedThumbnail("");
		setInputFile(null);
		setInputUrl("");
		setIsError("");
	};

	const handleInputUrlChange = (value: string) => 
	{
		clearForm();
		setInputUrl(value);
	};

	const handleInputFileChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => 
	{
		const selectedFile = event.target?.files?.[0];

		if (selectedFile?.type.includes("video")) 
		{
			clearForm();

			importFileandPreview(selectedFile).then((url: string) => 
			{
				setInputFile(selectedFile);
				setInputUrl(url);
			});
		}
	};

	const handleNumberOfThumbnails = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		const newValue = parseInt(e.target.value);

		setNumberOfThumbnails(newValue > maxThumbnails ? maxThumbnails : newValue);
	};

	const handleLoadAssync = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		setLoadAssync(e.target.checked);
	};

	return {
		handleGenerateThumbnails,
		handleLoadAssync,
		handleNumberOfThumbnails,
		inputUrl,
		isError,
		isLoading,
		loadAssync,
		numberOfThumbnails,
		selectedThumbnail,
		setInputUrl,
		setIsError,
		setSelectedThumbnail,
		thumbnails,
		handleInputUrlChange,
		handleInputFileChange
	};
};

export default useVideoThumbnailForm;
