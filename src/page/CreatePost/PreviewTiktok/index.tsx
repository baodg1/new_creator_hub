import { useState } from "react";
import tiktokIcon from "@/asset/create-post/tiktok.svg";
import iconDesktop from "@/asset/create-post/desktop.svg"; 
import iconDesktopActive from "@/asset/create-post/desktop_active.svg";
import iconMobile from "@/asset/create-post/mobile.svg";
import iconMobileActive from "@/asset/create-post/mobile-active.svg";
import iconMusic from "@/asset/create-post/icon-music.svg";
import iconLoveTiktok from "@/asset/create-post/icon-love-tiktok.svg";
import iconAudio from "@/asset/create-post/icon-audio.svg";
import iconCommentTiktok from "@/asset/create-post/icon-comment-tiktok.svg";
import iconShare from "@/asset/create-post/icon-share.svg";
import { PreviewSocialI } from "./../interface/manage";

import {
	PreivewWrapper,
	TitleWrapper,
	PreviewContentWrapper,
	PreviewMobileWrapper
} from "./style";
import { CButton } from "@/component/Common";

const PreviewTiktok = ({ listImage, content }: PreviewSocialI) => 
{
	const [ currentType, setCurrentType ] = useState(0);

	const handleChangeType = (key: number) => 
	{
		setCurrentType(key);
	};

	return (
		<PreivewWrapper>
			<TitleWrapper>
				<CButton
					className={currentType ==0 ? `btn-active`: `btn-default mr-8`}
					label='Desktop'
					icon={<img src={currentType==0 ? iconDesktopActive: iconDesktop} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(0)}
				/>
				<CButton
					className={currentType ==1 ? `btn-active`: `btn-default mr-8`}
					label='Mobile'
					icon={<img src={currentType==1 ? iconMobileActive: iconMobile} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(1)}
				/>
			</TitleWrapper>
			<PreviewContentWrapper>
				<PreviewMobileWrapper>
					<div className='content'>
						<div className='name-bar'>
							<span>@tiktok account</span>
						</div>
						<div className='description'>
							<span>
								{content?.tiktok ?? ''}
							</span>
						</div>
						<div className='audio-bar'>
							<img src={iconMusic} width={10.71} height={14} />
							<span>Page Name · Original Audio</span>
						</div>
					</div>
					<div className='action-icons'>
						<div className='avatar'>
							<img src={tiktokIcon} />
						</div>
						<div className='icon-like'>
							<img src={iconLoveTiktok} />
							<br />
							<span>234</span>
						</div>
						<div className='icon-comment'>
							<img src={iconCommentTiktok} />
							<br />
							<span>45</span>
						</div>
						<div className='icon-share'>
							<img src={iconShare} />
							<br />
							<span>Share</span>
						</div>
						<img src={iconAudio} />
					</div>
					{ listImage.length == 0 ? 
						(<div className='image-bg'> Image/Video</div>) : (<img src={listImage[0].url} 	className='image-bg'/>) }
				
				</PreviewMobileWrapper>
			</PreviewContentWrapper>
		</PreivewWrapper>
	);
};

export default PreviewTiktok;
