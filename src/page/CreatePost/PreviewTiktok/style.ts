import styled from "styled-components";

export const PreivewWrapper = styled.div`
  .mr-8 {
    margin-right: 8px;
  }
  .pr-10 {
    padding-right: 10px;
  }
  .mt-10 {
    margin-top: 10px;
  }
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 8px;

  .btn-default {
    height: 44px;
    border: 1px solid #eaeaea;
    border-radius: 4px;

    span {
      img {
        background-color: red;
      }
    }
  }
  .btn-active{
    height: 44px;
    background: #FE2C55;
    border: 1px solid #FE2C55;
    border-radius: 4px;
    span{
      color: white;
    }
  }
`;

export const PreviewContentWrapper = styled.div`
  width: 100%;
`;

export const PreviewMobileWrapper = styled.div`
  margin-top: 24px;
  width: 335px;
  height: 595px;
  background: #ffffff;
  border-radius: 8px;

  /* Inside auto layout */

  flex: none;
  order: 2;
  flex-grow: 0;
  margin: 24px auto;

  .image-bg {
    background: #DDDDDD;
    top: 0%;
    bottom: 0%;
    object-fit: cover;
    width: 100%;
    height: 100%;
    border-radius: 8px;
  }
  .content {
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    padding: 0px;
    padding-left: 16px;
    position: absolute;
    bottom: 8px;
    .name-bar {
      span {
        font-family: "Open Sans";
        font-style: normal;
        font-weight: 700;
        font-size: 12px;
        line-height: 22px;
        /* identical to box height, or 183% */

        /* Elements White */

        color: #ffffff;

        /* Inside auto layout */

        flex: none;
        order: 1;
        flex-grow: 0;
      }
    }
    .description {
      width: 235px;
      margin-top: 8px;
      font-family: "Open Sans";
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 16px;
      /* or 133% */

      /* Elements White */

      color: #ffffff;

      /* Inside auto layout */

      flex: none;
      order: 0;
      flex-grow: 0;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2; /* number of lines to show */
              line-clamp: 2; 
      -webkit-box-orient: vertical;
    }
    .audio-bar {
      margin-top: 16px;
      display: flex;
      flex-direction: row;
      align-items: center;
      gap: 12px;

      width: 335px;
      height: 38px;

      /* Inside auto layout */

      flex: none;
      order: 2;
      align-self: stretch;
      flex-grow: 0;
      img {
        width: 10.71px;
        height: 14px;
        flex: none;
        order: 0;
        flex-grow: 0;
      }
      span {
        font-family: "Open Sans";
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 22px;
        /* identical to box height, or 183% */

        /* Elements White */

        color: #ffffff;

        /* Inside auto layout */

        flex: none;
        order: 1;
        flex-grow: 0;
      }
    }
  }

  .action-icons {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;
    padding: 0px;
    position: absolute;
    top: 69.75%;
    left: 65%;
    bottom: 2.02%;
    .avatar{
      text-align: center;
      border: 1px solid #FFFFFF;
      border-radius: 35px;
      margin-bottom: 28px;
      img{
        width: 35px;
        height: 35px;
        border-radius: 35px;
      }
    }
    .icon-like {
      text-align: center;
      img{
        width: 29.53px;
        height: 27px;
      }
      margin-bottom: 28px;

    }
    .icon-comment {
      text-align: center;
      img {
        width: 30.26px;
        height: 28.73px;
      }
      margin-bottom: 28px;

    } 
    .icon-share {
      text-align: center;
      img {
        width: 25.89px;
        height: 22.87px;
      }
      margin-bottom: 28px;

    }
    span {
      font-family: "Open Sans";
      font-style: normal;
      font-weight: 600;
      font-size: 12px;
      line-height: 16px;
      color: #ffffff;
    }
  }
`;
