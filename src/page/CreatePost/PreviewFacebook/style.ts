import styled from "styled-components";
import phone from "@/asset/create-post/phone.svg";

export const PreivewWrapper = styled.div`
  .mr-8 {
    margin-right: 8px;
  }
  .pr-10 {
    padding-right: 10px;
  }
  .mt-10 {
    margin-top: 10px;
  }
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 8px;

  .btn-default {
    height: 44px;
    border: 1px solid #eaeaea;
    border-radius: 4px;

    span {
      img {
        background-color: red;
      }
    }
  }
  .btn-active{
    height: 44px;
    background: #FE2C55;
    border: 1px solid #FE2C55;
    border-radius: 4px;
    span{
      color: white;
    }
  }
`;

export const PreviewContentWrapper = styled.div`
  width: 100%;
`;

export const PreviewMobileWrapper = styled.div`
  width: 416px;
  height: 844px;
  background-position: center center;           
  background-size: contain;
  background-image: url(${phone});
  background-repeat: no-repeat;
  /* Inside auto layout */
  flex: none;
  order: 2;
  flex-grow: 0;
  margin: 24px auto;
  .header {
    padding: 58px 42px 0px 42px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    .account {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      padding: 0px;
      background: #ffffff;
      flex: none;
      order: 0;
      flex-grow: 0;
      img {
        margin-right: 16px;
      }
      .name {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 22px;
        /* identical to box height, or 157% */

        /* Dashboard/Text - High Emphasis */

        color: #313335;
      }
      .time-ago {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 22px;
        color: #969696;
      }
    }
    .suff {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      gap: 2px;
      width: 19px;
      height: 5px;
    }
  }

  .content {
    width: 100%;
    padding: 0px 42px 0px 42px;
    .description {
      font-family: "Roboto";
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 22px;
      padding: 10px 0px;
    }
    .image {
      height: 370px;
      width: 100%;
      flex: none;
      order: 0;
      align-self: stretch;
      flex-grow: 0;
      border-radius: 8px;
      background: #DDDDDD;
      img{
        width: 100%;
        border-radius: 8px;
        object-fit: cover;
        height: 374px;
      }
      div{
        text-align: center;
        height: 100%;
        width: 100%;
        margin: auto;
      }
    }
    .react {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      padding: 0px;
      margin-top: 22px;
      .reaction {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;

        .reaction-icon {
          width: 20px;
          height: 20px;
          border-radius: 118.939px;
          padding: 0px;
        }
        .color1 {
          background: linear-gradient(180deg, #17adfd 0%, #016edf 100%);
        }
        .color2 {
          background: linear-gradient(180deg, #ea6874 0%, #d3373e 100%);
        }
        .color3 {
          background: linear-gradient(180deg, #f6dd79 0%, #eba94f 100%);
        }
      }
      .comment-share {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
      }
    }
  }
  .separator {
    height: 2.16px;
    background: #e5e5e5;
    padding:42pxpx 0px;
    width: calc(100% - 50px);
    margin: 10px auto;
  }
  .bottom {
    padding: 10px 42px 0px 42px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    .icon {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding: 8px 0px 8px 16px;
      gap: 12px;
      height: 40px;
    }
  }
`;

export const PreviewDesktopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 20px;
  gap: 24px;
  border: 1px solid #e7e7e7;
  border-radius: 8px;
  flex: none;
  order: 2;
  align-self: stretch;
  flex-grow: 0;
  margin-top: 24px;

  .header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    .account {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      padding: 0px;
      background: #ffffff;
      flex: none;
      order: 0;
      flex-grow: 0;
      img {
        margin-right: 16px;
      }
      .name {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 22px;
        /* identical to box height, or 157% */

        /* Dashboard/Text - High Emphasis */

        color: #313335;
      }
      .time-ago {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 22px;
        color: #969696;
      }
    }
    .suff {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      gap: 2px;
      width: 19px;
      height: 5px;
    }
  }

  .content {
    width: 100%;
    .description {
      font-family: "Roboto";
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 22px;
    }
    .image {
      height: 374px;
      width: 100%;
      flex: none;
      order: 0;
      align-self: stretch;
      flex-grow: 0;
      border-radius: 8px;
      background: #DDDDDD;
      img{
        width: 100%;
        border-radius: 8px;
        object-fit: cover;
        height: 374px;
      }
      div{
        text-align: center;
        height: 100%;
        width: 100%;
        margin: auto;
      }
    }
    .react {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      padding: 0px;
      margin-top: 22px;
      .reaction {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;

        .reaction-icon {
          width: 20px;
          height: 20px;
          border-radius: 118.939px;
          padding: 0px;
        }
        .color1 {
          background: linear-gradient(180deg, #17adfd 0%, #016edf 100%);
        }
        .color2 {
          background: linear-gradient(180deg, #ea6874 0%, #d3373e 100%);
        }
        .color3 {
          background: linear-gradient(180deg, #f6dd79 0%, #eba94f 100%);
        }
      }
      .comment-share {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
      }
    }
  }
  .separator {
    height: 2.16px;
    width: 100%;
    background: #e5e5e5;
  }
  .bottom {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 0px;
    width: 100%;
    .icon {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding: 8px 0px 8px 16px;
      gap: 12px;
      height: 40px;
    }
  }
`;
