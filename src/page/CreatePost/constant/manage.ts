export const FORMAT_DATETIME = "MMMM DD, YYYY HH:MM";

export const TYPE_SOCIAL = {
	FACEBOOK  : "Facebook",
	INSTAGRAM : "Instagram",
	YOUTUBE   : "Youtube",
	TIKTOK    : "Tiktok",
	TWITTER   : "Twitter"
};

export const STATUS_SOCIAL = {
	ACTIVE   : "Active",
	INACTIVE : "Inactive"
};

export const STATUS_SOCIAL_UPPER = {
	ACTIVE   : "ACTIVE",
	INACTIVE : "DEACTIVE"
};
