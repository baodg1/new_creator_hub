import { Col, Row } from "antd";
import TextArea from "antd/lib/input/TextArea";
import React from "react";

interface ContentPostProps {
	content?: string;
  setContent: Function;
	title?: string, 

}

const ContentPost = ({
	content,
	setContent
}:ContentPostProps) => 
{ 
	return (<Row className='textarea-content'>
		<Col span={24}>
			<TextArea rows={4} onChange={(e) => setContent(e.target.value)} />
		</Col>
	</Row>);
};

export default ContentPost;
