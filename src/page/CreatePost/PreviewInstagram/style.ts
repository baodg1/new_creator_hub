import styled from "styled-components";
import phone from "@/asset/create-post/phone.svg";

export const PreivewWrapper = styled.div`
  .mr-8 {
    margin-right: 8px;
  }
  .pr-10 {
    padding-right: 10px;
  }
  .mt-10 {
    margin-top: 10px;
  }
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 8px;

  .btn-default {
    height: 44px;
    border: 1px solid #eaeaea;
    border-radius: 4px;

    span {
      img {
        background-color: red;
      }
    }
  }
  .btn-active{
    height: 44px;
    background: #FE2C55;
    border: 1px solid #FE2C55;
    border-radius: 4px;
    span{
      color: white;
    }
  }
`;

export const PreviewContentWrapper = styled.div`
  width: 100%;
`;

export const PreviewMobileWrapper = styled.div`
  width: 416px;
  height: 844px;
  background-position: center center;           
  background-size: contain;
  background-image: url(${phone});
  background-repeat: no-repeat;
  /* Inside auto layout */

  flex: none;
  order: 2;
  flex-grow: 0;
  margin: 24px auto;
  .header {
    padding: 58px 42px 0px 42px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    
    .account {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      background: #ffffff;
      flex: none;
      order: 0;
      flex-grow: 0;
      img {
        margin-right: 16px;
      }
      .name {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 22px;
        /* identical to box height, or 157% */

        /* Dashboard/Text - High Emphasis */

        color: #313335;
      }
      .time-ago {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 22px;
        color: #969696;
      }
    }
    .suff {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      margin-top: 25px;
      width: 19px;
      height: 5px;
    }
  }

  .content {
    .description {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 16px;
      display: flex;
      align-items: center;
      color: #000000;
    }
    .image {
      width: calc(100% - 44px);
      height: 358px;
      flex: none;
      order: 0;
      align-self: stretch;
      flex-grow: 0;
      background: #DDDDDD;
      margin: 12px auto;
      div{
        text-align: center;
        height: 100%;
        width: 100%;
        margin: auto;
        margin: 12px 0px;
      }
      img {
        width: 100%;
        height: 358px;
        flex: none;
        order: 0;
        align-self: stretch;
        flex-grow: 0;
      }
    }
    .react {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      margin-top: 10px;
      .reaction {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
        .reaction-icon {
          margin-right: 25px;
          img{
            object-fit: fill;
            width: 28.32px;
            height: 28.18px;
          }
        }
      
      }
      .comment-share {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
      }
    }
    .like{
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 16px;
      display: flex;
      align-items: center;
      margin-top: 19px;
    }
    .footer{
      padding: 0px 42px;
      text-align: left;
      .see-more{
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 16px;
        display: flex;
        align-items: center;
        color: #8E8E8E;
      }
      .time{
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        color: #8E8E8E;
      }
    }
  .action-icons {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;
    padding: 0px;
    gap: 16px;

    position: absolute;
    top: 69.75%;
    left: 70%;
    bottom: 2.02%;
    .icon-like {
      text-align: center;
      width: 24px;
      height: 24px;
      margin-bottom: 16px;
    }
    .icon-comment {
      text-align: center;
      img {
        width: 19.2px;
        height: 18px;
      }
    }
    img {
      width: 24px;
      height: 24px;
    }
    span {
      font-family: "Open Sans";
      font-style: normal;
      font-weight: 600;
      font-size: 12px;
      line-height: 16px;
      color: #ffffff;
    }
  }
`;

export const PreviewDesktopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  border: 1px solid #e7e7e7;
  border-radius: 8px;
  flex: none;
  order: 2;
  align-self: stretch;
  flex-grow: 0;
  margin-top: 24px;

  .header {
    padding: 16px 25px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    .account {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      background: #ffffff;
      flex: none;
      order: 0;
      flex-grow: 0;
      img {
        margin-right: 16px;
      }
      .name {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 22px;
        /* identical to box height, or 157% */

        /* Dashboard/Text - High Emphasis */

        color: #313335;
      }
      .time-ago {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 22px;
        color: #969696;
      }
    }
    .suff {
      display: flex;
      flex-direction: row;
      align-items: center;
      padding: 0px;
      margin-top: 25px;
      width: 19px;
      height: 5px;
    }
  }

  .content {
    width: 100%;
    .description {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 16px;
      display: flex;
      align-items: center;
      color: #000000;
    }
    .image {
      width: 100%;
      height: 374px;
      flex: none;
      order: 0;
      align-self: stretch;
      flex-grow: 0;
      background: #DDDDDD;
      margin: 0 auto;
      img {
        height: 374px;
        object-fit: cover;
        width: 100%;
        flex: none;
        order: 0;
        align-self: stretch;
        flex-grow: 0;
      }
    }
    .react {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      .reaction {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
        .reaction-icon {
          margin-right: 25px;
          img{
            object-fit: fill;
            width: 28.32px;
            height: 28.18px;
          }
        }
      
      }
      .comment-share {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
      }
    }
    .like{
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 16px;
      display: flex;
      align-items: center;
      margin-top: 19px;
    }
    .footer{
      padding: 20px 30px;
      text-align: left;
      .see-more{
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 16px;
        display: flex;
        align-items: center;
        color: #8E8E8E;
      }
      .time{
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        color: #8E8E8E;
      }
    }
  }
`;
