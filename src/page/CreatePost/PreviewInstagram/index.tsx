import { useState } from "react";
import instagramIcon from "@/asset/create-post/instagram.svg";
import iconDesktop from "@/asset/create-post/desktop.svg"; 
import iconDesktopActive from "@/asset/create-post/desktop_active.svg";
import iconMobile from "@/asset/create-post/mobile.svg";
import iconMobileActive from "@/asset/create-post/mobile-active.svg";
import iconMore2 from "@/asset/create-post/icon-more2.svg";
import iconLoveIns from "@/asset/create-post/icon-love-ins.svg";
import iconCommentIns from "@/asset/create-post/icon-comment-ins.svg";
import iconShareIns from "@/asset/create-post/icon-share-ins.svg";
import iconSaveIns from "@/asset/create-post/icon-save-ins.svg";

import {
	PreivewWrapper,
	TitleWrapper,
	PreviewContentWrapper,
	PreviewMobileWrapper,
	PreviewDesktopWrapper
} from "./style";
import { CButton } from "@/component/Common";
import { PreviewSocialI } from "./../interface/manage";

const PreviewInstagram = ({ listImage, content }: PreviewSocialI) => 
{
	const [ currentType, setCurrentType ] = useState(0);

	const handleChangeType = (key: number) => 
	{
		setCurrentType(key);
	};

	return (
		<PreivewWrapper>
			<TitleWrapper>
				<CButton
					className={currentType ==0 ? `btn-active`: `btn-default mr-8`}
					label='Desktop'
					icon={<img src={currentType==0 ? iconDesktopActive: iconDesktop} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(0)}
				/>
				<CButton
					className={currentType ==1 ? `btn-active`: `btn-default mr-8`}
					label='Mobile'
					icon={<img src={currentType==1 ? iconMobileActive: iconMobile} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(1)}
				/>
			</TitleWrapper>
			<PreviewContentWrapper>
				{currentType == 1 ? (
					<PreviewMobileWrapper>
						<div className='header'>
							<div className='account'>
								<img src={instagramIcon} width={40} height={40} />
								<span className='name'>Instagram Account</span> <br />
							</div>
							<div className='suff'>
								<img src={iconMore2} />
							</div>
						</div>
						<div className='content'>
							<div className='content'>
								<div className='image'>
									{ listImage.length == 0 ? (<div> Image/Video</div>) : (<img src={listImage[0].url} />
									)}
								</div>
								<div className='footer'>
									<div className='react'>
										<div className='reaction'>
											<span className='reaction-icon icon-love'>
												<img src={iconLoveIns}/>
											</span>
											<span className='reaction-icon icon-comment' >
												<img src={iconCommentIns}/>
											</span>
											<span className='reaction-icon icon-share'>
												<img src={iconShareIns}/>
											</span>
										</div>
										<div className='comment-share'>
											<div className='comment mr-8'><img src={iconSaveIns}/></div>{" "}
										</div>
									</div>
									<p className='like'>9.999 likes</p>
									<p className='description'>
										{content ? (content.instagram): 'Post Content'}
									</p>
									<p className='see-more'>See 99 comments</p>	
									<p className='time'>9 HOURS AGO</p>
								</div>
							</div>
						</div>
					</PreviewMobileWrapper>
				) : (
					<PreviewDesktopWrapper>
						<div className='header'>
							<div className='account'>
								<img src={instagramIcon} width={40} height={40} />
								<span className='name'>Instagram Account</span> <br />
							</div>
							<div className='suff'>
								<img src={iconMore2} />
							</div>
						</div>
						<div className='content'>
							<div className='image'>
								{ listImage.length == 0 ? (<div> Image/Video</div>) : (<img src={listImage[0].url} />
								)}
							</div>
							<div className='footer'>
								<div className='react'>
									<div className='reaction'>
										<span className='reaction-icon icon-love'>
											<img src={iconLoveIns}/>
										</span>
										<span className='reaction-icon icon-comment' >
											<img src={iconCommentIns}/>
										</span>
										<span className='reaction-icon icon-share'>
											<img src={iconShareIns}/>
										</span>
									</div>
									<div className='comment-share'>
										<div className='comment mr-8'><img src={iconSaveIns}/></div>{" "}
									</div>
								</div>
								<p className='like'>9.999 likes</p>
								<p className='description'>
									{content ? (content.instagram): 'Post Content'}
								</p>
								<p className='see-more'>See 99 comments</p>	
								<p className='time'>9 HOURS AGO</p>
							</div>
						</div>
					</PreviewDesktopWrapper>
				)}
			</PreviewContentWrapper>
		</PreivewWrapper>
	);
};

export default PreviewInstagram;
