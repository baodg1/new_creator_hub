import { Col } from "antd";
import { useState } from "react";
import { TabsWrapper } from "../style";
import { PreviewI } from "../interface/manage";
import PreviewFacebook from "../PreviewFacebook";
import PreviewTiktok from "../PreviewTiktok";
import PreviewYoutube from "../PreviewYoutube";
import PreviewInstagram from "../PreviewInstagram";
import PreviewTwitter from "../PreviewTwitter";
import { TYPE_SOCIAL_INDEX } from "../constant/connect";

const PreviewPost = ({
	listTab,
	previewSocial
}: PreviewI) => 
{
	const [ currentTab, setCurrentTab ] = useState("1");
	
	const handleChangeTab = (key: string) => 
	{
		setCurrentTab(key);
	};

	const getPreview = (id: number) => 
	{
		switch (id) 
		{
		case TYPE_SOCIAL_INDEX.FACEBOOK:
			return (<PreviewFacebook listImage={previewSocial.listImage} content={previewSocial.content}/>);
		case TYPE_SOCIAL_INDEX.TIKTOK:
			return (<PreviewTiktok listImage={previewSocial.listImage} content={previewSocial.content} />);
		case TYPE_SOCIAL_INDEX.INSTAGRAM:
			return (<PreviewInstagram listImage={previewSocial.listImage} content={previewSocial.content} />);
		case TYPE_SOCIAL_INDEX.YOUTUBE:
			return <PreviewYoutube/>;
		case TYPE_SOCIAL_INDEX.TWITTER:
			return <PreviewTwitter/>;
		// default:
			// return <PreviewFacebook />;
		}
	};
	
	return (
		<Col xl={12}>
			<div className='preview'>
				<div className='title-section'>Preview in social media</div>
				<TabsWrapper
					className='tab-wrapper'
					defaultActiveKey={currentTab}
					items={listTab.map((e, index) => 
					{
						const label = e.icon ? (
							e.key === currentTab ? (
								<span className='tab-info'>
									<img src={e.icon} alt={e.label} width={24} height={24} />
									{e.label}
								</span>
							) : (
								<span className='tab-info'>
									<img src={e.icon} alt={e.label} width={24} height={24} />
								</span>
							)
						) : (
							<span className='tab-info'>{e.label}</span>
						);

						return {
							label    : label,
							key      : e.key,
							children : getPreview(parseInt(e.key))
						};
					})}
					onChange={handleChangeTab}
				/>
			</div>
		</Col>
	);
};

export default PreviewPost;
