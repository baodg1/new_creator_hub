export interface FileUploadI {
  file_name?: string;
  type: string;
  url: string;
}

export interface PostPlatformI {
  platform_id: number;
  brief_content?: string;
  content?: string;
  title?: string;
  social_account_list: number[];
}

export interface PostScheduleI {
  publish_at: Date;
}

export interface PostContent {
  draf?: string;
  facebook?: string;
  twitter?: string;
  youtube?: string;
  instagram?: string;
  tiktok?: string;
}
