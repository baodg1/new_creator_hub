import { FileUploadI, PostContent } from "./post";
export interface Platform{
  id: number;
  name: string;
}

export interface SocialAccountType {
  account_admin_name?: string;
  account_id?: string;
  account_name: string;
  avatar?: string;
  id: number;
  link?: string;
  link_admin?: string;
  platform: Platform;
  social_account_type: string;
  status: string;
}

export interface ColumnTableType {
  id: number;
  social_account_id: number;
  social_account: SocialAccountType;
  create_date: string;
  update_date: string;
  status: string;
  selected?: boolean;
}

export interface PaginateType {
  limit: number;
  offset: number;
  total: number;
  currentPage: number;
}

export interface TabType{
  label: string;
  key: string;
  icon?: any;
  children: any;
  platform?: Platform
  preview?: any;
}

export interface PreviewSocialI{
  listImage: FileUploadI[];
  content?: PostContent;
}

export interface PreviewI{
  listTab: TabType [],
  previewSocial: PreviewSocialI
}
