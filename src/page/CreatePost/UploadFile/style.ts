import styled from "styled-components";

const UploadFileBox = styled.div`
  display: block;
  .file-img {
    width: 200px;
    height: 200px;
    margin-right: 15px;
    float: left;
    margin-bottom: 15px;
    .ant-spin-nested-loading,
    .ant-spin-container {
      height: 100%;
    }
    img {
      width: 100%;
      height: 100%;
    }
    label {
      height: 100%;
      img {
        width: auto;
        height: auto;
      }
    }
  }
  label {
    background: #f7f7f7;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    flex-direction: column;
    text-align: center;
    p {
      font-weight: 600;
      font-size: 14px;
      line-height: 20px;
      color: #62666a;
      max-width: 136px;
      margin-top: 20px;
    }
  }
`;

export default UploadFileBox;
