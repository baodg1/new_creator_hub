import React, { useState } from "react";
import { Spin } from "antd";
import UploadFileBox from "./style";
import uploadFileIcon from "@/asset/create-post/upload-file-icon.svg";
import useVideoThumbnailForm from "./../hooks/useVideoThumbnailForm";
import Thumbnails from "./../VideoThumbnail/Thumbnails";
import { APIUploadFile } from "@/services/createPost";
import { error } from "@/component/Message";
import { FileUploadI } from "./../interface/post";

const typeList: string[] = [ "video", "image" ];

interface videoUrlType {
  videoUrl: string;
  setVideoUrl: Function;
  listImage: FileUploadI[];
  setListImage: Function;
}

const UploadFile = ({
	setVideoUrl,
	videoUrl,
	listImage,
	setListImage
}: videoUrlType) => 
{
	// const [listUrl, setListUrl] = useState<string[]>([]);
	const [ fileType, setFileType ] = useState<string>("");
	const [ isLoading, setIsLoading ] = useState(false);

	const {
		handleInputUrlChange,
		inputUrl,
		isError,
		selectedThumbnail,
		setSelectedThumbnail,
		thumbnails
	} = useVideoThumbnailForm({
		maxThumbnails : 20,
		type          : "url"
	});

	const onChange = (e: React.ChangeEvent<HTMLInputElement>) => 
	{
		if (!e?.target?.files) return;
		const typeImage = e?.target?.files[0]?.type;
		const typeImageSplit = typeImage?.split("/");

		const formData = new FormData();

		formData.append("file", e?.target?.files[0]);

		if (fileType === typeList[1] && typeImageSplit[0] === typeList[0])
			return error("Không thể upload video cùng ảnh");

		setIsLoading(true);
		APIUploadFile(formData).then((res) => 
		{
			setIsLoading(false);
			if (!(res && res?.success && res?.data)) return;
			if (typeImageSplit[0] === typeList[1]) 
			{
				setListImage([
					...listImage,
					{
						"file_name" : Date.now().toString(),
						"type"      : typeList[1],
						"url"       : res.data
					}
				]);
			}
			if (typeImageSplit[0] === typeList[0]) 
			{
				setVideoUrl(res.data);
				handleInputUrlChange(res.data);
			}
			if (typeList?.includes(typeImageSplit[0])) setFileType(typeImageSplit[0]);
		});
	};

	const renderImageList = () => 
	{
		if (!listImage.length) return;

		return listImage.map((item, key) => 
		{
			return (
				<div className='file-img' key={key}>
					<img src={item.url} alt={item?.file_name ?? ""} />
				</div>
			);
		});
	};

	return (
		<UploadFileBox className='upload-file'>
			{/* image */}
			{fileType !== typeList[0] && (
				<>
					{renderImageList()}
					<div className='file-img'>
						<Spin tip='Loading...' spinning={isLoading}>
							<input
								type='file'
								name='file'
								id='file'
								style={{ display: "none" }}
								accept='video/*,image/*'
								onChange={onChange}
							/>
							<label htmlFor='file'>
								<img src={uploadFileIcon} alt='upload file' />
								<p>Drag your item to upload</p>
							</label>
						</Spin>
					</div>
				</>
			)}
			{/* video */}
			{fileType === typeList[0] && (
				<>
					{inputUrl && (
						<div className='video text-center'>
							<video
								src={inputUrl}
								poster={selectedThumbnail || ""}
								controls
								height='200'
							/>
						</div>
					)}
					<Thumbnails
						thumbnails={thumbnails}
						selectedThumbnail={selectedThumbnail}
						setSelectedThumbnail={setSelectedThumbnail}
						isError={isError}
					/>
				</>
			)}
		</UploadFileBox>
	);
};

export default UploadFile;
