import { useState } from "react";
import fbIcon from "@/asset/create-post/facebook.svg";
import iconDesktop from "@/asset/create-post/desktop.svg"; 
import iconDesktopActive from "@/asset/create-post/desktop_active.svg";
import iconMobile from "@/asset/create-post/mobile.svg";
import iconMobileActive from "@/asset/create-post/mobile-active.svg";
import iconMusic from "@/asset/create-post/icon-music.svg";
import iconLike from "@/asset/create-post/icon-like.svg";
import iconLikeDesktop from "@/asset/create-post/icon-like-desktop.svg";
import iconComment from "@/asset/create-post/icon-comment.svg";
import iconCommentDesktop from "@/asset/create-post/icon-comment-desktop.svg";
import iconShare from "@/asset/create-post/icon-share.svg";
import iconShareDesktop from "@/asset/create-post/icon-share-desktop.svg";
import iconMore from "@/asset/create-post/icon-more.svg";
import iconMore2 from "@/asset/create-post/icon-more2.svg";

import {
	PreivewWrapper,
	TitleWrapper,
	PreviewContentWrapper,
	PreviewMobileWrapper,
	PreviewDesktopWrapper
} from "./style";
import { CButton } from "@/component/Common";

const PreviewTwitter = () => 
{
	const [ currentType, setCurrentType ] = useState(0);

	const handleChangeType = (key: number) => 
	{
		setCurrentType(key);
	};

	return (
		<PreivewWrapper>
			<TitleWrapper>
				<CButton
					className={currentType ==0 ? `btn-active`: `btn-default mr-8`}
					label='Desktop'
					icon={<img src={currentType==0 ? iconDesktopActive: iconDesktop} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(0)}
				/>
				<CButton
					className={currentType ==1 ? `btn-active`: `btn-default mr-8`}
					label='Mobile'
					icon={<img src={currentType==1 ? iconMobileActive: iconMobile} className='pr-10' />}
					type={2}
					onClick={() => handleChangeType(1)}
				/>
			</TitleWrapper>
			<PreviewContentWrapper>
				{currentType == 1 ? (
					<PreviewMobileWrapper>
						<div className='content'>
							<div className='name-bar'>
								<img src='https://i.pinimg.com/originals/b0/fc/61/b0fc61a60710300edb950cb7b55a7e22.jpg' />
								<span>Sau Nguyen</span>
							</div>
							<div className='description'>
								<span>
									Lorem ipsum dolor sit amet, eiusmod tempor numbas sator...
									more
								</span>
							</div>
							<div className='audio-bar'>
								<img src={iconMusic} width={10.71} height={14} />
								<span>Page Name · Original Audio</span>
							</div>
						</div>
						<div className='action-icons'>
							<div className='icon-like'>
								<img src={iconLike} />
								<br />
								<span>234</span>
							</div>
							<div className='icon-comment'>
								<img src={iconComment} />
								<br />
								<span>45</span>
							</div>

							<img src={iconShare} />
							<img src={iconMore} />
						</div>
						<img
							className='image-bg'
							src='https://i.pinimg.com/originals/b0/fc/61/b0fc61a60710300edb950cb7b55a7e22.jpg'
						/>
					</PreviewMobileWrapper>
				) : (
					<PreviewDesktopWrapper>
						<div className='header'>
							<div className='account'>
								<img src={fbIcon} width={40} height={40} />
								<span>
									<span className='name'>Facebook Account</span> <br />
									<span className='time-ago'>2 mins ago</span>
								</span>
								<div className='suff'>
									<img src={iconMore2} />
								</div>
							</div>
						</div>
						<div className='content'>
							<p className='description'>
								This is a sample Facebook post. Tests and updates by social
								networks may affect the final appearance.
							</p>
							<div className='image'>
								<img src='https://media.istockphoto.com/id/1322277517/photo/wild-grass-in-the-mountains-at-sunset.jpg?s=612x612&w=0&k=20&c=6mItwwFFGqKNKEAzv0mv6TaxhLN3zSE43bWmFN--J5w=' />
							</div>

							<div className='react'>
								<div className='reaction'>
									<span className='reaction-icon color1' />
									<span className='reaction-icon color2' />
									<span className='reaction-icon color3 mr-8' />3
								</div>
								<div className='comment-share'>
									<div className='comment mr-8'>20 Comments</div>{" "}
									<div className='share'>8 Shares</div>
								</div>
							</div>
						</div>
						<div className='separator' />
						<div className='bottom'>
							<div className='icon'>
								<img src={iconLikeDesktop} /> Like
							</div>
							<div className='icon'>
								<img src={iconCommentDesktop} /> Comment
							</div>
							<div className='icon'>
								<img src={iconShareDesktop} /> Share
							</div>
						</div>
					</PreviewDesktopWrapper>
				)}
			</PreviewContentWrapper>
		</PreivewWrapper>
	);
};

export default PreviewTwitter;
