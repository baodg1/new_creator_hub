import styled from "styled-components";
// import bgImage from "@/asset/auth/login_bg.svg";

export const PreivewWrapper = styled.div`
  .mr-8 {
    margin-right: 8px;
  }
  .pr-10 {
    padding-right: 10px;
  }
  .mt-10 {
    margin-top: 10px;
  }
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 8px;

  .btn-default {
    height: 44px;
    border: 1px solid #eaeaea;
    border-radius: 4px;

    span {
      img {
        background-color: red;
      }
    }
  }
  .btn-active{
    height: 44px;
    background: #FE2C55;
    border: 1px solid #FE2C55;
    border-radius: 4px;
    span{
      color: white;
    }
  }
`;

export const PreviewContentWrapper = styled.div`
  width: 100%;
`;

export const PreviewMobileWrapper = styled.div`
  margin-top: 24px;
  width: 335px;
  height: 595px;
  background: #ffffff;
  border-radius: 8px;

  /* Inside auto layout */

  flex: none;
  order: 2;
  flex-grow: 0;
  margin: 24px auto;

  .image-bg {
    top: 0%;
    bottom: 0%;
    object-fit: cover;
    width: 100%;
    height: 100%;
    border-radius: 8px;
  }
  .content {
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    padding: 0px;
    padding-left: 16px;
    position: absolute;
    bottom: 8px;
    .name-bar {
      img {
        width: 30px;
        height: 30px;
        border-radius: 30px;
        object-fit: cover;
      }
      span {
        margin-left: 8px;
        font-family: "Open Sans";
        font-style: normal;
        font-weight: 700;
        font-size: 12px;
        line-height: 22px;
        /* identical to box height, or 183% */

        /* Elements White */

        color: #ffffff;

        /* Inside auto layout */

        flex: none;
        order: 1;
        flex-grow: 0;
      }
    }
    .description {
      width: 235px;
      margin-top: 8px;
      font-family: "Open Sans";
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 16px;
      /* or 133% */

      /* Elements White */

      color: #ffffff;

      /* Inside auto layout */

      flex: none;
      order: 0;
      flex-grow: 0;
    }
    .audio-bar {
      margin-top: 16px;
      display: flex;
      flex-direction: row;
      align-items: center;
      gap: 12px;

      width: 335px;
      height: 38px;

      /* Inside auto layout */

      flex: none;
      order: 2;
      align-self: stretch;
      flex-grow: 0;
      img {
        width: 10.71px;
        height: 14px;
        flex: none;
        order: 0;
        flex-grow: 0;
      }
      span {
        font-family: "Open Sans";
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 22px;
        /* identical to box height, or 183% */

        /* Elements White */

        color: #ffffff;

        /* Inside auto layout */

        flex: none;
        order: 1;
        flex-grow: 0;
      }
    }
  }

  .action-icons {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;
    padding: 0px;
    gap: 16px;

    position: absolute;
    top: 69.75%;
    left: 70%;
    bottom: 2.02%;
    .icon-like {
      text-align: center;
      width: 24px;
      height: 24px;
      margin-bottom: 16px;
    }
    .icon-comment {
      text-align: center;
      img {
        width: 19.2px;
        height: 18px;
      }
    }
    img {
      width: 24px;
      height: 24px;
    }
    span {
      font-family: "Open Sans";
      font-style: normal;
      font-weight: 600;
      font-size: 12px;
      line-height: 16px;
      color: #ffffff;
    }
  }
`;

export const PreviewDesktopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 20px;
  gap: 24px;
  border: 1px solid #e7e7e7;
  border-radius: 8px;
  flex: none;
  order: 2;
  align-self: stretch;
  flex-grow: 0;
  margin-top: 24px;

  .header {
    .account {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      padding: 0px;
      background: #ffffff;
      flex: none;
      order: 0;
      flex-grow: 0;
      img {
        margin-right: 16px;
      }
      .name {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 700;
        font-size: 14px;
        line-height: 22px;
        /* identical to box height, or 157% */

        /* Dashboard/Text - High Emphasis */

        color: #313335;
      }
      .time-ago {
        font-family: "Roboto";
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 22px;
        color: #969696;
      }
    }
    .suff {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      padding: 0px;
      gap: 2px;
      margin-top: 25px;
      width: 19px;
      height: 5px;
    }
  }

  .content {
    .description {
      font-family: "Roboto";
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 22px;
    }
    .image {
      img {
        height: 374px;
        width: 100%;
        flex: none;
        order: 0;
        align-self: stretch;
        flex-grow: 0;
        border-radius: 8px;
      }
    }
    .react {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      padding: 0px;
      margin-top: 22px;
      .reaction {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;

        .reaction-icon {
          width: 20px;
          height: 20px;
          border-radius: 118.939px;
          padding: 0px;
        }
        .color1 {
          background: linear-gradient(180deg, #17adfd 0%, #016edf 100%);
        }
        .color2 {
          background: linear-gradient(180deg, #ea6874 0%, #d3373e 100%);
        }
        .color3 {
          background: linear-gradient(180deg, #f6dd79 0%, #eba94f 100%);
        }
      }
      .comment-share {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px;
      }
    }
  }
  .separator {
    height: 2.16px;
    width: 100%;
    background: #e5e5e5;
  }
  .bottom {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 0px;
    width: 100%;
    .icon {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding: 8px 0px 8px 16px;
      gap: 12px;
      height: 40px;
    }
  }
`;
