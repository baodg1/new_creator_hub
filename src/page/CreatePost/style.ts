import styled from "styled-components";
import { Tabs } from "antd";

const CreatePostPage = styled.div`
  padding: 15px 15px 0px 15px;
  .mb-10{
    margin-bottom: 10px;
  }
  .tab-type-post {
    background: #ffffff;
    border-radius: 4px;
    padding: 24px 30px;
    display: flex;
    align-items: center;
    span {
      font-weight: 600;
      font-size: 20px;
      line-height: 30px;
      cursor: pointer;
      &:first-child {
      }
      &.border-div {
        background: #eaeaea;
        margin-right: 20px;
        margin-left: 20px;
        display: inline-block;
        width: 1px;
        height: 19px;
      }
      &:last-child {
        color: #737373;
      }
      &.active {
        position: relative;
        &:after {
          content: "";
          position: absolute;
          bottom: 0;
          height: 3px;
          width: 50px;
          left: calc(50% - 25px);
          background: #fe2c55;
          border-radius: 12px;
          top: 40px;
        }
      }
    }
  }
  .choose-account {
    background: #ffffff;
    padding: 20px 30px;
    margin-top: 15px;
    border-radius: 4px;
    .head {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    span {
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
    }
    button {
      background: #f7f8f9;
      border: 1px solid #e7e7e7;
      border-radius: 8px;
      height: 44px;
    }
    .list-account-choose {
      display: flex;
      align-items: center;
      .ant-avatar-group {
        margin-right: 10px;
      }
    }
  }
  .post-content {
    background: #ffffff;
    padding: 20px 30px;
    margin-top: 15px;
    border-radius: 4px;
    font-size: 16px;
    .head {
      display: flex;
      justify-content: space-between;
      .ant-col {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      .title {
        font-weight: 600;
        font-size: 16px;
        line-height: 24px;
      }
      .switch {
        display: flex;
        align-items: center;
        button {
          margin-right: 10px;
        }
      }
    }
    .textarea-content {
      textarea {
        width: 100%;
        margin-top: 15px;
        height: 178px;
        border: 1px solid #e7e7e7;
        border-radius: 4px;
        &:focus {
          outline: none;
        }
      }
    }
  }
  .title-section {
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 15px;
  }
  .upload-video-image {
    background: #ffffff;
    padding: 20px 30px;
    margin-top: 15px;
    border-radius: 4px;
    font-size: 16px;
    display: block;
  }
  .when-to-post {
    background: #ffffff;
    padding: 20px 30px;
    margin-top: 15px;
    border-radius: 4px;
    font-size: 16px;
    ul {
      list-style-type: none;
      padding: 0;
      li {
        .title {
          display: flex;
          justify-content: space-between;
          align-items: center;
          h3 {
            font-weight: 600;
            font-size: 14px;
            line-height: 20px;
            color: #313335;
          }
        }
        p {
          font-weight: 400;
          font-size: 14px;
          line-height: 20px;
          color: #62666a;
        }
      }
    }
  }
  .preview {
    background: #ffffff;
    padding: 20px 30px;
    margin-top: 15px;
    border-radius: 4px;
  }
  .tab-wrapper{
    margin-top: 28px;
    .tab-info{
      img{
        margin-right: 12px; 
      }
      padding: 0px 14px;
    }
  }
 .bottom-create{
    height: 88px;
    width: 100%;
    margin-top: 16px; 
    background: #FFFFFF;
    box-shadow: 0px -4px 10px rgba(205, 205, 205, 0.15);
    border-radius: 4px;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    
 }
 .btn{
    height: 48px;
    width: 171px;
    border-radius: 8px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin: auto 0px;
  }
  .btn-cancel{
    border: 1px solid #FE2C55;
    margin-right: 16px;
    span{
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #FE2C55;
    }
  }
  .btn-add-time{
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 8px 16px;
    gap: 8px;
    width: 164px;
    height: 40px;
    background: linear-gradient(270deg, rgba(182, 78, 137, 0.15) 0%, rgba(236, 75, 98, 0.15) 100%);
    /* CMS/Primary test */

    border: 1px solid #D44C74;
    border-radius: 4px;
    color: #D44C74;
    font-weight: 600;
    font-size: 12px;
    line-height: 16px;
    margin-bottom: 10px;
  }
  .btn-create{
    background: #FE2C55;
      span{
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #FFFFFF;
    }
    margin-right: 20px;
  }
  .when-to-post{
    .error{
      display: inline-block;
      padding: 12px;
      background: rgba(225, 38, 38, 0.15);
      border-radius: 4px;
      color: #862945;
      font-family: 'Inter';
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      margin-bottom: 20px;
    }
  }
  .date-time{
    width: 536px;
    height: 48px
  }
`;

const TabsWrapper = styled(Tabs)`
  /* padding-bottom: 24px; */
  .ant-tabs-tab {
    font-size: 14;
  }
  .ant-tabs-tab + .ant-tabs-tab {
    margin-left: 20px;
  }
  .ant-tabs-tab-btn {
    color: #131317 !important;
    font-weight: 600;
    padding: 0 8px;
  }
  .ant-tabs-ink-bar {
    background-color: #fe2c55;
  }
`;

export { CreatePostPage, TabsWrapper };
