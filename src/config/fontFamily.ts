export const listFont = [
	{
		label : "Roboto",
		value : "Roboto"
	},
	{
		label : "Open Sans",
		value : "Open Sans"
	},
	{
		label : "Josefin Sans",
		value : "Josefin Sans"
	},
	{
		label : "Dancing Script",
		value : "Dancing Script"
	},
	{
		label : "Waiting for the Sunrise",
		value : "'Waiting for the Sunrise', cursive"
	}
];
