export const ACTION = {
	UNDO            : "UNDO",
	REDO            : "REDO",
	REDO_WITH_SHIFF : "REDO_WITH_SHIFF"
};

export const KEY_MAP = {
	[ACTION.UNDO]            : "ctrl+z",
	[ACTION.REDO]            : "ctrl+y",
	[ACTION.REDO_WITH_SHIFF] : "ctrl+shift+z"
};
