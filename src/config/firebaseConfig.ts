export const firebaseConfig = {
	apiKey :
    process.env.REACT_APP_ENV === "development"? "AIzaSyAI1KmJvfuV9v7W8n36g8P5tFk3nM8R-10": "",
	authDomain :
    process.env.REACT_APP_ENV === "development"? "creatorhub-web.firebaseapp.com": "",
	projectId : process.env.REACT_APP_ENV === "development" ? "creatorhub-web" : "",
	storageBucket :
    process.env.REACT_APP_ENV === "development"? "creatorhub-web.appspot.com": "",
	messagingSenderId :
    process.env.REACT_APP_ENV === "development" ? "468965276133" : "",
	appId :
    process.env.REACT_APP_ENV === "development"? "1:468965276133:web:73ecf5c33f41fe38efd70c": ""
};
