import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "../redux/reducer";

// const persistConfig = {
// 	key       : "edit_video_app",
// 	storage,
// 	whitelist : [ "Auth", "Preview" ]
// };

const store = createStore(rootReducer, composeWithDevTools());

export { store };
