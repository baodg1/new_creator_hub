/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
export const API_METHOD = {
	GET    : "get",
	POST   : "post",
	DELETE : "delete",
	PUT    : "put",
	PATCH  : "patch"
};

export const MODE = {
	UPLOAD_MEDIA    : "UPLOAD_MEDIA",
	EDIT_VIDEO      : "EDIT_VIDEO",
	EDIT_IMAGE      : "EDIT_IMAGE",
	EDIT_AUDIO      : "EDIT_AUDIO",
	EDIT_TEMPLATES  : "EDIT_TEMPLATES",
	PROJECT_SETTING : "PROJECT_SETTING",
	TEXT_SETTING    : "TEXT_SETTING",
	SUBTITLES       : "SUBTITLES",
	ELEMENTS        : "ELEMENTS",
	OVERLAY         : "OVERLAY",
	TRANSITION      : "TRANSITION"
};

export const SUBTITLES_MODE = {
	AUTO_SUBTITLE   : "AUTO_SUBTITLE",
	MANUAL_SUBTITLE : "MANUAL_SUBTITLE",
	IMPORT_SUBTITLE : "IMPORT_SUBTITLE",
	NONE            : "NONE"
};

export const API_TIMEOUT = 15000;
export const BASE_URL =
  process.env.REACT_APP_BASE_URL ?? "https://api.creatorhub.ai/api/v1";

export const MEDIA_TYPE = {
	IMAGE    : "IMAGE",
	VIDEO    : "VIDEO",
	AUDIO    : "AUDIO",
	TEXT     : "TEXT",
	SUBTITLE : "SUBTITLE"
};

export enum FileType {
	Image = "IMAGE",
	Video = "VIDEO",
	Audio = "AUDIO",
	Unknown = "UNKNOWN"
}

export const PLACEHOLDER_FRAME = "PLACEHOLDER_FRAME";

export const SCREEN_SIZE = {
	SIZE_16_9 : "16:9",
	SIZE_9_16 : "9:16",
	SIZE_1_1  : "1:1",
	SIZE_4_5  : "4:5",
	SIZE_5_4  : "5:4",
	SIZE_2_3  : "2:3",
	SIZE_21_9 : "21:9",
	SIZE_9_21 : "9:21"
};

export const DRAG_DROP_TYPE = {
	MEDIA : "MEDIA",
	FRAME : "FRAME"
};

export const BACKGROUND_TYPE = {
	IMAGE : "IMAGE",
	COLOR : "COLOR"
};

export const TIME_RULER_ID = "time-ruler";
export const PREVIEW_ID = "preview";
export const DEFAULT_IMAGE_PREFVIEW_SIZE = 0.5; // half of preview size

export const INITIAL_ROTATE_ANGLE = 0;
export const SMALLEST_Z_INDEX = -1000;
export const LARGEST_Z_INDEX = 1000;
export const LARGEST_Z_INDEX_1 = 1001;

export const LIST_DEVICE = {
	IPHONE_14     : "IPHONE_14",
	IPHONE_14_PRO : "IPHONE_14_PRO",
	IPHONE_X      : "IPHONE_X",
	IPHONE_SE     : "IPHONE_SE",
	ANDROID_SMALL : "ANDROID_SMALL",
	ANDROID_LARGE : "ANDROID_LARGE",
	NONE          : "NONE"
};

export const LIST_PLATFORM = {
	BLANK           : "BLANK",
	FACEBOOK_VIDEO  : "FACEBOOK_VIDEO",
	FACEBOOK_REEL   : "FACEBOOK_REEL",
	INSTAGRAM_STORY : "INSTAGRAM_STORY",
	INSTAGRAM_REEL  : "INSTAGRAM_REEL",
	YOUTUBE_VIDEO   : "YOUTUBE_VIDEO",
	YOUTUBE_SHORT   : "YOUTUBE_SHORT",
	TIKTOK          : "TIKTOK"
};

export const TEXT_ALIGN = {
	LEFT   : "left",
	CENTER : "center",
	RIGHT  : "right"
};

export const DEFAULT_DURATION = 5;

export const DURATION_OPTION = {
	AUTOMATIC : "AUTOMATIC",
	FIXED     : "FIXED"
};

export const MIN_PIXEL_PER_SECOND = 5;

export const VALID_UPLOAD_FILE = [
	"aac",
	"avi",
	"m4v",
	"mov",
	"mp4",
	"mpe",
	"mpg4",
	"webm",
	"flac",
	"aiff",
	"alac",
	"mp3",
	"m4a",
	"ogg",
	"wav",
	"amr",
	"wma",
	"aac",
	"psd",
	"webp",
	"jpg",
	"jpeg",
	"gif",
	"bmp",
	"svg",
	"png",
	"odd",
	"tiff"
];

export const MAX_LENGTH_UNDO_REDO = 20; // max length of undo, redo stack
export const MAX_FILE_SIZE = 250; // unit is MB

export const ROUTE_EDIT_VIDEO = "/edit/:id";

export const USER = {
	TOKEN : "USER_TOKEN"
};

// Max-Min username, pass
export const MIN_PASSWORD_LENGTH = 6;
export const MAX_PASSWORD_LENGTH = 20;
export const MAX_MAIL_LENGTH = 50;
export const MAX_FULLNAME_LENGTH = 255;
export const OTP_SIZE = 4;

// Social
export const PLATFORM_SOCIAL_SERVICE = {
	GOOGLE   : "google",
	FACEBOOK : "facebook"
};
