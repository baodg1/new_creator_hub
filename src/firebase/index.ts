import { initializeApp } from "firebase/app";
import { firebaseConfig } from "@/config/firebaseConfig";
import {
	GoogleAuthProvider,
	getAuth,
	signInWithPopup,
	FacebookAuthProvider
} from "firebase/auth";

initializeApp(firebaseConfig);

const googleProvider = new GoogleAuthProvider();
const facebookProvider = new FacebookAuthProvider();

const auth = getAuth();

/**
 * Đăng nhập với google
 * @returns
 */

export const handleLoginWithGoogle = async () =>
	await signInWithPopup(auth, googleProvider)
		.then((result) => 
		{
			const user = result.user;
      
			return user;
		})
		.catch((error) => 
		{
			// Handle Errors here.
			// const errorCode = error.code;
			// const errorMessage = error.message;
			// The email of the user's account used.
			// const email = error.customData.email;
			// The AuthCredential type that was used.
			// const credential = GoogleAuthProvider.credentialFromError(error);

			return error;
		});

/**
 * Đăng nhập với Facebook
 * @returns
 */
export const handleLoginWithFacebook = async () =>
	await signInWithPopup(auth, facebookProvider)
		.then((result) => 
		{
			// The signed-in user info.
			const user = result.user;
      
			return user;
		})
		.catch((error) => 
		{
			// const errorCode = error.code;
			// const errorMessage = error.message;
			// The email of the user's account used.
			// const email = error.customData.email;
			// The AuthCredential type that was used.
			// const credential = FacebookAuthProvider.credentialFromError(error);
      
			return error;
		});
