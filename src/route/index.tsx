import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { connect } from "react-redux";
import { actSaveGetListFile } from "@/redux/action/file";
import { actSetListTrack } from "@/redux/action/app";
import { routesConfig } from "./config";
import { LocalStorage } from "@/utils/config";
import { USER } from "@/config/constant";

const AppRoute = () => 
{
	const token = LocalStorage.get(USER.TOKEN);
	
	return (
		<BrowserRouter>
			<Routes>
				{routesConfig?.map((routeInfo, index) => 
				{
					const { element: ComponentInRoute, ...rest } = routeInfo;

					if (routeInfo?.privateRoute) 
					{
						return (
							<Route
								key={index}
								element={
									token ? (
										<ComponentInRoute />
									) : (
										<Navigate to='/auth/login' replace key={index} />
									)
								}
								{...rest}
							/>
						);
					}

					return <Route key={index} element={<ComponentInRoute />} {...rest} />;
				})}
			</Routes>
		</BrowserRouter>
	);
};

export default connect(
	() => ({}),
	{ actSaveGetListFile, actSetListTrack }
)(AppRoute);
