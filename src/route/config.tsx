import HomePage from "@/page/HomePage";
import NotFoundPage from "@/page/NotFound";
import EditPage from "@/page/Dashboard/EditPage";
import Exporting from "@/page/Dashboard/Exporting";
import ExportFail from "@/page/Dashboard/ExportFail";
import ExportSuccess from "@/page/Dashboard/ExportSuccess";
import MyProject from "@/page/Dashboard/MyProject";
import DashboardLayout from "@/component/DashboardLayout";
import EditVideoLayout from "@/component/EditVideoLayout";
import ProfileUser from "@/page/Dashboard/ProfileUser";
import SubscriptionPlan from "@/page/Dashboard/SubscriptionPlan";
import ForgotPassword from "@/page/Auth/ForgotPassword";
import Login from "@/page/Auth/Login";
import SignUp from "@/page/Auth/SignUp";
import ResetPasswordForm from "@/component/Auth/ResetPassword";
import Trash from "@/page/Dashboard/Trash";
import ManageAccount from "@/page/Account/Manage";
import ConnectAccount from "@/page/Account/Connect";
import ConnectAccountDetail from "@/page/Account/connectDetail";
import CreatePost from "@/page/CreatePost";
import MyProjectDetail from "@/page/Dashboard/MyProjectDetail";

export const routesConfig = [
	{
		exact        : true,
		privateRoute : false,
		path         : "/auth/signup",
		element      : SignUp
	},
	{
		exact        : true,
		privateRoute : false,
		path         : "/",
		element      : HomePage
	},
	{
		exact        : true,
		privateRoute : false,
		path         : "/auth/login",
		element      : Login
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/edit/:id",
		element      : () => (
			<EditVideoLayout>
				<EditPage />
			</EditVideoLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/dashboard/myproject",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<MyProject />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/dashboard/myproject/:id",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<MyProjectDetail />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/dashboard/trash",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<Trash />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/dashboard/profileuser",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={2}>
				<ProfileUser />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/dashboard/subscriptionplan",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={2}>
				<SubscriptionPlan />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : false,
		path         : "/auth/forgotpassword",
		element      : ForgotPassword
	},
	{
		exact        : true,
		privateRoute : false,
		path         : "/auth/resetpassword",
		element      : ResetPasswordForm
	},
	// cms
	{
		exact        : true,
		privateRoute : true,
		path         : "/account/manage-account",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<ManageAccount />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/account/connect-account",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<ConnectAccount />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/account/connect-account-detail",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<ConnectAccountDetail />
			</DashboardLayout>
		)
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/create-post",
		layout       : DashboardLayout,
		element      : () => (
			<DashboardLayout menuType={1}>
				<CreatePost />
			</DashboardLayout>
		)
	},
	// cms
	{
		exact        : true,
		privateRoute : true,
		path         : "/exporting",
		element      : Exporting
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/export-fail",
		element      : ExportFail
	},
	{
		exact        : true,
		privateRoute : true,
		path         : "/export-success",
		element      : ExportSuccess
	},
	{
		privateRoute : false,
		path         : "*",
		element      : NotFoundPage
	}
];
