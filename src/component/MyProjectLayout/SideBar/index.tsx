import React, { useState } from 'react';
import {
	SideBarWrapper,
	MenuWrapper,
	IconTitleMenu,
	SideBarContainerWrapper
} from "./style";
import homeIcon from "@/asset/home-icon.png";
import templateIcon from "@/asset/template-icon.png";
import trashIcon from "@/asset/trash-icon.png";
import expandIcon from "@/asset/expand-icon.png";
import myProjectActiveIcon from "@/asset/myproject-active-icon.png";
import { MenuProps } from "antd";
import { CButton } from "@/component/Common";
import { icCrownSvg } from "@/component/Icon/ic_crown";
import Icon from "@ant-design/icons";
import { useLocation, useNavigate } from "react-router-dom";

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
	label: React.ReactNode,
	key: React.Key,
	icon?: React.ReactNode,
	children?: MenuItem[],
	type?: "group"
): MenuItem 
{
	return {
		key,
		icon,
		children,
		label,
		type
	} as MenuItem;
}

const items: MenuProps["items"] = [
	getItem("Home", "/dashboard/home", <IconTitleMenu src={homeIcon} />),

	getItem("Template", "SubTemplate", <IconTitleMenu src={templateIcon} />, [
		getItem("Template", "/dashboard/template")
	]),

	getItem(
		"Video Editing",
		"groupvideo",
		null,
		[
			getItem(
				"My Project",
				"/dashboard/myproject",
				<IconTitleMenu src={myProjectActiveIcon} />
			),
			getItem("Trash", "/dashboard/trash", <IconTitleMenu src={trashIcon} />)
		],
		"group"
	),
	getItem(
		"CMS",
		"groupcms",
		null,
		[
			getItem(
				"Content Plan",
				"/dashboard/contentplan",
				<IconTitleMenu src={templateIcon} />,
				[
					getItem("Account", "/account", null, [
						getItem("Connect Account", "/account/connect-account"),
						getItem("Manage Account", "/account/manage-account")
					])
				]
			)
		],
		"group"
	)
];

const SideBar = () => 
{
	// TODO Logic default này đang không đúng do nếu link đến màn khác đầu tiên thì sẽ không đúng
	const location = useLocation();
	const [ menuSelectedKey ] = useState(location.pathname);
	const [ menuOpenKey ] = useState(location.pathname);
	const navigate = useNavigate();

	const handleSelectedMenu: MenuProps["onClick"] = (e) => 
	{
		navigate(e.key);
	};

	return (
		<SideBarContainerWrapper>
			<SideBarWrapper width={312}>
				<CButton
					label='Try CreatorHub Pro'
					type={1}
					className='btn-creatorhub'
					icon={
						<Icon
							component={icCrownSvg}
							style={{
								width  : "24px",
								height : "24px"
							}}
						/>
					}
				/>
				<MenuWrapper
					mode='inline'
					triggerSubMenuAction='click'
					expandIcon={(props) => (
						<IconTitleMenu
							src={expandIcon}
							style={props.isOpen ? { transform: "rotate(180deg)" } : {}}
						/>
					)}
					defaultSelectedKeys={[ menuSelectedKey ]}
					defaultOpenKeys={[ menuOpenKey ]}
					items={items}
					onSelect={handleSelectedMenu}
				/>
			</SideBarWrapper>
		</SideBarContainerWrapper>
	);
};

export default SideBar;
