import styled from "styled-components";
import { Layout } from "antd";
import { Content } from "antd/lib/layout/layout";

const LayoutWrapper = styled(Layout)`
  height: 100vh;
  overflow: hidden;
`;

const LayoutContentWrapper = styled(Layout)`
  height: calc(100vh - 82px);
  overflow: hidden;
  display: flex;
`;

const ContentWrapper = styled(Content)`
  flex: 1;
  overflow: hidden;
  position: relative;
  background-color: #ffffff;
`;
const ContentMainWrapper = styled(Content)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 8px;
    background-color: #fff;
  }
  ::-webkit-scrollbar-thumb {
    background: #c1c1c1;
    border-radius: 6px;
  }
`;

export {
	LayoutWrapper,
	LayoutContentWrapper,
	ContentWrapper,
	ContentMainWrapper
};
