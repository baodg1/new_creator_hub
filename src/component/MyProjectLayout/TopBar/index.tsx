import { TopBarWrapper } from "./style";
import { Col, Menu, MenuProps, Row } from "antd";
import menuIcon from "@/asset/menu-ic.svg";
import arrowDownIcon from "@/asset/arrow-down-icon.png";
import imageLogo from "@/asset/logo_white.svg";
import defaultAvatar from "@/asset/default-avatar.svg";

import settingIcon from "@/asset/setting-icon.svg";
import questionIcon from "@/asset/question-icon.svg";
import downloadVideoIcon from "@/asset/download-video-icon.svg";

import {
	DownOutlined,
	PictureOutlined,
	PlayCircleOutlined,
	PlusCircleFilled,
	SoundOutlined
} from "@ant-design/icons";
import { CButton } from "@/component/Common";

import { Header } from "antd/lib/layout/layout";

const lstMenu = [
	{
		Key    : "1",
		Title  : "Product",
		Childs : [
			{
				key : "1",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Video
					</a>
				),
				icon : <PlayCircleOutlined />
			},
			{
				key : "2",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Audio
					</a>
				),
				icon : <SoundOutlined />
			},
			{
				key : "3",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Image
					</a>
				),
				icon : <PictureOutlined />
			}
		],
		Icon : <DownOutlined />
	},
	{
		Key   : "2",
		Title : "Usecase"
	},
	{
		Key   : "3",
		Title : "Plan & Pricing"
	},
	{
		Key   : "4",
		Title : "Learn"
	}
];
const items1: MenuProps["items"] = lstMenu.map((x) => ({
	key      : x.Key,
	label    : x.Title,
	children : x.Childs ?? [],
	icon     : x.Icon
}));

const TopBar = () => 
{
	return (
		<TopBarWrapper>
			<Header className='header'>
				<Row align='middle' className='row' wrap={false} justify='center'>
					<img src={menuIcon} alt='' className='menu-icon' />
					<img src={imageLogo} alt='' className='imageLogo' />
					<Menu
						mode='horizontal'
						// defaultSelectedKeys={["2"]}
						items={items1}
					/>
					<Col flex={1} />
					<img src={settingIcon} alt='' className='header-btn-icon mr-35' />
					<img src={questionIcon} alt='' className='header-btn-icon mr-35' />
					<img src={downloadVideoIcon} alt='' className='header-btn-icon mr-35' />
					<CButton
						label='Create New Video'
						icon={<PlusCircleFilled />}
						type={1}
						className='btn-create-new-video mr-24'
					/>
					<img src={defaultAvatar} alt='' className='imgLogoUser mr-18' />
					<img src={arrowDownIcon} alt='' width={16} />
				</Row>
			</Header>
		</TopBarWrapper>
	);
};

export default TopBar;
