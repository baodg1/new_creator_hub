import styled from "styled-components";
import { Layout } from "antd";
const { Header } = Layout;
const TopBarWrapper = styled(Header)`
  height: 82px !important;
  z-index: 1;
  width: 100%;
  padding: 0 !important;
  box-shadow: 0px 3px 4px rgba(0, 0, 0, 0.05);

  .mr-35 {
    margin-right: 35px;
  }
  .mr-24 {
    margin-right: 24px;
  }
  .mr-18 {
    margin-right: 18px;
  }

  .ant-layout-header,
  .ant-menu {
    background: #000000;
  }

  .ant-menu .ant-menu-title-content {
    color: #ffffff;
  }

  .header {
    height: 82px !important;
  }
  .imageLogo {
    width: 180px;
    height: 40px;
    cursor: pointer;
  }
  .menu-icon {
    width: 24px !important;
    height: 24px !important;
    margin-right: 26px;
    cursor: pointer;
  }
  .logo {
    width: 170px;
    height: 22px;
    padding-right: 24px;
  }
  .title {
    font-weight: 600;
    font-size: 28px;
    font-style: normal;
    padding-left: 24px;
  }
  .row {
    height: 100%;
    &.row > div:first-child {
      img:first-child {
        width: 40px;
        margin-right: 32px;
      }
    }
  }
  .avatarUser {
    img:nth-child(1),
    img:nth-child(2),
    img:nth-child(3) {
      margin-right: 8px;
    }
    .imgLogoUser {
      width: 50px;
      height: 50px;
      margin-left: 16px;
    }
    img:last-child {
      margin-left: 16px;
    }
    img {
      cursor: pointer;
    }
  }
  .btn-create-new-video {
    width: 220px;
    height: 48px;
    background: #fe2c55;
    outline: none;
    border: none;
    :hover {
      background: #f9224c;
    }
    span {
      color: #ffffff !important;
    }
  }

  .header-btn-icon {
    height: 22px;
    width: 22px;
    cursor: pointer;
    opacity: 0.6;

    :hover {
      opacity: 1;
    }
  }
`;

export { TopBarWrapper };
