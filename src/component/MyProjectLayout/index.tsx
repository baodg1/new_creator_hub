import { connect } from "react-redux";
import TopBar from "@/component/MyProjectLayout/TopBar";
import SideBar from "@/component/MyProjectLayout/SideBar";
import {
	LayoutWrapper,
	LayoutContentWrapper,
	ContentWrapper,
	ContentMainWrapper
} from "./style";
import { IMyProjectLayoutProps } from "./interface/MyProjectLayout";

const MyProjectLayout = (props: IMyProjectLayoutProps) => 
{
	const { children } = props;

	return (
		<LayoutWrapper>
			<TopBar />
			<LayoutContentWrapper>
				<SideBar />
				<ContentWrapper>
					<ContentMainWrapper>{children}</ContentMainWrapper>
				</ContentWrapper>
			</LayoutContentWrapper>
		</LayoutWrapper>
	);
};

export default connect()(MyProjectLayout);
