import React from 'react';

export interface IMyProjectLayoutProps {
    children: React.ReactElement;
  }
