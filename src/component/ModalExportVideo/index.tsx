import { Modal, Select, Form, Button } from "antd";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RootState } from "@/redux/reducer";
import {
	actSetModalExportVideoOpen,
	actSetIsExporting
} from "@/redux/action/preview";
import { ModalExportVideoWrapper } from "./style";
import { ModalReloadAlertProps } from "./interface/ModalExportVideo";

const VIDEO_THUMBNAIL =
  "http://54.255.51.185:3000/files/images/28da1bf8-8ebe-4837-86c1-23ef08b0d618/1667915668555-70119014-bg.jpeg";

const ModalExportVideo = (props: ModalReloadAlertProps) => 
{
	const { isModalExportOpen } = props;
	const [ form ] = Form.useForm();
	const navigate = useNavigate();

	const onCloseModal = () => 
	{
		props?.actSetModalExportVideoOpen(false);
	};

	const onExport = () => 
	{
		onCloseModal();
		props?.actSetIsExporting(true);
		navigate({ pathname: "/export-fail" });
	};

	return (
		<Modal
			open={isModalExportOpen}
			onCancel={onCloseModal}
			className='custom-modal'
			footer={null}
			style={{ top: "10vh" }}
			maskClosable={false}
			width='45rem'
		>
			<ModalExportVideoWrapper>
				<div className='title'>Export Video</div>

				<div className='video'>
					<img src={VIDEO_THUMBNAIL} alt='' />

					<div className='detail'>
						<div className='duration'>03:33</div>
						<div className='capacity'>140MB</div>
					</div>
				</div>

				<Form form={form} layout='vertical'>
					<Form.Item label='Quality'>
						<Select
							placeholder='Select video quality'
							className='custom-select'
							size='large'
							options={[
								{
									value : "720",
									label : "720 HD"
								},
								{
									value : "1024",
									label : "1024 Full HD"
								},
								{
									value : "2048",
									label : "2048 2K"
								}
							]}
						/>
					</Form.Item>
				</Form>

				<Button
					type='primary'
					className='primary-btn'
					style={{ width: "100%", marginTop: "1rem" }}
					size='large'
					onClick={onExport}
				>
					Export
				</Button>
			</ModalExportVideoWrapper>
		</Modal>
	);
};

export default connect(
	(state: RootState) => ({
		isModalExportOpen : state?.Preview?.isModalExportOpen
	}),
	{ actSetModalExportVideoOpen, actSetIsExporting }
)(ModalExportVideo);
