export interface ModalReloadAlertProps {
    isModalExportOpen: boolean;
    actSetModalExportVideoOpen: (payload: boolean) => any;
    actSetIsExporting: (payload: boolean) => any;
  }
