import styled from "styled-components";

const ModalExportVideoWrapper = styled.div`
  .title {
    font-size: 1.8rem;
    color: white;
    font-weight: 500;
    text-align: center;
    margin: 1rem 0 2rem 0;
  }

  .video {
    border-radius: 1rem;
    overflow: hidden;
    height: 23rem;
    margin-bottom: 2rem;
    position: relative;

    img {
      background-size: cover;
      width: 100%;
      height: 100%;
    }

    .detail {
      position: absolute;
      right: 0;
      left: 0;
      bottom: 0;
      width: 100%;
      padding: 1rem 1rem;
      display: flex;
      justify-content: space-between;
      background-color: rgba(0, 0, 0, 0.2);

      .duration {
        font-size: 1.1rem;
        color: white;
      }

      .capacity {
        font-weight: 500;
        font-size: 1.1rem;
        color: white;
      }
    }
  }
`;

export { ModalExportVideoWrapper };
