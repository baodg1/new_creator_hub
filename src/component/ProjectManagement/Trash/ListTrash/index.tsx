import CardProject from "@/component/MyProject/Card";
import React, { useEffect, useState } from "react";
import { ListTrashWrapper } from "./style";
import { Button, Col, Row } from "antd";
import { Content } from "antd/lib/layout/layout";
import { ReloadOutlined } from "@ant-design/icons";
import { trashAPI } from "@/services/trashAPI";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect, useDispatch } from "react-redux";
import { RootState } from "@/redux/reducer";

import { actLoadTrash } from "@/redux/action/trash";
import { success } from "@/component/Message";
import { IProject } from "@/redux/interface";

const ListTrash = () => 
{
	const dispatch = useDispatch();
	const [ lstTrash, setLstTrash ] = useState<Array<IProject>>([]);
	const [ limit ] = useState(20);
	const fetchMoreTrashedProjects = async () => 
	{
		// gọi api lấy danh sách thùng rác set vào lstTrash
		const res = await trashAPI.getTrashedProject(
			limit,
			lstTrash.length > 0 ? lstTrash.length - 1 : 0
		);

		setLstTrash(res?.data);
		dispatch(actLoadTrash(res.total));
	};
	const reverseProject = async (id: number) => 
	{
		const res = await trashAPI.reverseProject(id);

		if (res?.success) 
		{
			success(res.message!);
			setLstTrash(lstTrash.filter((x) => x.id !== id));
		}
	};

	useEffect(() => 
	{
		fetchMoreTrashedProjects();
	}, []);

	return (
		<ListTrashWrapper className='trash-container'>
			<Content>
				<InfiniteScroll
					next={fetchMoreTrashedProjects}
					hasMore={false}
					dataLength={lstTrash.length}
					loader={<h4>Loading...</h4>}
					style={{ width: "100%" }}
				>
					<Row gutter={[ 24, 24 ]}>
						{lstTrash.map((value) => (
							<Col span={6} key={value.id}>
								<CardProject
									title={value?.name ?? `${value.id }`}
									imgSrc={value?.thumbnail_url}
									description={value?.updated_at}
									model={value}
									action={
										<Button
											type='text'
											icon={
												<ReloadOutlined
													style={{ transform: "scaleX(-1) rotate(-45deg)" }}
												/>
											}
											onClick={() => reverseProject(value.id)}
										/>
									}
								/>
							</Col>
						))}
					</Row>
				</InfiniteScroll>
			</Content>
		</ListTrashWrapper>
	);
};

const mapStateToProps = (state: RootState) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ListTrash);
