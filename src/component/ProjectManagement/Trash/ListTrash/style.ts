import styled from "styled-components";

export const ListTrashWrapper = styled("div")`
  display: flex;
  height: 100%;
  width: 100%;
  overflow-y: auto;
  .infinite-scroll-component {
    padding: 10px;
  }
`;
