import React from "react";
import { EmptyTrashWrapper } from "./style";
import emptyTrash from "@/asset/empty-trash.svg";

const EmptyTrash = () => 
{
	return (
		<EmptyTrashWrapper>
			<img src={emptyTrash} alt='' className='img-empty-trash' />
			<p className='txt-title'>Trash is empty!</p>
			<p className='txt-description'>
				All deleted projects are remained here for next 30 days
			</p>
		</EmptyTrashWrapper>
	);
};

export default EmptyTrash;
