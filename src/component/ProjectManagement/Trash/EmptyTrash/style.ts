import styled from "styled-components";

export const EmptyTrashWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;

  .img-empty-trash {
    width: 320px;
    height: 280px;
  }
  .btn-create-video {
    border-radius: 8px;
    width: 240px;
    height: 56px;
  }

  .txt-title {
    margin-top: 60px;
    margin-bottom: 8px;
    font-weight: 600;
    font-size: 28px;
    line-height: 42px;
  }

  .txt-description {
    font-weight: 400;
    font-size: 20px;
    line-height: 30px;
    margin-bottom: 40px;
  }
`;
