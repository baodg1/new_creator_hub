import { RootState } from "@/redux/reducer";
import { useSelector } from "react-redux";
import EmptyTrash from "./EmptyTrash";
import ListTrash from "./ListTrash";
import { TrashComponentWrapper } from "./style";

const TrashComponent = () => 
{
	const countTrash = useSelector((state: RootState) => state?.Trash.countTrash);

	return (
		<TrashComponentWrapper>
			{countTrash > 0 || countTrash === -1 ? <ListTrash /> : <EmptyTrash />}
		</TrashComponentWrapper>
	);
};

export default TrashComponent;
