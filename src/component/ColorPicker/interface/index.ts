export interface ColorIconProps {
    color?: string;
  }
export interface ColorPickerProps {
    value: any;
    initialValue: string;
    setValue: (value: any) => void;
    hideText?: boolean;
    disabled?: boolean;
  }
