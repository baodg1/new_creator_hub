import React, { useState } from "react";
import { Popover, Input } from "antd";
import { SketchPicker } from "react-color";
import ColorIcon from "./ColorIcon";
import { ColorPickerWrapper } from "./style";
import { ColorPickerProps } from "./interface";

const ColorPicker = (props: ColorPickerProps) => 
{
	const { value, setValue, hideText, disabled } = props;
	const [ open, setOpen ] = useState(false);
	const [ color, setColor ] = useState<any>({}); 
	// use to storage local state, 
	// @value is parent's state in string format which can not intepret by <SketchPicker />
	const onChangeComplete = (colorPicker: any) => 
	{
		setColor(colorPicker);
		setValue(colorPicker?.hex);
	};

	const handleOpen = (openPicker: boolean) => 
	{
		!disabled && setOpen(openPicker);
	};

	return (
		<ColorPickerWrapper>
			{!hideText ? (
				<Input
					onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
						setValue(event?.target?.value)
					}
					value={value}
					size='small'
					className='custom-input'
				/>
			) : null}

			<Popover
				content={
					<SketchPicker onChangeComplete={onChangeComplete} color={color} />
				}
				trigger='click'
				open={open}
				onOpenChange={handleOpen}
				overlayClassName='color-picker-popver'
			>
				<div
					className='icon'
					style={{ cursor: disabled ? "not-allowed" : "pointer" }}
				>
					<ColorIcon color={value} />
				</div>
			</Popover>
		</ColorPickerWrapper>
	);
};

export default ColorPicker;
