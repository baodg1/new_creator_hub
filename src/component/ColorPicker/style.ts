import styled from "styled-components";

const ColorPickerWrapper = styled.div`
  display: flex;
  align-items: center;

  .custom-input {
    margin-right: 1rem;
    padding: 0.5rem 1rem;
    font-size: 1.1rem;
    border: transparent !important;
    width: 8rem;
  }

  .icon {
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .text {
    font-size: 1.2rem;
    color: white;
    margin-right: 1rem;
  }
`;

export { ColorPickerWrapper };
