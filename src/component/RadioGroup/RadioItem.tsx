import { Radio } from "antd";
import { RadioItemProps } from "./interface/RadioGroup";
import { RadioItemWrapper } from "./style";

const RadioItem = (props: RadioItemProps) => 
{
	const { value, label, subElement: SubElement } = props;

	return (
		<RadioItemWrapper>
			<Radio value={value}>{label}</Radio>

			{SubElement && <div className='sub-element'>{SubElement}</div>}
		</RadioItemWrapper>
	);
};

export default RadioItem;
