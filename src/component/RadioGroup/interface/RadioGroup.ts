export interface RadioGroupProps {
    options: RadioItemProps[];
    value?: any;
    setValue: (value: any) => void;
  }

export interface RadioItemProps {
    value: any;
    label: string;
    subElement?: JSX.Element;
  }
