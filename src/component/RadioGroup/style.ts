import styled from "styled-components";

const RadioGroupWrapper = styled.div`
  border: 1px solid var(--color-gray);
  border-radius: 0.8rem;
  display: flex;
  flex-direction: column;
`;

const RadioItemWrapper = styled.div`
  width: 100%;
  padding: 1.5rem 1.5rem;
  display: flex;

  .ant-radio-wrapper {
    color: white;
    font-size: 1.3rem;
  }

  &:not(:last-of-type) {
    border-bottom: 1px solid var(--color-gray);
  }

  .sub-element {
    width: 100%;
    display: flex;
    justify-content: flex-end;
    margin-left: auto;
  }
`;

export { RadioGroupWrapper, RadioItemWrapper };
