import { Radio, RadioChangeEvent } from "antd";
import { RadioGroupProps, RadioItemProps } from "./interface/RadioGroup";
import RadioItem from "./RadioItem";
import { RadioGroupWrapper } from "./style";

const { Group } = Radio;

const RadioGroup = (props: RadioGroupProps) => 
{
	const { options, value, setValue } = props;

	const onChange = (event: RadioChangeEvent) => 
	{
		setValue(event.target.value);
	};

	return (
		<RadioGroupWrapper>
			<Group value={value} onChange={onChange}>
				{options?.map((item: RadioItemProps, index: number) => (
					<RadioItem
						value={item?.value}
						label={item?.label}
						subElement={item?.subElement}
						key={index}
					/>
				))}
			</Group>
		</RadioGroupWrapper>
	);
};

export default RadioGroup;
