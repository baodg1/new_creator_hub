import { HeadingProps } from "./interface/Heading";
import { HeadingWrapper } from "./style";

const Heading = (props: HeadingProps) => 
{
	const { title, isLarge } = props;

	return (
		<HeadingWrapper
			isLarge={isLarge}
			style={props.style}
			className={props.className}
		>
			{title}
		</HeadingWrapper>
	);
};

export default Heading;
