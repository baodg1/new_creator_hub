export interface HeadingProps {
    title: string;
    isLarge?: boolean;
    style?: any;
    className?: string;
  }
