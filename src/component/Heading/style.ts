import styled from "styled-components";

interface HeadingWrapperProps {
  isLarge?: boolean;
}

const HeadingWrapper = styled.div`
  color: ${(props: HeadingWrapperProps) =>
		(props?.isLarge ? "white" : "var(--color-text-gray)")};
  font-size: ${(props: HeadingWrapperProps) =>
		(props?.isLarge ? "1.8rem" : "1.2rem")};
  font-weight: 500;
`;

export { HeadingWrapper };
