import styled from "styled-components";

const SnackBarWrapper = styled("div")`
    display: flex;
    align-items: center;
    flex: 1;
    .message{
        color: #DEDEDE;
        font-weight: 400;
        font-size: 14px;
        line-height: 20px;
        flex: 1;
    }
    .ant-btn{
        color: #25F4EE;
        font-weight: 600;
        font-size: 14px;
        line-height: 20px;
    }
`;

export { SnackBarWrapper };
