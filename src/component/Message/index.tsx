import { message, notification } from "antd";
import { SnackBarWrapper } from "./style";

const destroy = () => 
{
	message.destroy();
};

export const success = (content: string) => 
{
	destroy();
	message.open({
		type : "success",
		content
	});
};

export const error = (content: string) => 
{
	destroy();
	message.open({
		type : "error",
		content
	});
};

export const warning = (content: string) => 
{
	destroy();
	message.open({
		type : "warning",
		content
	});
};

export const snackbar = (content: string, element: JSX.Element) => 
{
	notification.open({
		duration  : 5,
		placement : "bottom",
		closeIcon : (<></>),
		message : (
			<SnackBarWrapper>
				<div className='message'>{content}</div>
				{element}
			</SnackBarWrapper>
		),
		className : 'apero-notification-snackbar',
		style     : {
			backgroundColor : "#262935",
			boxShadow       : "none",
			width           : "auto",
			borderRadius    : "8px"
		}
	});
};
