import { ITrack } from "@/redux/interface";
import { IFile } from "@/redux/interface/file";

export interface ResetEditDataProps {
    actSaveGetListFile: (payload: IFile[]) => any;
    actSetListTrack: (payload: ITrack[]) => any;
    children: JSX.Element;
  }
