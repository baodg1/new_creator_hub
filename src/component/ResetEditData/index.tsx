import { connect } from "react-redux";
import { RootState } from "@/redux/reducer";
import { actSaveGetListFile } from "@/redux/action/file";
import { actSetListTrack } from "@/redux/action/app";
import { useEffect } from "react";
import { ResetEditDataProps } from "./interface/ResetEditData";

const ResetEditData = (props: ResetEditDataProps) => 
{
	const { children } = props;

	useEffect(() => 
	{
		props.actSaveGetListFile([]);
		props.actSetListTrack([]);
	}, []);

	return children;
};

export default connect((state: RootState) => ({}), {
	actSaveGetListFile,
	actSetListTrack
})(ResetEditData);
