import { Button, Modal } from "antd";
import styled from "styled-components";

const ProfileUserComponentWrapper = styled("div")`
  font-size: 16px;
  .profile-title-container {
    padding-bottom: 15px;
    .profile-title {
      font-weight: 600;
      font-size: 28px;
      line-height: 42px;
    }
  }
  .profile-detail-container {
    padding: 25px 0;
  }

  .pf-photo-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .ant-avatar-string {
      font-weight: 600;
      font-size: 28px;
    }
    margin-bottom: 32px;
  }
  .pf-photo-title {
    margin-bottom: 16px;
  }

  .pf-detail-title-style {
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 8px;
  }
  .border-bottom {
    border-bottom: 1px solid rgba(0, 0, 0, 0.15);
  }
  .w-2-3 {
    width: 66.66%;
  }
  .d-flex {
    display: flex;
    align-items: center;
  }
  .flex-1 {
    flex: 1;
  }
  .btn-secondary{
    width: 120px;
    /* margin-left: 16px; */
    border-radius: 8px;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    border: none;
    background: #edecec;
    color: rgba(0, 0, 0, 0.6);
  }
`;

const ModalConfirmResetPassWrapper = styled(Modal)`
  text-align: center;
  padding: 40px;
  .title{
    color: #131317;
    font-weight: 600;
    font-size: 22px;
    line-height: 33px;
    margin: 24px 0 8px 0;
  }
  .sub-title{
    color: #737373;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 40px;
  }
  .d-flex {
    display: flex;
    align-items: center;
  }
  .flex-column{
    flex-direction: column;
  }
  .flex-1{
    flex: 1;
  }
  .w-100{
    width: 100%;
  }
  .hoz-space-16{
    width: 16px;
  }
  .bold{
    color: #131317;
    font-weight: 600;
  }
`;

const ButtonDefaultWrapper = styled(Button)`
  background: #fff;
  border-radius: 8px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #131317;
  width: 100%;
  border: 1px solid #A0A0A0;
  &:hover,
  &:focus {
  }
`;
const ButtonPrimaryWrapper = styled(Button)`
  background: #fe2c55;
  border-radius: 8px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  width: 100%;
  border: 1px solid #fe2c55;
  &:hover,
  &:focus {
    background: #fe2c55;
    color: #ffffff;
    border: 1px solid #fe2c55;
  }
`;

export { 
	ProfileUserComponentWrapper, 
	ModalConfirmResetPassWrapper, 
	ButtonPrimaryWrapper, 
	ButtonDefaultWrapper 
};
