import ViewEditFieldComponent from "../ViewEditField";
import { UserInfomationComponentWrapper } from "./style";
import { useState } from "react";
import authAPI from "@/services/authAPI";
import { error, success } from "@/component/Message";
import { IUserInfomationComponent } from "../interface/UserInfomation";
import { Lang } from "@/utils/config";
import { MAX_FULLNAME_LENGTH, MAX_MAIL_LENGTH, MAX_PASSWORD_LENGTH, PLATFORM_SOCIAL_SERVICE } from "@/config/constant";

const UserInfomationComponent = (data: IUserInfomationComponent) => 
{
	const { user, handleUpdate, handleClickCallback } = data;
	const isNotPass = user && 
	(user.platform === PLATFORM_SOCIAL_SERVICE.GOOGLE || user.platform === PLATFORM_SOCIAL_SERVICE.FACEBOOK)
	&& !user.password;
	const [ loading, setLoading ] = useState(false);
	const [ reqMailError, setReqMailError ] = useState(false);

	/**
	 *
	 * @param values
	 */
	const onFinish = async (values: any, field: string) => 
	{
		setLoading(true);
		const param = {
				email      : user.email,
				avatar     : user.avatar,
				// eslint-disable-next-line camelcase
				first_name : user.first_name,
				// eslint-disable-next-line camelcase
				full_name  : user.full_name
			},
			newParam = { ...param, ...values };

		const res = await authAPI.putUserInfo(newParam);

		if (res && res.success) 
		{
			success(res?.message || "Update user profile successfully.");
			handleUpdate();
		}
		else 
		{
			error(res?.message || "Update user profile failed.");
		}
		setLoading(false);
	};

	/**
   * Xử lý click Reset password
   */
	const handleClickResetPass = () => 
	{
		const mail = user.email;

		if (mail) 
		{
			handleClickCallback("resetpass");
			setReqMailError(false);
		}
		else 
		{
			setReqMailError(true);
		}
	};
  
	return (
		<UserInfomationComponentWrapper className='pf-detail-user-info-body'>
			<div className='pf-detail-user-area'>
				<div className='pf-detail-title-style'>{Lang.trans("name")}</div>
				<ViewEditFieldComponent
					key='user-name'
					content={user?.full_name}
					editable
					field='full_name'
					onFinish={onFinish}
					loading={loading}
					maxLength={MAX_FULLNAME_LENGTH}
				/>
			</div>
			<div className='pf-detail-user-area'>
				<div className='pf-detail-title-style'>{Lang.trans("password")}</div>
				<ViewEditFieldComponent
					key='user-pass'
					content={"********"}
					editable={!isNotPass}
					field='password'
					onFinish={onFinish}
					loading={loading}
					type='password'
					isCustomeContent={isNotPass}
					customeContent={
						<span className='not-pass'>
							* To add a password to your account for the first time, you will
							need to use the{" "}
							<span onClick={handleClickResetPass} className='reset-action'>
								password reset
							</span>{" "}
							so we can verify your identity by your email.
						</span>
					}
					isCustomeClickEdit
					onCustomeclickEdit={handleClickResetPass}
					maxLength={MAX_PASSWORD_LENGTH}
				/>
			</div>
			<div className='pf-detail-user-area'>
				<div className='pf-detail-title-style'>Email</div>
				<ViewEditFieldComponent
					key='user-email'
					content={user?.email}
					editable
					field='email'
					onFinish={onFinish}
					loading={loading}
					rules={[
						{
							type    : "email",
							message : Lang.trans("invalid")
						}
					]}
					maxLength={MAX_MAIL_LENGTH}
					isCustomeContent={!user?.email}
					customeContent={(<span className='add-your-email'>{Lang.trans("profile_add_your_mail")}</span>)}
				/>

				{reqMailError &&
					<div className='error-not-email-container'>
						<div className='message'>
							{Lang.trans("profile_require_email_before_reset_pass")}
						</div>
						<div className='require-icon'>*</div>
					</div>
				}
				
			</div>
		</UserInfomationComponentWrapper>
	);
};

export default UserInfomationComponent;
