import styled from "styled-components";

const UserInfomationComponentWrapper = styled("div")`
  &.pf-detail-user-info-body {
    .pf-detail-user-area {
      margin-bottom: 16px;
    }
    .pf-detail-user-input {
      width: 100%;
      display: flex;
      align-items: center;
      .pfd-editing-area {
        width: 100%;
        display: flex;
        .ant-form {
          display: flex;
          flex: 1;
        }
        .input-detail {
          flex: 1;
        }
      }
      .grp-button {
        display: flex;
        .btn-save {
          background: #fe2c55;
          color: #ffffff;
        }
      }
    }
    button:not(.btn-edit-show) {
      width: 120px;
      margin-left: 16px;
      border-radius: 8px;
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
      border: none;
    }
    .btn-default {
      background: #edecec;
      color: rgba(0, 0, 0, 0.6);
    }
    .pfd-input-field {
      flex: 1;
    }
    .pfd-button-edit {
      margin-left: 16px;
      border-radius: 4px;
      button {
        outline: none;
        border: none;
        &:hover {
          background-color: #f5f5f5;
        }
      }
    }
    .not-pass {
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      .reset-action {
        color: #387dde;
        text-decoration: underline;
        cursor: pointer;
      }
    }

    .add-your-email{
      color: #131317;
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
    }

    .error-not-email-container{
      margin: 16px 0;
      padding: 8px 16px;
      background-color: #ffc7c9;
      display: flex;
      align-items: center;
      border-radius: 4px;
      .message {
          flex: 1;
          color: #1a1a1a;
          font-weight: 400;
          font-size: 16px;
          line-height: 24px;
      }
      .require-icon {
          width: 16px;
          text-align: center;
          color: #fe2c55;
      }
    }
  }
`;

export { UserInfomationComponentWrapper };
