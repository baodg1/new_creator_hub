import { Avatar, Button, Image } from "antd";
import { useEffect, useState } from "react";
import { ButtonDefaultWrapper, ButtonPrimaryWrapper, ModalConfirmResetPassWrapper, ProfileUserComponentWrapper } from "./style";
import authAPI from "@/services/authAPI";
import UserInfomationComponent from "./UserInfomation";
import UserProfileServiceComponent from "./UserProfileService";
import UserResetPassComponent from "./ResetPassword";
import { error } from "../Message";
import { Lang } from "@/utils/config";
import GoldKeyIcon from "@/asset/gold-key.svg";
import { PLATFORM_SOCIAL_SERVICE } from "@/config/constant";

const ProfileUserComponent = () => 
{
	const [ user, setUser ] = useState<any>(null);
	const [ openResetForm, setOpenResetForm ] = useState(false);
	const [ openResetQuestionForm, setOpenResetQuestionForm ] = useState(false);

	/**
   * Lấy thông tin người dùng
   */
	const fetchUser = async () => 
	{
		const res = await authAPI.getUserInfo();

		if (res.success && res.user) 
		{
			const userInfo = res.user;

			if (!userInfo.full_name) 
			{
				// Nếu trường hợp người dùng đăng nhập bằng email và chưa có tên mặc định
				// Xử lý tên = đầu email + id
				if (userInfo.platform === "email") 
				{
					const mail = userInfo.email || "";

					userInfo["full_name"] = `${mail.split("@")[0]}${userInfo.id}`;
				}
				else if (userInfo.platform === PLATFORM_SOCIAL_SERVICE.GOOGLE 
					|| userInfo.platform === PLATFORM_SOCIAL_SERVICE.FACEBOOK) 
				{
					userInfo["full_name"] = userInfo.first_name;
				}
			}
			setUser(userInfo);
		}
	};

	/**
   *
   */
	useEffect(() => 
	{
		fetchUser();
	}, []);

	/**
   *
   */
	const handleUpdateUser = () => 
	{
		fetchUser();
	};

	/**
   * xử lý hành động reset pass word
   */
	const handleResetpass = async () => 
	{
		try 
		{
			// Call api lấy OTP
			const res = await authAPI.resetPasswordOTP();

			if (res && res.success) 
			{
			// mở form
				setOpenResetForm(true);
			}
			else 
			{
				error(res?.message || "");
			}
		}
		catch (err) 
		{
			// TODO message with middleware
		}
	};

	/**
   * Xử lý callback click, đang dùng cho
   */
	const handleClickCallback = (action: string) => 
	{
		switch (action) 
		{
		case "resetpass":
			setOpenResetQuestionForm(true);
			// handleResetpass();
			break;
		case "disconnect":
			fetchUser();
			break;
		} 
	};
	// const getResetPassQuestion = () => 
	// {
	// 	let mes = Lang.trans("reset_password_message");

	// 	if (mes)
	// 	{
	// 		mes = mes.replace("{0}", user?.email);
	// 	}
		
	// 	return <span>{mes}</span>;
	// };
	
	return (
		<ProfileUserComponentWrapper>
			<div className='profile-title-container border-bottom'>
				<div className='profile-title'>Personal Information</div>
				<div className='profile-title-btn-group' />
			</div>
			<div className='profile-detail-container border-bottom w-2-3'>
				<div className='profile-detail-body'>
					<div className='pf-photo-container'>
						<div className='pf-photo-body'>
							<div className='pf-photo-title pf-detail-title-style'>
								Profile Photo
							</div>
							<div className='pf-photo-content'>
								<div className='pf-photo-area'>
									<Avatar
										size={160}
										src={user?.avatar ? user.avatar : ""}
										className='imgLogoUser mr-18'
									>
										{user?.avatar
											? ""
											: user?.email
												? user.email[0].toUpperCase()
												: ""}
									</Avatar>
								</div>
								<div className='pf-photo-button'>
									<Button
										key={`btn-change-avatar`}
										onClick={() => {}}
										className='btn-secondary btn-default'
										size='large'
									>
										Change
									</Button>
								</div>
							</div>
						</div>
					</div>
					<div className='pf-detail-user-info-container'>
						<UserInfomationComponent
							user={user}
							handleUpdate={handleUpdateUser}
							handleClickCallback={handleClickCallback}
						/>
					</div>
				</div>
			</div>
			<div className='profile-service-container w-2-3'>
				<div className='profile-service-body'>
					{user?.platform !== "email" && (
						<UserProfileServiceComponent
							key='login-service'
							user={user}
							title='Services that you use to log in to CreatorHub'
							handleClickCallback={handleClickCallback}
						/>
					)}
				</div>
			</div>
			
			<UserResetPassComponent
				key='reset-pass'
				handleUpdate={() => {}}
				open={openResetForm}
				user={user}
				handleCloseForm={() => 
				{
					setOpenResetForm(false);
					fetchUser();
				}}
			/>

			<ModalConfirmResetPassWrapper
				key='reset-pass-question'
				open={openResetQuestionForm}
				afterClose={() => setOpenResetQuestionForm(false)}
				footer={null}
				width={600}
				onCancel={() => 
				{
					setOpenResetQuestionForm(false);
				}}
			>
				<div className='question-reset-pass-container'>
					<div className='form-body d-flex flex-column'>
						<div className='icon-container'>
							<Image preview={false} src={GoldKeyIcon} width={160} height={160}/>
						</div>
								
						<div className='w-100 title'>
							{Lang.trans("reset_password")}
						</div>
								
						<div className='w-100 sub-title'>
							<span>{Lang.trans("reset_password_message_1")}</span>
							<span className='bold'> "{user?.email}" </span>
							<span>{Lang.trans("reset_password_message_2")}</span>
						</div>
						<div className='btn-group w-100 mt-40 d-flex'>
							<ButtonDefaultWrapper className='flex-1' size='large' onClick={() => setOpenResetQuestionForm(false)}>
								{Lang.trans("cancel")}
							</ButtonDefaultWrapper>
							<div className='hoz-space-16' />
							<ButtonPrimaryWrapper className='flex-1' size='large' onClick={() => 
							{
								handleResetpass();
								setOpenResetQuestionForm(false);
							}}
							>
								{Lang.trans("ok")}
							</ButtonPrimaryWrapper>
						</div>
					</div>
				</div>
			</ModalConfirmResetPassWrapper>
		</ProfileUserComponentWrapper>
	);
};

export default ProfileUserComponent;
