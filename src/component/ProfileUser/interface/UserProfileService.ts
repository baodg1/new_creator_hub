export interface IUserProfileServiceComponentProps {
    key: string;
    title?: string;
    user?: any;
    handleClickCallback: (action: string) => any;
  }
