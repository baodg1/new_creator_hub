import React from "react";

export interface IViewEditFieldComponentProps {
    key: string;
    content?: string;
    isCustomeContent?: boolean;
    customeContent?: React.ReactElement;
    editable?: boolean;
    field: string;
    maxLength: number;
    loading?: boolean;
    rules?: Array<{}>;
    type?: string;
    isCustomeClickEdit?: boolean;
    onCustomeclickEdit?: () => any;
    onFinish: (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
      field: string
    ) => any;
  }
