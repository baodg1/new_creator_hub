export interface IUserResetPassComponent {
    user?: any;
    open?: boolean;
    handleUpdate: () => any;
    handleCloseForm: () => any;
  }
