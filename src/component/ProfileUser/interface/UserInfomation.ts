export interface IUserInfomationComponent {
    user?: any;
    handleUpdate: () => any;
    handleClickCallback: (action: string) => any;
  }
