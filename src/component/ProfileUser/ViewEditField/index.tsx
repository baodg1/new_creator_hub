import pencilIcon from "@/asset/edit-pencil.svg";
import { Lang } from "@/utils/config";
import { Button, Form, Image, Input } from "antd";
import { Content } from "antd/lib/layout/layout";
import { useState } from "react";
import { IViewEditFieldComponentProps } from "../interface/ViewEditField";

const ViewEditFieldComponent = (props: IViewEditFieldComponentProps) => 
{
	const {
		editable,
		loading,
		onFinish,
		field,
		rules,
		content,
		isCustomeContent,
		customeContent,
		isCustomeClickEdit,
		maxLength,
		onCustomeclickEdit
	} = props;
	const [ editing, setEditing ] = useState(false);

	/**
   * Xử lý action hủy
   */
	const handleCancel = () => 
	{
		setEditing(false);
	};

	/**
   *
   * @param values
   */
	const onFormFinish = async (values: any) => 
	{
		await onFinish(values, field);
		setEditing(false);
	};

	return (
		<div className='pf-detail-user-input'>
			<div className='pfd-input-field'>
				{editing && editable ? (
					<div className='pfd-editing-area'>
						<Form
							name='basic'
							autoComplete='off'
							onFinish={onFormFinish}
							size='large'
						>
							<Form.Item
								className='input-detail'
								name={field}
								rules={[
									{
										required : true,
										message  : Lang.trans("invalid")
									},
									...(rules || [])
								]}
								hasFeedback
							>
								<Input
									className='input-item'
									size='large'
									key={`txt${ props.key}`}
									defaultValue={content}
									onFocus={(e) => e.target.select()}
									maxLength={maxLength}
									autoFocus
								/>
							</Form.Item>
							<Form.Item className='grp-button'>
								<Button
									key={`btn-cancel-${ props.key}`}
									onClick={() => 
									{
										handleCancel();
									}}
									className='btn-cancel btn-default '
								>
									{Lang.trans("cancel")}
								</Button>
								<Button
									key={`btn-save-${ props.key}`}
									className='btn-save'
									htmlType='submit'
									size='large'
									loading={loading}
								>
									{Lang.trans("save")}
								</Button>
							</Form.Item>
						</Form>
					</div>
				) : (
					<Content>{isCustomeContent ? customeContent : content}</Content>
				)}
			</div>

			{editable && !editing && (
				<div className='pfd-button-edit'>
					<Button
						icon={
							<Image
								src={pencilIcon}
								preview={false}
								onClick={() => 
								{
									isCustomeClickEdit && onCustomeclickEdit != null
										? onCustomeclickEdit()
										: setEditing(true);
								}}
							/>
						}
						key={`btn-${ props.key}`}
						disabled={editing}
						className='btn-edit-show'
					/>
				</div>
			)}
		</div>
	);
};

export default ViewEditFieldComponent;
