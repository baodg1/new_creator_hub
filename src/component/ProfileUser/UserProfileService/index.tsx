import facebookIcon from "@/asset/facebook.svg";
import googleIcon from "@/asset/google.svg";
import { Avatar, Button, Image } from "antd";
import { useState } from "react";
import authAPI from "@/services/authAPI";
import { success } from "@/component/Message";
import { IUserProfileServiceComponentProps } from "../interface/UserProfileService";
import { ModalUserServiceWrapper, UserProfileServiceWrapper } from "./style";
import { Lang } from "@/utils/config";
import { ButtonDefaultWrapper, ButtonPrimaryWrapper } from "../style";
import DeleteIcon from "@/asset/delete-modal-icon.svg";
import { PLATFORM_SOCIAL_SERVICE } from "@/config/constant";

const UserProfileServiceComponent = (
	props: IUserProfileServiceComponentProps
) => 
{
	const { key, title, user, handleClickCallback } = props;
	const { platform } = user || {};
	const [ validate, setValidate ] = useState(true);
	const [ openModalConfirm, setOpenModalConfirm ] = useState(false);

	/**
   * Ngắt kết nối dịch vụ
   */
	const handleDisconnect = async () => 
	{
		const isValidate = user.password;

		// Kiểm tra có pass chưa
		if (isValidate) 
		{
			setValidate(true);
			// TODO ngắt kết nối
			const res = await authAPI.userProfileDisconnectService();

			if (res && res.success) 
			{
				success(Lang.trans("disconnect_social_successfully"));
				handleClickCallback("disconnect");
			}
			else 
			{
				success(res?.message || "");
			}
		}
		else 
		{
			setValidate(false);
		}
	};

	return (
		<UserProfileServiceWrapper>
			<div className='profile-service-item-body'>
				<div className='profile-service-item-message'>{title}</div>
				<div className='profile-service-item-info'>
					<div className='profile-service-item-info-body d-flex'>
						<div className='profile-service-item-info-area flex-1 d-flex'>
							<div className='profile-service-item-info-logo'>
								<Avatar
									size={80}
									src={
										<Image
											src={
												platform === PLATFORM_SOCIAL_SERVICE.GOOGLE
													? googleIcon
													: platform === PLATFORM_SOCIAL_SERVICE.FACEBOOK
														? facebookIcon
														: ""
											}
											className='img-service-avatar'
											preview={false}
										/>
									}
									className='service-avatar mr-18'
								/>
							</div>
							<div className='profile-service-item-info-detail flex-1'>
								<div className='profile-service-item-info-branch'>
									{platform === PLATFORM_SOCIAL_SERVICE.GOOGLE
										? "Google"
										: platform === PLATFORM_SOCIAL_SERVICE.FACEBOOK
											? "Facebook"
											: ""}
								</div>
								<div className='profile-service-item-info-account'>
									{platform === PLATFORM_SOCIAL_SERVICE.GOOGLE ? user?.email : user?.full_name}
								</div>
							</div>
						</div>
						<div className='profile-service-item-info-action'>
							<Button
								key={`btn-disconnect-${ key}`}
								onClick={(e) => 
								{
									if (user.password)
									{
										setOpenModalConfirm(true);
									}
									else 
									{
										setValidate(false);
									}
									
								}}
								className='btn-disconnect btn-default btn-secondary'
								size='large'
							>
								{Lang.trans("disconnect")}
							</Button>
						</div>
					</div>
					<div className='profile-service-item-info-footer'>
						{!validate && (
							<div className='ps-validate'>
								<div className='ps-error-message'>
									{Lang.trans("disconnect_service_login_error")}
								</div>
								<div className='ps-require-icon'>*</div>
							</div>
						)}
					</div>
				</div>
			</div>

			<ModalUserServiceWrapper
				key='user-service-modal'
				open={openModalConfirm}
				afterClose={() => setOpenModalConfirm(false)}
				footer={null}
				width={600}
				onCancel={() => 
				{
					setOpenModalConfirm(false);
				}}
			>
				<div className='question-reset-pass-container'>
					<div className='form-body d-flex flex-column'>
						<div className='icon-container'>
							<Image preview={false} src={DeleteIcon} width={128} height={128}/>
						</div>
							
						<div className='w-100 title'>
							{Lang.trans("disconnect_service_login_title")}
						</div>
							
						<div className='w-100 sub-title'>
							<span>{Lang.trans("disconnect_service_login_message_1")}</span>
							<span className='bold'> "{user?.email}" </span>
							<span>{Lang.trans("disconnect_service_login_message_2")}</span>
						</div>
						<div className='btn-group w-100 mt-40 d-flex'>
							<ButtonDefaultWrapper className='flex-1' size='large' onClick={() => setOpenModalConfirm(false)}>
								{Lang.trans("cancel")}
							</ButtonDefaultWrapper>
							<div className='hoz-space-16' />
							<ButtonPrimaryWrapper className='flex-1' size='large' onClick={async () => 
							{
								await handleDisconnect();
								setOpenModalConfirm(false);
							}}
							>
								{Lang.trans("disconnect")}
							</ButtonPrimaryWrapper>
						</div>
					</div>
				</div>
			</ModalUserServiceWrapper>
		</UserProfileServiceWrapper>
	);
};

export default UserProfileServiceComponent;
