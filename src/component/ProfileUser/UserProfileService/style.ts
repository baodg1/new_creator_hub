import { Modal } from "antd";
import styled from "styled-components";

const UserProfileServiceWrapper = styled('div')`
    .profile-service-item-message {
      font-weight: 600;
      font-size: 20px;
      line-height: 30px;
      margin: 18px 0;
    }
    .img-service-avatar {
      width: 80px;
      height: 80px;
    }
    .profile-service-item-info-logo {
      margin-right: 24px;
    }
    .profile-service-item-info-detail {
      line-height: 24px;
    }
    .profile-service-item-info-branch {
      font-weight: 600;
      font-size: 16px;
    }
    .profile-service-item-info-account {
      font-size: 14px;
    }
    .profile-service-item-info-footer {
        margin: 16px 0;
        .ps-validate {
        padding: 8px 16px;
        background-color: #ffc7c9;
        display: flex;
        .ps-error-message {
            flex: 1;
            color: #1a1a1a;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
        }
        .ps-require-icon {
            width: 16px;
            text-align: center;
            color: #fe2c55;
        }
        }
    }
`;
const ModalUserServiceWrapper = styled(Modal)`
    text-align: center;
    padding: 40px;
  .title{
    color: #131317;
    font-weight: 600;
    font-size: 22px;
    line-height: 33px;
    margin: 24px 0 8px 0;
  }
  .sub-title{
    color: #737373;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 40px;
  }
  .d-flex {
    display: flex;
    align-items: center;
  }
  .flex-column{
    flex-direction: column;
  }
  .flex-1{
    flex: 1;
  }
  .w-100{
    width: 100%;
  }
  .hoz-space-16{
    width: 16px;
  }
  .bold{
    color: #131317;
    font-weight: 600;
  }
`;

export {
	UserProfileServiceWrapper,
	ModalUserServiceWrapper
};
