import { Button, Form, Modal } from "antd";
import styled from "styled-components";
import logo from "@/asset/auth/logo.svg";

const ModalWrapper = styled(Modal)`
  &.model-reset-password {
    .content-model {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-align: center;
      .mb-16 {
        margin-bottom: 16px;
      }
    }
  }
  .ant-modal-body {
    padding: 40px 60px;
  }
  .content-body {
    width: 100%;
  }
  .title-forgot {
    font-weight: 600;
    font-size: 28px;
    line-height: 34px;
    color: #313335;
    text-align: center;
  }

  .sub-title-forgot {
    font-size: 16px;
    line-height: 24px;
    text-align: center;
  }
  form.ant-form {
    width: 100%;
  }
  .mt-btn {
    margin-top: 25px !important;
    margin-bottom: 0px !important;
  }
  .ant-form-item-required:before {
    display: none !important;
  }

  .label-color {
    color: #313335 !important;
  }
  .btn-verify {
    margin-top: 60px !important;
  }
  .otp-title-forgot {
    margin-bottom: 25px !important;
  }
  .logo-container {
    width: 100%;
    height: 120px;

    display: flex;
    justify-content: center;
  }
  .font-bold {
    font-weight: 600;
  }
  .success-container {
    padding: 40px;
  }
  .ant-form-item-label{
    color:  #131317;
  }
  .ant-form-item-row .ant-form-item-control{
    text-align: left;
  }
  .mt-40{
    margin-top: 40px;
  }
  .form-success{
    .title-forgot{
      color: #131317;
      font-weight: 600;
      font-size: 22px;
      line-height: 33px;
    }
    .sub-title-forgot{
      color: #737373;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
    }
  }
`;

const ForgotOTPFormWrapper = styled(Form)`
  .resend-container {
    text-align: center;
    margin-top: 15px;
    button {
      padding: inherit;
    }
  }
`;
const OtpInputContainerWrapper = styled("div")`
  .otp-input-container {
    align-items: center;
    justify-content: center;
    column-gap: 24px;
    .otp-input-item {
      width: 80px;
      height: 100px;
      input {
        width: 80px !important;
        height: 100px !important;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        font-weight: 600;
        font-size: 28px;
        outline: none;
        &:focus,
        &:active {
          border-color: #FE2C55 !important;
          background-color: #FFF2F5;
        }
      }
    }
  }
`;

const ButtonSubmitWrapper = styled(Button)`
  background: #fe2c55;
  border-radius: 8px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  width: 100%;
  border: 1px solid #fe2c55;
  &:hover,
  &:focus {
    background: #fe2c55;
    color: #ffffff;
    border: 1px solid #fe2c55;
  }
`;
const AuthLogoWrapper = styled("div")`
  background-image: url(${logo});
  width: 220px;
  height: 60px;
  background-size: cover;
  position: absolute;
  top: 30px;
  left: auto;
`;

export {
	ModalWrapper,
	OtpInputContainerWrapper,
	ForgotOTPFormWrapper,
	ButtonSubmitWrapper,
	AuthLogoWrapper
};
