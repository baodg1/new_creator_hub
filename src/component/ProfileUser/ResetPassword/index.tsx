import { useState } from "react";
import authAPI from "@/services/authAPI";
import {
	AuthLogoWrapper,
	ButtonSubmitWrapper,
	ForgotOTPFormWrapper,
	ModalWrapper,
	OtpInputContainerWrapper
} from "./style";
import { Button, Form, Image } from "antd";
import OtpInput from "react18-input-otp";
import LoginInput from "@/component/Auth/Common/LoginInput/LoginInput";
import successIcon from "@/asset/success-change.svg";
import { error, success } from "@/component/Message";
import { IUserResetPassComponent } from "../interface/ResetPassword";
import { Lang } from "@/utils/config";
import { MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH, OTP_SIZE } from "@/config/constant";

const UserResetPassComponent = (props: IUserResetPassComponent) => 
{
	const RESET_FORM_STATE = {
		OTP        : 1,
		RESET_PASS : 2,
		SUCCESS    : 3
	};
	const { open, user, handleCloseForm } = props;
	const { email } = user || {};
	const [ resetFormState, setResetFormState ] = useState(RESET_FORM_STATE.OTP);
	const [ otp, setOTP ] = useState("");
	const [ loading, setLoading ] = useState(false);

	/**
   *
   * @param enteredOtp
   */
	const handleChange = (enteredOtp: string) => 
	{
		setOTP(enteredOtp);
	};

	/**
   * Gửi lại OTP
   */
	const handleResendCode = async () => 
	{
		// TODO xử lý logic call server
		const res = await authAPI.resetPasswordOTP();

		if (res && res.success) 
		{
			success(Lang.trans("profile_resend_otp_success"));
		}
		else 
		{
			error(res?.data?.message || Lang.trans("profile_resend_otp_failed"));
		}
	};

	/**
   * Verify OTP
   * @param
   */
	const onFinishOTP = async (e: any, value: string) => 
	{
		setLoading(true);
		
		if (value) 
		{
			try 
			{
				// TODO xử lý logic call server
				const res = await authAPI.resetPasswordConfirmOTP(value);

				if (res && res.success) 
				{
					success(Lang.trans("profile_reset_pass_otp_success"));
					setOTP(value);
					setResetFormState(RESET_FORM_STATE.RESET_PASS);
				}
				else 
				{
					error(res?.data?.message || Lang.trans("profile_reset_pass_otp_failed"));
				}
			}
			catch (err) 
			{
				// TODO message with middleware
			}
		}
		else 
		{
			error(Lang.trans("invalid"));
		}
		
		setLoading(false);
	};

	/**
   * Submit changepass
   */
	const onFinishChangePass = async (values: any, password: string) => 
	{
		setLoading(true);

		// TODO xử lý logic call server
		const res = await authAPI.resetPassword(otp, password);

		if (res && res.success) 
		{
			//   success("Reset password successfully", { position: "top-center" });
			setResetFormState(RESET_FORM_STATE.SUCCESS);
		}
		else 
		{
			error(res?.message || "");
		}
		setLoading(false);
	};

	/**
   * Đóng form khi đổi pass thành công
   */
	const onClickSuccess = () => 
	{
		setResetFormState(RESET_FORM_STATE.OTP);
		handleCloseForm();
	};

	const handleAfterModal = () => 
	{
		setResetFormState(RESET_FORM_STATE.OTP);
		setOTP("");
	};
  
	return (
		<ModalWrapper
			className='model-reset-password'
			title=''
			centered
			closable
			open={open}
			afterClose={() => handleAfterModal()}
			footer={null}
			width={550}
			onCancel={() => 
			{
				handleCloseForm();
			}}
		>
			<div className='content-model'>
				{resetFormState !== RESET_FORM_STATE.SUCCESS && 
				<div className='logo-container'>
					<AuthLogoWrapper />
				</div>
				}
				
				<div className='content-body'>
					{resetFormState === RESET_FORM_STATE.OTP && (
						<div className='form-body form-otp'>
							<div className='form-otp-body'>
								<ForgotOTPFormWrapper
									name='basic'
									layout='vertical'
									initialValues={{ remember: true }}
									autoComplete='off'
									onFinish={(e) => 
									{
										onFinishOTP(e, otp);
									}}
									size='large'
								>
									<Form.Item className='otp-title-forgot'>
										<div className='title-forgot label-color'>
											Verify your email address{" "}
										</div>
										<div className='sub-title-forgot label-color'>
											We emailed you the four digit code to{" "}
											<span className='font-bold'>{email}</span> <br />
											Enter the code below to confirm your email address
										</div>
									</Form.Item>
									<Form.Item>
										<OtpInputContainerWrapper>
											<OtpInput
												value={otp}
												onChange={handleChange}
												numInputs={OTP_SIZE}
												className='otp-input-item'
												containerStyle='otp-input-container'
												inputStyle='otp-input'
											/>
										</OtpInputContainerWrapper>
									</Form.Item>
									<Form.Item className='mt-btn btn-verify'>
										<ButtonSubmitWrapper
											htmlType='submit'
											size='large'
											loading={loading}
										>
											VERIFY
										</ButtonSubmitWrapper>
									</Form.Item>
									<div className='resend-container'>
										<span>If you didn’t receive a code! </span>
										<Button type='link' onClick={handleResendCode}>
											Resend code
										</Button>
									</div>
								</ForgotOTPFormWrapper>
							</div>
						</div>
					)}
					{resetFormState === RESET_FORM_STATE.RESET_PASS && (
						<div className='form-body form-rest-pass'>
							<div className='form-reset-pass-body'>
								<Form
									name='basic'
									layout='vertical'
									initialValues={{ remember: true }}
									autoComplete='off'
									onFinish={(e) => 
									{
										onFinishChangePass(e, e.password);
									}}
									size='large'
								>
									<Form.Item>
										<div className='title-forgot label-color'>
											Create your new password
										</div>
									</Form.Item>
									<Form.Item
										className='label'
										label='New Password'
										name='password'
										required={false}
										rules={[
											{ required: true, message: Lang.trans("invalid") },
											{
												max     : MAX_PASSWORD_LENGTH,
												message : Lang.trans("invalid")
											},
											{
												min     : MIN_PASSWORD_LENGTH,
												message : "Password must be at least 6 characters"
											}
										]}
									>
										<LoginInput
											placeholder='Enter your password here'
											size='large'
											maxLength={MAX_PASSWORD_LENGTH}
											type='password'
										/>
									</Form.Item>
									<Form.Item
										className='label'
										label='Confirm New Password'
										name='confirm_password'
										required={false}
										rules={[
											{ required: true, message: Lang.trans("invalid") },
											({ getFieldValue }) => ({
												validator(_, value) 
												{
													if (!value || getFieldValue("password") === value) 
													{
														return Promise.resolve();
													}
													
													return Promise.reject(
														new Error("Password don't match")
													);
												}
											})
										]}
									>
										<LoginInput
											placeholder='Enter your confirm password here'
											size='large'
											maxLength={MAX_PASSWORD_LENGTH}
											type='password'
										/>
									</Form.Item>
									<Form.Item className='mt-btn'>
										<ButtonSubmitWrapper
											htmlType='submit'
											size='large'
											loading={loading}
										>
											Create
										</ButtonSubmitWrapper>
									</Form.Item>
								</Form>
							</div>
						</div>
					)}
					{resetFormState === RESET_FORM_STATE.SUCCESS && (
						<div className='form-body form-success'>
							<div className='form-success-body'>
								<div className='success-container'>
									<Image preview={false} src={successIcon} />
								</div>
								
								<div className='title-forgot'>
									Success
								</div>
								
								<div className='sub-title-forgot'>
									You created new password successfully!
								</div>
								<div className='btn-close mt-40'>
									<ButtonSubmitWrapper size='large' onClick={onClickSuccess}>
										{Lang.trans("ok")}
									</ButtonSubmitWrapper>
								</div>
							</div>
						</div>
					)}
				</div>
			</div>
		</ModalWrapper>
	);
};

export default UserResetPassComponent;
