import React from 'react';

export interface IDashboardLayoutProps {
    children: React.ReactNode;
    menuType: number;
  }
