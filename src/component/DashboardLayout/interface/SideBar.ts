import { MenuProps } from 'antd';
import React from 'react';

type MenuItem = Required<MenuProps>["items"][number];

export interface IDashboardSideBarProps {
    menuType?: number;
  }

export interface IMenuItemProps {
    label: React.ReactNode;
    key: React.Key;
    icon?: React.ReactNode;
    menuType?: number;
    children?: MenuItem[];
    type?: "group";
  }
