import { DropdownButtonWrapper, TopBarWrapper } from "./style";
import { Avatar, Col, Menu, MenuProps, Row } from "antd";
import menuIcon from "@/asset/menu-ic.svg";
import arrowDownIcon from "@/asset/arrow-down-icon.svg";
import imageLogo from "@/asset/logo_white.svg";
import settingIcon from "@/asset/setting-icon.svg";
import questionIcon from "@/asset/question-icon.svg";
import downloadVideoIcon from "@/asset/download-video-icon.svg";
import projectApi from "@/services/projectAPI";

import {
	DownOutlined,
	PictureOutlined,
	PlayCircleOutlined,
	PlusCircleFilled,
	SoundOutlined
} from "@ant-design/icons";
import { CButton } from "@/component/Common";

import { Header } from "antd/lib/layout/layout";
import { useDispatch } from "react-redux";
import { actSaveCreatedProject } from "@/redux/action/project";
import { useNavigate } from "react-router-dom";
import { actUserLogOut } from "@/redux/action/auth";
import { useEffect, useState } from "react";
import authAPI from "@/services/authAPI";
import { error, success } from "@/component/Message";
import { LocalStorage } from "@/utils/config";
import { USER } from "@/config/constant";

const lstMenu = [
	{
		Key    : "1",
		Title  : "Product",
		Childs : [
			{
				key : "1",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Video
					</a>
				),
				icon : <PlayCircleOutlined />
			},
			{
				key : "2",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Audio
					</a>
				),
				icon : <SoundOutlined />
			},
			{
				key : "3",
				label : (
					<a target='_blank' rel='noopener noreferrer' href='/'>
						Convert Image
					</a>
				),
				icon : <PictureOutlined />
			}
		],
		Icon : <DownOutlined />
	},
	{
		Key   : "2",
		Title : "Usecase"
	},
	{
		Key   : "3",
		Title : "Plan & Pricing"
	},
	{
		Key   : "4",
		Title : "Learn"
	}
];
const items1: MenuProps["items"] = lstMenu.map((x) => ({
	key      : x.Key,
	label    : x.Title,
	children : x.Childs ?? [],
	icon     : x.Icon
}));

/**
 * Danh sách menu cho thiết lập người dùng
 */
const userItems = [
	{
		key   : "user-profile",
		label : "Account setting"
	},
	{
		key   : "user-subscription",
		label : "Subscription Plan"
	},
	{
		key   : "user-help",
		label : "Help"
	},
	{
		key   : "user-policy",
		label : "Privacy Policy"
	},
	{
		key   : "user-logout",
		label : "Logout"
	}
];

const TopBar = () => 
{
	const [ createLoading, setCreateLoading ] = useState(false);
	const [ user, setUser ] = useState<any>(null);
	const dispatch = useDispatch();
	const navigate = useNavigate();

	useEffect(() => 
	{
		async function fetchUser() 
		{
			const res = await authAPI.getUserInfo();

			if (res.success === true) 
			{
				setUser(res.user);
			}
		}
		fetchUser();
	}, []);

	/**
   * Xử lý thêm mới dự án
   */
	const handleCreateProject = async () => 
	{
		setCreateLoading(true);
		const res = await projectApi.createProject({}, {});

		if (res && res.success && res.data) 
		{
			success("Create project success!");

			// TODO Link edit view
			dispatch(actSaveCreatedProject(res.data.id));
			navigate(`/edit/${res.data.id}`);
		}
		else 
		{
			error("Create failed.");
		}
		setCreateLoading(false);
	};

	/**
   * Xử lý redirect Thiết lập người dùng
   */
	const handleUserProfile = () => 
	{
		navigate("/dashboard/profileuser");
	};

	/**
   * Xử lý đăng xuất
   */
	const handleLogout = () => 
	{
		dispatch(actUserLogOut());
		LocalStorage.remove(USER.TOKEN);
		navigate("/auth/login");
	};
  
	/**
   * Xử lý sự kiện khi click vào user menu
   * @param e
   */
	const onUserMenuClick: MenuProps["onClick"] = (e) => 
	{
		const key = e.key;

		switch (key) 
		{
		case "user-profile":
			handleUserProfile();
			break;
		case "user-logout":
			handleLogout();
			break;
		}
	};

	return (
		<TopBarWrapper>
			<Header className='header'>
				<Row align='middle' className='row' wrap={false} justify='center'>
					<img src={menuIcon} alt='' className='menu-icon' />
					<img
						src={imageLogo}
						alt=''
						className='imageLogo'
						onClick={() => 
						{
							navigate("/dashboard/myproject");
						}}
					/>
					<Menu
						mode='horizontal'
						// defaultSelectedKeys={["2"]}
						items={items1}
					/>
					<Col flex={1} />
					<img src={settingIcon} alt='' className='header-btn-icon mr-35' />
					<img src={questionIcon} alt='' className='header-btn-icon mr-35' />
					<img
						src={downloadVideoIcon}
						alt=''
						className='header-btn-icon mr-35'
					/>
					<CButton
						label='Create New Video'
						icon={<PlusCircleFilled />}
						type={1}
						className='btn-create-new-video mr-24'
						onClick={handleCreateProject}
						loading={createLoading}
					/>
					<Avatar
						size={50}
						src={user?.avatar ? user.avatar : ""}
						className='imgLogoUser mr-18'
					>
						{user?.avatar ? "" : user?.email ? user.email[0].toUpperCase() : ""}
					</Avatar>

					<DropdownButtonWrapper
						menu={{ items: userItems, onClick: onUserMenuClick }}
						icon={<img src={arrowDownIcon} alt='' width={16} />}
						className='user-dropdown'
					/>
				</Row>
			</Header>
		</TopBarWrapper>
	);
};

export default TopBar;
