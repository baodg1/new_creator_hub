import React from 'react';
import { connect } from "react-redux";
import TopBar from "@/component/DashboardLayout/TopBar";
import SideBar from "@/component/DashboardLayout/SideBar";
import {
	LayoutWrapper,
	LayoutContentWrapper,
	ContentWrapper,
	ContentMainWrapper
} from "./style";
import { IDashboardLayoutProps } from './interface/DashboardLayout';

const DashboardLayout = (props: IDashboardLayoutProps) => 
{
	const { children, menuType } = props;

	return (
		<LayoutWrapper>
			<TopBar />
			<LayoutContentWrapper>
				<SideBar menuType={menuType} />
				<ContentWrapper>
					<ContentMainWrapper>{children}</ContentMainWrapper>
				</ContentWrapper>
			</LayoutContentWrapper>
		</LayoutWrapper>
	);
};

export default connect()(DashboardLayout);
