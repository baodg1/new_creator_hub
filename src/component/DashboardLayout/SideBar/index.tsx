import React, { useState } from 'react';
import {
	SideBarWrapper,
	MenuWrapper,
	IconTitleMenu,
	SideBarContainerWrapper,
	SideBarBodyWrapper
} from "./style";
import homeIcon from "@/asset/home-icon.svg";
import templateIcon from "@/asset/template-icon.svg";
import trashIcon from "@/asset/trash-icon.svg";
import expandIcon from "@/asset/expand-icon.png";
import myProjectIcon from "@/asset/myproject-icon.svg";
import profileUserIcon from "@/asset/profile-user-icon.svg";
import subscriptionPlanIcon from "@/asset/subscription-plan-icon.svg";
import { MenuProps } from "antd";
import { CButton } from "@/component/Common";
import { icCrownSvg } from "@/component/Icon/ic_crown";
import Icon from "@ant-design/icons";
import { useLocation, useNavigate } from "react-router-dom";
import { IDashboardSideBarProps, IMenuItemProps } from '../interface/SideBar';

type MenuItem = Required<MenuProps>["items"][number];

const SideBar = (props: IDashboardSideBarProps) => 
{

	// TODO Logic default này đang không đúng do nếu link đến màn khác đầu tiên thì sẽ không đúng
	const location = useLocation();
	const [ menuSelectedKey ] = useState(location.pathname);
	const [ menuOpenKey ] = useState(location.pathname);
	const navigate = useNavigate();

	const getItem = (
		label: React.ReactNode,
		key: React.Key,
		icon?: React.ReactNode,
		menuType?: number,
		children?: MenuItem[],
		type?: "group"
	) => 
	{
		return {
			label,
			key,
			icon,
			menuType,
			children,
			type
		} as IMenuItemProps;
	};
	const sourceMenu = [
		getItem("Home", "/dashboard/home", <IconTitleMenu src={homeIcon} />, 1),

		getItem(
			"Template",
			"SubTemplate",
			<IconTitleMenu src={templateIcon} />,
			1,
			[ getItem("Template", "/dashboard/template") ]
		),

		getItem(
			"Video Editing",
			"groupvideo",
			null,
			1,
			[
				getItem(
					"My Project",
					"/dashboard/myproject",
					<IconTitleMenu src={myProjectIcon} />
				),
				getItem("Trash", "/dashboard/trash", <IconTitleMenu src={trashIcon} />)
			],
			"group"
		),
		getItem(
			"CMS",
			"groupcms",
			null,
			1,
			[
				getItem(
					"Content Plan",
					"/dashboard/contentplan",
					<IconTitleMenu src={templateIcon} />,
					1,
					[
						getItem("Account", "/account", null, 1, [
							getItem("Connect Account", "/account/connect-account"),
							getItem("Manage Account", "/account/manage-account")
						]),
						getItem("Create Post", "/create-post")
					]
				)
			],
			"group"
		),

		getItem(
			"User Profile",
			"group-userprofile",
			null,
			2,
			[
				getItem(
					"My Profile",
					"/dashboard/profileuser",
					<IconTitleMenu src={profileUserIcon} />
				),
				getItem(
					"Subscription Plan",
					"/dashboard/subscriptionplan",
					<IconTitleMenu src={subscriptionPlanIcon} />
				)
			],
			"group"
		)
	];
	const items: MenuProps["items"] = sourceMenu
		.filter((x) => x.menuType === props.menuType)
		.map((x) => x as MenuItem);

	/**
   * Xử lý sự kiện click menu
   * @param e
   */
	const handleSelectedMenu: MenuProps["onClick"] = (e) => 
	{
		navigate(e.key);
	};

	return (
		<SideBarContainerWrapper>
			<SideBarBodyWrapper>
				<SideBarWrapper width={312}>
					<CButton
						label='Try CreatorHub Pro'
						type={1}
						className='btn-creatorhub'
						icon={
							<Icon
								component={icCrownSvg}
								style={{
									width  : "24px",
									height : "24px"
								}}
							/>
						}
					/>
					<MenuWrapper
						mode='inline'
						triggerSubMenuAction='click'
						expandIcon={({ isOpen }) => (
							<IconTitleMenu
								src={expandIcon}
								style={isOpen ? { transform: "rotate(180deg)" } : {}}
							/>
						)}
						defaultSelectedKeys={[ menuSelectedKey ]}
						defaultOpenKeys={[ menuOpenKey ]}
						items={items}
						onSelect={handleSelectedMenu}
					/>
				</SideBarWrapper>
			</SideBarBodyWrapper>
		</SideBarContainerWrapper>
	);
};

export default SideBar;
