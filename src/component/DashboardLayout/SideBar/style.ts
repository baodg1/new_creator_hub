import styled from "styled-components";
import { Layout, Menu } from "antd";
const { Sider } = Layout;
const SideBarContainerWrapper = styled("div")`
  width: 312px;
  height: 100%;
  overflow: hidden;
  background-color: #fff;
  z-index: 1;
  box-shadow: 4px 0px 4px rgba(0, 0, 0, 0.03);
  position: relative;
`;
const SideBarBodyWrapper = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: auto;
  ::-webkit-scrollbar {
    width: 8px;
    background-color: #fff;
  }
  ::-webkit-scrollbar-thumb {
    background: #c1c1c1;
    border-radius: 6px;
  }
`;
const SideBarWrapper = styled(Sider)`
  width: 312px;
  background-color: #fff;
  padding: 24px;

  .ant-menu-item-group-title {
    color: #a0a0a0;
  }

  .ant-menu-inline {
    border-right: none !important;
  }
`;
const MenuWrapper = styled(Menu)`
  margin-top: 24px;
  margin-bottom: 24px;

  .btn-creatorhub {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .ant-menu-title-content {
    font-weight: 400;
    font-size: 16px;
  }
  .ant-menu-sub.ant-menu-inline {
    background-color: unset;
  }
  .ant-menu-item-selected {
    background-color: #333333 !important;
    .ant-menu-title-content {
      color: #fff !important;
      font-weight: 600 !important;
    }
  }
  .ant-menu-item-group-title {
    font-weight: 400;
    font-size: 16px;
    color: #707070;
  }
  .ant-menu-item {
    border-radius: 8px;
    background-color: unset;
    height: 64px !important;
    &:hover {
    }
    &.ant-menu-item-selected {
      overflow: unset;
    }
    &.ant-menu-item-selected:after {
      opacity: 0;
    }
    &.ant-menu-item-selected:before {
      content: "";
      position: absolute;
      width: 5px;
      height: 24px;
      left: -2px;
      background: #25f4ee;
      border-radius: 999px;
    }
    &.ant-menu-item-selected .ant-menu-item-icon {
      filter: brightness(0) invert(1);
    }
  }
`;

const IconTitleMenu = styled.img`
  width: 24px;
  height: 24px;
  /* &.ant-menu-item-selected img {
    color: linear-gradient(270deg, #7846a0 0%, #b64e89 41.15%, #ec4b62 100%);
  } */
  transition: background 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
    transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
    top 0.3s cubic-bezier(0.645, 0.045, 0.355, 1),
    color 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
`;

export {
	SideBarContainerWrapper,
	SideBarWrapper,
	MenuWrapper,
	IconTitleMenu,
	SideBarBodyWrapper
};
