import styled from "styled-components";

const HeaderWrapper = styled.div`
  background-color: white;
  padding: 2rem 0;

  .main {
    display: flex;
    justify-content: flex-start;

    .logo {
      margin-right: auto;
      height: 2rem;
    }

    .nav {
      display: flex;

      &_item {
        cursor: pointer;
        font-weight: 500;

        &:not(:last-of-type) {
          margin-right: 4rem;
        }

        a {
          color: var(--color-text);
          transition: all 0.1s ease-in-out;

          &:hover {
            color: var(--color-blue);
          }
        }
      }
    }
  }
`;

export { HeaderWrapper };
