import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import logo from "@/asset/logo.png";
import { HeaderWrapper } from "./style";

const Header = (props: any) => 
{
	return (
		<HeaderWrapper>
			<Row justify='center'>
				<Col span={20} xs={22} sm={22} md={21} lg={21} xl={18} xxl={14}>
					<div className='main'>
						<img src={logo} alt='' className='logo' />

						<div className='nav'>
							<div className='nav_item'>
								<Link to='/edit'>Convert</Link>
							</div>
							<div className='nav_item'>
								<Link to='/edit'>Download</Link>
							</div>
							<div className='nav_item'>
								<Link to='/edit'>Compress</Link>
							</div>
							<div className='nav_item'>
								<Link to='/blog'>Blog</Link>
							</div>
							<div className='nav_item'>
								<Link to='/auth/login'>Login</Link>
							</div>
							<div className='nav_item'>
								<Link to='/auth/signup'>Sign up</Link>
							</div>
						</div>
					</div>
				</Col>
			</Row>
		</HeaderWrapper>
	);
};

export default Header;
