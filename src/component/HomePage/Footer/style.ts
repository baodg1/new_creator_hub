import styled from "styled-components";

const FooterWrapper = styled.div`
  background-color: white;
  margin-top: auto;
  padding: 2rem 0;

  .main {
    display: flex;
    flex-direction: column;

    .link {
      display: flex;
      justify-content: space-between;
      padding-bottom: 1rem;

      .nav {
        display: flex;

        &_item {
          cursor: pointer;
          font-size: 1.4rem;

          &:not(:last-of-type) {
            margin-right: 2rem;
          }

          a {
            color: var(--color-text);
            transition: all 0.1s ease-in-out;

            &:hover {
              color: var(--color-blue);
            }
          }
        }
      }

      .social {
        a {
          color: var(--color-text);
        }

        &_icon {
          font-size: 2.3rem;
          margin-left: 1.5rem;
        }
      }
    }

    .copyright {
      border-top: 1px solid var(--color-border);
      padding-top: 1.5rem;
      display: flex;
      font-size: 1.3rem;
      position: relative;

      span {
        font-weight: 600;
      }

      img {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        margin-top: 1rem;
        height: 1.5rem;
      }
    }
  }
`;

export { FooterWrapper };
