import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import logo from "@/asset/logo.png";
import {
	AiOutlineFacebook,
	AiOutlineInstagram,
	AiOutlineTwitter
} from "react-icons/ai";
import { FooterWrapper } from "./style";

const Footer = () => 
{
	return (
		<FooterWrapper>
			<Row justify='center'>
				<Col span={20} xs={22} sm={22} md={21} lg={21} xl={18} xxl={14}>
					<div className='main'>
						<div className='link'>
							<div className='nav'>
								<div className='nav_item'>
									<Link to='/'>Homepage</Link>
								</div>
								<div className='nav_item'>
									<Link to='/edit'>Convert</Link>
								</div>
								<div className='nav_item'>
									<Link to='/edit'>Download</Link>
								</div>
								<div className='nav_item'>
									<Link to='/edit'>Compress</Link>
								</div>
							</div>

							<div className='social'>
								<a
									href='https://www.facebook.com/'
									target='_blank'
									rel='noreferrer'
								>
									<AiOutlineFacebook className='social_icon' />
								</a>
								<a
									href='https://www.instagram.com/'
									target='_blank'
									rel='noreferrer'
								>
									<AiOutlineInstagram className='social_icon' />
								</a>
								<a href='https://twitter.com/' target='_blank' rel='noreferrer'>
									<AiOutlineTwitter className='social_icon' />
								</a>
							</div>
						</div>

						<div className='copyright'>
							<div className='text'>
								Copyright © {new Date().getFullYear()} by{" "}
								<span>FreeLab Video Convert</span>{" "}
							</div>
							<img src={logo} alt='' className='logo' />
						</div>
					</div>
				</Col>
			</Row>
		</FooterWrapper>
	);
};

export default Footer;
