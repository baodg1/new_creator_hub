import styled from "styled-components";

const TopBarWrapper = styled.div`
  background-color: var(--color-background-gray);
  z-index: 10;
  color: white;
  padding: 1rem 1vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.15);

  .logo {
    margin-right: auto;
    height: 1.5rem;
  }

  .menu {
    display: flex;
    align-items: center;

    & > * {
      margin-left: 1.5rem;
    }

    &_icon {
      cursor: pointer;
      color: var(--color-gray);
      display: flex;
      font-size: 1.8rem;
    }
  }
`;

export { TopBarWrapper };
