import { FaRegUserCircle } from "react-icons/fa";
import { BiLogOutCircle } from "react-icons/bi";
import { connect, useSelector } from "react-redux";
import { Button, Dropdown, Menu, message } from "antd";
import { Link } from "react-router-dom";
import { RiArrowGoBackLine, RiArrowGoForwardLine } from "react-icons/ri";
import { CloudUploadOutlined } from "@ant-design/icons";
import { useUndoRedo } from "@/hook";
import { actSetModalExportVideoOpen } from "@/redux/action/preview";
import projectApi from "@/services/projectAPI";
// import { persistor } from "@/redux/store";
import { RootState } from "@/redux/reducer";
import logo from "@/asset/logo-white.png";
import { TopBarWrapper } from "./style";
import React, { Fragment, useMemo } from "react";
import { useCurrentPath } from "@/hook/useCurrentPath";
import { ROUTE_EDIT_VIDEO, USER } from "@/config/constant";
import { LocalStorage } from "@/utils/config";
interface TopbarProps {
  projectId: number;
  isExporting: boolean;
  actSetModalExportVideoOpen: (payload: boolean) => unknown;
  undoStack: unknown[];
  isRedoEnable: boolean;
}

const TopBar = (props: TopbarProps) => 
{
	const { projectId, isExporting, undoStack, isRedoEnable } = props;
	const { onRedo, onUndo } = useUndoRedo();
	const currentProject = useSelector(
		(state: RootState) => state.Project.currentProject
	);

	const currentPath = useCurrentPath();

	const undoEnable = useMemo(() => 
	{
		return undoStack?.length >= 1;
	}, [ undoStack ]);

	const onLogout = async () => 
	{
		await projectApi.deleteProject(Number(projectId));
		message.warning("All data of project has been deleted");

		LocalStorage.remove(USER.TOKEN);
		// setTimeout(() => 
		// {
		// 	persistor.purge();
		// }, 1000);
	};

	const onOpenExportVideoModal = () => 
	{
		props?.actSetModalExportVideoOpen(true);
	};

	const menu = (
		<Menu>
			<Menu.Item icon={<BiLogOutCircle />} onClick={onLogout} key='1'>
				Logout
			</Menu.Item>
		</Menu>
	);

	return (
		<TopBarWrapper>
			<div style={{ display: "flex", alignItems: "center" }}>
				<Link to='/'>
					<img src={logo} alt='' className='logo' />
				</Link>
				{currentPath === ROUTE_EDIT_VIDEO && currentProject ? (
					<div style={{ marginLeft: "40px" }}>{currentProject?.name??currentProject?.id}</div>
				) : null}
			</div>

			<div className='menu'>
				{!isExporting ? (
					<Fragment>
						<RiArrowGoBackLine
							className='menu_icon'
							onClick={onUndo}
							style={{
								color : undoEnable
									? "var(--color-primary)"
									: "var(--color-gray)",
								cursor : undoEnable ? "pointer" : "not-allowed"
							}}
						/>
						<RiArrowGoForwardLine
							className='menu_icon'
							onClick={onRedo}
							style={{
								color : isRedoEnable
									? "var(--color-primary)"
									: "var(--color-gray)",
								cursor : isRedoEnable ? "pointer" : "not-allowed"
							}}
						/>

						<Button
							type='primary'
							className='primary-btn'
							icon={<CloudUploadOutlined />}
							onClick={onOpenExportVideoModal}
						>
							Export
						</Button>
					</Fragment>
				) : null}

				<Dropdown overlay={menu}>
					<FaRegUserCircle className='menu_icon' />
				</Dropdown>
			</div>
		</TopBarWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		projectId    : Number(state?.Auth?.projectId),
		isExporting  : state?.Preview?.isExporting,
		undoStack    : state?.UndoRedo?.undoStack,
		isRedoEnable : state?.UndoRedo?.isRedoEnable
	}),
	{ actSetModalExportVideoOpen }
)(TopBar);
