import { ItemProps } from "../interface/SiderBar";
import { ItemWrapper } from "./style";

const Item = (props: ItemProps) => 
{
	const { label, icon: Icon, active, onClick, mode } = props;

	return (
		<ItemWrapper
			className={active ? "active" : ""}
			onClick={() => onClick(mode.toLowerCase())}
		>
			<div className='icon-wrapper'>
				<Icon />
			</div>
			<div className='label'>{label}</div>
		</ItemWrapper>
	);
};

export default Item;
