import styled from "styled-components";

const SideBarWrapper = styled.div`
  background-color: var(--color-background-gray);
  padding-top: 3rem;
`;

const ItemWrapper = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: var(--color-text-gray);
  cursor: pointer;
  padding: 1.3rem 1.7rem;
  border-radius: 2.4rem 0 0 2.4rem;
  transition: all 0.1s ease-in-out;

  &.active {
    background-color: var(--color-background-dark);
    color: var(--color-primary);
  }

  &:hover {
    background-color: var(--color-background-dark);
    color: var(--color-primary);
  }

  .icon-wrapper {
    font-size: 2.3rem;
    display: flex;
    justify-content: center;
    align-content: center;
    margin-bottom: 0.5rem;
  }

  .label {
    font-size: 1.1rem;
  }
`;

export { SideBarWrapper, ItemWrapper };
