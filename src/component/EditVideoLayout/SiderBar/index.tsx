import React from 'react';
import _ from "lodash";
import { useNavigate, useLocation, useSearchParams } from "react-router-dom";
import { connect } from "react-redux";
import { actSetSelectedFrame } from "@/redux/action/app";
import { MODE } from "@/config/constant";
import { RootState } from "@/redux/reducer";
import { SideBarWrapper } from "./style";
import Item from "./Item";
import { IItem, SidebarProps } from '../interface/SiderBar';

import { ReactComponent as TemplateIcon } from "@/asset/edit-sidebar/template.svg";
import { ReactComponent as MediaIcon } from "@/asset/edit-sidebar/media.svg";
import { ReactComponent as SettingIcon } from "@/asset/edit-sidebar/setting.svg";
import { ReactComponent as VideoIcon } from "@/asset/edit-sidebar/video.svg";
import { ReactComponent as ImageIcon } from "@/asset/edit-sidebar/image.svg";
import { ReactComponent as AudioIcon } from "@/asset/edit-sidebar/audio.svg";
import { ReactComponent as SubtitleIcon } from "@/asset/edit-sidebar/subtitle.svg";
import { ReactComponent as TextIcon } from "@/asset/edit-sidebar/text.svg";
import { ReactComponent as ElementIcon } from "@/asset/edit-sidebar/element.svg";
import { ReactComponent as OverlayIcon } from "@/asset/edit-sidebar/overlay.svg";
import { ReactComponent as TransitionIcon } from "@/asset/edit-sidebar/transition.svg";

const listItem = [ 
	{
		label : "Templates",
		icon  : TemplateIcon,
		mode  : MODE.EDIT_TEMPLATES
	},
	{
		label : "Media",
		icon  : MediaIcon,
		mode  : MODE.UPLOAD_MEDIA
	},
	{
		label : "Setting",
		icon  : SettingIcon,
		mode  : MODE.PROJECT_SETTING
	},
	{
		label : "Video",
		icon  : VideoIcon,
		mode  : MODE.EDIT_VIDEO
	},
	{
		label : "Image",
		icon  : ImageIcon,
		mode  : MODE.EDIT_IMAGE
	},
	{
		label : "Audio",
		icon  : AudioIcon,
		mode  : MODE.EDIT_AUDIO
	},
	{
		label : "Subtitles",
		icon  : SubtitleIcon,
		mode  : MODE.SUBTITLES
	},
	{
		label : "Text",
		icon  : TextIcon,
		mode  : MODE.TEXT_SETTING
	},
	{
		label : "Elements",
		icon  : ElementIcon,
		mode  : MODE.ELEMENTS
	},
	{
		label : "Overlay",
		icon  : OverlayIcon,
		mode  : MODE.OVERLAY
	},
	{
		label : "Transition",
		icon  : TransitionIcon,
		mode  : MODE.TRANSITION
	}
];

const SideBar = (props: SidebarProps) => 
{
	const { selectedFrame } = props;
	const location = useLocation();
	const navigate = useNavigate();
	const [ searchParams ] = useSearchParams();
	const query = Object.fromEntries([ ...searchParams ]);
	const { mode } = query;

	const onClick = (value: string) => 
	{
		navigate({ pathname: location.pathname, search: `?mode=${value}` });

		if (!_.isEmpty(selectedFrame)) 
		{
			props.actSetSelectedFrame(null);
		}
	};

	return (
		<SideBarWrapper>
			<div className='sidebar-content'>
				<div className='list-item'>
					{listItem?.map((item: IItem, index: number) => (
						<Item
							{...item}
							key={index}
							active={mode === item?.mode?.toLowerCase()}
							onClick={onClick}
						/>
					))}
				</div>
			</div>
		</SideBarWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		selectedFrame : state.App.selectedFrame
	}),
	{ actSetSelectedFrame }
)(SideBar);
