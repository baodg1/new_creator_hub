import React from 'react';

export interface EditVideoLayoutProps {
    isFullScreen: boolean;
    isExporting: boolean;
    children: React.ReactElement;
    actCreateNewFile: any;
    draggingItem: any;
  }
