export interface TopbarProps {
    projectId: number;
    isExporting: boolean;
    actSetModalExportVideoOpen: (payload: boolean) => any;
    undoStack: any[];
    isRedoEnable: boolean;
  }
