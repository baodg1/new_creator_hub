import { IFrame } from "@/redux/interface";

export interface IItem {
    label: string;
    icon: unknown;
    mode: string;
  }

export interface SidebarProps {
    selectedFrame: IFrame | null;
    actSetSelectedFrame: (payload: IFrame | null) => void;
  }

export interface ItemProps {
    label: string;
    icon: any;
    mode: string;
    active?: boolean;
    onClick: (mode: string) => void;
  }
