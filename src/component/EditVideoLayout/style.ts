import styled from "styled-components";
import { LARGEST_Z_INDEX_1 } from "@/config/constant";

export const sidebarWidth = "24rem";
export const collapsedSidebarWidth = "7rem";

const LayoutWrapper = styled.div`
  background-color: var(--color-background);
  font-size: 1.6rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  min-height: 100vh;

  .upload-file {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: var(--color-background-primary);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: ${LARGEST_Z_INDEX_1};
  }
`;

const MainSectionWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: flex-start;
  flex-grow: 1;
`;

const ContentWrapper = styled.div`
  flex-grow: 1;
  position: relative;
  width: calc(100% - 71px);
  background-color: var(--color-background-dark);
`;

export { LayoutWrapper, ContentWrapper, MainSectionWrapper };
