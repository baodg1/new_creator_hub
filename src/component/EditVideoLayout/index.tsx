import React, { useMemo, useState } from 'react';
import { UploadFile } from "@/component";
import { actCreateNewFile } from "@/redux/action/file";
import { RootState } from "@/redux/reducer";
import { LayoutWrapper, ContentWrapper, MainSectionWrapper } from "./style";
import Topbar from "./TopBar";
import SiderBar from "./SiderBar";
import { LARGEST_Z_INDEX_1, SMALLEST_Z_INDEX } from "@/config/constant";
import { connect } from "react-redux";
import { EditVideoLayoutProps } from './interface/EditVideoLayout';

const EditVideoLayout = (props: EditVideoLayoutProps) => 
{
	const { children, isFullScreen, isExporting, draggingItem } = props;
	const [ isDragFile, setIsDragFile ] = useState(false);

	const displayFileUpload = useMemo(() => 
	{
		return !draggingItem && isDragFile;
	}, [ isDragFile, draggingItem ]);

	const onDragEnter = () => 
	{
		!draggingItem && !isDragFile && setIsDragFile(true);
	};

	// do not use event.preventDefault() here
	const onDrop = (event: React.DragEvent) => 
	{
		setIsDragFile(false);
	};

	// const onUploadFile = (payload: any) => 
	// {
	// 	props?.actCreateNewFile([
	// 		{ ...payload, isLoading: true, isPreparing: true }
	// 	]);
	// };

	return (
		<LayoutWrapper onDragEnter={onDragEnter} onDrop={onDrop}>
			{/* {!isFullScreen ? <Topbar /> : null} */}

			<MainSectionWrapper>
				{/* {!isFullScreen && !isExporting ? <SiderBar /> : null} */}
				<ContentWrapper>{children}</ContentWrapper>
			</MainSectionWrapper>

			<div
				className='upload-file'
				style={{
					zIndex : displayFileUpload ? LARGEST_Z_INDEX_1 : SMALLEST_Z_INDEX
				}}
			>
				<UploadFile fullSize />
			</div>
		</LayoutWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		isFullScreen : state?.App?.isFullScreen,
		isExporting  : state?.Preview?.isExporting,
		draggingItem : state?.App?.draggingItem
	}),
	{
		actCreateNewFile
	}
)(EditVideoLayout);
