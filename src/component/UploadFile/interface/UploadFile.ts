import { IFile } from "@/redux/interface/file";

export interface UploadFileProps {
    fullSize?: boolean;
    projectId: number;
    actUpdateFile: (payload: IFile) => void;
    actCreateNewFile: (payload: IFile) => void;
    actDeleteFile: (payloay: IFile) => void;
}
