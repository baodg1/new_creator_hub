import React from 'react';
import { AxiosProgressEvent } from "axios";
import { BsFillCloudUploadFill } from "react-icons/bs";
import { connect } from "react-redux";
import { message } from "antd";
import { v4 as uuidv4 } from "uuid";
import { actDeleteFile, actCreateNewFile, actUpdateFile } from "@/redux/action/file";
import {
	FileType,
	MEDIA_TYPE
} from "@/config/constant";
import { getMediaType } from "@/utils/file";
import { UploadFileWrapper } from "./style";
import { UploadFileProps } from "./interface/UploadFile";
import { RootState } from '@/redux/reducer';
import { uploadAPI } from '@/services/uploadAPI';
import { IAudioFile, IFile, IImageFile, IVideoFile } from '@/redux/interface/file';
import fileApi from '@/services/fileAPI';
import { IBase64AudioResponse } from '@/services/interface/fileAPI';

const videoSeedUrl =
  "https://images.unsplash.com/photo-1570559120097-e6c3388329e6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80";

const UploadFile = (props: UploadFileProps) => 
{
	const { projectId, fullSize = false } = props;

	const onUploadProgress = (
		progressEvent: AxiosProgressEvent,
		file: IFile
	) => 
	{
		const percentage = Math.round(
			(100 * progressEvent.loaded) / progressEvent.total!
		);

		if (percentage === 100) 
		{
			file.isPreparing = true;
			file.isLoading = false;
		}

		file.loadingPercent = percentage;
		props.actUpdateFile(file);
	};

	const onFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => 
	{
		const { files } = event.target;

		if (files === null) 
		{
			return;
		}
		
		const file = files[0];
		const fileId = uuidv4();
		const tempUrl = URL.createObjectURL(file);
		const fileName = file.name;
		const mediaType = getMediaType(file.type);
		let fileMetadata: IFile | undefined;

		switch (mediaType) 
		{
		case FileType.Image: {
			fileMetadata = {
				id             : fileId,
				name           : fileName,
				url            : tempUrl,
				type           : mediaType,
				isLoading      : true,
				loadingPercent : 0,
				isPreparing    : false,
				storedName     : fileName,
				width          : 0,
				height         : 0
			} as IImageFile;
		}
			break;

		case FileType.Video: {
			fileMetadata = {
				id               : fileId,
				name             : fileName,
				thumbnailUrl   	 : videoSeedUrl,
				base64           : "",
				url              : tempUrl,
				type             : mediaType,
				isLoading        : true,
				loadingPercent   : 0,
				isPreparing      : false,
				storedName       : fileName,
				width            : 0,
				height           : 0,
				frameUrls        : [],
				durationInSecond : 0
			} as IVideoFile;

			break;
		}

		case FileType.Audio:{
			fileMetadata = {
				id               : fileId,
				name             : fileName,
				url              : tempUrl,
				type             : mediaType,
				isLoading        : true,
				loadingPercent   : 0,
				isPreparing      : false,
				storedName       : fileName,
				durationInSecond : 0
			} as IAudioFile;
		}
			break;

		default:
			break;
		}

		if (fileMetadata !== undefined) 
		{
			props.actCreateNewFile(fileMetadata);

			uploadAPI.uploadToProject(
				projectId, fileMetadata, file, onUploadProgress)
				.then((data) => 
				{
					switch (mediaType) 
					{
					case MEDIA_TYPE.IMAGE: {
						const imageInfo: IImageFile = {
							id             : fileId,
							name           : data.name,
							url            : data.path,
							type           : mediaType,
							isLoading      : false,
							loadingPercent : 0,
							isPreparing    : false,
							storedName     : data.storedName,
							width          : data.width ? data.width : 0,
							height         : data.height ? data.height : 0
						};

						props.actUpdateFile(imageInfo);
					}
						break;

					case MEDIA_TYPE.VIDEO: {
						fileApi.convertToBase64(file)
							.then((base64: IBase64AudioResponse) => 
							{
								const videoInfo: IVideoFile = {
									id               : fileId,
									name             : data.name,
									base64           : base64.base64,
									url              : data.path,
									thumbnailUrl     : data.frames!.length > 0 ? data.frames![0] : videoSeedUrl,
									type             : mediaType,
									isLoading        : false,
									loadingPercent   : 0,
									isPreparing      : false,
									storedName       : data.storedName,
									width            : data.width ? data.width : 0,
									height           : data.height ? data.height : 0,
									frameUrls        : data.frames ? data.frames : [],
									durationInSecond : data.duration ? data.duration : 0
								};

								props.actUpdateFile(videoInfo);
							});

						break;
					}

					case MEDIA_TYPE.AUDIO:{
						fileApi.convertToBase64(file)
							.then((base64: IBase64AudioResponse) => 
							{
								const audioInfo: IAudioFile = {
									id               : fileId,
									name             : data.name,
									url              : base64.url,
									base64           : base64.base64,
									type             : mediaType,
									isLoading        : false,
									loadingPercent   : 0,
									isPreparing      : false,
									storedName       : data.storedName,
									durationInSecond : data.duration ? data.duration : 0
								};
		
								props.actUpdateFile(audioInfo);
							});
						
					}
						break;

					default:
						break;
					}
					
				})
				.catch((e) => 
				{
					props.actDeleteFile(fileMetadata!);
					message.error(`Error while upload media ${e}`);
				});
		
		}
	};

	return (
		<UploadFileWrapper fullSize={fullSize}>
			{!fullSize ? (
				<div className='placeholder'>
					<div className='icon'>
						<BsFillCloudUploadFill />
					</div>

					<div className='title'>Upload a File</div>
					<div className='sub-title'>
						Click to <span>Browse</span>, or drag & drop a file here
					</div>
				</div>
			) : (
				<div className='text'>Drop file here to upload</div>
			)}

			<input type='file' onChange={onFileChange} />
		</UploadFileWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		projectId : state.Preview.setting.projectId
	}),
	{
		actUpdateFile,
		actDeleteFile,
		actCreateNewFile
	}
)(UploadFile);
