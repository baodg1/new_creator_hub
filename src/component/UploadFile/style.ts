import styled from "styled-components";

interface UploadFileWrapperProps {
  fullSize: boolean;
}

const UploadFileWrapper = styled.div`
  height: ${(props: UploadFileWrapperProps) =>
		(props?.fullSize ? "100%" : "17rem")};
  border: ${(props: UploadFileWrapperProps) =>
		(props?.fullSize ? "none" : "1px dashed var(--color-border)")};
  border-radius: ${(props: UploadFileWrapperProps) =>
		(props?.fullSize ? "none" : "var(--border-radius-1)")};
  position: relative;
  width: 100%;
  transition: all 0.2s ease-in-out;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props: UploadFileWrapperProps) =>
		(props?.fullSize ? "transparent" : "var(--color-background-gray)")};

  &:hover {
    border: 1px dashed var(--color-primary);
    background-color: var(--color-border-light);
  }

  .placeholder {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 99%;
    height: 99%;
    border-radius: var(--border-radius-1);
    transform: translate(-50%, -50%);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: var(--color-text-gray);

    .icon {
      font-size: 2.5rem;
    }

    .title {
      font-weight: 400;
      font-size: 1.6rem;
    }

    .sub-title {
      font-size: 1.4rem;
      margin-top: 0.3rem;
      font-weight: 300;
      text-align: center;
      padding: 0 20px;

      span {
        color: var(--color-primary);
      }
    }
  }

  .text {
    color: var(--color-text-light);
    font-size: 3rem;
    font-weight: 500;
  }

  input {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
    opacity: 0;
    /* border: 1px solid red; */
  }
`;

export { UploadFileWrapper };
