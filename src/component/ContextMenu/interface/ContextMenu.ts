import { IFrame, IMousePosition, ITrack } from "@/redux/interface";

export interface ContextMenuProps {
    menuProps?: any;
    anchorPoint: IMousePosition;
    toggleMenu: (payload: boolean) => any;
    selectedFrame: IFrame | null;
    actDeleteFrame: (payload: IFrame) => any;
    actDragDropMediaToVirtualTrack: any;
    actSetSelectedFrame: (payload: IFrame | null) => any;
    actUpsertFrame: (payload: IFrame) => any;
    actMoveFrameBetweenTrack: any;
    actMoveFrameToVirtualTrack: any;
    listTrack: ITrack[];
    timeRulerValue: number;
  }
