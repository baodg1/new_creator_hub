import styled from "styled-components";

const ContextMenuWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;

  .divider {
    height: 1px;
    width: 90%;
    background-color: var(--color-border);
    margin: 0.5rem 0.7rem;
  }

  // overwrite default style of @szhsin/react-menu
  .szh-menu {
    background-color: var(--color-background-gray);
    border-radius: 0.8rem;

    .szh-menu__item {
      color: white;
      font-size: 1.2rem;
      padding: 0.7rem 1.5rem;
    }

    .szh-menu__item--hover {
      background-color: var(--color-background-dark);
    }
  }
`;

const MenuItemWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;

  &.disable {
    cursor: not-allowed;
    opacity: 0.5;
  }

  .icon {
    display: flex;
    align-items: center;
    margin-right: 1rem;
  }

  .label {
    flex-basis: 1;
    margin-right: auto;
  }

  .short-key {
    margin-left: 1.5rem;
    text-align: right;
    color: var(--color-border);
  }
`;

export { ContextMenuWrapper, MenuItemWrapper };
