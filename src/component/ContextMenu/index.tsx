import _ from "lodash";
import { useMemo } from "react";
import { ControlledMenu, MenuItem, SubMenu } from "@szhsin/react-menu";
import { IoCopyOutline } from "react-icons/io5";
import { TbUnlink, TbTrash } from "react-icons/tb";
import { useNavigate, useLocation } from "react-router-dom";
import { ImStack } from "react-icons/im";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { MEDIA_TYPE, LARGEST_Z_INDEX_1 } from "@/config/constant";
import { IFrame } from "@/redux/interface";
import { RootState } from "@/redux/reducer";
import { useUndoRedo } from "@/hook/undoRedo";
import {
	actDeleteFrame,
	actMoveFrameBetweenTrack,
	actMoveFrameToVirtualTrack,
	actSetSelectedFrame,
	actUpsertFrame,
	actDragDropMediaToVirtualTrack
} from "@/redux/action/app";
import { ContextMenuWrapper, MenuItemWrapper } from "./style";
import { ContextMenuProps } from "./interface/ContextMenu";

const ContextMenu = (props: ContextMenuProps) => 
{
	const {
		menuProps,
		anchorPoint,
		toggleMenu,
		selectedFrame,
		listTrack,
		timeRulerValue
	} = props;
	const navigate = useNavigate();
	const location = useLocation();
	const { saveTrackState } = useUndoRedo();

	const shouldDislay = useMemo(() => 
	{
		return !_.isEmpty(selectedFrame);
	}, [ selectedFrame ]);

	const canUseDetachAudio = useMemo(() => 
	{
		return selectedFrame?.type === MEDIA_TYPE.VIDEO;
	}, [ selectedFrame ]);

	const onDeleteFrame = () => 
	{
		if (!shouldDislay) 
		{
			return;
		}
		saveTrackState();

		props?.actDeleteFrame(selectedFrame!);
		props?.actSetSelectedFrame(null);

		navigate({ pathname: location.pathname });
	};

	const onBringBackward = () => 
	{
		if (!shouldDislay) 
		{
			return;
		}

		saveTrackState();

		const currentTrackIndex = _.findIndex(listTrack, {
			id : selectedFrame?.trackId
		});
		const targetTrackIndex = currentTrackIndex + 1;

		if (currentTrackIndex < listTrack?.length - 1) 
		{
			const targetTrack = listTrack[targetTrackIndex];

			props?.actMoveFrameBetweenTrack({
				data          : selectedFrame,
				targetTrackId : targetTrack?.id
			});
		}
		else 
		{
			props?.actMoveFrameToVirtualTrack({
				frame             : selectedFrame,
				virtualTrackIndex : targetTrackIndex
			});
		}

		props?.actSetSelectedFrame(null);
	};

	const onBringForward = () => 
	{
		if (!shouldDislay) 
		{
			return;
		}

		saveTrackState();

		const currentTrackIndex = _.findIndex(listTrack, {
			id : selectedFrame?.trackId
		});

		if (currentTrackIndex > 0) 
		{
			const targetTrackIndex = currentTrackIndex - 1;
			const targetTrack = listTrack[targetTrackIndex];

			props?.actMoveFrameBetweenTrack({
				data          : selectedFrame,
				targetTrackId : targetTrack?.id
			});
		}
		else 
		{
			const targetTrackIndex = 0;

			props?.actMoveFrameToVirtualTrack({
				frame             : selectedFrame,
				virtualTrackIndex : targetTrackIndex
			});
		}

		props?.actSetSelectedFrame(null);
	};

	const onBringToFront = () => 
	{
		saveTrackState();

		const targetTrackIndex = 0;

		props?.actMoveFrameToVirtualTrack({
			frame             : selectedFrame,
			virtualTrackIndex : targetTrackIndex
		});

		props?.actSetSelectedFrame(null);
	};

	const onSendToBack = () => 
	{
		saveTrackState();

		const targetTrackIndex = listTrack.length;

		props?.actMoveFrameToVirtualTrack({
			frame             : selectedFrame,
			virtualTrackIndex : targetTrackIndex
		});
		props?.actSetSelectedFrame(null);
	};

	const detachAudioFromVideo = () => 
	{
		// TODO detach audio from video

		// if (!selectedFrame?.audioPath) 
		// {
		// 	message.error("Detach audio failed");
			
		// 	return;
		// }
		// saveTrackState();

		// const videoFrame = { ...selectedFrame };
		// // create audio frame next to this frame
		// const audioFrame: IFrame = {
		// 	id              : uuidv4(),
		// 	type            : MEDIA_TYPE.AUDIO,
		// 	mediaUrl        : videoFrame.timeline.audioPath,
		// 	duration        : videoFrame.timeline.duration,
		// 	fromSecond      : videoFrame.fromSecond,
		// 	trimStartSecond : videoFrame?.trimStartSecond,
		// 	trimEndSecond   : videoFrame?.trimEndSecond,
		// 	name            : videoFrame?.audioName,
		// 	setting         : { audioOptions: { volume: 1 } }
		// };

		// // create audio frame
		// props?.actDragDropMediaToVirtualTrack({
		// 	frame             : audioFrame,
		// 	virtualTrackIndex : listTrack?.length
		// });

		// // remove audio from VideoFrame
		// props?.actUpsertFrame({ ...selectedFrame});
	};

	const onDuplicateFrame = () => 
	{
		saveTrackState();

		const newFrame: IFrame = {
			...selectedFrame!,
			id       : uuidv4(),
			timeline : {
				...selectedFrame?.timeline!,
				fromSecond : timeRulerValue
			}
		};

		props?.actUpsertFrame(newFrame);
	};

	return (
		<ContextMenuWrapper
			style={{
				display : shouldDislay ? "inline" : "none",
				zIndex  : LARGEST_Z_INDEX_1
			}}
		>
			<ControlledMenu
				{...menuProps}
				anchorPoint={anchorPoint}
				direction='right'
				onClose={() => toggleMenu(false)}
			>
				<MenuItem onClick={onDuplicateFrame}>
					<MenuItemWrapper>
						<div className='icon'>
							<IoCopyOutline />
						</div>
						<div className='label'>Duplicate</div>
					</MenuItemWrapper>
				</MenuItem>

				<MenuItem onClick={detachAudioFromVideo}>
					<MenuItemWrapper className={!canUseDetachAudio ? "disable" : ""}>
						<div className='icon'>
							<TbUnlink />
						</div>
						<div className='label'>Detach Audio</div>
					</MenuItemWrapper>
				</MenuItem>

				<SubMenu
					label={
						<MenuItemWrapper>
							<div className='icon'>
								<ImStack />
							</div>
							<div className='label'>Arrange</div>
						</MenuItemWrapper>
					}
				>
					<MenuItem onClick={onBringForward}>
						<MenuItemWrapper>
							<div className='label'>Bring Forward</div>
							<div className='short-key'>Ctrl &#8593;</div>
						</MenuItemWrapper>
					</MenuItem>

					<MenuItem onClick={onBringToFront}>
						<MenuItemWrapper>
							<div className='label'>Bring to Front</div>
							<div className='short-key'>f</div>
						</MenuItemWrapper>
					</MenuItem>

					<div className='divider' />

					<MenuItem onClick={onBringBackward}>
						<MenuItemWrapper>
							<div className='label'>Sent Backward</div>
							<div className='short-key'>Ctrl &#8595;</div>
						</MenuItemWrapper>
					</MenuItem>

					<MenuItem onClick={onSendToBack}>
						<MenuItemWrapper>
							<div className='label'>Sent to Back</div>
							<div className='short-key'>b</div>
						</MenuItemWrapper>
					</MenuItem>
				</SubMenu>

				<div className='divider' />

				<MenuItem onClick={onDeleteFrame}>
					<MenuItemWrapper>
						<div className='icon'>
							<TbTrash />
						</div>
						<div className='label'>Trash</div>
					</MenuItemWrapper>
				</MenuItem>
			</ControlledMenu>
		</ContextMenuWrapper>
	);
};

export default connect(
	(state: RootState) => ({
		selectedFrame  : state?.App?.selectedFrame,
		listTrack      : state?.App?.listTrack,
		timeRulerValue : state?.TimeRuler?.value
	}),
	{
		actDeleteFrame,
		actMoveFrameBetweenTrack,
		actMoveFrameToVirtualTrack,
		actSetSelectedFrame,
		actUpsertFrame,
		actDragDropMediaToVirtualTrack
	}
)(ContextMenu);
