import React from 'react';

export interface ForgotEmailProps {
    onFinish: (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => any;
    loading: boolean;
  }
