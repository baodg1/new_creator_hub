import styled from "styled-components";

export const SignUpFormWrapper = styled("div")`
  height: 90%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  .form-signup {
    height: 100%;
    display: flex;
    background: #fff;
    padding: 3% 2.5%;
    justify-content: space-around;
    flex-direction: column;
    font-size: 1.6rem;
    border-radius: 4px;
    position: relative;
    @media screen and (max-width: 576px) {
      width: 90%;
    }

    @media screen and (min-width: 576px) {
      width: 80%;
    }

    @media screen and (min-width: 768px) {
      width: 60%;
    }

    @media screen and (min-width: 992px) {
      width: 600px;
    }
    .logo {
      text-align: center;
    }
    .txt-signup {
      font-weight: 600;
      font-size: 2.8rem;
      line-height: 3.4rem;
      color: #313335;
      text-align: center;
    }
    .txt-content {
      font-weight: 400;
      line-height: 2.4rem;
      color: #313335;
      text-align: center;
    }
    .ant-form-item-label{
      color:  #313335;
    }
    /* .label,
    label {
      font-size: 1.6rem;
      font-weight: 400;
    } */
    .input-item {
      background: #ffffff;
      border: 1px solid #eaeaea;
      border-radius: 8px;
      box-shadow: none;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;

      &.ant-input-affix-wrapper-status-error {
        border-color: #fb3c3c;
      }
      &.ant-input-status-error {
        border-color: #fb3c3c;
      }
    }
  }

  .terms {
    font-size: 16px;
    margin-bottom: 1rem;
    &:hover {
      .ant-checkbox-inner {
        border-color: #fe2c55;
      }
    }
    .ant-checkbox {
      &:hover {
        .ant-checkbox-inner {
          border-color: #fe2c55;
        }
      }
      .ant-checkbox-inner {
        background-color: #efefef;
      }
      &.ant-checkbox-checked {
        &:after {
          border-color: #fe2c55;
        }
        .ant-checkbox-inner {
          background-color: #fe2c55;
          border-color: #fe2c55;
        }
      }
      & + span {
        color: #313335;
      }
    }
  }

  .btn-submit {
    width: 100%;
    background-color: #fe2c55;
    outline: none;
    border-radius: 8px;
    border: none;
    &[disabled] {
      background-color: #6c6c6c;
      color: #ffffff;
      font-size: 16px;
      &:hover {
        background-color: #6c6c6c;
        color: #ffffff;
        font-size: 16px;
      }
    }
  }
  .login-fb-gg {
    .btn {
      border: 1px solid #e7e7e7;
      border-radius: 0.8rem;
      width: 100%;
      color: #131317;
      line-height: 2.4rem;
      display: flex;
      align-items: center;
      justify-content: center;
      margin: 1rem 0rem;
      img {
        margin-right: 1.6rem;
        width: 2.4rem;
        height: 2.4rem;
      }
    }
  }

  .have-account {
    color: #a9aaac;
    text-align: center;
  }
`;
