import { SignUpFormWrapper } from "./style";
import logoCreatorHub from "@/asset/auth/logo.svg";
import { Button, Checkbox, Form } from "antd";
import authAPI from "@/services/authAPI";
import LoginInput from "../Common/LoginInput/LoginInput";
import { useNavigate, Link } from "react-router-dom";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { useState } from "react";
import LoginWithSocial from "../LoginWithSocial";
import { error, success } from "@/component/Message";
import { Lang, LocalStorage } from "@/utils/config";
import { MAX_MAIL_LENGTH, MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH, USER } from "@/config/constant";
import { XCloseIcon } from "@/page/Auth/Common/XCloseIcon";

const SignUpForm = () => 
{
	const navigate = useNavigate();
	const [ checked, setChecked ] = useState(false);
	const [ form ] = Form.useForm();
	const formLayout = "vertical";

	const formOnFinsh = async (model: any) => 
	{
		const res = await authAPI.signUp(model.email, model.password);

		if (res && res.success) 
		{
			success(res.message!);
			const result = await authAPI.login(model.email, model.password);

			if (result && result?.success) 
			{
				LocalStorage.set(USER.TOKEN, result.data.accessToken);
				navigate("/dashboard/myproject");
			}
		}
		else 
		{
			const mes = res?.data?.message || "Sign Up fail";

			error(mes);
		}
	};

	const handleCheckTerms = (e: CheckboxChangeEvent) => 
	{
		setChecked(e.target.checked);
	};

	return (
		<SignUpFormWrapper>
			<div className='form-signup'>
				<XCloseIcon />
				<div className='logo'>
					<img src={logoCreatorHub} alt='' />
				</div>
				<div className='login-fb-gg'>
					<LoginWithSocial />
				</div>
				<div className='txt-signup'>Sign Up</div>
				<Form
					layout='vertical'
					form={form}
					initialValues={{ layout: formLayout }}
					className='frm-login'
					onFinish={formOnFinsh}
				>
					<Form.Item
						className='label'
						label='Email'
						name='email'
						required={false}
						rules={[
							{ required: true, message: Lang.trans("invalid") },
							{
								max     : MAX_MAIL_LENGTH,
								message : Lang.trans("invalid")
							},
							{ type: "email", message: Lang.trans("invalid") }
						]}
					>
						<LoginInput
							placeholder='Enter your mail here'
							size='large'
							maxLength={MAX_MAIL_LENGTH}
							className='input-item'
						/>
					</Form.Item>
					<Form.Item
						className='label'
						label='Password'
						name='password'
						required={false}
						rules={[ 
							{ required: true, message: Lang.trans("invalid") }, 
							{
								max     : MAX_PASSWORD_LENGTH,
								message : Lang.trans("invalid")
							},
							{
								min     : MIN_PASSWORD_LENGTH,
								message : "Password must be at least 6 characters"
							} 
						]}
					>
						<LoginInput
							placeholder='Enter your password here'
							size='large'
							type='password'
							maxLength={MAX_PASSWORD_LENGTH}
							className='input-item'
						/>
					</Form.Item>
					<Form.Item
						className='label'
						label='Confirm Password'
						name='confirm_password'
						required={false}
						rules={[
							{ required: true, message: Lang.trans("invalid") },
							{
								max     : MAX_PASSWORD_LENGTH,
								message : Lang.trans("invalid")
							},
							({ getFieldValue }) => ({
								validator(_, value) 
								{
									if (!value || getFieldValue("password") === value) 
									{
										return Promise.resolve();
									}
									
									return Promise.reject(
										new Error(
											"Password don't match"
										)
									);
								}
							})
						]}
					>
						<LoginInput
							placeholder='Enter your confirm password here'
							size='large'
							type='password'
							maxLength={MAX_PASSWORD_LENGTH}
							className='input-item'
						/>
					</Form.Item>

					<div className='terms'>
						<Checkbox checked={checked} onChange={handleCheckTerms}>
							Agree with Terms and Conditions
						</Checkbox>
					</div>

					<Form.Item>
						<Button
							type='primary'
							htmlType='submit'
							className='btn-submit'
							size='large'
							disabled={!checked}
						>
							Sign Up
						</Button>
					</Form.Item>
				</Form>

				<label className='have-account'>
					You already have an account?{" "}
					<span>
						<Link to={"/auth/login"}>Login</Link>
					</span>
				</label>
			</div>
		</SignUpFormWrapper>
	);
};

export default SignUpForm;
