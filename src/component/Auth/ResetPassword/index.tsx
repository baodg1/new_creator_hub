import React from "react";
import { ResetPasswordWrapper } from "./style";

const ResetPasswordForm = () => 
{
	return (
		<ResetPasswordWrapper>
			<div>Reset password from Forgot pass</div>
		</ResetPasswordWrapper>
	);
};

export default ResetPasswordForm;
