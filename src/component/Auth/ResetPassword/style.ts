import styled from "styled-components";

const ResetPasswordWrapper = styled("div")`
  width: 100%;

  font-family: "Inter";
  font-style: normal;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;

  .title-forgot {
    font-weight: 600;
    font-size: 28px;
    line-height: 34px;
    color: #313335;
    text-align: center;
  }

  .sub-title-forgot {
    font-size: 16px;
    line-height: 24px;
    text-align: center;
  }
  form.ant-form {
    width: 100%;
  }
  .mt-btn {
    margin-top: 25px !important;
    margin-bottom: 0px !important;
  }
  .ant-form-item-required:before {
    display: none !important;
  }

  .label-color {
    color: #313335 !important;
  }
  .input-item {
    background: #ffffff;
    border: 1px solid #eaeaea;
    border-radius: 8px;
    box-shadow: none;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    border-color: #eaeaea;
    &:hover {
      border-color: #eaeaea;
    }
    &.ant-input-affix-wrapper-status-error {
      border-color: #fb3c3c;
    }
    &.ant-input-status-error {
      border-color: #fb3c3c;
    }
  }
`;

export { ResetPasswordWrapper };
