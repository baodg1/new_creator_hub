import styled from "styled-components";

export const LoginFormWrapper = styled.div`
  .ant-form {
    .ant-form-item {
      .ant-form-item-label{
        color:  #313335;
      }
      .input-item {
        background: #ffffff;
        border: 1px solid #eaeaea;
        border-radius: 8px;
        box-shadow: none;
        font-weight: 400;
        font-size: 16px;
        line-height: 24px;

        &.ant-input-affix-wrapper-status-error {
          border-color: #fb3c3c;
        }
        &.ant-input-status-error {
          border-color: #fb3c3c;
        }
      }
      .remember-me {
        font-size: 16px;
        &:hover {
          .ant-checkbox-inner {
            border-color: #fe2c55;
          }
        }
        .ant-checkbox {
          &:hover {
            .ant-checkbox-inner {
              border-color: #fe2c55;
            }
          }
          .ant-checkbox-inner {
            background-color: #efefef;
          }
          &.ant-checkbox-checked {
            &:after {
              border-color: #fe2c55;
            }
            .ant-checkbox-inner {
              background-color: #fe2c55;
              border-color: #fe2c55;
            }
          }
          & + span {
            color: #313335;
          }
        }
      }
      .forget-password {
        font-size: 16px;
        color: #387dde;
      }
      .btn-login {
        background: #fe2c55;
        border-radius: 8px;
        font-weight: 600;
        font-size: 16px;
        line-height: 24px;
        color: #ffffff;
        width: 100%;
      }
    }
  }
  .sign-up {
    text-align: center;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    color: #a9aaac;
    margin-top: 2rem;
    a {
      color: #387dde;
    }
  }
`;
