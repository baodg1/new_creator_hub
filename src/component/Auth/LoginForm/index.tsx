import authAPI from "@/services/authAPI";
import { Button, Col, Form, Row } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { LoginFormWrapper } from "./style";
import LoginInput from "../Common/LoginInput/LoginInput";
import { error } from "@/component/Message";
import { Lang, LocalStorage } from "@/utils/config";
import { MAX_MAIL_LENGTH, MAX_PASSWORD_LENGTH, USER } from "@/config/constant";

const LoginForm = () => 
{
	const navigate = useNavigate();

	const handleLogin = async (data: any) => 
	{
		try 
		{
			const res = await authAPI.login(data.email, data.password);

			if (res && res.success && res.data) 
			{
				LocalStorage.set(USER.TOKEN, res.data.accessToken);
				navigate("/dashboard/myproject");
			}
			else 
			{
				error(res?.data?.message);
			}
		}
		catch (err) 
		{
			// TODO message with middleware
		}
	};
  
	return (
		<LoginFormWrapper>
			<Form layout='vertical' name='login-form' onFinish={handleLogin}>
				<Form.Item
					label='Email'
					name='email'
					required={false}
					rules={[
						{ required: true, message: Lang.trans("invalid") },
						{ type: "email", message: Lang.trans("invalid") },
						{
							max     : MAX_MAIL_LENGTH,
							message : Lang.trans("invalid")
						}
					]}
				>
					<LoginInput
						className='input-item'
						placeholder='Enter your email here'
						maxLength={MAX_MAIL_LENGTH}
						size='large'
					/>
				</Form.Item>
				<Form.Item
					label='Password'
					name='password'
					required={false}
					rules={[
						{ required: true, message: Lang.trans("invalid") },
						{
							max     : MAX_PASSWORD_LENGTH,
							message : Lang.trans("invalid")
						}
					]}
				>
					<LoginInput
						className='input-item'
						placeholder='Enter your password here'
						maxLength={MAX_PASSWORD_LENGTH}
						size='large'
						type='password'
					/>
				</Form.Item>
				<Form.Item>
					<Row justify='space-between'>
						<Col />
						<Col>
							<Link className='forget-password' to={"/auth/forgotpassword"}>
								Forgot your password?
							</Link>
						</Col>
					</Row>
				</Form.Item>
				<Form.Item>
					<Button className='btn-login' htmlType='submit' size='large'>
						Log in
					</Button>
				</Form.Item>
			</Form>
			<div className='sign-up'>
				Don’t have an account yet?{" "}
				<span>
					<Link to={"/auth/signup"}>Sign Up </Link>
				</span>
			</div>
		</LoginFormWrapper>
	);
};

export default LoginForm;
