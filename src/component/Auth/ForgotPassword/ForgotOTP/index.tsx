import { Button, Form } from "antd";
import { useState } from "react";
import OtpInput from "react18-input-otp";
import { ButtonSubmitWrapper } from "../style";
import { ForgotOTPFormWrapper, OtpInputContainerWrapper } from "./style";
import authAPI from "@/services/authAPI";
import { error, success } from "@/component/Message";
import { OTP_SIZE } from "@/config/constant";

interface ForgotOTPProps {
  email: string;
  onFinish: (e: any, otp: string) => any;
  loading: boolean;
}
const ForgotOTPForm = (props: ForgotOTPProps) => 
{
	const { onFinish, email, loading } = props;
	const [ otp, setOtp ] = useState("");

	/**
   *
   * @param enteredOtp
   */
	const handleChange = (enteredOtp: string) => 
	{
		setOtp(enteredOtp);
	};

	/**
   * Gửi lại OTP
   */
	const handleResendCode = async () => 
	{
		// TODO xử lý logic call server
		const res = await authAPI.forgotPassword(email);

		if (res && res.success) 
		{
			success("Resend code success");
		}
		else 
		{
			error(res?.data?.message || "Failed!");
		}
	};
  
	return (
		<ForgotOTPFormWrapper
			name='basic'
			layout='vertical'
			initialValues={{ remember: true }}
			autoComplete='off'
			onFinish={(e) => 
			{
				onFinish(e, otp);
			}}
			size='large'
		>
			<Form.Item>
				<div className='title-forgot label-color'>
					Verify your email address{" "}
				</div>
				<div className='sub-title-forgot label-color'>
					We emailed you the four digit code to{" "}
					<span className='font-bold'>{email}</span> <br />
					Enter the code below to confirm your email address
				</div>
			</Form.Item>
			<Form.Item>
				<OtpInputContainerWrapper>
					<OtpInput
						value={otp}
						onChange={handleChange}
						numInputs={OTP_SIZE}
						className='otp-input-item'
						containerStyle='otp-input-container'
						inputStyle='otp-input'
					/>
				</OtpInputContainerWrapper>
			</Form.Item>
			<Form.Item className='mt-btn'>
				<ButtonSubmitWrapper htmlType='submit' size='large' loading={loading}>
					VERIFY
				</ButtonSubmitWrapper>
			</Form.Item>
			<div className='resend-container'>
				<span>If you didn’t receive a code! </span>
				<Button type='link' onClick={handleResendCode}>
					Resend code
				</Button>
			</div>
		</ForgotOTPFormWrapper>
	);
};

export default ForgotOTPForm;
