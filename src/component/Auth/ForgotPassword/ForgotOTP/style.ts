import { Form } from "antd";
import styled from "styled-components";

const ForgotOTPFormWrapper = styled(Form)`
  .resend-container {
    text-align: center;
    margin-top: 15px;
    button {
      padding: inherit;
    }
  }
`;
const OtpInputContainerWrapper = styled("div")`
  .otp-input-container {
    align-items: center;
    justify-content: center;
    column-gap: 24px;
    .otp-input-item {
      width: 80px;
      height: 100px;
      input {
        width: 80px !important;
        height: 100px !important;
        border-radius: 4px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        font-weight: 600;
        font-size: 28px;
        outline: none;
        &:focus,
        &:active {
          border-color: #FE2C55 !important;
          background-color: #FFF2F5;
        }
      }
    }
  }
`;

export { OtpInputContainerWrapper, ForgotOTPFormWrapper };
