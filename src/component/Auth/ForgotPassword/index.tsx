import { useState } from "react";
import { ForgotFormWrapper } from "./style";
import authAPI from "@/services/authAPI";
import ForgotEmailForm from "./ForgotEmail";
import ForgotOTPForm from "./ForgotOTP";
import ForgotChangePassForm from "./ForgotChangePass";
import { useNavigate } from "react-router-dom";
import { error, success } from "@/component/Message";
import { ReactComponent as BackLeft } from "@/asset/back-left.svg";
import { Lang } from "@/utils/config";

const ForgotPasswordForm = () => 
{
	const [ forgotFormState, setForgotFormState ] = useState(1);
	const [ email, setEmail ] = useState("");
	const [ otp, setOTP ] = useState("");
	const [ loading, setLoading ] = useState(false);
	const navigate = useNavigate();

	/**
   * Xác thực email
   * @param values
   */
	const onFinishEmail = async (values: any) => 
	{
		setLoading(true);
		try 
		{
			
			// TODO xử lý logic call server
			const res = await authAPI.forgotPassword(values.email);

			if (res && res.success) 
			{
				setForgotFormState(2);
				setEmail(values.email);
			}
			else 
			{
				error(res?.data?.message);
			}
		}
		catch (err) 
		{
			// TODO message with middleware
		}
		setLoading(false);
	};

	/**
   * Verify OTP
   * @param
   */
	const onFinishOTP = async (e: any, value: string) => 
	{
		setLoading(true);
		
		if (value)
		{
			try 
			{
				// TODO xử lý logic call server
				const res = await authAPI.forgotPasswordConfirmCode(email, value);

				if (res && res.success) 
				{
					success(Lang.trans("profile_reset_pass_otp_success"));
					setOTP(value);
					setForgotFormState(3);
				}
				else 
				{
					error(res?.data?.message || Lang.trans("profile_reset_pass_otp_failed"));
				}
			}
			catch (err) 
			{
				// TODO message with middleware
			}
		}
		else 
		{
			error(Lang.trans("invalid"));
		}
		
		setLoading(false);
	};

	/**
   * ChangePass
   * @param values
   */
	const onFinishChangePass = async (values: any, password: string) => 
	{
		setLoading(true);
		try 
		{
			// TODO xử lý logic call server
			const res = await authAPI.forgotPasswordReset(otp, email, password);

			if (res && res.success) 
			{
				success(Lang.trans("reset_pasword_success"));

				setForgotFormState(1);
				navigate("/auth/login");
			}
			else 
			{
				error(res?.data?.message || Lang.trans("reset_pasword_failed"));
			}
		}
		catch (err) 
		{
			// TODO message with middleware
		}
		setLoading(false);
	};
  
	return (
		<ForgotFormWrapper>
			{forgotFormState === 1 && (
				<ForgotEmailForm
					onFinish={onFinishEmail}
					loading={loading}
				/>
			)}
			{forgotFormState === 2 && (
				<>
					<div className='btn-back-forgot-email' onClick={() => setForgotFormState(1)}>
						<BackLeft />
					</div>
					<ForgotOTPForm
						email={email}
						onFinish={onFinishOTP}
						loading={loading}
					/>
				</>
			)}
			{forgotFormState === 3 && (
				<ForgotChangePassForm
					email={email}
					onFinish={onFinishChangePass}
					loading={loading}
				/>
			)}
		</ForgotFormWrapper>
	);
};

export default ForgotPasswordForm;
