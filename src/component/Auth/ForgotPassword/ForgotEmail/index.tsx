import { Form } from "antd";
import { Link } from "react-router-dom";
import { ButtonSubmitWrapper } from "../style";
import { ForgotEmailProps } from '../../interface';
import LoginInput from "../../Common/LoginInput/LoginInput";
import { Lang } from "@/utils/config";
import { MAX_MAIL_LENGTH } from "@/config/constant";

const ForgotEmailForm = (props: ForgotEmailProps) => 
{
	const { onFinish, loading } = props;
  
	return (
		<Form
			name='basic'
			layout='vertical'
			initialValues={{ remember: true }}
			autoComplete='off'
			onFinish={onFinish}
			size='large'
		>
			<Form.Item>
				<div className='title-forgot label-color'>Forget Password</div>
			</Form.Item>
			<Form.Item
				name='email'
				label='Email'
				className='label-color'
				rules={[
					{
						type    : "email",
						message : Lang.trans("invalid")
					},
					{
						required : true,
						message  : Lang.trans("invalid")
					}
				]}
				// hasFeedback
			>
				<LoginInput
					className='input-item'
					placeholder='Enter your email here'
					maxLength={MAX_MAIL_LENGTH}
					size='large'
				/>
			</Form.Item>
			<Form.Item className='mt-btn'>
				<ButtonSubmitWrapper htmlType='submit' size='large' loading={loading}>
					Send Instruction
				</ButtonSubmitWrapper>
			</Form.Item>
			<div className='back-login'>
				<Link to='/auth/login'>Back to Login</Link>
			</div>
		</Form>
	);
};

export default ForgotEmailForm;
