import styled from "styled-components";
import forrgotSuccess from "@/asset/auth/forgot_success.svg";
import { Button } from "antd";

const ForgotFormWrapper = styled("div")`
  width: 100%;

  font-family: "Inter";
  font-style: normal;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  /* position: relative; */ 

  .title-forgot {
    font-weight: 600;
    font-size: 28px;
    line-height: 34px;
    color: #313335;
    text-align: center;
  }

  .sub-title-forgot {
    font-size: 16px;
    line-height: 24px;
    text-align: center;
  }
  form.ant-form {
    width: 100%;
  }
  .mt-btn {
    margin-top: 25px !important;
    margin-bottom: 0px !important;
  }
  .ant-form-item-required:before {
    display: none !important;
  }

  .label-color {
    color: #313335 !important;
  }
  .ant-form-item-label{
    color:  #313335;
  }
  .input-item {
    background: #ffffff;
    border: 1px solid #eaeaea;
    border-radius: 8px;
    box-shadow: none;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    border-color: #eaeaea;
    &:hover {
      border-color: #eaeaea;
    }
    &.ant-input-affix-wrapper-status-error {
      border-color: #fb3c3c;
    }
    &.ant-input-status-error {
      border-color: #fb3c3c;
    }
  }
  .back-login {
    text-align: center;
    margin-top: 4rem;
    a {
      color: #387dde;
    }
  }

  .btn-back-forgot-email{
    position: absolute;
    top: 18px;
    left: 18px;
    width: 24px;
    height: 24px;
    cursor: pointer;
    &:hover{
        background-color: #f5f5f5;
        border-radius: 4px;
    }
  }
`;
const ButtonSubmitWrapper = styled(Button)`
  background: #fe2c55;
  border-radius: 8px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  width: 100%;
  border: 1px solid #fe2c55;
  &:hover,
  &:focus {
    background: #fe2c55;
    color: #ffffff;
    border: 1px solid #fe2c55;
  }
`;

const FormForgotDoneWrapper = styled("div")`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;

  .forgot-success {
    background-image: url(${forrgotSuccess});
    background-repeat: no-repeat;
    background-size: cover;
    width: 120px;
    height: 120px;
  }
  .message-title {
    font-weight: 600;
  }
  .btn-back-login {
    margin: 30px 0;
  }
  .orther-message {
    color: #787878;
  }
  .message-title {
    font-size: 28px;
  }
  .message-content {
    font-size: 14px;
  }
`;

export { ButtonSubmitWrapper, FormForgotDoneWrapper, ForgotFormWrapper };
