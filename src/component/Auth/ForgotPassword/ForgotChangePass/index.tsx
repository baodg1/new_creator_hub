import React from 'react';
import { Form } from "antd";
import LoginInput from "../../Common/LoginInput/LoginInput";
import { ButtonSubmitWrapper } from "../style";
import { MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH } from '@/config/constant';
import { Lang } from '@/utils/config';

interface ForgotEmailProps {
  email: string;
  onFinish: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    password: string
  ) => any;
  loading: boolean;
}
const ForgotChangePassForm = (props: ForgotEmailProps) => 
{
	const { onFinish, loading } = props;
  
	return (
		<Form
			name='basic'
			layout='vertical'
			initialValues={{ remember: true }}
			autoComplete='off'
			onFinish={(e) => 
			{
				onFinish(e, e.password);
			}}
			size='large'
		>
			<Form.Item>
				<div className='title-forgot label-color'>Create your new password</div>
			</Form.Item>
			<Form.Item
				className='label'
				label='New Password'
				name='password'
				required={false}
				rules={[
					{ required: true, message: Lang.trans("invalid") },
					{
						max     : MAX_PASSWORD_LENGTH,
						message : Lang.trans("invalid")
					},
					{
						min     : MIN_PASSWORD_LENGTH,
						message : "Password must be at least 6 characters"
					}
				]}
			>
				<LoginInput
					placeholder='Enter your password here'
					size='large'
					maxLength={MAX_PASSWORD_LENGTH}
					type='password'
				/>
			</Form.Item>
			<Form.Item
				className='label'
				label='Confirm New Password'
				name='confirm_password'
				required={false}
				rules={[
					{ required: true, message: Lang.trans("invalid") },
					({ getFieldValue }) => ({
						validator(_, value) 
						{
							if (!value || getFieldValue("password") === value) 
							{
								return Promise.resolve();
							}
							
							return Promise.reject(
								new Error("Password don't match")
							);
						}
					})
				]}
			>
				<LoginInput
					placeholder='Enter your confirm password here'
					size='large'
					maxLength={MAX_PASSWORD_LENGTH}
					type='password'
				/>
			</Form.Item>
			<Form.Item className='mt-btn'>
				<ButtonSubmitWrapper htmlType='submit' size='large' loading={loading}>
					Change
				</ButtonSubmitWrapper>
			</Form.Item>
		</Form>
	);
};

export default ForgotChangePassForm;
