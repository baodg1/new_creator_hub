import { Button } from "antd";
import IconGoogle from "@/asset/auth/ic_google.svg";
import IconFacebook from "@/asset/auth/ic_facebook.svg";
import { handleLoginWithFacebook, handleLoginWithGoogle } from "@/firebase";
import authAPI from "@/services/authAPI";
import { useNavigate } from "react-router-dom";
import { error } from "@/component/Message";
import { LocalStorage } from "@/utils/config";
import { PLATFORM_SOCIAL_SERVICE, USER } from "@/config/constant";

const LoginWithSocial = () => 
{
	const navigate = useNavigate();

	const loginWithGoogle = async () => 
	{
		const user = await handleLoginWithGoogle();

		// Kiểm tra xác thực từ gg
		if (user && user.accessToken) 
		{
			const res = await authAPI.signInSocial(PLATFORM_SOCIAL_SERVICE.GOOGLE, user.accessToken);
			// Kiểm tra đăng nhập với hệ thống

			if (res && res.success && res.data) 
			{
				LocalStorage.set(USER.TOKEN, res.data.accessToken);
				navigate("/dashboard/myproject");
			}
			else 
			{
				error("Login failed.");
			}
		}
		else 
		{
			error("Login failed.");
		}
	};

	const loginWithFacebook = async () => 
	{
		const user = await handleLoginWithFacebook();
		// Kiểm tra xác thực từ fb

		if (user && user.accessToken) 
		{
			const res = await authAPI.signInSocial(PLATFORM_SOCIAL_SERVICE.FACEBOOK, user.accessToken);

			// Kiểm tra đăng nhập với hệ thống
			if (res && res.success && res.data) 
			{
				LocalStorage.set(USER.TOKEN, res.data.accessToken);
				navigate("/dashboard/myproject");
			}
			else 
			{
				error("Login failed.");
			}
		}
		else 
		{
			error("Login failed.");
		}
	};
  
	return (
		<>
			<div>
				<Button
					className='btn'
					icon={<img src={IconGoogle} alt='' />}
					onClick={loginWithGoogle}
					size='large'
				>
					Continue with Google
				</Button>
			</div>
			<div>
				<Button
					className='btn'
					icon={<img src={IconFacebook} alt='' />}
					onClick={loginWithFacebook}
					size='large'
				>
					Continue with Facebook
				</Button>
			</div>
		</>
	);
};

export default LoginWithSocial;
