import { EyeFilled, EyeInvisibleFilled } from "@ant-design/icons";
import { Form, Input, InputProps } from "antd";
import icError from "@/asset/auth/ic_error.svg";
import { FunctionComponent, useState } from "react";

const LoginInput: FunctionComponent<InputProps> = (props: InputProps) => 
{
	const { status } = Form.Item.useStatus();
	const [ showPassword, setShowPassword ] = useState(false);

	/**
   * @param currentState true => show password, false => hide password
   */
	const toggleShowPassword = (currentState: boolean) => 
	{
		setShowPassword(!currentState);
	};

	return (
		<Input
			{...props}
			type={
				props.type === "password"
					? showPassword
						? "text"
						: "password"
					: props.type
			}
			suffix={
				<div>
					{status === "error" ? (
						<img src={icError} alt='' style={{ marginRight: "8px" }} />
					) : null}
					{props.type === "password" ? (
						showPassword ? (
							<EyeFilled onClick={() => toggleShowPassword(true)} />
						) : (
							<EyeInvisibleFilled onClick={() => toggleShowPassword(false)} />
						)
					) : null}
				</div>
			}
		/>
	);
};

export default LoginInput;
