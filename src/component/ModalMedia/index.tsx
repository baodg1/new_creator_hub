import { useEffect, useMemo, useState } from "react";
import { Modal, Button } from "antd";
import { connect } from "react-redux";
import { actSetModalMediaOpen } from "@/redux/action/media";
import { MEDIA_TYPE } from "@/config/constant";
import { RootState } from "@/redux/reducer";
import ImageItem from "./ImageItem";
import { ModalMediaWrapper } from "./style";
import { ModalMediaProps } from "./interface/ModalMedia";
import { IImageFile } from "@/redux/interface/file";

const INITIAL_FILE_INDEX = -1;

const ModalMedia = (props: ModalMediaProps) => 
{
	const { isModalOpen, fileList, type, onOk } = props;
	const [ selectedFileIndex, setSelectedFileIndex ] =
    useState(INITIAL_FILE_INDEX);

	useEffect(() => 
	{
		setSelectedFileIndex(INITIAL_FILE_INDEX);
	}, [ isModalOpen ]);

	const listFileForType = useMemo(() => 
	{
		return fileList.filter((file) => file.type === type);
	}, [ type, fileList ]);

	const onCloseModal = () => 
	{
		props?.actSetModalMediaOpen(false);
	};

	const onSelectFile = (index: number) => 
	{
		setSelectedFileIndex(index);
	};

	const renderListFile = () => 
	{
		return listFileForType?.map((file, index) => 
		{
			if (file?.type === MEDIA_TYPE?.IMAGE) 
			{
				return (
					<div className='file-wrapper' key={index}>
						<ImageItem
							imageFile={file as IImageFile}
							index={index}
							onSelectFile={onSelectFile}
							isActive={index === selectedFileIndex}
						/>
					</div>
				);
			}

			return null;
		});
	};

	const onConfirm = () => 
	{
		onOk(listFileForType?.[selectedFileIndex]?.url);
	};

	return (
		<Modal
			title='Media'
			open={isModalOpen}
			onOk={onOk}
			onCancel={onCloseModal}
			maskClosable={false}
			className='custom-modal'
			footer={null}
			width='80rem'
		>
			<ModalMediaWrapper>
				<div className='content'>
					{listFileForType?.length! > 0 ? (
						renderListFile()
					) : (
						<span style={{ textAlign: "center", width: "100%" }}>
							There is no file
						</span>
					)}
				</div>

				<div className='footer'>
					<Button
						size='large'
						ghost
						className='ghost-btn'
						onClick={onCloseModal}
					>
						Cancel
					</Button>

					<Button
						type='primary'
						size='large'
						className='primary-btn'
						disabled={selectedFileIndex < 0}
						onClick={onConfirm}
					>
						Select
					</Button>
				</div>
			</ModalMediaWrapper>
		</Modal>
	);
};

export default connect(
	(state: RootState) => ({
		isModalOpen : state?.Media?.isModalOpen,
		fileList    : [ ...state.File.fileList.values() ]
	}),
	{ actSetModalMediaOpen }
)(ModalMedia);
