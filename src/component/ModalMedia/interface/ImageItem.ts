import { IImageFile } from "@/redux/interface/file";

export interface ImageItemProps {
    imageFile: IImageFile;
    index: number;
    onSelectFile: (index: number) => any;
    isActive: boolean;
  }
