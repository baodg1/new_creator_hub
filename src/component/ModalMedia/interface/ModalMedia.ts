import { IFile } from "@/redux/interface/file";

export interface ModalMediaProps {
    isModalOpen?: boolean;
    actSetModalMediaOpen: (payload: boolean) => any;
    fileList: IFile[];
    type: string;
    onOk: any;
  }
