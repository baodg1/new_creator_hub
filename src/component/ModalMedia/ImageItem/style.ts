import styled from "styled-components";

const ImageItemWrapper = styled.div`
  border-radius: 0.8rem;

  img {
    background-color: var(--color-background-gray);
    width: 100%;
    height: 100%;
    object-fit: cover;
    height: 12rem;
    cursor: grabbing;
    border-radius: var(--border-radius);
    transition: all 0.1s ease-in-out;
  }

  .name {
    margin-top: 0.3rem;
    margin-left: 0.3rem;
    font-size: 1.3rem;
    color: var(--color-text-gray);
  }
`;

export { ImageItemWrapper };
