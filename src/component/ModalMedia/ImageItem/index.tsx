import { ImageItemProps } from "../interface/ImageItem";
import { ImageItemWrapper } from "./style";

const ImageItem = (props: ImageItemProps) => 
{
	const { imageFile: file, index, onSelectFile, isActive } = props;

	return (
		<ImageItemWrapper onClick={() => onSelectFile(index)}>
			<img
				src={file?.url}
				alt='url'
				style={{
					border : isActive
						? "1px solid var(--color-primary)"
						: "1px solid transparent"
				}}
			/>

			<div className='name'>
				{file?.name&&file?.name?.length < 13
					? file?.name
					: `${file?.name?.slice(0, 10)}...`}
			</div>
		</ImageItemWrapper>
	);
};

export default ImageItem;
