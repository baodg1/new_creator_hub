import styled from "styled-components";

const ModalMediaWrapper = styled.div`
  .content {
    display: flex;
    flex-wrap: wrap;

    .file-wrapper {
      flex-basis: 20%;
      padding: 1rem;
    }
  }

  .footer {
    display: flex;
    justify-content: flex-end;

    & > * {
      margin-left: 1.5rem;
    }
  }
`;

export { ModalMediaWrapper };
