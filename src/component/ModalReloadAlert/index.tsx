import { Modal, Button } from "antd";
import { connect } from "react-redux";
import { RootState } from "@/redux/reducer";
import { actSetModalAlertReloadVisible } from "@/redux/action/app";
import { ModalReloadAlertWrapper } from "./style";

interface ModalReloadAlertProps {
  isModalAlertReloadVisible: boolean;
  actSetModalAlertReloadVisible: (payload: boolean) => any;
}

const ModalReloadAlert = (props: ModalReloadAlertProps) => 
{
	const { isModalAlertReloadVisible } = props;

	const onCloseModal = () => 
	{
		props?.actSetModalAlertReloadVisible(false);
	};

	return (
		<Modal
			open={isModalAlertReloadVisible}
			onCancel={onCloseModal}
			className='custom-modal'
			footer={null}
			style={{ top: "30vh" }}
			maskClosable={false}
		>
			<ModalReloadAlertWrapper>
				<div className='heading'>Reload site?</div>
				<div className='description'>Changes you made may not be saved</div>

				<div className='footer'>
					<Button
						ghost
						size='large'
						onClick={onCloseModal}
						className='ghost-btn'
					>
						Cancel
					</Button>

					<Button
						size='large'
						type='primary'
						className='primary-btn'
						onClick={onCloseModal}
					>
						Reload
					</Button>
				</div>
			</ModalReloadAlertWrapper>
		</Modal>
	);
};

export default connect(
	(state: RootState) => ({
		isModalAlertReloadVisible : state?.App?.isModalAlertReloadVisible
	}),
	{ actSetModalAlertReloadVisible }
)(ModalReloadAlert);
