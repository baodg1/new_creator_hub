import styled from "styled-components";

const ModalReloadAlertWrapper = styled.div`
  margin-top: 2rem;

  .heading {
    font-size: 2.4rem;
    font-weight: 600;
    text-align: center;
    margin-bottom: 1rem;
  }

  .description {
    font-size: 1.5rem;
    text-align: center;
    margin-bottom: 3rem;
  }

  .footer {
    display: flex;
    justify-content: center;

    button {
      padding: 0.6rem 3rem;
      margin-left: 1rem;
      margin-right: 1rem;
    }
  }
`;

export { ModalReloadAlertWrapper };
