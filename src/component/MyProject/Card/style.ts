import { Card } from "antd";
import styled from "styled-components";
// import empty_trash from "@/asset/project-management/emptytrash.svg";

const CardWrapper = styled(Card)`
  border-radius: 4px;
  width: 100%;
  border: none;
  .ant-card-cover {
    min-height: 194px;
    margin: 0px !important;
  }
  .ant-card-body {
    display: none;
  }
`;
const CardCoverWrapper = styled("div")`
  position: relative;
  .card-cover-status {
    position: absolute;
    top: 0;
    right: 0px;
    z-index: 1;
    border-top-right-radius: 4px;
    background-color: #000000;
    color: #fff;

    .card-cover-status-item {
      border: 1px solid #fff;
      padding: 8px 16px;
      border-top-right-radius: 4px;
      font-weight: 600;
      font-size: 14px;
      line-height: 14px;
      border-bottom-left-radius: 8px;
      &.exported {
        color: #5ef2e0;
        border-color: #5ef2e0;
      }
      &.draft {
        color: #5ef2e0;
        border-color: #5ef2e0;
      }
    }
  }

  .card-video-timer {
    position: absolute;
    bottom: 16px;
    left: 16px;
    z-index: 1;
    border-radius: 4px;
    background-color: transparent;
    color: #fff;
    padding: 4px 8px;
    font-size: 14px;
    line-height: 14px;
  }
  .card-thumb-nail {
    width: 100%;
    height: 100%;
    background-color: #f5f5f5;
    border-radius: 4px;
    &.thumb-nail-empty {
      height: 194px;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: #000000;
    }
  }

  .card-name-container {
    position: absolute;
    bottom: 42px;
    left: 16px;
    right: 16px;
    z-index: 1;
    border-radius: 4px;
    background-color: transparent;
    color: #fff;
    padding: 4px 8px;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;

    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    display: -webkit-box;
    input {
      color: #fff !important;
      font-weight: 600;
      font-size: 16px;
    }
  }
  .drp-card-action {
    width: 16px;
    height: 16px;
    .ant-btn-default {
      display: none;
    }
    .ant-btn-default.ant-dropdown-trigger {
      display: block;
      border: none;
      outline: none;
      rotate: 90deg;
    }
  }
  .card-action-container {
    position: absolute;
    bottom: 16px;
    right: 16px;
    width: 24px;
    height: 24px;
    z-index: 1;
    border-radius: 4px;
    background-color: #262935;
    color: #fff;
    .ant-btn {
      width: 24px;
      height: 24px;
      border-radius: 4px;
      background-color: transparent;
      color: #fff;
    }
    .ant-dropdown-trigger {
    }
  }
`;
const CardDetailContainer = styled("div")`
  width: 100%;
  .card-detail-content {
    display: flex;

    .ant-card-meta {
      flex: 1;
    }
    .drp-card-action {
      width: 16px;
      height: 16px;
      .ant-btn-default {
        display: none;
      }
      .ant-btn-default.ant-dropdown-trigger {
        display: block;
        border: none;
        outline: none;
        rotate: 90deg;
      }
    }
    .ant-card-meta-title {
      .ant-layout-content {
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  }
`;

export { CardDetailContainer, CardWrapper, CardCoverWrapper };
