import { Image, Input } from "antd";
import { CardCoverWrapper, CardWrapper } from "./style";
import React from "react";
import { Content } from "antd/lib/layout/layout";
import projectApi from "@/services/projectAPI";
import { ICardProjectProps } from "../interface";

const CardProject = (props: ICardProjectProps) => 
{
	const { isShowStatus, onClickCard } = props;
	const [ editing, setEditing ] = React.useState(false);
	const [ projectName, setprojectName ] = React.useState(props.title);
	const [ model, setModel ] = React.useState(props.model);

	/**
   * Thay đổi tên project
   * @param e event blur hoặc enter press
   */
	const saveChange = async (e: any) => 
	{
		const newName = e.target.value;

		if (newName !== projectName) 
		{
			const newModel = { ...model, name: newName };
			const res = await projectApi.update(newModel);

			if (res?.success) 
			{
				setModel(newModel);
				setprojectName(newName);
			}
		}

		setEditing(false);
	};

	/**
   * Xử lý gender card cover
   */
	const renderCardCover = () => 
	{
		const isExported = props.is_exported;

		return (
			<CardCoverWrapper
				onClick={() => 
				{
					if (onClickCard != null) 
					{
						onClickCard();
					}
				}}
			>
				{isShowStatus === true && (
					<div className='card-cover-status'>
						<div
							className={`card-cover-status-item ${
								isExported ? "export" : "draft"
							}`}
						>
							{isExported ? "Export" : "Draft"}
						</div>
					</div>
				)}

				<div className='card-cover-img'>
					{props.imgSrc ? (
						<Image
							alt=''
							src={props.imgSrc}
							preview={false}
							className='card-thumb-nail'
						/>
					) : (
						<div className='card-thumb-nail thumb-nail-empty'>
							{/* <div className="thumb-nail-empty-bg"></div> */}
						</div>
					)}
				</div>
				<div className='card-name-container'>
					{editing && props.editable ? (
						<Input
							defaultValue={projectName}
							bordered={false}
							onPressEnter={saveChange}
							onFocus={(e) => e.target.select()}
							onBlur={saveChange}
							autoFocus
						/>
					) : (
						<Content onClick={() => setEditing(true)}>{projectName}</Content>
					)}
				</div>
				<div className='card-video-timer'>
					<div className='card-video-timer-item'>
						{props.video_time || "00:00"}
					</div>
				</div>
				{props.action != null && (
					<div className='card-action-container'>{props.action}</div>
				)}
			</CardCoverWrapper>
    ) as React.ReactElement;
	};

	return <CardWrapper hoverable cover={renderCardCover()} />;
};

export default CardProject;
