import styled from "styled-components";

const SearchWrapper = styled.div`
     background-color:#F1F1F1;
     padding: 8px 16px;
     .inputSearch{
         width:480px;
     }
     .btnSort{
         width: 89px;
         height: 36px;
         background: #FEFDFF;
         border-radius: 4px;
     }
`;

export { SearchWrapper };
