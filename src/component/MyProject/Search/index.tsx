import { Row } from "antd";
import { CButton, CInput } from "@/component/Common";
import searchIcon from "@/asset/search-icon.png";
import sortIcon from "@/asset/sort-icon.png";
import { SearchWrapper } from "./style";
import { SearchProps } from "../interface/Search";

const Index = (props: SearchProps) => 
{
	const { handleSearch } = props;

	return (
		<SearchWrapper className='search'>
			<Row justify='space-between' align='middle'>
				<CInput
					className='inputSearch'
					type={1}
					placeholder='Search by Name Project'
					height={40}
					icon={<img src={searchIcon} alt='' width={20} />}
					onChange={handleSearch}
				/>
				<CButton
					className='btnSort'
					label='Sort'
					icon={
						<img
							src={sortIcon}
							style={{ marginRight: "8px" }}
							width={20}
							alt=''
						/>
					}
				/>
			</Row>
		</SearchWrapper>
	);
};

export default Index;
