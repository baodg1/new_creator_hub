import { Breadcrumb, Button, Col, Dropdown, Image, Menu, Row } from "antd";
import { useNavigate, useParams, Link } from "react-router-dom";
import {
	BreadcrumbWrapper,
	DetailProjectComponentWrapper,
	ModalWrapper
} from "./style";
import projectApi from "@/services/projectAPI";
import React, { useEffect, useState } from "react";
import { Content } from "antd/lib/layout/layout";
import { IProject } from "@/redux/interface";
import CardProject from "../Card";
import { DropdownMenuCardWrapper } from "../TabAllProject/style";
import { MenuInfo } from "rc-menu/lib/interface";
import editIcon from "@/asset/edit-icon.svg";
import duplicateIcon from "@/asset/duplicate-icon.svg";
import deleteIcon from "@/asset/delete-icon.svg";
import deleteModalIcon from "@/asset/delete-modal-icon.svg";
import arrowRight from "@/asset/arrow-right.svg";
import { useDispatch } from "react-redux";
import { actSaveCreatedProject } from "@/redux/action/project";
import { error, success, snackbar } from "@/component/Message";
import { Lang } from "@/utils/config";
import { trashAPI } from "@/services/trashAPI";

const DetailProjectComponent = () => 
{
	const params = useParams();
	const { id } = params;
	const [ detailProject, setDetailProject ] = useState<Array<IProject>>([]);
	const [ project, setProject ] = useState<IProject>();
	const [ deleteSeleted, setDeleteSeleted ] = useState<IProject>();
	const [ modalDeleteConfirmOpen, setModalDeleteConfirmOpen ] = useState(false);
	const dispatch = useDispatch();
	const navigate = useNavigate();

	/**
   * Danh sách action cho các card
   * Sử dụng '& bit' để xác định action theo dạng video draft hay Export
   * value: 1,2,4,8,16,...
   * Với: draft = 1 ; Export = 2
   * ===>> draft = 1 ; Export = 2; All = 3
   */
	const menuItems = [
		{
			key      : "btnEditProject",
			label    : Lang.trans("edit"),
			icon     : <img src={editIcon} alt='' className='card-action-icon' />,
			category : 3
		},
		{
			key      : "btnDuplicateProject",
			label    : Lang.trans("duplicate"),
			icon     : <img src={duplicateIcon} alt='' className='card-action-icon' />,
			category : 3
		},
		{
			key      : "btnDeleteProject",
			label    : Lang.trans("delete"),
			icon     : <img src={deleteIcon} alt='' className='card-action-icon' />,
			category : 3
		}
	];
	
	/**
   * Lấy chi tiết danh sách video theo Project
   */
	const handleGetDetailProject = async () => 
	{
		if (id) 
		{
			const res = await projectApi.listVideoByProjectId(Number(id));

			if (res && res.data) 
			{
				setDetailProject(res.data);
			}
		}
	};

	/**
   * Xử lý sửa
   * @param data
   */
	const handleEditProject = async (data: IProject) => 
	{
		// TODO Link edit view
		dispatch(actSaveCreatedProject(Number(data.id)));
		navigate(`/edit/${data.id}`);
	};
 
	/**
	* Xử lý nhân bản
	* @param data
	*/
	const handleDuplicateProject = async (data: IProject) => 
	{
		const res = await projectApi.duplicateProject(data.id);

		try 
		{
			if (res && res.success && res.data) 
			{
				success(Lang.trans("project_duplicate_success"));
 
				// TODO Link edit view
				dispatch(actSaveCreatedProject(res.data.id));
				navigate(`/edit/${res.data.id}`);
 
				// TODO load list Project
				handleGetDetailProject();
			}
			else 
			{
				error(Lang.trans("project_duplicate_failed"));
			}
		}
		catch (err) 
		{
			error(Lang.trans("project_duplicate_failed"));
		}
		
	};
 
	/**
	* Xử lý sự kiện xóa Project
	* @param data
	*/
	const handleDeleteActionProject = async (data: IProject) => 
	{
		setDeleteSeleted(data);
		setModalDeleteConfirmOpen(true);
	};
 
	/**
	 * Handle undo
	 */
	const handleUndoDeleteProject = async (delId?: number) => 
	{
		try 
		{
			if (delId)
			{
				const res = await trashAPI.reverseProject(delId);

				if (res?.success) 
				{
					success(res.message || "");
					// TODO load list Project
					handleGetDetailProject();
				}
			}
		}
		catch (err) 
		{
			error(Lang.trans("failed"));
		}
	};

	/**
	* Xử lý sự kiện xác nhận xóa dự án
	*/
	const handleAcceptDeleteProject = async () => 
	{
		// TODO Call API
		if (deleteSeleted) 
		{
			try 
			{
				const res = await projectApi.deleteProject(deleteSeleted.id);
 
				if (res && res.success) 
				{
					success(Lang.trans("project_delete_success"));
					handleGetDetailProject();
					snackbar(Lang.trans("delete_successfully"), 
						(
							<Button type='text' size='small' 
								onClick={async () => { await handleUndoDeleteProject(deleteSeleted?.id); }}
							>
								{Lang.trans("undo")}
							</Button>)
					);

				}
				else 
				{
					error(Lang.trans("project_delete_failed"));
				}
			}
			catch (err) 
			{
				error(Lang.trans("project_delete_failed"));
			}
		}
		setModalDeleteConfirmOpen(false);
	};

	/**
	* Xử lý sau khi đóng form xóa project
	*/
	const handleAfterCloseDeleteProject = () => 
	{
		setDeleteSeleted(undefined);
	};

	/**
   * Xử lý các sự kiện click vào button trên card
   * @param e
   * @param data
   */
	const handleActionProject = (e: MenuInfo, data: IProject) => 
	{
		const key = e.key.split("__")[0];
 
		switch (key) 
		{
		case "btnEditProject":
			handleEditProject(data);
			break;
		case "btnDuplicateProject":
			handleDuplicateProject(data);
			break;
		case "btnDeleteProject":
			handleDeleteActionProject(data);
			break;
		}
	};

	/**
   * Khởi tạo phần từ cho Card action
   * @param data
   * @returns
   */
	const cardActions = (data: IProject) => 
	{
		const category = !data.is_exported ? 1 : 2,
			menu = menuItems.filter((x) => (category & x.category) === category),
			menuEl = (
				<DropdownMenuCardWrapper
					className='menu-card-project'
					onClick={(e) => 
					{
						handleActionProject(e, data);
					}}
				>
					{menu &&
            menu.length > 0 &&
            menuItems.map((item, i) => (<Menu.Item icon={item.icon} key={`${item.key}__${data.id}`}>{item.label}</Menu.Item>))}
				</DropdownMenuCardWrapper>
			);
    
		return (
			<Dropdown.Button
				overlay={menuEl}
				className='drp-card-action'
			/>
		);
	};

	/**
   * Lấy data project
   */
	const handleGetProjectById = async () => 
	{
		if (id) 
		{
			const res = await projectApi.detail(Number(id));

			if (res && res.success && res.data) 
			{
				setProject(res.data);
			}
		}
	};

	/**
   * Effect Project Id
   */

	useEffect(() => 
	{
		// Lấy thông tin Project
		handleGetProjectById();
		// Lấy danh sách video
		handleGetDetailProject();
	}, [ id ]);

	return (
		<DetailProjectComponentWrapper>
			<div className='breadcrumb-container'>
				<BreadcrumbWrapper separator=''>
					<Breadcrumb.Item>
						<Link to={"/dashboard/myproject"}>
							<Image src={arrowRight} preview={false} />
						</Link>
					</Breadcrumb.Item>
					<Breadcrumb.Item>
						{project?.name || project?.id || "Detail"}
					</Breadcrumb.Item>
				</BreadcrumbWrapper>
			</div>
			<div className='detail-project-container'>
				<Content>
					<Row gutter={[ 24, 24 ]}>
						{detailProject.map((item, i) => (
							<Col span={6} key={item.id}>
								<CardProject
									imgSrc={item?.thumbnail_url}
									title={item?.name ?? `${item.id }`}
									description={item?.updated_at}
									action={cardActions(item)}
									editable={false}
									model={item}
									isShowStatus
									is_exported={item.is_exported}
								/>
							</Col>
						))}
					</Row>
				</Content>
			</div>
			<ModalWrapper
				className='model-delete-confirm'
				title=''
				centered
				closable={false}
				open={modalDeleteConfirmOpen}
				onOk={() => handleAcceptDeleteProject()}
				onCancel={() => setModalDeleteConfirmOpen(false)}
				afterClose={() => handleAfterCloseDeleteProject()}
				okText='Delete'
				okButtonProps={{ className: "btn-confirm-delete", size: "large" }}
				cancelButtonProps={{ className: "btn-cancel-delete", size: "large" }}
			>
				<div className='content-model'>
					<div className='delete-icon-container mb-16'>
						<img src={deleteModalIcon} className='img-delete-modal' alt='' />
					</div>
					<div className='delete-question mb-16'>
						{Lang.trans("are_you_sure_delete")}
					</div>
					<div className='delete-message'>
						<span>Deleting this project will move all its version(s) to </span>
						<span>
							<strong className='bold'>“Trash” </strong>
						</span>
						<br />
						<span>
							within <strong className='bold'>30 days</strong>.
						</span>
					</div>
				</div>
			</ModalWrapper>
			
		</DetailProjectComponentWrapper>
	);
};

export default DetailProjectComponent;
