import { Breadcrumb, Modal } from "antd";
import styled from "styled-components";

const DetailProjectComponentWrapper = styled("div")`
  .breadcrumb-container {
    height: 50px;
    border-bottom: 2px solid #eaeaea;
    display: flex;
    align-items: center;
    margin-bottom: 15px;
  }
`;

const ModalWrapper = styled(Modal)`
  &.model-delete-confirm {
    .ant-modal-body {
      padding: 24px 36px;
    }
    .content-model {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-align: center;
      .mb-16 {
        margin-bottom: 16px;
      }

      .delete-icon-container {
        width: 160px;
        height: 160px;
        border-radius: 50%;
        background-color: #f2f2f2;
        display: flex;
        align-items: center;
        justify-content: center;

        .img-delete-modal {
          width: 160px;
          height: 160px;
        }
      }
      .delete-question {
        font-weight: 600;
        font-size: 22px;
        line-height: 33px;
        color: #131317;
      }
      .delete-message{
        font-weight: 400;
        font-size: 16px;
        line-height: 24px;
        color: #737373;
        .bold{
          color: #131317;
        }
      }
    }
    .ant-modal-footer {
      border-top: none;
      display: flex;
      justify-content: center;
      padding: 36px 36px;

      .ant-btn {
        flex: 1;
        border-radius: 8px;
        font-weight: 600;
        font-size: 16px;

        &.btn-confirm-delete {
          background-color: #fe2c55;
          border-color: #fe2c55;
          color: #ffffff;
        }
        &.btn-cancel-delete {
          border-color: #A0A0A0;
          background-color: transparent;
          color: #131317;
        }
      }
    }
  }
`;
const BreadcrumbWrapper = styled(Breadcrumb)`
  .ant-image {
    line-height: 16px;
    margin: 0 16px;
    &:hover {
      background-color: #f5f5f5;
    }
  }
`;

export { DetailProjectComponentWrapper, BreadcrumbWrapper, ModalWrapper };
