import React from "react";
export interface ICardProjectProps {
    title?: string;
    imgSrc?: string;
    description?: string;
    action?: React.ReactElement;
    editable?: boolean;
    model: any;
    is_exported?: boolean;
    video_time?: number;
    isShowStatus?: boolean;
    onClickCard?: () => any;
  }
