import React from 'react';
export interface SearchProps {
    handleSearch: (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => void;
}
