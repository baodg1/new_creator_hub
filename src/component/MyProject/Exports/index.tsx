import { Col, Row, Space, Menu, Dropdown } from "antd";
import emptyImage from "@/asset/empty-content.svg";
import deleteIcon from "@/asset/delete-icon.svg";
import deleteModalIcon from "@/asset/delete-modal-icon.svg";
import { Content } from "antd/lib/layout/layout";
import { useEffect, useState } from "react";
import { IProject } from "@/redux/interface";
import { MenuInfo } from "rc-menu/lib/interface";
import projectApi from "@/services/projectAPI";
import {
	DropdownMenuCardWrapper,
	ModalWrapper,
	TabPaneContentWrapper,
	TabPaneWrapper
} from "../TabAllProject/style";
import CardProject from "../Card";
import { success } from "@/component/Message";

const TabPaneExports = () => 
{
	const [ deleteSeleted, setDeleteSeleted ] = useState<IProject>();
	const [ modalDeleteConfirmOpen, setModalDeleteConfirmOpen ] = useState(false);
	const items = [
		{
			key   : "btnDeleteProject",
			label : "Delete",
			icon  : <img src={deleteIcon} alt='' className='card-action-icon' />
		}
	];
	const [ filters ] = useState({
		search        : "",
		limit         : 100,
		offset        : 0,
		status        : "ACTIVE",
		"is_exported" : 1
	});

	/**
   * Xử lý sự kiện xóa Project
   * @param project
   */
	const handleDeleteActionProject = async (project: IProject) => 
	{
		setDeleteSeleted(project);
		setModalDeleteConfirmOpen(true);
	}; 

	/**
		* Xử lý các sự kiện click vào button trên card
		* @param e
		* @param project
		*/
	const handleActionProject = (e: MenuInfo, project: IProject) => 
	{
		const key = e.key.split("__")[0];

		switch (key) 
		{
		case "btnDeleteProject":
			handleDeleteActionProject(project);
			break;
		}
	};

	/**
   * Khởi tạo phần từ cho Card action
   * @param project
   * @returns
   */
	const cardActions = (project: IProject) => 
	{
		const menuEl = (
			<DropdownMenuCardWrapper
				className='menu-card-project'
				onClick={(e) => 
				{
					handleActionProject(e, project);
				}}
			>
				{items.map((item, i) => (
					<Menu.Item icon={item.icon} key={`${item.key}__${project.id}`}>
						{item.label}
					</Menu.Item>
				))}
			</DropdownMenuCardWrapper>
		);
    
		return (
			<Dropdown.Button
				overlay={menuEl}
				className='drp-card-action'
			/>
		);
	};

	const [ projects, setProjects ] = useState<Array<IProject>>([]);

	/**
   * Xử lý lấy danh sách dự án
   */
	const handleGetListProject = async () => 
	{
		const res = await projectApi.listProject({ ...filters });

		if (res && res.success && res.data) 
		{
			setProjects(res.data);
		}
	};

	/**
   * Xử lý sự kiện xác nhận xóa dự án
   */
	const handleAcceptDeleteProject = async () => 
	{
		// TODO Call API
		if (deleteSeleted) 
		{
			const res = await projectApi.deleteProject(deleteSeleted.id);

			if (res && res.success) 
			{
				success("Delete success!");
				handleGetListProject();
			}
		}
		setModalDeleteConfirmOpen(false);
	};

	/**
   * Xử lý sau khi đóng form xóa project
   */
	const handleAfterCloseDeleteProject = () => 
	{
		setDeleteSeleted(undefined);
	};

	/**
   * Effect List Project
   */

	useEffect(() => 
	{
		handleGetListProject();
	}, [ filters ]);
	
	return (
		<TabPaneWrapper key='1' className='tabExport'>
			{projects.length > 0 && (
				<TabPaneContentWrapper className='content-container'>
					<Content style={{ padding: "24px 0 0 0" }}>
						<Row gutter={[ 24, 24 ]}>
							{projects.map((item, i) => (
								<Col span={6} key={item.id}>
									<CardProject
										imgSrc={item?.thumbnail_url}
										title={item?.name ?? `${item.id }`}
										description={item?.updated_at}
										action={cardActions(item)}
										editable={false}
										model={item}
									/>
								</Col>
							))}
						</Row>
					</Content>
				</TabPaneContentWrapper>
			)}
			{projects.length === 0 && (
				<TabPaneContentWrapper className='empty-content-container'>
					<Content style={{ padding: "70px 0" }}>
						<Space
							align='center'
							direction='vertical'
							size={"large"}
							style={{ width: "100%" }}
						>
							<div className='img-no-project'>
								<img className='empty-img' src={emptyImage} alt='' />
							</div>
							<div className='title-no-content'>
								<div className='title'>No Projects</div>
							</div>
						</Space>
					</Content>
				</TabPaneContentWrapper>
			)}
			<ModalWrapper
				className='model-delete-confirm'
				title=''
				centered
				closable={false}
				open={modalDeleteConfirmOpen}
				onOk={() => handleAcceptDeleteProject()}
				onCancel={() => setModalDeleteConfirmOpen(false)}
				afterClose={() => handleAfterCloseDeleteProject()}
				okText='Delete'
				okButtonProps={{ className: "btn-confirm-delete", size: "large" }}
				cancelButtonProps={{ className: "btn-cancel-delete", size: "large" }}
			>
				<div className='content-model'>
					<div className='delete-icon-container mb-16'>
						<img src={deleteModalIcon} className='img-delete-modal' alt='' />
					</div>
					<div className='delete-question mb-16'>
						Are you sure to delete all?
					</div>
					<div className='delete-message'>
						<span>Moving this version project to </span>
						<span>
							<strong>“Trash”</strong>
						</span>
						<span>
							within <strong>30 days</strong>
						</span>
					</div>
				</div>
			</ModalWrapper>
		</TabPaneWrapper>
	);
};

export default TabPaneExports;
