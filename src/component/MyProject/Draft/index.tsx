import { Col, Row, Space, Menu, Dropdown } from "antd";
import emptyImage from "@/asset/empty-content.svg";
import editIcon from "@/asset/edit-icon.svg";
import duplicateIcon from "@/asset/duplicate-icon.svg";
import { Content } from "antd/lib/layout/layout";
import { useEffect, useState } from "react";
import { IProject } from "@/redux/interface";
import { MenuInfo } from "rc-menu/lib/interface";
import projectApi from "@/services/projectAPI";
import { useNavigate } from "react-router-dom";
import { actSaveCreatedProject } from "@/redux/action/project";
import { useDispatch } from "react-redux";
import {
	DropdownMenuCardWrapper,
	TabPaneContentWrapper,
	TabPaneWrapper
} from "../TabAllProject/style";
import CardProject from "../Card";
import { error, success } from "@/component/Message";

const TabPaneDraft = () => 
{
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const [ projects, setProjects ] = useState<Array<IProject>>([]);
	const items = [
		{
			key   : "btnEditProject",
			label : "Edit",
			icon  : <img src={editIcon} alt='' className='card-action-icon' />
		},
		{
			key   : "btnDuplicateProject",
			label : "Duplicate",
			icon  : <img src={duplicateIcon} alt='' className='card-action-icon' />
		}
	];
	const [ filters ] = useState({
		search        : "",
		limit         : 100,
		offset        : 0,
		status        : "ACTIVE",
		"is_exported" : 0
	});
	
	/**
   * Xử lý lấy danh sách dự án
   */
	const handleGetListProject = async () => 
	{
		const res = await projectApi.listProject({ ...filters });	 
 
		if (res && res.success && res.data) 
		{
			setProjects(res.data);
		}
	};
 
	/**
	* Xử lý sửa
	* @param project
	*/
	const handleEditProject = async (project: IProject) => 
	{
		// TODO Link edit view
		dispatch(actSaveCreatedProject(Number(project.id)));
		navigate(`/edit/${project.id}`);
	};
  
	/**
	 * Xử lý nhân bản
	 * @param project
	 */
	const handleDuplicateProject = async (project: IProject) => 
	{
		const res = await projectApi.duplicateProject(project.id);
  
		if (res && res.success && res.data) 
		{
			success("Duplicate project success!");
  
			// TODO Link edit view
			dispatch(actSaveCreatedProject(res.data.id));
			navigate(`/edit/${res.data.id}`);
  
			// TODO load list Project
			handleGetListProject();
		}
		else 
		{
			error("Duplicate failed.");
		}
	};

	/**
   * Xử lý các sự kiện click vào button trên card
   * @param e
   * @param project
   */
	const handleActionProject = (e: MenuInfo, project: IProject) => 
	{
		const key = e.key.split("__")[0];

		switch (key) 
		{
		case "btnEditProject":
			handleEditProject(project);
			break;
		case "btnDuplicateProject":
			handleDuplicateProject(project);
			break;
		}
	};

	/**
   * Khởi tạo phần từ cho Card action
   * @param project
   * @returns
   */
	const cardActions = (project: IProject) => 
	{
		const menuEl = (
			<DropdownMenuCardWrapper
				className='menu-card-project'
				onClick={(e) => 
				{
					handleActionProject(e, project);
				}}
			>
				{items.map((item, i) => (
					<Menu.Item icon={item.icon} key={`${item.key}__${project.id}`}>
						{item.label}
					</Menu.Item>
				))}
			</DropdownMenuCardWrapper>
		);
    
		return (
			<Dropdown.Button
				overlay={menuEl}
				className='drp-card-action'
			/>
		);
	};

	/**
   * Effect List Project
   */

	useEffect(() => 
	{
		handleGetListProject();
	}, [ filters ]);
	
	return (
		<TabPaneWrapper key='1' className='tabDraft'>
			{projects.length > 0 && (
				<TabPaneContentWrapper className='content-container'>
					<Content style={{ padding: "24px 0 0 0" }}>
						<Row gutter={[ 24, 24 ]}>
							{projects.map((item, i) => (
								<Col span={6} key={item.id}>
									<CardProject
										imgSrc={item?.thumbnail_url}
										title={item?.name ?? `${item.id }`}
										description={item?.updated_at}
										action={cardActions(item)}
										editable={false}
										model={item}
									/>
								</Col>
							))}
						</Row>
					</Content>
				</TabPaneContentWrapper>
			)}
			{projects.length === 0 && (
				<TabPaneContentWrapper className='empty-content-container'>
					<Content style={{ padding: "70px 0" }}>
						<Space
							align='center'
							direction='vertical'
							size={"large"}
							style={{ width: "100%" }}
						>
							<div className='img-no-project'>
								<img className='empty-img' src={emptyImage} alt='' />
							</div>
							<div className='title-no-content'>
								<div className='title'>No Projects</div>
							</div>
						</Space>
					</Content>
				</TabPaneContentWrapper>
			)}
		</TabPaneWrapper>
	);
};

export default TabPaneDraft;
