import { CButton } from "@/component/Common";
import { Menu, Modal } from "antd";
import styled from "styled-components";

const TabPaneWrapper = styled("div")`
  &.tabProject {
    .empty {
      padding-top: 85px;
      text-align: center;
      img {
        opacity: 0.5;
      }
      div > div {
        padding-top: 60px;
        font-weight: 600;
        font-size: 28px;
      }
      div > span {
        padding-top: 8px;
        font-size: 20px;
        color: #62666a;
      }
    }
  }
`;

const TabPaneContentWrapper = styled("div")`
  .title-no-content {
    text-align: center;
    .title {
      font-weight: 600;
      font-size: 22px;
      color: #4A4A4D;
    }
    .message {
      font-weight: 400;
      font-size: 16px;
      line-height: 25px;
      color: #C4C4C4;
    }
  }
`;

const BtnCreateVideo = styled(CButton)`
  width: 240px;
  height: 56px;
  border: 1px solid #fe2c55;
  border-radius: 8px;
  color: #fe2c55;
  span {
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 19px;
  }
`;

const ModalWrapper = styled(Modal)`
  &.model-delete-confirm {
    .ant-modal-body {
      padding: 24px 36px;
    }
    .content-model {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-align: center;
      .mb-16 {
        margin-bottom: 16px;
      }

      .delete-icon-container {
        width: 160px;
        height: 160px;
        border-radius: 50%;
        background-color: #f2f2f2;
        display: flex;
        align-items: center;
        justify-content: center;

        .img-delete-modal {
          width: 85px;
          height: 105px;
        }
      }
      .delete-question {
        font-weight: 600;
        font-size: 28px;
        color: #fe2c55;
      }
    }
    .ant-modal-footer {
      border-top: none;
      display: flex;
      justify-content: center;
      padding: 36px 36px;

      .ant-btn {
        flex: 1;
        border-radius: 8px;
        font-weight: 600;
        font-size: 16px;

        &.btn-confirm-delete {
          background-color: #fe2c55;
          border-color: #fe2c55;
          color: #ffffff;
        }
        &.btn-cancel-delete {
          border-color: #fe2c55;
          background-color: #ffffff;
          color: #fe2c55;
        }
      }
    }
  }
`;

const DropdownMenuCardWrapper = styled(Menu)`
  &.ant-dropdown-menu {
    padding: 0px !important;
    border-radius: 4px;
    .ant-dropdown-menu-item {
      height: 52px;
      min-width: 150px;
    }
  }
`;

export {
	TabPaneWrapper,
	BtnCreateVideo,
	TabPaneContentWrapper,
	ModalWrapper,
	DropdownMenuCardWrapper
};
