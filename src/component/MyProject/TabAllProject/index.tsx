import { TabPaneWrapper, TabPaneContentWrapper, BtnCreateVideo } from "./style";
import { Col, Row, Space } from "antd";
import emptyImage from "@/asset/empty-content.svg";
import { PlusCircleFilled } from "@ant-design/icons";
import { Content } from "antd/lib/layout/layout";
import { useEffect, useState } from "react";
import { IProject } from "@/interface/project";
import projectApi from "@/services/projectAPI";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { actSaveCreatedProject } from "@/redux/action/project";
import { error, success } from "@/component/Message";
import ProjectFolderConponent from "./ProjectFolder";
import { Lang } from "@/utils/config";

const TabPaneAllProject = () => 
{
	const [ projects, setProjects ] = useState<Array<IProject>>([]);
	const [ filters ] = useState({
		search : "",
		limit  : 100,
		offset : 0,
		status : "ACTIVE"
	});

	const dispatch = useDispatch();
	const navigate = useNavigate();

	/**
   * Xử lý lấy danh sách dự án
   */
	const handleGetListProject = async () => 
	{
		const res = await projectApi.listProject({ ...filters });

		if (res && res.success && res.data) 
		{
			setProjects(res.data);
		}
	};

	/**
   * Xử lý thêm mới dự án
   */
	const handleCreateProject = async () => 
	{
		const res = await projectApi.createProject({}, {});

		if (res && res.success && res.data) 
		{
			success(Lang.trans("create_project_success"));

			// TODO Link edit view
			dispatch(actSaveCreatedProject(res.data.id));
			navigate(`/edit/${res.data.id}`);

			// TODO load list Project
			handleGetListProject();
		}
		else 
		{
			error(Lang.trans("create_project_failed"));
		}
	};

	/**
   * Xử lý click card Detail
   * @param id
   */
	const onClickProjectDetail = (id: number) => 
	{
		if (id) 
		{
			navigate(`/dashboard/myproject/${id}`);
		}
	};

	/**
   * Effect List Project
   */

	useEffect(() => 
	{
		handleGetListProject();
	}, [ filters ]);

	return (
		<TabPaneWrapper key='1' className='tabProject'>
			{projects.length > 0 && (
				<TabPaneContentWrapper className='content-container'>
					<Content style={{ padding: "24px 0 0 0" }}>
						<Row gutter={[ 24, 24 ]}>
							{projects.map((item, i) => (
								<Col span={6} key={item.id}>
									<ProjectFolderConponent
										key={item?.id}
										thumbnailUrl={item?.thumbnail_url}
										name={item?.name || String(item?.id)}
										videoCount={item.video_count || 0}
										model={item}
										editable
										onClickFolder={() => 
										{
											onClickProjectDetail(item.id);
										}}
									/>
								</Col>
							))}
						</Row>
					</Content>
				</TabPaneContentWrapper>
			)}
			{projects.length === 0 && (
				<TabPaneContentWrapper className='empty-content-container'>
					<Content style={{ padding: "70px 0" }}>
						<Space
							align='center'
							direction='vertical'
							size={"large"}
							style={{ width: "100%" }}
						>
							<div className='img-no-project'>
								<img className='empty-img' src={emptyImage} alt='' />
							</div>
							<div className='title-no-content'>
								<div className='title'>
									{Lang.trans("project_no_item_title")}
								</div>
								<div className='message'>
									{Lang.trans("project_no_item_message")}
								</div>
							</div>
							<div className='btn-create-video'>
								<BtnCreateVideo
									label={Lang.trans("create_new_video")}
									icon={<PlusCircleFilled />}
									onClick={handleCreateProject}
								/>
							</div>
						</Space>
					</Content>
				</TabPaneContentWrapper>
			)}
		</TabPaneWrapper>
	);
};

export default TabPaneAllProject;
