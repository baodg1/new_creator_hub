import react from "react";
import { Input } from "antd";
import { ProjectFolderConponentWrapper } from "./style";
import projectApi from "@/services/projectAPI";
import { Content } from "antd/lib/layout/layout";
import { IProjectFolderConponent } from "../../interface";
import { Lang } from "@/utils/config";
import { InputStatus } from "antd/lib/_util/statusUtils";
import { error } from "@/component/Message";

const ProjectFolderConponent = (props: IProjectFolderConponent) => 
{
	const { key, videoCount, onClickFolder } = props;

	const [ projectName, setprojectName ] = react.useState(props.name);
	const [ editing, setEditing ] = react.useState(false);
	const [ model, setModel ] = react.useState(props.model);
	const [ editStatus, setEditStatus ] = react.useState<InputStatus>("");

	/**
   * Thay đổi tên project
   * @param e event blur hoặc enter press
   */
	const saveChange = async (e: any) => 
	{
		const newName = e.target.value;

		if (newName == null || newName == "")
		{
			setEditStatus("error");
			error(Lang.trans("project_name_not_empty"));
			
			return;
		}

		if (newName && newName !== projectName) 
		{
			const newModel = { ...model, name: newName };
			const res = await projectApi.update(newModel);

			if (res?.success) 
			{
				setModel(newModel);
				setprojectName(newName);
			}
		}

		setEditing(false);
	};

	return (
		<ProjectFolderConponentWrapper key={key}>
			<div className='folder-item-body'>
				<div
					className='folder-thumbnail p-absolute-0'
					onClick={() => 
					{
						if (onClickFolder != null) 
						{
							onClickFolder();
						}
					}}
				>
					<div className='folder-bg p-absolute-0'>
						{/* <Image src={folderProjectDefault} preview={false} /> */}
					</div>
					<div className='folder-thumb-nail p-absolute-0'>
						{/* <Image src='https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png' preview={false} /> */}
					</div>
				</div>
				<div className='project-name'>
					<div className='name'>
						{editing && props.editable ? (
							<Input
								defaultValue={projectName}
								// bordered={false}
								onPressEnter={saveChange}
								onFocus={(e) => e.target.select()}
								onBlur={saveChange}
								status={editStatus}
								autoFocus
							/>
						) : (
							<Content onClick={() => setEditing(true)}>{projectName}</Content>
						)}
					</div>
				</div>
				<div className='video-count'>
					<div className='count'>{videoCount || 0} {Lang.trans("files_total")}</div>
				</div>
			</div>
		</ProjectFolderConponentWrapper>
	);
};

export default ProjectFolderConponent;
