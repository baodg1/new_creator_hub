import styled from "styled-components";
import folderProjectDefault from "@/asset/folder-project-default.svg";

const ProjectFolderConponentWrapper = styled("div")`
  border-radius: 8px;
  width: 100%;
  border: none;
  .folder-item-body {
    border-radius: 8px;
    min-height: 194px;
    margin: 0px !important;
    position: relative;
  }
  .folder-thumbnail {
    cursor: pointer;
    z-index: 0;
    .folder-bg {
      background-image: url(${folderProjectDefault});
      background-size: 100% auto;
      background-repeat: no-repeat;
      background-position: center center;
      border-radius: 12px;
    }
    .folder-thumb-nail {
      /* background-color: blue; */
      /* -webkit-clip-path: polygon(
        0% 20%,
        0% 100%,
        100% 100%,
        100% 9%,
        52% 9%,
        44% 20%
      );
      clip-path: polygon(0% 20%, 0% 100%, 100% 100%, 100% 9%, 52% 9%, 44% 20%);
      border-radius: 12px; */
    }
  }
  .video-count {
    position: absolute;
    bottom: 16px;
    left: 16px;
    z-index: 1;
    border-radius: 4px;
    background-color: transparent;
    color: #fff; //#d9d7d7
    padding: 4px 8px;
    font-weight: 400;
    font-size: 12px;
    line-height: 16px;
  }

  .project-name {
    position: absolute;
    bottom: 42px;
    left: 16px;
    right: 16px;
    z-index: 1;
    border-radius: 4px;
    background-color: transparent;
    color: #fff;
    padding: 4px 8px;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;

    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    display: -webkit-box;
    input {
      color: #fff !important;
      font-weight: 600;
      font-size: 16px;
      background-color: transparent !important;
      border-top: none;
      border-left: none;
      border-right: none;
      box-shadow: none;
    }
  }
  .p-absolute-0 {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
`;

export { ProjectFolderConponentWrapper };
