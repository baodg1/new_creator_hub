import { Input } from "antd";
import styled from "styled-components";
import { ICInputProps } from "./interface/CInput";

const inputStyleSearch = `
    width: 100%;
    height: 40px;
    padding: 8px 16px;
    border: 1px solid #E7E7E7;
    border-radius: 8px;
    .ant-input-prefix{
        margin-right:16px;
    }
`;
const CInput = (props: ICInputProps) => 
{
	const {
		placeholder,
		icon,
		disabled,
		style,
		type,
		className,
		width,
		onChange
	} = props;
	const InputComponent = styled(Input)`
    ${type === 1 ? inputStyleSearch : style}
  `;
  
	return (
		<InputComponent
			disabled={disabled}
			prefix={icon}
			placeholder={placeholder}
			className={className}
			width={width}
			onChange={onChange}
		/>
	);
};

export default CInput;
