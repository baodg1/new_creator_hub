import React from 'react';

export interface ICInputProps {
    placeholder?: string;
    icon?: any;
    disabled?: boolean;
    width?: number;
    type?: 1 | 2;
    height?: number;
    style?: string;
    className?: string;
    onChange?: (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => any;
  }
