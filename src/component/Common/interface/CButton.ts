export interface ICButtonProps {
    label?: string;
    icon?: any;
    disabled?: boolean;
    style?: string;
    type?: number;
    width?: number;
    height?: number;
    shape?: "round" | "circle" | "default";
    className?: string;
    loading?: boolean;
    onClick?: () => void;
  }
