import { Button } from "antd";
import styled from "styled-components";
import { ICButtonProps } from "./interface/CButton";

const buttonStyle = `
    width: 100%;
    height: 56px;
    background: #EFEFEF;
    border-radius: 8px;
    span{
        font-style: normal;
        font-weight: 600;
        font-size: 16px;
        color: #000000;
    }
`;

const CButton = (props: ICButtonProps) => 
{
	const { label, icon, disabled, style, type, shape, className, ...rest } =
    props;
	const ButtonComponent = styled(Button)`
    ${type === 1 ? buttonStyle : style}
  `;
  
	return (
		<ButtonComponent
			className={className}
			disabled={disabled}
			icon={icon}
			shape={shape}
			{...rest}
		>
			{label}
		</ButtonComponent>
	);
};

export default CButton;
