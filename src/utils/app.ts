import { IFrame } from "@/redux/interface";
import _ from "lodash";

const getTimeFormat = (durationInSecond: number, withMillisecond?: boolean) => 
{
	let minute: string|number = Math.floor(durationInSecond / 60);
	let second: string|number = withMillisecond
		? (durationInSecond % 60)?.toFixed(1)
		: Math.floor(durationInSecond % 60);

	if (minute < 10) 
	{
		minute = `0${minute}`;
	}

	if (second < 10 && !withMillisecond) 
	{
		second = `0${second}`;
	}

	return `${minute}:${second}`;
};

const parseTime = (durationInSecond: number) => 
{
	const minute: string|number = Math.floor(durationInSecond / 60);
	const second: string|number = (durationInSecond % 60)?.toFixed(1);

	return {
		minute,
		second
	};
};

const parseStringToTime = (timeStr: string) => 
{
	const listValue = timeStr?.split(":");
	let minute = 0;
	let second = 0;

	if (listValue?.length === 2) 
	{
		minute = Number(listValue[0]);
		second = Number(listValue[1]);
	}

	return {
		minute,
		second,
		isValid : typeof minute === "number" && typeof second === "number"
	};
};

const getSecondFromPositionX = (
	positionX: number,
	elementId: string,
	pixelPerSecond: number
) => 
{
	const timeRuler = document.getElementById(elementId) as HTMLDivElement;
	const timeRulerRect = timeRuler?.getBoundingClientRect();

	const fromSecond = (positionX - timeRulerRect?.left) / Number(pixelPerSecond);
  
	return fromSecond;
};

const getResizerFrameId = (trackIndex: number, frameIndex: number) => 
{
	return `resizer-${trackIndex}-frame-${frameIndex}`;
};

const getTrackId = (trackIndex: number) => 
{
	return `track-${trackIndex}`;
};

const shouldPreviewDisplay = (
	fromSecond: number,
	toSecond: number,
	timeRulerValue: number
) => 
{
	return fromSecond! <= timeRulerValue && toSecond! >= timeRulerValue;
};

const getReservseZIndex = (): number[] => 
{
	// create z-index list from 100, 99, ..., 2, 1 , 0
	return _.reverse(
		Array(100)
			.fill(null)
			.map((value: any, index: number) => index + 1)
	);
};

const getPreviewSpeed = (frame: IFrame): number => 
{
	const speed = 1;

	// TODO: Return spped
	// switch (frame?.type) 
	// {
	// case MEDIA_TYPE.VIDEO: {
	// 	const { videoOptions } = (frame?.setting || {}) as IVideoSetting;

	// 	speed = videoOptions?.speed || 1;
	// 	break;
	// }
		
	// case MEDIA_TYPE.AUDIO:{
	// 	const { audioOptions } = (frame?.setting || {}) as IAudioSetting;

	// 	speed = audioOptions?.speed || 1;
	// 	break;
	// }
	// default:{
	// 	speed = 1;
	// 	break;
	// }
	// }

	return speed;
};

export {
	getTimeFormat,
	getSecondFromPositionX,
	getResizerFrameId,
	getTrackId,
	shouldPreviewDisplay,
	getReservseZIndex,
	getPreviewSpeed,
	parseTime,
	parseStringToTime
};
