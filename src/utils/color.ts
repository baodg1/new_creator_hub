export type HexColor = string;

export interface RGBColor {
    r: number;
    g: number;
    b: number;
}

export function hexToRGB(color: HexColor): RGBColor
{
	const rgbColor = {
		r : parseInt(color.slice(1, 3), 16),
		g : parseInt(color.slice(3, 5), 16),
		b : parseInt(color.slice(5, 7), 16)
	};
    
	return rgbColor;
}

export function rgbToHex(rgbColor: RGBColor): HexColor 
{ 
	const { r, g, b } = rgbColor;
	
	return `#${ [ r, g, b ].map((x) => 
	{
		const hex = x.toString(16);
		
		return hex.length === 1 ? `0${ hex}` : hex;
	}).join('')}`;
}

export const BLACK_COLOR: HexColor = "#000000";
export const WHITE_COLOR: HexColor = "#ffffff";
