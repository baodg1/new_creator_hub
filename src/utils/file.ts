import { FileType } from "@/config/constant";

export const getMediaType = (type: string): FileType => 
{
	if (type?.includes("image")) 
	{
		return FileType.Image;
	}
	else if (type?.includes("video")) 
	{
		return FileType.Video;
	}
	else if (type.includes("audio")) 
	{
		return FileType.Audio;
	}

	return FileType.Unknown;
};
