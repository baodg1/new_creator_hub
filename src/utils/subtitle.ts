import { DEFAULT_SUBTITLE_TEXT_CONFIG, SUBTITLE_TRACK_ID, SUBTITLE_TRACK_INDEX } from "@/redux/constant/subtitle";
import { DEFAULT_FRAME_SPEED, DEFAULT_PREVIEW_CONFIG } from "@/redux/constant/track";
import { ISubtitleFrame } from "@/redux/interface";
import { ISubtitle } from "@/redux/interface/subtitle";
import { MediaType } from "@/services/interface/project";
import { uuidv4 } from "@firebase/util";

export const createFrameFromSubtitle = (subtitle: ISubtitle, index: number) => 
{
	const {
		startTime,
		duration,
		textContent
	} = subtitle;

	const subtitleFrame: ISubtitleFrame = {
		id         : uuidv4(),
		trackId    : SUBTITLE_TRACK_ID,
		trackIndex : SUBTITLE_TRACK_INDEX,
		frameIndex : index,
		type       : MediaType.Subtitle,
		timeline   : {
			fromSecond      : startTime,
			toSecond        : startTime + duration,
			speed           : DEFAULT_FRAME_SPEED,
			trimStartSecond : 0,
			trimEndSecond   : duration,
			duration        : duration
		},
		textConfig : {
			...DEFAULT_SUBTITLE_TEXT_CONFIG,
			textContent : textContent
		},
		preview : DEFAULT_PREVIEW_CONFIG
	};
    
	return subtitleFrame;
};
