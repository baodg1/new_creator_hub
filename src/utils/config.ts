export class LocalStorage 
{
	static get = (key: any) => 
	{
		if (!key) return;
		
		return JSON.parse(window?.localStorage?.getItem(key) as never);
	};

	static set = (key: any, value: any) => 
	{
		if (!key || !value) return;
		window.localStorage.setItem(key, JSON.stringify(value));
	};

	static remove = (key: any) => 
	{
		if (!key) return;
		window.localStorage.removeItem(key);
	};

	static clear = () => 
	{
		window.localStorage.clear();
	};
}

export class Lang 
{
	static trans = (key: string) => 
	{
		if (!key) return "";
		try 
		{
			const env = LocalStorage.get("lang") || "en";
			const dict = require(`@/lang/${env}/${env}.json`);
      
			return dict?.[key];
		}
		catch (err) 
		{
			// console.log("loi", err);
			
			return "";
		}
	};
}
