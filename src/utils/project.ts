/* eslint-disable */

import { DURATION_OPTION, FileType } from "@/config/constant";
import { IAudioConfig, IAudioFrame, IFrame, IImageFrame, IPreviewConfig, ISubtitleFrame, ISubtitleTrack, ITextConfig, ITextFrame, ITimelineConfig, ITrack, IVideoFrame, TrackType } from "@/redux/interface";
import { IMediaAssetList } from "@/interface";
import { uuidv4 } from "@firebase/util";
import { BackgroundType, IAudioAsset, IAudioOptions, IImageAsset, IImageOptions, IMediaAsset, IProjectResponse, ITextAsset, ITextOptions, ITimelineOptions, IVideoAsset, MediaType, TextDirection } from "@/services/interface/project";
import { IProjectSetting } from "@/redux/interface/setting";
import { BLACK_COLOR, HexColor, hexToRGB, rgbToHex } from "./color";
import { DEFAULT_SUBTITLE_TRACK, SUBTITLE_TRACK_ID, SUBTITLE_TRACK_INDEX } from "@/redux/constant/subtitle";

export const formatVideoToApi = (frame:IFrame, layerIndex:number) => 
{
	// return {
	// 	"type"                  : MEDIA_TYPE.VIDEO,
	// 	"url"                   : frame.mediaUrl,
	// 	"timeline_options" : [
	// 		{
	// 			"layer_index"          : layerIndex,
	// 			"playback_start_time"  : frame.fromSecond?.toString(),
	// 			"duration"             : frame.duration?.toString(),
	// 			"trim_from_start_time" : "1",
	// 			"trim_from_end_time"   : "1",
	// 			"play_back_rate"       : 1
	// 		}
	// 	],
	// 	"image_options" : [
	// 		{
	// 			"is_flip_x_applied"         : 1,
	// 			"is_flip_y_applied"         : 1,
	// 			"rotate_angel"              : 1,
	// 			"translate_x"               : frame?.preview?.centerX?.toString()??"1",
	// 			"translate_y"               : frame?.preview?.centerY?.toString()??"11",
	// 			"width"                     : "1",
	// 			"height"                    : "1",
	// 			"comer_radius_top_left"     : 1,
	// 			"comer_radius_top_right"    : 1,
	// 			"comer_radius_bottom_left"  : 1,
	// 			"comer_radius_bottom_right" : 1,
	// 			"crop_left"                 : "1",
	// 			"crop_right"                : "1",
	// 			"crop_top"                  : "1",
	// 			"crop_bottom"               : "1"
	// 		}
	// 	],
	// 	"media_overlay" : [
	// 		{
	// 			"overlay_id" : 1,
	// 			"intensity"  : "1",
	// 			"duration"   : "1"
	// 		}
	// 	]
	// };
};

export const formatSubtitleFromApi = (listSubtitle :IMediaAssetList[]): any => 
{
	// const listTrackSubtitle: ITrack = {
	// 	id : listSubtitle[0]?.timeline_options && 
	// 		listSubtitle[0]?.timeline_options?.layer_index,
	// 	// trackIndex    : uuidv4(),
	// 	subtitleTrack : true,
	// 	frames        : listSubtitle.map((item, index) => 
	// 	{
	// 		const fromSecond = item?.timeline_options 
	// 			&& item?.timeline_options?.playback_start_time
	// 			&& Number(item?.timeline_options?.playback_start_time);

	// 		const duration = item?.timeline_options && item?.timeline_options?.duration 
	// 			&& Number(item?.timeline_options?.duration);

	// 		const trimEndSecond = item?.timeline_options && item?.timeline_options?.duration
	// 			&& Number(item?.timeline_options?.duration);
				
	// 		return {
	// 			type            : item.type,
	// 			fromSecond      : fromSecond,
	// 			duration        : duration,
	// 			trimStartSecond : 0,
	// 			trimEndSecond   : trimEndSecond,
	// 			setting         : {
	// 				textOptions : {
	// 					textContent     : item?.text_options && item?.text_options?.content,
	// 					textColor       : item?.text_options && item?.text_options?.text_color,
	// 					fontWeight      : item?.text_options && item?.text_options?.font_weight,
	// 					align           : item?.text_options && item?.text_options?.align,
	// 					fontSize        : item?.text_options && item?.text_options?.font_size,
	// 					lineHeight      : item?.text_options && item?.text_options?.line_height,
	// 					letterSpacing   : item?.text_options && item?.text_options?.letter_spacing,
	// 					fontFamily      : item?.text_options && item?.text_options?.font_family,
	// 					backgroundColor : item?.text_options && item?.text_options?.back_ground_color
	// 				}
	// 			},
	// 			id         : item?.text_options && item?.text_options?.id,
	// 			toSecond   : fromSecond+duration,
	// 			trackIndex : index,
	// 			trackId    : item?.timeline_options && item?.timeline_options?.layer_index,
	// 			frameIndex : index
	// 		};
	// 	})
			
	// };
		
	// return listTrackSubtitle;
};

export const formatImageFromApi = (listImage :IMediaAssetList[]) => 
{
	// const listTrackImage: ITrack ={
	// 	id : listImage[0]?.timeline_options && 
	// 	listImage[0]?.timeline_options?.layer_index,
	// 	// trackIndex : uuidv4(),
	// 	frames : listImage.map((item, index) => 
	// 	{
	// 		const fromSecond = item?.timeline_options 
	// 		&& item?.timeline_options?.playback_start_time
	// 		&& Number(item?.timeline_options?.playback_start_time);

	// 		const duration = item?.timeline_options && item?.timeline_options?.duration 
	// 		&& Number(item?.timeline_options?.duration);

	// 		const trimEndSecond = item?.timeline_options && item?.timeline_options?.duration
	// 		&& Number(item?.timeline_options?.duration);
			
	// 		return {
	// 			type            : item.type,
	// 			fromSecond      : fromSecond,
	// 			duration        : duration,
	// 			trimStartSecond : 0,
	// 			trimEndSecond   : trimEndSecond,
	// 			mediaUrl        : listImage&&listImage[0]?.url,
	// 			id              : uuidv4(),
	// 			preview         : {
	// 				centerX : item?.image_options && 
	// 				item?.image_options?.translate_x && Number(item?.image_options?.translate_x),
	// 				centerY : item?.image_options && 
	// 				item?.image_options?.translate_y && Number(item?.image_options?.translate_y),
	// 				width          : 0,
	// 				height         : 0,
	// 				maxHeight      : 0,
	// 				maxWidth       : 0,
	// 				isFlipXApplied : false,
	// 				isFlipYApplied : false,
	// 				rotateAngle    : 0
	// 			},
	// 			toSecond   : fromSecond+duration,
	// 			trackIndex : index,
	// 			trackId    : item?.timeline_options && item?.timeline_options?.layer_index,
	// 			frameIndex : index
	// 		};
	// 	})
	// };

	// return listTrackImage;
};

export const convertProjectSettingToApi = (projectSetting: IProjectSetting, listTrack: ITrack[], subtitleTrack: ISubtitleTrack) =>
{
	const {
		backgroundColor,
		projectId,
		name,
		backgroundImage,
		backgroundType,
		aspectRatioXValue,
		aspectRatioYValue,
		durationType,
		fixedDuration
	} = projectSetting;

	const {r, g, b} = hexToRGB(backgroundColor);

	const projectResponse: IProjectResponse = {
		id: projectId,
		name: name,
		thumbnail_url: "", // TODO: update thumbnail frequently
		video_url: "", // TODO: get video url from export
		background_color_a: 1, // we don't use alpha channel now
		background_color_r: r,
		background_color_g: g,
		background_color_b: b,
		background_type: backgroundType,
		background_image_url: backgroundImage,
		output_width_ratio: aspectRatioXValue, 
		output_height_ratio: aspectRatioYValue, 
		output_duration: fixedDuration, 
		is_fixed_duration: durationType === DURATION_OPTION.FIXED,
		media_asset_list: convertSubtitleTrackToApi(subtitleTrack),
	}

	return projectResponse;
};

export const convertSubtitleTrackToApi = (subtitleTrack: ISubtitleTrack) =>
{
	const subtitleAssets: ITextAsset[] = subtitleTrack.frames.map(frame => {
		const subtitleAsset: ITextAsset = {
			id: 0,
			type: frame.type,
			text_options: convertTextConfigToApi(frame.textConfig),
			image_options: convertImageConfigToApi(frame.preview),
			timeline_options: convertTimelineConfigToApi(frame.timeline, SUBTITLE_TRACK_INDEX),
			audio_options: {}
		};

		return subtitleAsset;
	});

	return subtitleAssets;
};

export const convertTextConfigToApi = (textConfig: ITextConfig) =>
{
	const {
		textContent,
		textColor,
		backgroundColor,
		align,
		letterSpacing,
		fontFamily,
		fontSize,
		fontWeight,
		fontStyle,
		lineHeight,
		isUppercase,
		borderRadius
	} = textConfig;

	const textOptions: ITextOptions = {
		content: textContent,
		text_color: textColor,
		back_ground_color: backgroundColor,
		align: align,
		letter_spacing: letterSpacing,
		font_family: fontFamily,
		font_size: fontSize,
		font_weight: fontWeight,
		font_style: fontStyle,
		direction: TextDirection.LeftToRight,
		line_height: lineHeight,
		is_uppercase: isUppercase,
		border_radius: borderRadius
	};

	return textOptions;
}

export const convertImageConfigToApi = (imageConfig: IPreviewConfig) =>
{
	const {
		width,
		height,
		// cropTop,
		// cropBottom,
		// cropLeft,
		// cropRight,
		centerX,
		centerY,
		roundCornerTopLeft,
		roundCornerTopRight,
		roundCornerBottomLeft,
		roundCornerBottomRight,
		rotateAngle,
		isFlipXApplied,
		isFlipYApplied
	} = imageConfig;

	const imageOptions: IImageOptions = {
		is_flip_x_applied: isFlipXApplied,
		is_flip_y_applied: isFlipYApplied,
		rotate_angle: rotateAngle,
		translate_x: centerX,
		translate_y: centerY,
		width: width,
		height: height,
		corner_radius_top_left: roundCornerTopLeft ? roundCornerTopLeft : 0,
		corner_radius_top_right: roundCornerTopRight ? roundCornerTopRight : 0,
		corner_radius_bottom_left: roundCornerBottomLeft ? roundCornerBottomLeft : 0,
		corner_radius_bottom_right: roundCornerBottomRight ? roundCornerBottomRight : 0,
		crop_left: 0,
		crop_right: 0,
		crop_top: 0,
		crop_bottom: 0
	};

	return imageOptions;
}

export const convertTimelineConfigToApi = (timelineConfig: ITimelineConfig, trackIndex: number) =>
{
	const {
		fromSecond,
		trimStartSecond,
		trimEndSecond,
		duration,
		speed
	} = timelineConfig;

	const timelineOptions: ITimelineOptions = {
		playback_start_time: fromSecond,
		trim_from_start_time: trimStartSecond,
		trim_from_end_time: trimEndSecond,
		duration: duration,
		play_back_rate: speed,
		layer_index: trackIndex
	};

	return timelineOptions;
}

export const getProjectSettingFromApi = (projectData: IProjectResponse) =>
{
	let backgroundColor: HexColor = BLACK_COLOR;
	if (projectData.background_type === BackgroundType.BackgroundColor)
	{
		backgroundColor = rgbToHex({
			r: projectData.background_color_r,
			g: projectData.background_color_g,
			b: projectData.background_color_b
		})
	}

	const projectSetting: IProjectSetting = {
		projectId: projectData.id,
		backgroundType: projectData.background_type,
		backgroundColor: backgroundColor,
		backgroundImage: projectData.background_image_url,
		aspectRatioXValue: projectData.output_width_ratio,
		aspectRatioYValue: projectData.output_height_ratio,
		fixedDuration: projectData.output_duration, 
		name: projectData.name,
		durationType: DURATION_OPTION.AUTOMATIC // TODO: Add this field to DB
	}

	return projectSetting;
}

export const getTimelineConfigFromApi = (timelineOptions: ITimelineOptions) => {
	const {
		playback_start_time,
		duration,
		trim_from_start_time,
		trim_from_end_time,
		play_back_rate
	} = timelineOptions;

	const timelineConfig: ITimelineConfig = {
		fromSecond: playback_start_time,
		duration: duration,
		trimStartSecond: trim_from_start_time,
		trimEndSecond: trim_from_end_time,
		toSecond: playback_start_time - trim_from_start_time + trim_from_end_time,
		speed: play_back_rate
	}

	return timelineConfig;
}

export const getPreviewConfigFromApi = (previewOptions: IImageOptions): IPreviewConfig => {
	const {
		is_flip_x_applied,
		is_flip_y_applied,
		rotate_angle,
		translate_x,
		translate_y,
		width,
		height,
		corner_radius_top_left,
		corner_radius_top_right,
		corner_radius_bottom_left,
		corner_radius_bottom_right,
		crop_left,
		crop_right,
		crop_top,
    	crop_bottom
	} = previewOptions;

	const previewConfig: IPreviewConfig = {
		width,
		height,
		maxWidth: 0,
		maxHeight: 0,
		cropTop: crop_top,
		cropBottom: crop_bottom,
		cropLeft: crop_left,
		cropRight: crop_right,
		centerX: translate_x,
		centerY: translate_y,
		roundCornerTopLeft: corner_radius_top_left,
		roundCornerTopRight: corner_radius_top_right,
		roundCornerBottomLeft: corner_radius_bottom_left,
		roundCornerBottomRight: corner_radius_bottom_right,
		rotateAngle: rotate_angle,
		isFlipXApplied: is_flip_x_applied,
		isFlipYApplied: is_flip_y_applied
	}

	return previewConfig;
}

export const getAudioConfigFromApi = (audioOptions: IAudioOptions): IAudioConfig => {
	const {
		volume,
		fade_int_duration,
		fade_out_duration,
		is_mute
	} = audioOptions;
	
	const audioConfig: IAudioConfig = {
		volume: volume,
		fadeInDuration: fade_int_duration,
		fadeOutDuration: fade_out_duration,
		isMute: is_mute
	};

	return audioConfig;
}

export const getVideoFrameFromApi = (videoAsset: IVideoAsset): IVideoFrame => {
	const {
		id,
		url,
		timeline_options,
		image_options,
		audio_options
	} = videoAsset;

	const videoFrame: IVideoFrame = {
		id: String(id),
		type: MediaType.Video,
		mediaUrl: url,
		file: {
			base64: "",
			thumbnailUrl: "",
			frameUrls: [],
			durationInSecond: timeline_options.duration,
			width: image_options.width,
			height: image_options.height,
			id: uuidv4(),
			name: "",
			url: "",
			type: FileType.Video, 
			isLoading: false,
			loadingPercent: 0, 
			isPreparing: false, 
			storedName: ""
		},
		trackId: String(timeline_options.layer_index),
		trackIndex: timeline_options.layer_index,
		frameIndex: 0,
		timeline: getTimelineConfigFromApi(timeline_options), // TODO: Turn this field into object, not array
		preview: getPreviewConfigFromApi(image_options),
		audioConfig: getAudioConfigFromApi(audio_options)
	}

	return videoFrame;
}

export const getImageFrameFromApi = (imageAsset: IImageAsset): IImageFrame => {
	const {
		id,
		url,
		timeline_options,
		image_options
	} = imageAsset;

	const imageFrame: IImageFrame = {
		id: String(id),
		type: MediaType.Image,
		mediaUrl: url,
		file: {
			width: image_options.width,
			height: image_options.height,
			id: uuidv4(),
			name: "",
			url: "",
			type: FileType.Image, 
			isLoading: false,
			loadingPercent: 0, 
			isPreparing: false, 
			storedName: ""
		},
		trackId: String(timeline_options.layer_index),
		trackIndex: timeline_options.layer_index,
		frameIndex: 0,
		timeline: getTimelineConfigFromApi(timeline_options),
		preview: getPreviewConfigFromApi(image_options),
	}

	return imageFrame;
}

export const getAudioFrameFromApi = (audioAsset: IAudioAsset): IAudioFrame => {
	const {
		id,
		url,
		timeline_options,
		audio_options
	} = audioAsset;

	const videoFrame: IAudioFrame = {
		id: String(id),
		type: MediaType.Audio,
		mediaUrl: url,
		file: {
			base64: "",
			durationInSecond: timeline_options.duration,
			id: uuidv4(),
			name: "",
			url: "",
			type: FileType.Audio, 
			isLoading: false,
			loadingPercent: 0, 
			isPreparing: false, 
			storedName: ""
		},
		trackId: String(timeline_options.layer_index),
		trackIndex: timeline_options.layer_index,
		frameIndex: 0,
		timeline: getTimelineConfigFromApi(timeline_options),
		audioConfig: getAudioConfigFromApi(audio_options)
	}

	return videoFrame;
}

export const getTextConfigFromApi = (textOptions: ITextOptions): ITextConfig => {
	const {
		content,
		direction,
		font_size,
		text_color,
		align,
		font_family,
		font_weight,
		font_style,
		line_height,
		letter_spacing,
		back_ground_color,
		border_radius,
		is_uppercase
	} = textOptions;

	const textConfig: ITextConfig = {
		textContent: content,
		textColor: text_color,
		align: align,
		justify: 0,
		isUppercase: is_uppercase,
		fontFamily: font_family,
		fontWeight: font_weight,
		fontStyle: font_style,
		fontSize: font_size,
		lineHeight: line_height,
		letterSpacing: letter_spacing,
		backgroundToTextGap: 0, // unused for now
		boxToBackgroundGap: 0, // unused for now
		backgroundColor: back_ground_color,
		borderRadius: border_radius,
	}

	return textConfig;
}

export const getTextFrameFromApi = (textAsset: ITextAsset): ITextFrame => {
	const {
		id,
		timeline_options,
		text_options,
		image_options
	} = textAsset;
	

	const textFrame: ITextFrame = {
		id: String(id),
		type: MediaType.Text,
		trackId: String(timeline_options.layer_index),
		trackIndex: timeline_options.layer_index,
		frameIndex: 0,
		timeline: getTimelineConfigFromApi(timeline_options),
		textConfig: getTextConfigFromApi(text_options),
		preview: getPreviewConfigFromApi(image_options)
	}

	return textFrame;
}

export const getSubtitleFrameFromApi = (textAsset: ITextAsset, index: number): ITextFrame => {
	const {
		id,
		timeline_options,
		text_options,
		image_options
	} = textAsset;
	

	const textFrame: ITextFrame = {
		id: String(id),
		type: MediaType.Subtitle,
		trackId: SUBTITLE_TRACK_ID,
		trackIndex: SUBTITLE_TRACK_INDEX,
		frameIndex: index,
		timeline: getTimelineConfigFromApi(timeline_options), // TODO: Turn this field into object, not array
		textConfig: getTextConfigFromApi(text_options),
		preview: getPreviewConfigFromApi(image_options)
	}

	return textFrame;
}

function addNewElementToMapArray<Type>(arrayMap: Map<number, Type[]>, newElement: Type, id: number)
{
	let listElement: Type[] = [];
	if (arrayMap.has(id))
	{
		listElement = arrayMap.get(id)!;
	}

	listElement.push(newElement);
	arrayMap.set(id, listElement);
}

export const getTrackListFromApi = (mediaAssetList: IMediaAsset[]) => {
	const listTrack: ITrack[] = [];
	const listAudioFrame = new Map<number, IAudioFrame[]>();
	const listNormalFrame = new Map<number, IFrame[]>();
	const listSubtitleFrame: ISubtitleFrame[] = [];

	for (const mediaAsset of mediaAssetList)
	{
		switch (mediaAsset.type)
		{
			case MediaType.Audio:
				{
					const audioFrame = getAudioFrameFromApi(mediaAsset as IAudioAsset);
					const { trackIndex } = audioFrame; 
					addNewElementToMapArray(listAudioFrame, audioFrame, trackIndex);
				}
				break;
			case MediaType.Video:
				{
					const videoFrame = getVideoFrameFromApi(mediaAsset as IVideoAsset);
					const { trackIndex } = videoFrame; 
					addNewElementToMapArray(listNormalFrame, videoFrame, trackIndex);
				}
				break;
			case MediaType.Text:
				{
					const textFrame = getTextFrameFromApi(mediaAsset as ITextAsset);
					const { trackIndex } = textFrame; 
					addNewElementToMapArray(listNormalFrame, textFrame, trackIndex);
				}
				break;
			case MediaType.Image:
				{
					const imageFrame = getImageFrameFromApi(mediaAsset as IImageAsset);
					const { trackIndex } = imageFrame; 
					addNewElementToMapArray(listNormalFrame, imageFrame, trackIndex);
				}
				break;
			case MediaType.Subtitle:
				listSubtitleFrame.push(getSubtitleFrameFromApi(mediaAsset as ITextAsset, listSubtitleFrame.length));
		}
	}

	// There is always a subtitle track
	// const subtitleFrames = {
	// 	id: SUBTITLE_TRACK_ID,
	// 	trackIndex: SUBTITLE_TRACK_INDEX,
	// 	type: TrackType.Subtitle,
	// 	frames: listSubtitleFrame.map((frame, index) => {
	// 		frame.frameIndex = index;
	// 		frame.trackId = SUBTITLE_TRACK_ID;
	// 		frame.trackIndex = SUBTITLE_TRACK_INDEX;
	// 		return frame;
	// 	})
	// };

	[...listNormalFrame.entries()].forEach(([trackIndex, frames]) => {
		const trackid = uuidv4();

		const normalTrack: ITrack = {
			id: trackid,
			type: TrackType.Normal,
			trackIndex: trackIndex,
			frames: frames.map((frame, index) => {
				frame.frameIndex = index;
				frame.trackId = trackid;
				frame.trackIndex = trackIndex;
				return frame;
			})
		}
		listTrack.push(normalTrack);
	});

	// Audio tracks are always at the end
	[...listAudioFrame.entries()].forEach(([trackIndex, frames]) => {
		const trackid = uuidv4();

		const audioTrack: ITrack = {
			id: trackid,
			type: TrackType.Audio,
			trackIndex: trackIndex,
			frames: frames.map((frame, index) => {
				frame.frameIndex = index;
				frame.trackId = trackid;
				frame.trackIndex = trackIndex;
				return frame;
			})
		}
		listTrack.push(audioTrack);
	})

	return {listSubtitleFrame, listTrack};
}
