import fbIcon from "@/asset/create-post/facebook.svg";
import instagramIcon from "@/asset/create-post/instagram.svg";
import twitterIcon from "@/asset/create-post/twitter.svg";
import youtubeIcon from "@/asset/create-post/youtube.svg";
import tiktokIcon from "@/asset/create-post/tiktok.svg";
import {
	FileUploadI,
	PostPlatformI,
	PostScheduleI
} from "@/page/CreatePost/interface/post";

const TYPE_FB = {
	PAGE  : "page",
	GROUP : "group"
};

const TYPE_FB_INDEX = {
	PAGE  : 1,
	GROUP : 2,
	INS   : 3
};

const TYPE_SOCIAL_INDEX = {
	FACEBOOK  : 1,
	INSTAGRAM : 2,
	YOUTUBE   : 3,
	TIKTOK    : 4,
	TWITTER   : 5
};

const TYPE_SOCIAL_TEXT = {
	FACEBOOK  : 1,
	INSTAGRAM : "BUSINESS",
	YOUTUBE   : "CHANNEL",
	TIKTOK    : "PROFILE",
	TWITTER   : "PROFILE"
};

const PERMISSIONS_PAGE = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile"
];

const PERMISSIONS_GROUP = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"groups_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile",
	"pages_manage_posts"
];

const PERMISSIONS_INS = [
	"user_link",
	"publish_video",
	"pages_show_list",
	"pages_read_engagement",
	"pages_manage_posts",
	"public_profile",
	"instagram_basic",
	"instagram_manage_comments",
	"instagram_manage_insights",
	"instagram_content_publish"
];

const getIcon = (id: number) => 
{
	switch (id) 
	{
	case TYPE_SOCIAL_INDEX.FACEBOOK:
		return fbIcon;
	case TYPE_SOCIAL_INDEX.INSTAGRAM:
		return instagramIcon;
	case TYPE_SOCIAL_INDEX.TIKTOK:
		return tiktokIcon;
	case TYPE_SOCIAL_INDEX.TWITTER:
		return twitterIcon;
	case TYPE_SOCIAL_INDEX.YOUTUBE:
		return youtubeIcon;
	default:
		return fbIcon;
	}
};

const bodyCreatePost = (
	files: FileUploadI[],
	postPlatforms: PostPlatformI[],
	postSchedules: PostScheduleI[]
) => 
{
	return {
		"file_upload_list"   : files,
		"post_platform_list" : postPlatforms,
		"post_schedule_list" : postSchedules
	};
};

export {
	TYPE_FB,
	TYPE_FB_INDEX,
	TYPE_SOCIAL_INDEX,
	TYPE_SOCIAL_TEXT,
	PERMISSIONS_PAGE,
	PERMISSIONS_GROUP,
	PERMISSIONS_INS,
	getIcon,
	bodyCreatePost
};
