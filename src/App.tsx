import { Provider } from "react-redux";
import { QueryClient, QueryClientProvider } from "react-query";
import { ConfigProvider } from "antd";
import { store } from "@/config/store";
import AppRoute from "@/route";
import "antd/dist/antd.variable.min.css";
import "@szhsin/react-menu/dist/index.css";
import { GlobalStyle } from "./style";

ConfigProvider.config({
	theme : {
		primaryColor : "#5EF2E0"
	}
});

const queryClient = new QueryClient();

const App = () => 
{
	return (
		<Provider store={store}>
			{/* <PersistGate persistor={persistor}> */}
			<QueryClientProvider client={queryClient}>
				<AppRoute />
			</QueryClientProvider>
			<GlobalStyle />
			{/* </PersistGate> */}
		</Provider>
	);
};

export default App;
