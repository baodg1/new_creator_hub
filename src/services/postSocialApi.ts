import http from "@/services";

const postSocialApi = {
	/**
   * Thêm mới
   * @returns
   */
	create : (requestBody: object, params?: object) =>
		http.post("post/create", requestBody, { params, private: true })
	
};

export default postSocialApi;
