import HttpApiService from '../api/httpcommon';
import { IProjectResponse } from './interface/project';

export class ProjectService extends HttpApiService 
{
	detail = (projectId: number) => 
	{
		return this.get(`project/detail/${projectId}`);
	};

	update = (data: IProjectResponse) =>
	{
		return this.put(`project/update/${data.id}`, data);
	};

}
export const ProjectAPIService = new ProjectService();
