import request from "@/services";

export const APIConnectAccount = (data: object) => 
{
	return request.post("/social/connect", data, { private: true });
};

export const APIGroupList = (params: object) => 
{
	return request.get("/social/list-group", {
		params,
		private : true
	});
};

export const APIPageList = (params: object) => 
{
	return request.get("/social/list-page", {
		params,
		private : true
	});
};
