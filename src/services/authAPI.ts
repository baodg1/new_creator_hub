import http from "@/services";

const authAPI = {
	/**
   * Đăng ký
   * @param email
   * @param password
   * @returns
   */
	signUp : (email: string, password: string) =>
		http.post("users/sign-up", { email, password }),

	/**
   * Đăng nhập
   * @param email
   * @param password
   * @returns
   */
	login : async (email: string, password: string) =>
		http.post("users/sign-in", { email, password }),

	/**
   * Đăng nhập với mạng xã hội
   * @param platform
   * @param accessToken
   * @returns
   */
	signInSocial : async (platform: string, accessToken: string) =>
		http.post("users/sign-in-social", { platform, "access_token": accessToken }),

	/**
   * forgot password
   * @param email
   * @returns
   */
	forgotPassword : async (email: string) =>
		http.post("users/forgot-password", { email }),

	/**
   * forgot password
   * @param otp
   * @returns
   */
	forgotPasswordConfirmCode : async (email: string, code: string) =>
		http.post("users/forgot-confirm-code", { email, code }),

	/**
   * reset password forgot
   * @param otp
   * @param email
   * @param password
   * @returns
   */
	forgotPasswordReset : async (code: string, email: string, password: string) =>
		http.post("users/reset-forgot-password", {
			code,
			email,
			password
		}),

	/**
   * Lấy thông tin user
   */
	getUserInfo : async () => http.get("users/profile", { private: true }),

	/**
   * Cập nhật thông tin user
   */
	putUserInfo : async (param: {}) =>
		http.put("users/profile", param, { private: true }),

	/**
   * Gọi lấy OTP reset pass
   * @returns
   */
	resetPasswordOTP : async () =>
		http.post("users/reset-password", {}, { private: true }),

	/**
   * Confirm OTP reset pass
   * @returns
   */
	resetPasswordConfirmOTP : async (code: string) =>
		http.post("users/reset-confirm-code", { code }, { private: true }),

	/**
   * resetpass
   * @returns
   */
	resetPassword : async (code: string, password: string) =>
		http.post(
			"users/reset-confirm-password",
			{ code, password },
			{ private: true }
		),

	/**
   * Disconnect
   * @returns
   */
	userProfileDisconnectService : async () =>
		http.post("users/disconnect", {}, { private: true })
};

export default authAPI;
