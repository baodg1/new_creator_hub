import { IFile } from "@/redux/interface/file";
import http from "@/services";
import { AxiosProgressEvent } from "axios";
import { UPLOAD_TIMEOUT_IN_SECOND } from "./constants/upload";
import { IUploadMediaResponse } from "./interface/upload";

export const uploadAPI = {
	
	upload : async (formData:object, projectId:number, 
		onUploadProgress:Function, mediaType:string, fileId:string) => 
	{
		return await http.post(
			`media/${projectId}/upload`,
			formData,
			{
				private : true,
				headers : {
					"Content-Type" : "multipart/form-data"					
				},
				onUploadProgress : (e: AxiosProgressEvent) =>
					onUploadProgress(e, mediaType, fileId)
			});
	},

	uploadToProject : async (
		projectId: number, 	
		metadata : IFile, 
		file: File,
		onUploadProgress: (progressEvent: AxiosProgressEvent, fileMetadata: IFile) => void
	) => 
	{
		const formData = new FormData();

		formData.append("file", file);
		
		return new Promise<IUploadMediaResponse>((resolve, reject) => 
		{ 
			http.post(
				`/media/${projectId}/upload`,
				formData,
				{
					private : true,
					timeout : UPLOAD_TIMEOUT_IN_SECOND * 1000,
					headers : {
						"Content-Type" : "multipart/form-data"					
					},
					onUploadProgress : (e: AxiosProgressEvent) =>
						onUploadProgress(e, metadata)
				}
			)
				.then((response) => 
				{
					if (response.success) 
					{
						resolve(response.data as IUploadMediaResponse);
					}
					else
					{
						reject(response.message);
					}
				})
				.catch((error) => reject(error));
		});
	}
};
