export interface IBase64AudioResponse {
	base64:string,
	url: string
}
