export interface IUploadMediaResponse {
    name: string;
    path: string;
    storedName: string;
    width?: number; // only for image and video
    height?: number; // only for image and video
    duration?: number; // only for video and audio
    frames?: string[]; // array of frame URL, only for video
}
