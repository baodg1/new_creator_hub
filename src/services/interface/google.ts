// interface for response from google speech to text api
export interface WordInfo {
    "startTime": string,
  "endTime": string,
  "word": string,
  "confidence": number,
  "speakerTag": number
}

export interface SpeechRecognitionAlternative { 
    "transcript": string,
    "confidence": number,
    "words": WordInfo[]
}

export interface SpeechRecognitionResult {
    "alternatives": SpeechRecognitionAlternative[],
      "channelTag": number,
      "resultEndTime": string,
      "languageCode": string
}

export interface GoogleSpeechToTextResponse {
    totalBilledTime: string,
    requestId: string,
    results: SpeechRecognitionResult[]
    
}
