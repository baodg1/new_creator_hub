import "axios";

declare module "axios" {
  export interface AxiosRequestConfig {
    private?: boolean;
  }
  export interface AxiosResponse {
    message?: string;
    success?: boolean;
    user?: any;
    total?: number;
    projectId?: number; // home content
    storedName?: string; // api file old
    path?: string; // api file old
  }
}
