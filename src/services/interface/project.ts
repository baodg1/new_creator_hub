/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */

import { TextAlignOption } from "@/redux/interface";
import { HexColor } from "@/utils/color";

export type FontFamily = string;

export enum BackgroundType {
    BackgroundImage = "IMAGE",
    BackgroundColor = "COLOR"
}

export enum MediaType {
    Image = "IMAGE",
    Video = "VIDEO",
    Text = "TEXT",
    Audio = "AUDIO",
    Subtitle = "SUBTITLE"
}

export enum TextDirection {
    LeftToRight = "ltr",
    RightToLeft = "rtl",
}

export enum TextAlign {
    Left = "left",
    Right = "right",
    Center = "center"
}

export interface ITimelineOptions {
    layer_index: number,
    playback_start_time: number,
    duration: number,
    play_back_rate: number,
    trim_from_start_time: number,
    trim_from_end_time: number
}

export interface IImageOptions {
    is_flip_x_applied: boolean,
    is_flip_y_applied: boolean,
    rotate_angle: number,
    translate_x: number,
    translate_y: number,
    width: number,
    height: number,
    corner_radius_top_left: number,
    corner_radius_top_right: number,
    corner_radius_bottom_left: number,
    corner_radius_bottom_right: number,
    crop_left: number,
    crop_right: number,
    crop_top: number,
    crop_bottom: number
}

export interface IAudioOptions {
    volume: number,
    fade_int_duration: number,
    fade_out_duration: number,
    is_mute: boolean
}

export interface ITextOptions {
    content: string,
    direction: TextDirection,
    text_color: HexColor,
    align: TextAlignOption,
    font_family: FontFamily,
    font_weight: number,
    font_style: string,
    font_size: number,
    line_height: number,
    letter_spacing: number,
    back_ground_color: string,
    is_uppercase: boolean,
    border_radius: number
}

export interface IMediaAsset {
    id: number;
    type: MediaType;
    timeline_options: ITimelineOptions;
}

export interface IMediaOverlay {
    overlay_id: number,
    intensity: number,
    duration: number
}

export interface IPreviewAsset extends IMediaAsset {
    image_options: IImageOptions;
}

export interface ILargeMediaAsset extends IMediaAsset {
    url: string;
}

export interface IAudioAsset extends ILargeMediaAsset {
    audio_options: IAudioOptions;
}

export interface IImageAsset extends ILargeMediaAsset, IPreviewAsset {
    transition_id: number;
}

export interface IVideoAsset extends IImageAsset, IAudioAsset {
    image_options: IImageOptions;
}

export interface ITextAsset extends IPreviewAsset {
    text_options: ITextOptions;
    audio_options: IAudioOptions | {};
}

export interface IProjectResponse {
    id: number,
    name: string,
    thumbnail_url: string,
    video_url: string,
    background_color_r: number,
    background_color_g: number,
    background_color_b: number,
    background_color_a: number,
    background_type: BackgroundType,
    background_image_url: string | null,
    output_width_ratio: number,
    output_height_ratio: number,
    output_duration: number,
    is_fixed_duration: boolean,
    user_id?: number, // we don't use it for now
    is_exported?: number, // we don't use it for now
    status?: string, // we don't use it for now
    created_at?: string, // we don't use it for now
    updated_at?: string, // we don't use it for now
    parent_id?: number, // we don't use it for now
    media_asset_list: IMediaAsset[],
}
