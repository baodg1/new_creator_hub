import { API_TIMEOUT, USER } from "@/config/constant";
import axios from "axios";
import { error } from "@/component/Message";
import { LocalStorage } from "@/utils/config";

const instance = axios.create({
	baseURL : `${process.env.REACT_APP_BASE_URL}`,
	headers : {
		"content-type" : "application/json"
	},
	timeout : API_TIMEOUT
});

// Add a request interceptor
instance.interceptors.request.use(
	function(config) 
	{
		const token = LocalStorage.get(USER.TOKEN);

		if (config?.private) 
		{
			config.headers = {
				Authorization : token ? `Bearer ${token}` : ""
			};
		}

		return config;
	},
	function(err) 
	{
		//  request error
		return Promise.reject(err);
	}
);

instance.interceptors.response.use(
	function(response) 
	{
		
		if (response.data) 
		{
			return response.data;
		}

		return Promise.reject(response);
	},
	function(err) 
	{
		if (err && err.response) 
		{
			let mes = "";

			switch (err.response.status) 
			{
			case 403:
				mes = err.response.statusText;
				break;
			default:
				mes = err.response.data?.message || "";
				break;
			}
			error(mes);
		}

		return Promise.reject(err);
	}
);

export default instance;
