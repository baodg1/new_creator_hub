import request from "@/services";

export const APIGetSocialList = (params: object) => 
{
	return request.get("/social", { params, private: true });
};

export const APIChangeStatus = (data: object) => 
{
	return request.post("/social/disconnect", data, { private: true });
};

export const APIDeleteConnect = (id: number) => 
{
	return request.delete(`/social/delete/${id}`, { private: true });
};
