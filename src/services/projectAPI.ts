import http from "@/services";

const projectApi = {
	/**
   * Thêm mới
   * @returns
   */
	createProject : (requestBody: object, params: object) =>
		http.post("project/create", requestBody, { params, private: true }),

	/**
   * Danh sách
   * @param params
   * @returns
   */
	listProject : (params: object) =>
		http.get("project", { params, private: true }),

	/**
   * Danh sách video Draft, Exported theo Project Id
   * @param params
   * @returns
   */
	listVideoByProjectId : (projectId: number) =>
		http.get(`project/list-exported/${projectId}`, {
			params  : { "is_exported": null },
			private : true
		}),

	/**
   * Xóa
   * @param projectId
   * @returns
   */
	deleteProject : (projectId: number) =>
		http.delete(`project/delete/${projectId}`, { private: true }),

	/**
   * Nhân bản
   * @param projectId
   * @returns
   */
	duplicateProject : (projectId: number) =>
		http.post(`project/duplicate/${projectId}`, null, { private: true }),

	/**
   * detail project
   * @param projectId
   * @returns
   */
	detailProject : (projectId: number) =>
		http.get(`project/detail/${projectId}`, { private: true }),

	/**
   *
   * @param model
   * @returns
   */
	update : (model: any) =>
		http.put(`project/update/${model.id}`, model, { private: true }),

	/**
   *
   * @param model
   * @returns
   */
	detail : (projectId: number) =>
		http.get(`project/detail/${projectId}`, { private: true })
};

export default projectApi;
