import axios from "axios";
import { GoogleSpeechToTextResponse } from "./interface/google";

const speechApiUrl = process.env.REACT_APP_GOOGLE_SPEECH_API_URL?.replace("/$", "") ?? "";

const getPath = (uri: string) => 
{
	return `${[ speechApiUrl, uri.replace("^/", "") ].join("/")}?key=${process.env.REACT_APP_GOOGLE_SPEECH_API_KEY}`;
};

export const googleAPI = {
	speechToText : async (baseBase64AudioFile: string, 
		languageCode: string) => 
	{
		try 
		{
			const res = await axios.post<GoogleSpeechToTextResponse>(getPath("speech:recognize"), {
				audio : {
					content : baseBase64AudioFile
				},
				config : {
					"encoding"              : "MP3",
					"languageCode"          : languageCode,
					"enableWordTimeOffsets" : true
				}
			});
            
			return res.data;
		}
		catch (err) 
		{
			return Promise.reject(err);
		}
	}
};
