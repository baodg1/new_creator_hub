import http from "@/services";
import { UPLOAD_TIMEOUT_IN_SECOND } from "./constants/upload";
import { IBase64AudioResponse } from "./interface/fileAPI";

const fileApi = {
	detachAudioFromVideo : (projectId: number, storedName: string) =>
		http.post(`projects/${projectId}/extract-audio`, {
			video : storedName
		}),

	convertToBase64 : async (
		file: File
		// onUploadProgress: (progressEvent: AxiosProgressEvent, fileMetadata: IFile) => void
	) => 
	{
		const formData = new FormData();

		formData.append("file", file);
		
		return new Promise<IBase64AudioResponse>((resolve, reject) => 
		{ 
			http.post(
				`/media/video-base64`,
				formData,
				{
					private : true,
					timeout : UPLOAD_TIMEOUT_IN_SECOND * 1000,
					headers : {
						"Content-Type" : "multipart/form-data"					
					}
					// onUploadProgress : (e: AxiosProgressEvent) =>
					// 	onUploadProgress(e, metadata)
				}
			)
				.then((response) => 
				{
					if (response.success) 
					{
						resolve(response.data as IBase64AudioResponse);
					}
					else
					{
						reject(response.message);
					}
				})
				.catch((error) => reject(error));
		});
	},

	baseBase64AudioFile : async (videoUrl: string) => 
	{
		try 
		{
			const response = await http.post<IBase64AudioResponse>(`media/video-base64`, {
				url : videoUrl
			}, {
				private : true
			});

			if (response.success) 
			{
				return response.data;
			}
		}
		catch (err) 
		{
			return Promise.reject(err);
		}
	}
};

export default fileApi;
