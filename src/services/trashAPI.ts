import http from "@/services";

export const trashAPI = {
	/**
   * Lấy danh sách project trong thùng rác
   * @returns Trashed project
   */
	getTrashedProject : async (limit: number, offset: number) => 
	{
		return await http.get(`project`, {
			params : {
				limit  : limit,
				offset : offset,
				status : "DEACTIVE"
			},
			private : true
		});
	},

	/**
   * phục hồi project
   * @param id ID project
   * @returns
   */
	reverseProject : async (id: number) => 
	{
		return await http.post(`project/reverse/${id}`, null, {
			private : true
		});
	}
};
