import request from "@/services";

export const APIUploadFile = (data: object) => 
{
	return request.post("/media/upload", data, { private: true });
};

export const APILoginTiktok = (params: object) => 
{
	return request.get("/oauth/tiktok-profile", { params, private: true });
};
