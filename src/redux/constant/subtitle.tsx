import { ISubtitleTrack, ITextConfig, TextAlignOption, TrackType } from "../interface";
import { ISubtitle } from "../interface/subtitle";

export const CURRENT_SUBTITLE_MODE = "CURRENT_SUBTITLE_MODE";
export const DEFAULT_SUBTITLE_DURATION = 3; // seconds
export const SUBTITLE_TRACK_INDEX = 0; // subtitle track always at first
export const SUBTITLE_TRACK_ID = "subtitle"; // just to clarify

export const DEFAULT_SUBTITLE_TEXT_CONFIG: ITextConfig = {
	textContent     : "Subtitle",
	textColor       : "#DEDEDE",
	fontWeight      : 500,
	align           : TextAlignOption.Center,
	fontSize        : 48,
	lineHeight      : 1,
	letterSpacing   : 1,
	fontFamily      : "Roboto",
	backgroundColor : "#535A61",
	justify         : 1,
	isUppercase     : false,
	fontStyle       : "",
	borderRadius    : 0
};

export const DEFAULT_SUBTITLE: ISubtitle = {
	startTime   : 0,
	duration    : DEFAULT_SUBTITLE_DURATION,
	textContent : "Subtitle"
};

export const DEFAULT_SUBTITLE_TRACK: ISubtitleTrack = {
	id         : SUBTITLE_TRACK_ID,
	type       : TrackType.Subtitle,
	frames     : [], 
	trackIndex : SUBTITLE_TRACK_INDEX
};
