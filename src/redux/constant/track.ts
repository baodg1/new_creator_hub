import { IPreviewConfig } from "../interface";

export const DEFAULT_FRAME_SPEED = 1; 
// every frame should have its own speed, but only video/audio can change this property
export const DEFAULT_TRACK_ID = "";
export const DEFAULT_TEXT_DURATION = 5; // seconds

export const DEFAULT_PREVIEW_CONFIG: IPreviewConfig = {
	width                  : 0.5,
	height                 : 0.5,
	maxWidth               : 1,
	maxHeight              : 1,
	cropTop                : 0,
	cropBottom             : 0,
	cropLeft               : 0,
	cropRight              : 0,
	centerX                : 0,
	centerY                : 0,
	roundCornerTopLeft     : 0,
	roundCornerTopRight    : 0,
	roundCornerBottomLeft  : 0,
	roundCornerBottomRight : 0,
	rotateAngle            : 0,
	isFlipXApplied         : false,
	isFlipYApplied         : false
};
