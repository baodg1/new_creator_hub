export const SAVE_TO_UNDO = "SAVE_TO_UNDO";
export const SET_TO_UNDO_STACK = "SET_TO_UNDO_STACK";
export const SET_TO_REDO_STACK = "SET_TO_REDO_STACK";
