import { IMousePosition } from "@/redux/interface";

export interface IPreviewState {
    position: IMousePosition;
    value: number;
    maxValue: number;
    pixelPerSecond: number;
    totalSeconds: number;
  }
