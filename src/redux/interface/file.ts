export interface IFile {
    id: string;
    name: string;
    url: string;
    type: string;
    isLoading: boolean;
    loadingPercent: number;
    isPreparing: boolean;
    storedName: string;
}
  
export interface IImageFile extends IFile {
    width: number;
    height: number;
}

export interface IPlayableFile extends IFile {
    durationInSecond: number;
    base64: string;
}
  
export interface IVideoFile extends IImageFile, IPlayableFile {
    thumbnailUrl: string;
    frameUrls: string[];
    audioPath?: string;
    audioName?: string;
}
  
export interface IAudioFile extends IPlayableFile {
}

export interface IUploadedList {
    fileList: Map<string, IFile>;
}
