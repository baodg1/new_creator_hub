/* eslint-disable no-unused-vars */
import { MediaType } from "@/services/interface/project";
import { IAudioFile, IImageFile, IVideoFile } from "./file";

export interface IPreviewConfig {
    width: number;
    height: number;
    maxWidth: number;
    maxHeight: number;
    cropTop?: number;
    cropBottom?: number;
    cropLeft?: number;
    cropRight?: number; 
    centerX: number;
    centerY: number;
    roundCornerTopLeft?: number;
    roundCornerTopRight?: number;
    roundCornerBottomLeft?: number;
    roundCornerBottomRight?: number;
    rotateAngle: number;
    isFlipXApplied: boolean;
    isFlipYApplied: boolean;
}

export interface IAudioConfig {
    volume: number;
    fadeInDuration: number;
    fadeOutDuration: number;
    isMute: boolean;
}

// eslint-disable-next-line no-shadow
export enum TextAlignOption {
    Center = "center",
    Left = "left",
    Right = "right",
}

export interface ITextConfig {
    textContent: string;
    textColor: string;
    align: TextAlignOption;
    justify: number;
    isUppercase: boolean;
    fontFamily: string;
    fontWeight: number;
    fontStyle: string;
    fontSize: number;
    lineHeight: number;
    letterSpacing: number;
    backgroundToTextGap?: number; // khoảng cách từ background đến text
    boxToBackgroundGap?: number; // khoảng cách từ text box đến background
    backgroundColor: string;
    borderRadius: number;
}

export interface ITimelineConfig {
    fromSecond: number;
    toSecond: number;
    trimStartSecond: number;
    trimEndSecond: number;
    duration: number;
    speed: number; // this field is mutable only for video/audio
}
  
export interface IFrame {
    id: string;
    trackId: string;
    type: MediaType;
    timeline: ITimelineConfig;
    trackIndex: number;
    frameIndex: number;
}

/**
 * Frames which contains media files: image, video (except text)
 */
export interface IMediaFrame extends IFrame {
    mediaUrl: string;
}

/**
 * Frames which can be shown on preview: text, image, video (except audio)
 */
export interface IPreviewFrame extends IFrame {
    preview: IPreviewConfig;
}

/**
 * Frames which can play audio: video, audio
 */
export interface IAudibleFrame extends IMediaFrame {
    audioConfig: IAudioConfig;
}

export interface IImageFrame extends IMediaFrame {
    file: IImageFile;
    preview: IPreviewConfig;
}

export interface IVideoFrame extends IImageFrame, IAudibleFrame {
    file: IVideoFile;
    audioPath?: string; // for VideoFrame
    audioName?: string; // for VideoFrame
}

export interface IAudioFrame extends IAudibleFrame {
    file: IAudioFile;
}

export interface ITextFrame extends IPreviewFrame {
    textConfig: ITextConfig;
}

export type ISubtitleFrame = ITextFrame; // the only different is value of `type`

// eslint-disable-next-line no-shadow
export enum TrackType {
    Audio,
    Subtitle,
    Normal
}
  
export interface ITrack {
    id: string;
    type: TrackType;
    locked?: boolean;
    frames: IFrame[]; // By default, only contain non-audio and non-subtitle frames
    trackIndex: number;
}

export interface IAudioTrack extends ITrack {
    frames: IAudioFrame[];
}

export interface ISubtitleTrack extends ITrack {
    frames: ISubtitleFrame[];
}
