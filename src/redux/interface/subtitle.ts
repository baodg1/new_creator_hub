export interface ISubtitle {
    textContent: string;
    startTime: number;
    duration: number;
}
