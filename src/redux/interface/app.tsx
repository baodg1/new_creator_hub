import { IFrame, IMousePosition, ISubtitleTrack, ITrack } from "@/redux/interface";

export interface IAppState {
    listTrack: ITrack[];
    subtitleTrack: ISubtitleTrack;
    mouse: IMousePosition;
    draggingItem: any;
    draggingTrackId: string;
    selectedFrame: IFrame | null;
    offsetPositionX: number;
    isModalAlertReloadVisible: boolean;
    isFullScreen: boolean;
  }
