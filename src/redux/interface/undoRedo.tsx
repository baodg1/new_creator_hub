export interface IUndoRedoState {
    undoStack: any[];
    redoStack: any[];
    isRedoEnable: boolean;
  }
