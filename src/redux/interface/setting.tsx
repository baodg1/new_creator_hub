import { BackgroundType } from "@/services/interface/project";

// interface IImageOption {
//   isFlipXApplied?: boolean;
//   isFlipYApplied?: boolean;
//   rotateAngle?: number;
//   roundCornerTopLeft?: number;
//   roundCornerTopRight?: number;
//   roundCornerBottomLeft?: number;
//   roundCornerBottomRight?: number;
// }

// image setting
// export interface IImageSetting {
//   zIndex?: number;
//   imageOptions?: IImageOption;
//   timelineOptions?: ITimelineOption;
//   playerOptions?: IPlayerOption;
// }

// interface IVideoOption {
//   isFlipXApplied?: boolean;
//   isFlipYApplied?: boolean;
//   volume?: number;
//   rotateAngle?: number;
//   speed?: number;
//   isFillScreen?: boolean;
//   isFitScreen?: boolean;
// }

// video setting
// export interface IVideoSetting {
//   zIndex?: number;
//   videoOptions?: IVideoOption;
//   timelineOptions?: ITimelineOption;
//   playerOptions?: IPlayerOption;
// }

// export interface ITextOptions {
//   textContent?: string;
//   rotateAngle?: number;
//   textColor?: string;
//   align?: string;
//   justify?: number;
//   isUppercase?: boolean;
//   fontFamily?: string;
//   fontWeight?: number;
//   fontStyle?: string;
//   fontSize?: number;
//   lineHeight?: number;
//   letterSpacing?: number;
//   backgroundToTextGap?: number; // khoảng cách từ background đến text
//   boxToBackgroundGap?: number; // khoảng cách từ text box đến background
//   backgroundColor?: string;
//   borderRadius?: number;
// }

// text setting
// export interface ITextSetting {
//   zIndex?: number;
//   timelineOptions?: ITimelineOption;
//   textOptions?: ITextOptions;
//   playerOptions?: IPlayerOption;
// }

// interface IAudioOption {
//   volume?: number; // unit is percent
//   speed?: number;
// }

// // audio setting
// export interface IAudioSetting {
//   timelineOptions?: ITimelineOption;
//   audioOptions?: IAudioOption;
// }

// project setting
export interface IProjectSetting {
  name: string;
  projectId: number;
  backgroundType: BackgroundType; // IMAGE or COLOR
  backgroundImage: string | null;
  backgroundColor: string;
  aspectRatioXValue: number;
  aspectRatioYValue: number;
  resolutionValue?: number;
  fixedDuration: number;
  durationType: string;
}
