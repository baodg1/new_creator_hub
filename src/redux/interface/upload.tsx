export interface UploadState {
    percent: number;
    isUploading?: boolean;
    type: string;
    id: string; // file id
  }

export type ListUploadState = {
    [key: string]: UploadState;
  };
