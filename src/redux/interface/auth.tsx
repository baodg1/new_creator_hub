export interface AuthState {
    token: string;
    projectId: number;
}
