import { IPreviewOnPhone } from "@/redux/interface";
import { IProjectSetting } from "@/redux/interface/setting";

export interface IPreviewState {
    isPlay: boolean;
    setting: IProjectSetting;
    onPhone: IPreviewOnPhone;
    volume: number;
    isModalExportOpen: boolean;
    isExporting: boolean;
    rotateAngle: number;
    shouldRefresh: boolean;
  }
