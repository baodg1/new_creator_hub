import { SAVE_CREATED_PROJECT, SET_CURRENT_PROJECT } from "../constant/project";

export const actSaveCreatedProject = (payload: number) => 
{
	return {
		type : SAVE_CREATED_PROJECT,
		payload
	};
};

export const actSaveCurrentProject = (payload: string) => 
{
	return {
		type : SET_CURRENT_PROJECT,
		payload
	};
};
