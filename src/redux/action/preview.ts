import { IProjectSetting } from "@/redux/interface/setting";
import { IPreviewOnPhone } from "@/redux/interface";
import { SET_ROTATE_ANGLE } from "../constant/app";
import { SET_PREVIEW_PLAY, SET_PROJECT_SETTING, SET_PREVIEW_ON_PHONE, SET_PREVIEW_VOLUME, SET_MODAL_EXPORT_VIDEO_OPEN, SET_IS_EXPORTING, SET_SHOULD_REFRESH } from "../constant/preview";

export const actSetPreviewPlay = (payload: boolean) => 
{
	return {
		type : SET_PREVIEW_PLAY,
		payload
	};
};

export const actSetProjectSetting = (payload: IProjectSetting) => 
{
	return {
		type : SET_PROJECT_SETTING,
		payload
	};
};

export const actSetPreviewOnPhone = (payload: IPreviewOnPhone) => 
{
	return {
		type : SET_PREVIEW_ON_PHONE,
		payload
	};
};

export const actSetPreviewVolume = (payload: number) => 
{
	return {
		type : SET_PREVIEW_VOLUME,
		payload
	};
};

export const actSetModalExportVideoOpen = (payload: boolean) => 
{
	return {
		type : SET_MODAL_EXPORT_VIDEO_OPEN,
		payload
	};
};

export const actSetIsExporting = (payload: boolean) => 
{
	return {
		type : SET_IS_EXPORTING,
		payload
	};
};

export const actSetRotateAngle = (payload: number) => 
{
	return {
		type : SET_ROTATE_ANGLE,
		payload
	};
};

export const actSetShouldRefresh = (payload: boolean) => 
{
	return {
		type : SET_SHOULD_REFRESH,
		payload
	};
};
