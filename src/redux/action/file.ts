import { CREATE_NEW_FILE, DELETE_FILE, SAVE_GET_LIST_FILE, UPDATE_FILE } from "../constant/upload";
import { IFile } from "../interface/file";

export const actCreateNewFile = (payload: IFile) => 
{
	return {
		type : CREATE_NEW_FILE,
		payload
	};
};

export const actDeleteFile = (payload: IFile) => 
{
	return {
		type : DELETE_FILE,
		payload
	};
};

export const actSaveGetListFile = (payload: IFile[]) => 
{
	return {
		type : SAVE_GET_LIST_FILE,
		payload
	};
};

export const actUpdateFile = (payload: IFile) => 
{
	return {
		type : UPDATE_FILE,
		payload
	};
};
