import { SAVE_USER_LOG_IN, USER_LOG_OUT } from "../constant/auth";

export const actSaveUserLogIn = (payload: any) => 
{
	return {
		type : SAVE_USER_LOG_IN,
		payload
	};
};

export const actUserLogOut = () => 
{
	return {
		type : USER_LOG_OUT
	};
};
