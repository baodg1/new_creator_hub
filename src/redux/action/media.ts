import { SET_MODAL_MEDIA_OPEN } from "../constant/mediaFile";

export const actSetModalMediaOpen = (payload: boolean) => 
{
	return {
		type : SET_MODAL_MEDIA_OPEN,
		payload
	};
};
