import { TRASH_ITEM_COUNT } from "../constant/project";

export const actLoadTrash = (payload: any) => 
{
	return {
		type : TRASH_ITEM_COUNT,
		payload
	};
};
