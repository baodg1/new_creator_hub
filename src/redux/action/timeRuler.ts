import { IMousePosition } from "@/redux/interface";
import { UPDATE_TIME_RULER_POSITION, SET_TIME_RULER_VALUE, SET_PIXEL_PER_SECOND, SET_TOTAL_SECONDS, SET_MAX_TIME_RULER_VALUE } from "../constant/app";

export const actUpdateTimeRulerPosition = (payload: IMousePosition) => 
{
	return {
		type : UPDATE_TIME_RULER_POSITION,
		payload
	};
};

export const actSetTimeRulerValue = (payload: number) => 
{
	return {
		type : SET_TIME_RULER_VALUE,
		payload
	};
};

export const actSetPixelPerSecond = (payload: number) => 
{
	return {
		type : SET_PIXEL_PER_SECOND,
		payload
	};
};

export const actSetTotalSeconds = (payload: number) => 
{
	return {
		type : SET_TOTAL_SECONDS,
		payload
	};
};

export const actSetMaxTimeRulerValue = (payload: number) => 
{
	return {
		type : SET_MAX_TIME_RULER_VALUE,
		payload
	};
};
