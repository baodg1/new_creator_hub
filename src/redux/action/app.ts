import { IFrame, IMousePosition, ISubtitleFrame, ITrack } from "@/redux/interface";
import { CREATE_SUBTITLE_TRACK, DELETE_FRAME, DRAG_DROP_MEDIA_TO_TRACK, DRAG_DROP_MEDIA_TO_VIRTUAL_TRACK, MOVE_FRAME_BETWEEN_TRACK, MOVE_FRAME_TO_VIRTUAL_TRACK, SET_DRAGGING_ITEM, SET_IS_FULL_SCREEN, SET_LIST_TRACK, SET_MODAL_ALERT_RELOAD_VISIBLE, SET_MOUSE_POSITION, SET_OFFSET_POSITION_X, SET_SELECTED_FRAME, SPLIT_FRAME, UPDATE_DRAGGING_TRACK_INDEX, UPDATE_SUBTITLE_FRAMES, UPDATE_TRACK, UPSERT_FRAME } from "../constant/app";
import { ADD_MEDIA_TO_TRACK } from "../constant/mediaFile";
import { ISubtitle } from "../interface/subtitle";

export const actSetListTrack = (payload: ITrack[]) => 
{
	return {
		type : SET_LIST_TRACK,
		payload
	};
};

export const actUpdateTrack = (payload: ITrack) => 
{
	return {
		type : UPDATE_TRACK,
		payload
	};
};

export const actSetMousePosition = (payload: IMousePosition) => 
{
	return {
		type : SET_MOUSE_POSITION,
		payload
	};
};

export const actSetDraggingItem = (payload: any) => 
{
	return {
		type : SET_DRAGGING_ITEM,
		payload
	};
};

export const actMoveFrameBetweenTrack = (payload: any) => 
{
	return {
		type : MOVE_FRAME_BETWEEN_TRACK,
		payload
	};
};

export const actAddMediaToTrack = (payload: IFrame) => 
{
	return {
		type : ADD_MEDIA_TO_TRACK,
		payload
	};
};

export const actDragDropMediaToTrack = (payload: IFrame) => 
{
	return {
		type : DRAG_DROP_MEDIA_TO_TRACK,
		payload
	};
};

// create if not exist or update frame
export const actUpsertFrame = (payload: IFrame) => 
{
	return {
		type : UPSERT_FRAME,
		payload
	};
};

export const actSetSelectedFrame = (payload: IFrame | null) => 
{
	return {
		type : SET_SELECTED_FRAME,
		payload
	};
};

export const actUpdateDraggingTrackIndex = (payload: string) => 
{
	return {
		type : UPDATE_DRAGGING_TRACK_INDEX,
		payload
	};
};

export interface ISplitFramePayload {
  frame: IFrame;
  splitAtSecond: number;
}

export const actSplitFrame = (payload: ISplitFramePayload) => 
{
	return {
		type : SPLIT_FRAME,
		payload
	};
};

export interface IMoveFrameToVirtualTrackPayload {
  virtualTrackIndex: number;
  frame: IFrame;
}

export const actMoveFrameToVirtualTrack = (
	payload: IMoveFrameToVirtualTrackPayload
) => 
{
	return {
		type : MOVE_FRAME_TO_VIRTUAL_TRACK,
		payload
	};
};

export const actDragDropMediaToVirtualTrack = (
	payload: IMoveFrameToVirtualTrackPayload
) => 
{
	return {
		type : DRAG_DROP_MEDIA_TO_VIRTUAL_TRACK,
		payload
	};
};

export const actSetOffsetPositionX = (payload: number) => 
{
	return {
		type : SET_OFFSET_POSITION_X,
		payload
	};
};

export const actDeleteFrame = (payload: IFrame) => 
{
	return {
		type : DELETE_FRAME,
		payload
	};
};

export const actSetModalAlertReloadVisible = (payload: boolean) => 
{
	return {
		type : SET_MODAL_ALERT_RELOAD_VISIBLE,
		payload
	};
};

export const actSetIsFullScreen = (payload: boolean) => 
{
	return {
		type : SET_IS_FULL_SCREEN,
		payload
	};
};

export const actCreateSubtitleTrack = (payload: ISubtitle[]) =>
{
	return {
		type : CREATE_SUBTITLE_TRACK,
		payload
	};
};

export const actUpdateSubtitleFrames = (payload: ISubtitleFrame[]) =>
{
	return {
		type : UPDATE_SUBTITLE_FRAMES,
		payload
	};
};
