import { CURRENT_SUBTITLE_MODE } from "../constant/subtitle";

export const actPickSubtitleMode = (payload: any) => 
{
	return {
		type : CURRENT_SUBTITLE_MODE,
		payload
	};
};
