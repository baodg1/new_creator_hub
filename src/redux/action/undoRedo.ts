import { SAVE_TO_UNDO, SET_TO_UNDO_STACK, SET_TO_REDO_STACK } from "../constant/undoRedo";

export const actSaveToUndo = (payload: any) => 
{
	return {
		type : SAVE_TO_UNDO,
		payload
	};
};

export const actSetUndoStack = (payload: any) => 
{
	return {
		type : SET_TO_UNDO_STACK,
		payload
	};
};

export const actSetRedoStack = (payload: any) => 
{
	return {
		type : SET_TO_REDO_STACK,
		payload
	};
};
