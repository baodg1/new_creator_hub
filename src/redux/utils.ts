import { IFrame, ITrack } from "@/redux/interface";

const updateListData = (indexOfData: number, listData: any[], data?: any[]) => 
{
	const tempListData = [ ...listData ];

	if (indexOfData !== -1) 
	{
		tempListData.splice(indexOfData, 1, ...data!);
	}

	return tempListData;
};

const filterListTrack = (listTrack: ITrack[]) => 
{
	// Remove all empty tracks;
	listTrack = listTrack.filter((track) => track.frames.length !== 0);

	listTrack.forEach((track: ITrack, trackIndex: number) => 
	{
		track.trackIndex = trackIndex;
		const frames: IFrame[] = [];

		track.frames.forEach((frame: IFrame, frameIndex: number) => 
		{
			frames.push({
				...frame,
				frameIndex,
				trackId    : track?.id,
				trackIndex : trackIndex
			});
		});

		track.frames = frames;
	});

	return listTrack;
};

const deleteItemInList = (indexOfData: number, listData: any[]) => 
{
	const tempListData = [ ...listData ];

	if (indexOfData !== -1) 
	{
		tempListData.splice(indexOfData, 1);
	}

	return tempListData;
};

const addItemAtIndex = (indexOfData: number, listData: any[], data?: any) => 
{
	const tempListData = [ ...listData ];

	if (indexOfData !== -1) 
	{
		tempListData.splice(indexOfData, 0, data);
	}

	return tempListData;
};

const updateFrameInTrack = (targetTrack: ITrack, targetFrame: IFrame) => 
{
	const newListFrame: IFrame[] = [];
	const targetFrameId = targetFrame.id;

	for (const currentFrame of targetTrack.frames) 
	{
		if (currentFrame.id === targetFrameId) 
		{
			continue;
		}

		const currentEndSecond = currentFrame.timeline.fromSecond +
								currentFrame.timeline.trimEndSecond -
								currentFrame.timeline.trimStartSecond;
		const targetEndSecond = targetFrame.timeline.fromSecond +
								targetFrame.timeline.trimEndSecond -
								targetFrame.timeline.trimStartSecond;

		// insert a frame between 2 different frames
		if (currentFrame.timeline.fromSecond! < targetFrame.timeline.fromSecond) 
		{
			newListFrame.push(currentFrame);

			if (currentEndSecond > targetFrame.timeline.fromSecond) 
			{
				targetFrame.timeline.fromSecond = currentEndSecond;
			}
		}
		else 
		{
			newListFrame.push(targetFrame);

			if (currentFrame.timeline.fromSecond < targetEndSecond) 
			{
				currentFrame.timeline.fromSecond = targetEndSecond;
			}
			targetFrame = currentFrame;
		}
	}

	newListFrame.push(targetFrame);
	targetTrack.frames = newListFrame;

	return targetTrack;
};

export {
	updateListData,
	deleteItemInList,
	addItemAtIndex,
	updateFrameInTrack,
	filterListTrack
};
