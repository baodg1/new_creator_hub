import { ReduxAction } from "@/redux/interface/common";
import { SET_MODAL_MEDIA_OPEN } from "../constant/mediaFile";

interface IMediaState {
  isModalOpen?: boolean;
}

const initialState: IMediaState = {
	isModalOpen : false
};

const reducer = (state = initialState, action: ReduxAction) => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SET_MODAL_MEDIA_OPEN:
		return {
			isModalOpen : payload
		};

	default:
		return state;
	}
};

export default reducer;
