import { ReduxAction } from "@/redux/interface/common";
import { TrashState } from "../interface/trash";
import { TRASH_ITEM_COUNT } from "../constant/project";

const initialState: TrashState = {
	countTrash : -1
};

const reducer = (state = initialState, action: ReduxAction): TrashState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case TRASH_ITEM_COUNT:
		return {
			...state,
			countTrash : payload
		};

	default:
		return state;
	}
};

export default reducer;
