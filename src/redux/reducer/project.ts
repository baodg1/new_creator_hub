import { ReduxAction } from "@/redux/interface/common";
import { SET_CURRENT_PROJECT } from "../constant/project";

const initialState = {
	currentProject : undefined
};

const reducer = (state = initialState, action: ReduxAction) => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SET_CURRENT_PROJECT:
		return {
			currentProject : payload
		};

	default:
		return state;
	}
};

export default reducer;
