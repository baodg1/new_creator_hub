import { ReduxAction } from "@/redux/interface/common";
import { SAVE_USER_LOG_IN, USER_LOG_OUT } from "../constant/auth";
import { SAVE_CREATED_PROJECT } from "../constant/project";
import { AuthState } from "../interface/auth";

const initialState: AuthState = {
	token     : "",
	projectId : 0
};

const reducer = (state = initialState, action: ReduxAction): AuthState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SAVE_USER_LOG_IN:
		return {
			...state,
			token : payload?.token
		};

	case SAVE_CREATED_PROJECT:
		return {
			...state,
			projectId : payload
		};

	case USER_LOG_OUT:
		return initialState;

	default:
		return state;
	}
};

export default reducer;
