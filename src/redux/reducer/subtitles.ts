import { SUBTITLES_MODE } from "@/config/constant";
import { ReduxAction } from "../interface/common";
import { CURRENT_SUBTITLE_MODE } from "../constant/subtitle";
import { SubtitleState } from "../interface/subtitles";

const initialState: SubtitleState = {
	subtitleMode : SUBTITLES_MODE.NONE
};

const reducer = (state = initialState, action: ReduxAction): SubtitleState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case CURRENT_SUBTITLE_MODE:
		return {
			...state,
			subtitleMode : payload
		};

	default:
		return state;
	}
};

export default reducer;
