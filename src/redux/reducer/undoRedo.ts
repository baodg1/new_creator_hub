import { MAX_LENGTH_UNDO_REDO } from "@/config/constant";
import { ReduxAction } from "@/redux/interface/common";
import { IUndoRedoState } from "../interface/undoRedo";
import { SAVE_TO_UNDO, SET_TO_UNDO_STACK, SET_TO_REDO_STACK } from "../constant/undoRedo";

const initialState: IUndoRedoState = {
	undoStack    : [],
	redoStack    : [],
	isRedoEnable : false
};

const reducer = (state = initialState, action: ReduxAction): IUndoRedoState => 
{
	const { type, payload } = action;

	switch (type) 
	{

	case SAVE_TO_UNDO: {
		const tmpUndoStack = [ ...state?.undoStack ];

		tmpUndoStack.push(payload);

		// remove most last used item in stack if stack overflow
		if (tmpUndoStack.length > MAX_LENGTH_UNDO_REDO) 
		{
			tmpUndoStack.splice(0, 1);
		}

		return {
			...state,
			undoStack    : tmpUndoStack,
			redoStack    : [],
			isRedoEnable : false
		};
	}

	case SET_TO_UNDO_STACK: {
		return {
			...state,
			undoStack    : payload,
			isRedoEnable : state?.redoStack?.length > 1
		};
	}

	case SET_TO_REDO_STACK: {
		return {
			...state,
			redoStack    : payload,
			isRedoEnable : payload?.length > 1
		};
	}

	default:
		return state;
	}
};

export default reducer;
