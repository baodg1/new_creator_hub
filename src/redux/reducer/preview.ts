import { ReduxAction } from "@/redux/interface/common";
import { IPreviewState } from "../interface/preview";
import { SET_ROTATE_ANGLE } from "../constant/app";
import { SET_PREVIEW_PLAY, SET_PROJECT_SETTING, SET_PREVIEW_ON_PHONE, SET_PREVIEW_VOLUME, SET_MODAL_EXPORT_VIDEO_OPEN, SET_IS_EXPORTING, SET_SHOULD_REFRESH } from "../constant/preview";
import { DEFAULT_PROJECT_SETTING } from "@/constant/project";

const initialState: IPreviewState = {
	isPlay            : false,
	setting           : DEFAULT_PROJECT_SETTING,
	onPhone           : {},
	volume            : 0.5,
	isModalExportOpen : false,
	isExporting       : false,
	rotateAngle       : 0,
	shouldRefresh     : false
};

const reducer = (state = initialState, action: ReduxAction): IPreviewState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SET_PREVIEW_PLAY:
		return {
			...state,
			isPlay : payload
		};

	case SET_PROJECT_SETTING:
		return {
			...state,
			setting : payload
		};

	case SET_PREVIEW_ON_PHONE:
		return {
			...state,
			onPhone : payload
		};

	case SET_PREVIEW_VOLUME:
		return {
			...state,
			volume : payload
		};

	case SET_MODAL_EXPORT_VIDEO_OPEN:
		return {
			...state,
			isModalExportOpen : payload
		};

	case SET_IS_EXPORTING:
		return {
			...state,
			isExporting : payload
		};

	case SET_ROTATE_ANGLE:
		return {
			...state,
			rotateAngle : payload
		};

	case SET_SHOULD_REFRESH:
		return {
			...state,
			shouldRefresh : payload
		};

	default:
		return state;
	}
};

export default reducer;
