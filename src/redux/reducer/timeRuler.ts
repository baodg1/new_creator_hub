import { ReduxAction } from "@/redux/interface/common";
import { MIN_PIXEL_PER_SECOND } from "@/config/constant";
import { UPDATE_TIME_RULER_POSITION, SET_TIME_RULER_VALUE, SET_PIXEL_PER_SECOND, SET_TOTAL_SECONDS, SET_MAX_TIME_RULER_VALUE } from "../constant/app";
import { IPreviewState } from "../interface/timeRuler";

const DEFAULT_TOTAL_SECOND = 30;

const initialState: IPreviewState = {
	position       : { x: 0, y: 0 },
	value          : 0,
	maxValue       : 0,
	pixelPerSecond : MIN_PIXEL_PER_SECOND,
	totalSeconds   : DEFAULT_TOTAL_SECOND
};

const reducer = (state = initialState, action: ReduxAction): IPreviewState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case UPDATE_TIME_RULER_POSITION:
		return {
			...state,
			position : payload
		};

	case SET_TIME_RULER_VALUE:
	{
		const newTimeRulerValue = Math.max(0, payload);
      
		return {
			...state,
			value : newTimeRulerValue
		};
	}

	case SET_PIXEL_PER_SECOND:
		return {
			...state,
			pixelPerSecond : payload
		};

	case SET_TOTAL_SECONDS:
		return {
			...state,
			totalSeconds : payload
		};

	case SET_MAX_TIME_RULER_VALUE:
		return {
			...state,
			maxValue : payload
		};

	default:
		return state;
	}
};

export default reducer;
