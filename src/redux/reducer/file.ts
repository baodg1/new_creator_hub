import { ReduxAction } from "@/redux/interface/common";
import { IFile, IUploadedList } from "../interface/file";
import { SAVE_GET_LIST_FILE, CREATE_NEW_FILE, DELETE_FILE, UPDATE_FILE } from "../constant/upload";

const initialState: IUploadedList = {
	fileList : new Map<string, IFile>()
};

const reducer = (state = initialState, action: ReduxAction): IUploadedList => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SAVE_GET_LIST_FILE:
		return {
			...state,
			fileList : new Map<string, IFile>()
		};

	case CREATE_NEW_FILE:
	{
		const newFile = payload as IFile;

		state.fileList.set(newFile.id, newFile);
		
		return {
			...state,
			fileList : state.fileList
		};
	}

	case DELETE_FILE: {

		const deleteFile = payload as IFile;

		state.fileList.delete(deleteFile.id);
		
		return {
			...state,
			fileList : state.fileList
		};
	}

	case UPDATE_FILE:
	{
		const uploadedFile = payload as IFile;

		state.fileList.set(uploadedFile.id, uploadedFile);

		return {
			...state,
			fileList : state.fileList
		};
	}

	default:
		return state;
	}
};

export default reducer;
