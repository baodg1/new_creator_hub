import { ReduxAction } from "@/redux/interface/common";
import { ListUploadState } from "../interface/upload";
import { SET_UPLOAD_PROGRESS } from "../constant/upload";

const initialState = {};

const reducer = (
	state = initialState,
	action: ReduxAction
): ListUploadState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SET_UPLOAD_PROGRESS:
		return {
			...state,
			[payload?.id] : {
				percent     : payload?.percent,
				isUploading : payload?.percent < 100,
				type        : payload?.type,
				id          : payload.id
			}
		};

	default:
		return state;
	}
};

export default reducer;
