import _ from "lodash";
import { ReduxAction } from "@/redux/interface/common";
import {
	addItemAtIndex,
	deleteItemInList,
	filterListTrack,
	updateFrameInTrack,
	updateListData
} from "@/redux/utils";
import { MEDIA_TYPE } from "@/config/constant";
import { v4 as uuidv4 } from "uuid";
import { ITrack, IFrame, TrackType, ISubtitleFrame } from "@/redux/interface";
import {
	IMoveFrameToVirtualTrackPayload,
	ISplitFramePayload
} from "../action/app";
import { IAppState } from "../interface/app";
import { SET_LIST_TRACK, UPDATE_TRACK, SET_MOUSE_POSITION, SET_DRAGGING_ITEM, MOVE_FRAME_BETWEEN_TRACK, DRAG_DROP_MEDIA_TO_TRACK, UPSERT_FRAME, DELETE_FRAME, MOVE_FRAME_TO_VIRTUAL_TRACK, DRAG_DROP_MEDIA_TO_VIRTUAL_TRACK, SET_SELECTED_FRAME, SPLIT_FRAME, UPDATE_DRAGGING_TRACK_INDEX, SET_OFFSET_POSITION_X, SET_MODAL_ALERT_RELOAD_VISIBLE, SET_IS_FULL_SCREEN, UPDATE_SUBTITLE_FRAMES } from "../constant/app";
import { ADD_MEDIA_TO_TRACK } from "../constant/mediaFile";
import { SET_EXPAND_TOGGLE_MENU } from "../constant/project";
import { DEFAULT_SUBTITLE_TRACK } from "../constant/subtitle";
import { MediaType } from "@/services/interface/project";

const initialState: IAppState = {
	listTrack                 : [],
	subtitleTrack             : DEFAULT_SUBTITLE_TRACK,
	mouse                     : { x: 0, y: 0 },
	draggingItem              : null,
	selectedFrame             : null,
	draggingTrackId           : "",
	offsetPositionX           : 0,
	isModalAlertReloadVisible : false,
	isFullScreen              : false
};

const reducer = (state = initialState, action: ReduxAction): IAppState => 
{
	const { type, payload } = action;

	switch (type) 
	{
	case SET_LIST_TRACK:
		return {
			...state,
			listTrack : payload
		};

	case UPDATE_TRACK:
		return {
			...state,
			listTrack : updateListData(payload?.trackIndex, state.listTrack, [
				payload
			])
		};

	case SET_MOUSE_POSITION:
		return {
			...state,
			mouse : payload
		};

	case SET_DRAGGING_ITEM:
		return {
			...state,
			draggingItem : payload
		};

	case MOVE_FRAME_BETWEEN_TRACK: {
		const { targetTrackId, data } = payload;

		const sourceTrackIndex = _.findIndex(state?.listTrack, {
			id : data?.trackId
		});
		const targetTrackIndex = _.findIndex(state?.listTrack, {
			id : targetTrackId
		});
		const sourceTrack = state.listTrack[sourceTrackIndex];
		const targetTrack = state.listTrack[targetTrackIndex];
		const frameIndex = _.findIndex(sourceTrack?.frames, { id: data?.id });
		let newListTrack = [ ...state.listTrack ];

		// remove frame from source track
		newListTrack[sourceTrackIndex].frames = deleteItemInList(
			frameIndex,
			sourceTrack?.frames
		);

		// create and insert new frame
		newListTrack[targetTrackIndex] = updateFrameInTrack(
			newListTrack[targetTrackIndex],
			{
				...data,
				id         : uuidv4(),
				trackId    : targetTrack?.id,
				trackIndex : targetTrackIndex
			}
		);

		// remove all empty tracks
		newListTrack = filterListTrack(newListTrack);

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case ADD_MEDIA_TO_TRACK: {
		const trackWithFrame: ITrack = {
			frames     : [ payload as IFrame ],
			type       : TrackType.Normal,
			id         : payload.trackId,
			trackIndex : 0
		};
		const newListTrack = [ trackWithFrame, ...state.listTrack ];

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case DRAG_DROP_MEDIA_TO_TRACK: {
		const { targetTrackIndex, data } = payload;
		const newListTrack = [ ...state.listTrack ];
		const targetTrack = newListTrack[targetTrackIndex];

		targetTrack.frames = [
			...targetTrack.frames,
			{ ...data, trackId: targetTrack.id }
		];
		newListTrack.splice(targetTrackIndex, 1, targetTrack);

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case UPSERT_FRAME: {
		const targetFrame = payload as IFrame;
		let targetTrack: ITrack | undefined;

		if (targetFrame.type === MediaType.Subtitle)
		{
			targetTrack = state.subtitleTrack;
		}
		else
		{
			targetTrack = _.find(state.listTrack, {
				id : targetFrame.trackId
			});
		}

		if (!targetTrack)
		{
			return state;
		}

		const targetTrackIndex = _.findIndex(state.listTrack, {
			id : targetFrame.trackId
		});

		if (
			targetFrame.type === MEDIA_TYPE.AUDIO || targetFrame.type === MEDIA_TYPE.VIDEO
		) 
		{
			// validate value of trim second
			if (targetFrame.timeline.trimStartSecond < 0) 
			{
				targetFrame.timeline.trimStartSecond = 0;
				const oldTargetFrame = targetTrack?.frames?.[targetFrame.frameIndex];

				targetFrame.timeline.fromSecond =
            oldTargetFrame?.timeline.fromSecond! - oldTargetFrame?.timeline.trimStartSecond!;
			}

			if (targetFrame.timeline.trimEndSecond > targetFrame.timeline.duration) 
			{
				targetFrame.timeline.trimEndSecond = targetFrame.timeline.duration;
			}
		}

		// validate value of fromSecond
		if (targetFrame.timeline.fromSecond < 0) 
		{
			targetFrame.timeline.fromSecond = 0;
		}

		targetTrack = updateFrameInTrack(targetTrack!, targetFrame);
		let newListTrack = [ ...state.listTrack ];

		newListTrack = updateListData(targetTrackIndex, newListTrack, [
			targetTrack
		]);

		newListTrack = filterListTrack(newListTrack);
		
		return {
			...state,
			listTrack : newListTrack
		};
	}

	case DELETE_FRAME: {
		const targetFrame = payload as IFrame;
		const targetTrack = _.find(state?.listTrack, {
			id : targetFrame?.trackId
		});
		const trackIndex = _.findIndex(state?.listTrack, {
			id : targetFrame?.trackId
		});
		const frameIndex = _.findIndex(targetTrack?.frames, {
			id : targetFrame?.id
		});

		let newListTrack = [ ...state.listTrack ];

		newListTrack[trackIndex].frames = deleteItemInList(
			frameIndex,
        targetTrack?.frames!
		);
		newListTrack = filterListTrack(newListTrack);

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case MOVE_FRAME_TO_VIRTUAL_TRACK: {
		const { frame: targetFrame, virtualTrackIndex } =
        payload as IMoveFrameToVirtualTrackPayload;
		let newListTrack = [ ...state.listTrack ];
		const sourceTrack = _.find(newListTrack, { id: targetFrame.trackId });
		const sourceTrackIndex = _.findIndex(newListTrack, {
			id : targetFrame.trackId
		});
		const frameIndex = _.findIndex(sourceTrack?.frames, {
			id : targetFrame?.id
		});

		// remove item from source track
		newListTrack[sourceTrackIndex].frames = deleteItemInList(
			frameIndex,
        sourceTrack?.frames!
		);

		// Create new track
		const targetTrack: ITrack = {
			id         : uuidv4(),
			type       : TrackType.Normal,
			trackIndex : virtualTrackIndex,
			frames     : [ targetFrame ]
		};

		// insert new track to list
		newListTrack = addItemAtIndex(
			virtualTrackIndex,
			newListTrack,
			targetTrack
		);

		// filter tracks
		newListTrack = filterListTrack(newListTrack);

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case DRAG_DROP_MEDIA_TO_VIRTUAL_TRACK: {
		const { frame: targetFrame, virtualTrackIndex } =
        payload as IMoveFrameToVirtualTrackPayload;

		let newListTrack = [ ...state.listTrack ];

		// Create new track
		const targetTrack: ITrack = {
			id         : uuidv4(),
			type       : TrackType.Normal,
			trackIndex : virtualTrackIndex,
			frames     : [ targetFrame ]
		};

		targetFrame.id = uuidv4();
		targetFrame.trackId = targetTrack.id;
		targetFrame.trackIndex = virtualTrackIndex;

		// insert new track to list
		newListTrack = addItemAtIndex(
			virtualTrackIndex,
			newListTrack,
			targetTrack
		);

		// filter tracks
		newListTrack = filterListTrack(newListTrack);

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case SET_SELECTED_FRAME:
		return {
			...state,
			selectedFrame : payload
		};

	case SPLIT_FRAME: {
		const { frame, splitAtSecond } = payload as ISplitFramePayload;
		const newListTrack = [ ...state.listTrack ];
		const targetTrack = newListTrack[frame.trackIndex];
		const targetFrame = targetTrack.frames[frame.frameIndex];
		const offsetSecond = splitAtSecond - targetFrame.timeline.fromSecond;

		// calculate left frame after splitted
		const leftSplittedFrame: IFrame = { ...targetFrame };

		leftSplittedFrame.id = uuidv4();
		leftSplittedFrame.timeline.trimEndSecond = targetFrame.timeline.trimStartSecond + offsetSecond;

		// calculate right frame after splitted
		const rightSplittedFrame: IFrame = { ...targetFrame };

		rightSplittedFrame.id = uuidv4();
		rightSplittedFrame.timeline.fromSecond = splitAtSecond;
		rightSplittedFrame.timeline.trimStartSecond =
        targetFrame.timeline.trimStartSecond + offsetSecond;

		const newListFrame: IFrame[] = [];

		for (const frameItem of targetTrack.frames) 
		{
			if (frameItem.id !== targetFrame.id) 
			{
				newListFrame.push(frameItem);
			}
			else 
			{
				newListFrame.push(leftSplittedFrame);
				newListFrame.push(rightSplittedFrame);
			}
		}

		targetTrack.frames = newListFrame;
		newListTrack[targetFrame.trackIndex!] = targetTrack;

		return {
			...state,
			listTrack : newListTrack
		};
	}

	case UPDATE_SUBTITLE_FRAMES: {
		const subtitles = payload as ISubtitleFrame[];
		const subtitleTrack = { ...state.subtitleTrack };

		subtitleTrack.frames = subtitles;

		return {
			...state,
			subtitleTrack : subtitleTrack
		} as IAppState;
	}

	case UPDATE_DRAGGING_TRACK_INDEX: {
		return {
			...state,
			draggingTrackId : payload
		};
	}

	case SET_OFFSET_POSITION_X: {
		return {
			...state,
			offsetPositionX : payload
		};
	}

	case SET_MODAL_ALERT_RELOAD_VISIBLE: {
		return {
			...state,
			isModalAlertReloadVisible : payload
		};
	}

	case SET_IS_FULL_SCREEN:
		return { ...state, isFullScreen: payload };
	case SET_EXPAND_TOGGLE_MENU:
		return { ...state };
	default:
		return state;
	}
};

export default reducer;
