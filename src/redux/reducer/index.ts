import { combineReducers } from "redux";
import Auth from "./auth";
import Upload from "./upload";
import App from "./app";
import Preview from "./preview";
import TimeRuler from "./timeRuler";
import Media from "./media";
import UndoRedo from "./undoRedo";
import File from "./file";
import Trash from "./trash";
import Subtitle from "./subtitles";
import Project from "./project";

const rootReducer = combineReducers({
	Auth,
	Upload,
	App,
	Preview,
	TimeRuler,
	Media,
	UndoRedo,
	File,
	Trash,
	Subtitle,
	Project
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
