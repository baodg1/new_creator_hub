import { DURATION_OPTION } from "@/config/constant";
import { IProjectSetting } from "@/redux/interface/setting";
import { BackgroundType } from "@/services/interface/project";
import { BLACK_COLOR } from "@/utils/color";

export const DEFAULT_PROJECT_SETTING: IProjectSetting = {
	name              : "",
	projectId         : -1,
	backgroundType    : BackgroundType.BackgroundColor,
	backgroundImage   : "",
	backgroundColor   : BLACK_COLOR,
	aspectRatioXValue : 16,
	aspectRatioYValue : 9,
	fixedDuration     : 0,
	durationType      : DURATION_OPTION.AUTOMATIC
};
