export interface IProjectFolder {
  id: number;
  name?: string;
  thumbnail_url?: string;
  video_count?: number
}

export interface IMediaAssetList{
  id: number,
  url?: string,
  animation?: string,
  type?: string,
  is_reversed?: number,
  transition_id?: number,
  project_id?: number,
  transition?: any,
  timeline_options_list?: Array<any>,
  image_options_list?: Array<any>,
  audio_options_list?: Array<any>,
  text_options_list?:Array<any>, 
  media_overlay_list?: Array<any>
}

export interface IProject {
  id: number;
  name?: string;
  thumbnail_url?: string;
  video_url?: string;
  background_color_r?: number;
  background_color_g?: number;
  background_color_b?: number;
  background_color_a?: number;
  background_image_url?: string;
  background_type?: string;
  output_duration?: number;
  output_width_ratio?: number;
  output_heigh_ratio?: number;
  created_at?: string;
  updated_at?: string;
  is_exported?: boolean;
  video_time?: number;
  media_asset_list?:Array<IMediaAssetList>;
  video_count?: number;
}
