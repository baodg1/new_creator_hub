// import {
// 	IImageSetting,
// 	IVideoSetting,
// 	IAudioSetting,
// 	ITextSetting
// } from "./setting";

export * from "./project";

export interface IPreviewItem {
  width: number;
  height: number;
  maxWidth?: number;
  maxHeight?: number;
  cropTop?: number;
  cropBottom?: number;
  cropLeft?: number;
  cropRight?: number; 
  centerX: number;
  centerY: number;
  rotateAngle: number;
  isFlipXApplied: boolean;
  isFlipYApplied: boolean;
}

// export interface IFrame {
//   id?: string;
//   trackId?: string;
//   type?: string;
//   duration?: number;
//   mediaUrl?: string;
//   audioPath?: string; // for VideoFrame
//   audioName?: string; // for VideoFrame
//   listVideoThumbnail?: string[];
//   trackIndex?: number;
//   frameIndex?: number;
//   leftPositionX?: number;
//   rightPositionX?: number;
//   name?: string;
//   fromSecond?: number;
//   toSecond?: number;
//   trimStartSecond?: number;
//   trimEndSecond?: number;
//   preview?: IPreviewItem;
//   setting?: IImageSetting | IVideoSetting | IAudioSetting | ITextSetting;
// }

// export interface ITrack {
//   id: string;
//   locked?: boolean;
//   frames: IFrame[];
//   trackIndex?: number;
//   subtitleTrack?: boolean;
// }

// export interface IResizableFrame {
//   children: JSX.Element;
//   trackIndex?: number;
//   frameIndex?: number;
//   data?: IFrame;
// }

export interface IMousePosition {
  x: number;
  y: number;
}

export interface IPreviewOnPhone {
  enable?: boolean;
  deviceType?: string;
  platform?: string;
}

export interface ICrop {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}
